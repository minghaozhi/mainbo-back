FROM java:8
EXPOSE 8080

VOLUME /tmp
ADD manage-1.0.0.jar  /back.jar
RUN bash -c 'touch /manage.jar'
ENTRYPOINT ["java","-jar","/manage.jar"]
