package com.mainbo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * @author moshang
 * @date 2020-03-01
 **/
@Configuration
@ImportResource(locations={"classpath:spring-*.xml"})
public class ConfigClass {
}
