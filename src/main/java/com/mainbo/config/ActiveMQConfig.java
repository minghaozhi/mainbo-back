package com.mainbo.config;

import com.mainbo.core.common.constant.ApolloConstants;
import com.mainbo.core.activemq.JmsSenderImpl;
import com.mainbo.modular.jms.JmsServiceImpl;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.RedeliveryPolicy;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;

/**
 * activemq配置类
 * @author moshang
 * @date 2020-03-01
 **/
@Configuration
public class ActiveMQConfig {

    private final static String brokerUrl= ApolloConstants.readActiveMqUrl();
    private final static String secretKey= ApolloConstants.readServerSecretKey();
    @Value("${spring.activemq.user}")
    private String usrName;

    @Value("${spring.activemq.password}")
    private  String password;

    public static final String topicDestination = "PF.TOPIC.EVENT";
    public static final String p2pMessageQueue = "PF.QUEUE.AWK";


    @Bean("connectionFactory")
    public ActiveMQConnectionFactory connectionFactory() {
        ActiveMQConnectionFactory connectionFactory=new ActiveMQConnectionFactory();
        connectionFactory.setBrokerURL(brokerUrl);
        connectionFactory.setUserName(usrName);
        connectionFactory.setPassword(password);
        connectionFactory.setTrustAllPackages(true);
        connectionFactory.setRedeliveryPolicy(activeMQRedeliveryPolicy());
        connectionFactory.setUseAsyncSend(true);
        return connectionFactory;
    }
    @Bean("activeMQRedeliveryPolicy")
    public   RedeliveryPolicy activeMQRedeliveryPolicy(){
        RedeliveryPolicy redeliveryPolicy= new RedeliveryPolicy();
        //是否在每次尝试重新发送失败后,增长这个等待时间
        redeliveryPolicy.setUseExponentialBackOff(true);
        redeliveryPolicy.setCollisionAvoidancePercent((short) 15);
        redeliveryPolicy.setUseCollisionAvoidance(true);
        //重发次数,默认为6次
        redeliveryPolicy.setMaximumRedeliveries(6);
        //重发时间间隔,默认为5秒
        redeliveryPolicy.setInitialRedeliveryDelay(5000);
        //第一次失败后重新发送之前等待500毫秒,第二次失败再等待500 * 5毫秒,这里的5就是value
        redeliveryPolicy.setBackOffMultiplier(5);
        //最大传送延迟，只在useExponentialBackOff为true时有效。延迟发送最大隔离时间
        redeliveryPolicy.setMaximumRedeliveryDelay(-1);
        return redeliveryPolicy;

    }
    @Bean("topicDestination")
    public ActiveMQTopic topicDestination(){
        return  new ActiveMQTopic(topicDestination);
    }
    @Bean("p2pMessageQueue")
    public ActiveMQQueue  p2pMessageQueue(){
    return new ActiveMQQueue(p2pMessageQueue);
    }

    @Bean("jmsTemplate")
    public JmsTemplate jmsTemplate(){
   JmsTemplate jmsTemplate=new JmsTemplate();
   jmsTemplate.setConnectionFactory(connectionFactory());
   jmsTemplate.setDefaultDestination(topicDestination());
   jmsTemplate.setPubSubDomain(true);
   jmsTemplate.setDeliveryMode(2);
   jmsTemplate.setReceiveTimeout(0);
   jmsTemplate.setExplicitQosEnabled(true);
   return jmsTemplate;
    }

    @Bean("jmsSenderImpl")
    public JmsSenderImpl jmsSenderImpl(){
     JmsSenderImpl jmsSender=new JmsSenderImpl();
     jmsSender.setSecretKey(secretKey);
     jmsSender.setJmsTemplate(jmsTemplate());
     jmsSender.setDestination(p2pMessageQueue());
     return jmsSender;
    }
    @Bean("jmsService")
    public JmsServiceImpl jmsService(){
    return  new JmsServiceImpl();
    }
}
