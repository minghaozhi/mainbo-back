package com.mainbo.modular.listener;

import java.util.Collection;

/**
 * @author moshang
 * @date 2020-03-02
 **/
public interface Listenable {

    /**
     * The ListenableEvent type for the "entity before save" event.
     */
    public static final String BEFORE_ADD_EVENT = "before_add";

    /**
     * The ListenableEvent type for the "entity after save" event.
     */
    public static final String AFTER_ADD_EVENT = "after_add";

    /**
     * The ListenableEvent type for the "entity before update" event.
     */
    public static final String BEFORE_UPDATE_EVENT = "before_update";

    /**
     * The ListenableEvent type for the "entity after update" event.
     */
    public static final String AFTER_UPDATE_EVENT = "after_update";

    /**
     * The ListenableEvent type for the "entity before delete" event.
     */
    public static final String BEFORE_DELETE_EVENT = "before_delete";

    /**
     * The ListenableEvent type for the "entity after delete" event.
     */
    public static final String AFTER_DELETE_EVENT = "after_delete";

    /**
     * The ListenableEvent type for the "entity before delete" event.
     */
    public static final String BEFORE_BATCH_INSERT_EVENT = "before_batch_insert_delete";

    /**
     * Add a Listener listener to this component.
     *
     * @param listener
     *          The listener to add
     */
    void addListener(Listener listener);

    /**
     * Get the life listeners. If this
     * component has no listeners registered, a empty collection is returned.
     */
    Collection<Listener> findListeners();

    /**
     * Remove a Listener listener from this component.
     *
     * @param listener
     *          The listener to remove
     */
    void removeListener(Listener listener);

    /**
     * 支持的类型
     *
     * @return
     */
    Object[] supportTypes();

}
