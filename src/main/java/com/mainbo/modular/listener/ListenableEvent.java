package com.mainbo.modular.listener;

import java.util.EventObject;

/**
 * @author moshang
 * @date 2020-03-02
 **/
public class ListenableEvent extends EventObject {

    private static final long serialVersionUID = 1L;

    // ----------------------------------------------------------- Constructors

    /**
     * Construct a new ListenableEvent with the specified parameters.
     *
     * @param lifecycle
     *          Component on which this event occurred
     * @param type
     *          Event type (required)
     * @param data
     *          Event data (if any)
     */
    public ListenableEvent(Listenable listenable, String type, Object data) {

        super(listenable);
        this.type = type;
        this.data = data;
    }

    // ----------------------------------------------------- Instance Variables

    /**
     * The event data associated with this event.
     */
    private Object data = null;

    /**
     * The event type this instance represents.
     */
    private String type = null;

    // ------------------------------------------------------------- Properties

    /**
     * Return the event data of this event.
     */
    public Object getData() {

        return (this.data);

    }

    /**
     * Return the Lifecycle on which this event occurred.
     */
    public Listenable getListenable() {

        return (Listenable) getSource();

    }

    /**
     * Return the event type of this event.
     */
    public String getType() {
        return (this.type);
    }

}
