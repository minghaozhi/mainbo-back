package com.mainbo.modular.listener;

/**
 * @author moshang
 * @date 2020-03-02
 **/
public interface Listener {
    /**
     * Acknowledge the occurrence of the specified event.
     *
     * @param event
     *          LifecycleEvent that has occurred
     */
    void lifecycleEvent(ListenableEvent event);

    /**
     * Make sure this listener supports the event,
     * defaut supportsType is the entity class name
     *
     * @param supportsType type
     * @return true if supported
     */
    boolean supports(Object[] supportsType);
}
