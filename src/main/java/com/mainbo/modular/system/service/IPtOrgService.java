package com.mainbo.modular.system.service;

import com.mainbo.modular.jms.MessageTypes;
import com.mainbo.modular.system.model.PtOrg;
import com.baomidou.mybatisplus.service.IService;
import com.mainbo.modular.system.model.vo.OrgVo;

import java.util.List;

/**
 * <p>
 * 机构数据类 服务类
 * </p>
 *
 * @author moshang
 * @since 2020-02-21
 */
public interface IPtOrgService extends IService<PtOrg> {

    String getAreaNameById(Integer areaId);

    List<PtOrg> getOrgByAreaId(Integer areaId, Integer type);

    PtOrg listOrgList();

    List<OrgVo> findList(PtOrg org);

    Integer updateOrg(PtOrg org);
    /**
     * 获取areaIds
     *
     * @param areaId
     *          区域id
     * @return List
     */
    List<Integer> getAreaIds(Integer areaId);

    void sendMessage(MessageTypes orgUpdate, PtOrg selectById);
}
