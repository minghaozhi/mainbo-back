package com.mainbo.modular.system.service.impl;

import com.mainbo.core.shiro.SessionKey;
import com.mainbo.core.shiro.ShiroKit;
import com.mainbo.modular.system.model.PtAccount;
import com.mainbo.modular.system.model.PtNews;
import com.mainbo.modular.system.dao.PtNewsMapper;
import com.mainbo.modular.system.model.PtOrg;
import com.mainbo.modular.system.service.IPtNewsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mainbo.modular.system.service.IPtResourcesService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author moshang
 * @since 2020-03-11
 */
@Service
public class PtNewsServiceImpl extends ServiceImpl<PtNewsMapper, PtNews> implements IPtNewsService {

    @Resource
    private IPtResourcesService resourcesService;
    @Resource
    private PtNewsMapper newsMapper;
    private static final Pattern RESID_PATTERN = Pattern.compile("#RESID\\[(\\w{32})\\]");
    @Override
    public void saveNews(PtNews news) {
        PtAccount currentUser = ShiroKit.getSessionAttr(SessionKey.CURRENT_USER);
        if (StringUtils.isEmpty(news.getOrgId())) {
            PtOrg org = ShiroKit.getSessionAttr(SessionKey.CURRENT_ORG);
            news.setOrgId(org.getId());
        }
        if (StringUtils.isNotBlank(news.getId())) {
            news.setLastupDttm(new Date());
            news.setLastupId(currentUser.getId());
            PtNews oldNews =selectById(news.getId());
            if (StringUtils.isNotBlank(news.getImg()) && !news.getImg().equals(oldNews.getImg())) {
                resourcesService.deleteResources(oldNews.getImg());
                resourcesService.updateTmptResources(news.getImg());
            }
            Set<String> oldResIdList = filterContent(oldNews.getContent());
            Set<String> resIdList = filterContent(news.getContent());
            // 删除旧的
            for (String resId : oldResIdList) {
                if (oldResIdList.contains(resId)) {
                    resIdList.remove(resId);
                } else {
                    resourcesService.deleteResources(resId);
                }
            }
            // 更新新资源
            for (String resId : resIdList) {
                resourcesService.updateTmptResources(resId);
            }
            updateById(news);
        //    LoggerUtils.updateLogger(LoggerModule.PTXWGL, "平台公告——" + news.getCode() + "——修改，ID：" + news.getId());
        } else {
            if (currentUser != null) {
                news.setCrtId(currentUser.getId());
                if (StringUtils.isNotBlank(news.getOrgId())&&(!news.getOrgId().equals("0"))) {
                }else {
                    news.setOrgId(currentUser.getOrgId());
                }
                news.setCrtId(currentUser.getId());
                if (StringUtils.isBlank(news.getAuthor())) {
                    news.setAuthor(currentUser.getAccount());
                }
            }
            news.setCrtDttm(new Date());
            Set<String> resIdList = filterContent(news.getContent());
            // 更新新闻图片资源状态
            for (String resId : resIdList) {
                resourcesService.updateTmptResources(resId);
            }
            insert(news);
            if (StringUtils.isNotBlank(news.getImg())) {
                resourcesService.updateTmptResources(news.getImg());
            }
            //LoggerUtils.insertLogger(LoggerModule.PTXWGL, "平台公告——" + news.getCode() + "——新增，ID：" + news.getId());
        }
    }

    @Override
    public void deleteNews(String batchDeleteIds) {
        PtNews model;
        if (!StringUtils.isEmpty(batchDeleteIds)) {
            for (String id : batchDeleteIds.split(",")) {
                model = newsMapper.selectById(id);
                if (!StringUtils.isEmpty(model.getImg())) {
                    resourcesService.deleteResources(model.getImg());
                }
                Set<String> images = filterContent(model.getContent());
                // 删除内容中的图片
                for (String resId : images) {
                    resourcesService.deleteResources(resId);
                }
                newsMapper.deleteById(id);
            }
        }
    }

    Set<String> filterContent(String content) {
        Set<String> resIdList = new HashSet<String>();
        if (StringUtils.isNotEmpty(content)) {
            Matcher m = RESID_PATTERN.matcher(content);
            while (m.find()) {
                resIdList.add(m.group(1));
            }
        }
        return resIdList;
    }
}
