package com.mainbo.modular.system.service.impl;

import com.mainbo.modular.system.model.PtFriendlink;
import com.mainbo.modular.system.dao.PtFriendlinkMapper;
import com.mainbo.modular.system.service.IPtFriendlinkService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2020-03-11
 */
@Service
public class PtFriendlinkServiceImpl extends ServiceImpl<PtFriendlinkMapper, PtFriendlink> implements IPtFriendlinkService {

}
