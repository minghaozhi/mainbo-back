package com.mainbo.modular.system.service.impl;

import com.mainbo.modular.system.model.PtConfig;
import com.mainbo.modular.system.dao.PtConfigMapper;
import com.mainbo.modular.system.service.IPtConfigService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author moshang
 * @since 2020-02-25
 */
@Service
public class PtConfigServiceImpl extends ServiceImpl<PtConfigMapper, PtConfig> implements IPtConfigService {

}
