package com.mainbo.modular.system.service.impl;

import com.mainbo.core.common.node.ZTreeNode;
import com.mainbo.modular.system.model.PtArea;
import com.mainbo.modular.system.dao.PtAreaMapper;
import com.mainbo.modular.system.service.IPtAreaService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.xml.ws.Action;
import java.util.List;

/**
 * <p>
 * 地域表 服务实现类
 * </p>
 *
 * @author moshang
 * @Date 2020-02-17 21:23:57
 */
@Service
public class PtAreaServiceImpl extends ServiceImpl<PtAreaMapper, PtArea> implements IPtAreaService {

    @Resource
    private PtAreaMapper ptAreaMapper;
    @Override
    public List<ZTreeNode> tree() {
        return this.baseMapper.tree();
    }
}
