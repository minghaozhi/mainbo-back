package com.mainbo.modular.system.service.impl;

import com.mainbo.modular.system.model.PtSectionCategory;
import com.mainbo.modular.system.dao.PtSectionCategoryMapper;
import com.mainbo.modular.system.service.IPtSectionCategoryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author moshang
 * @since 2020-03-10
 */
@Service
public class PtSectionCategoryServiceImpl extends ServiceImpl<PtSectionCategoryMapper, PtSectionCategory> implements IPtSectionCategoryService {

}
