package com.mainbo.modular.system.service;

import com.baomidou.mybatisplus.service.IService;
import com.mainbo.modular.system.model.CommUser;
import com.mainbo.modular.system.model.People;
import com.mainbo.modular.system.model.UserSearchModel;

import java.util.List;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/2/26   14:18
 **/
public interface PeopleService  extends IService<People> {
    CommUser getOne(String orgId, String name, String cellphone);

    Object findPeopleByCondition(UserSearchModel um);

    People saveParents(People people);

    List<People> exportPeoList(UserSearchModel um);
}
