package com.mainbo.modular.system.service;

import com.baomidou.mybatisplus.service.IService;
import com.mainbo.modular.system.model.CommUser;
import com.mainbo.modular.system.model.UserSearchModel;
import com.mainbo.modular.system.model.Student;
import com.mainbo.modular.system.model.vo.ExportInfoVo;

import java.util.List;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/2/26   14:12
 **/
public interface StudentService  extends IService<Student> {
    CommUser getOne(String name, String orgId);
    void saveStudent(Student ptStudent);

    List<Student> findStudentByCondition(Student student);

    List<ExportInfoVo> exportStuList(UserSearchModel um);

    void updateStudent(Student student);

    List<Student> findStudentsWithoutClass(Student student);
}
