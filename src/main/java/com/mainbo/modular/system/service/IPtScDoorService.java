package com.mainbo.modular.system.service;

import com.mainbo.modular.system.model.PtScDoor;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 墨殇
 * @since 2020-03-15
 */
public interface IPtScDoorService extends IService<PtScDoor> {

}
