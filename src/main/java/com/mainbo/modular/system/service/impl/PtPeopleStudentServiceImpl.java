package com.mainbo.modular.system.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mainbo.modular.system.dao.PtClassMapper;
import com.mainbo.modular.system.dao.PtClassUserMapper;
import com.mainbo.modular.system.model.PtClass;
import com.mainbo.modular.system.model.PtClassUser;
import com.mainbo.modular.system.model.PtPeopleStudent;
import com.mainbo.modular.system.dao.PtPeopleStudentMapper;
import com.mainbo.modular.system.model.meta.MetaUtils;
import com.mainbo.modular.system.model.meta.vo.Meta;
import com.mainbo.modular.system.service.IPtPeopleStudentService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author moshang
 * @since 2020-03-02
 */
@Service
public class PtPeopleStudentServiceImpl extends ServiceImpl<PtPeopleStudentMapper, PtPeopleStudent> implements IPtPeopleStudentService {

    @Resource
    private PtPeopleStudentMapper ptPeopleStudentMapper;
    @Resource
    private PtClassUserMapper classUserMapper;
    @Resource
    private PtClassMapper classMapper;
    @Override
    public List<PtPeopleStudent> findAll(PtPeopleStudent ptPeopleStudent) {
        EntityWrapper<PtPeopleStudent> entityWrapper=new EntityWrapper<>();
        entityWrapper.eq("org_id",ptPeopleStudent.getOrgId());
        entityWrapper.eq("people_id",ptPeopleStudent.getPeopleId());
        List<PtPeopleStudent> list=ptPeopleStudentMapper.selectList(entityWrapper);
        this.loadDataInfo(list);
        return list;
    }

    private void loadDataInfo(List<PtPeopleStudent> dataList) {
        for (PtPeopleStudent peopleStudent : dataList) {
            PtClassUser user = new PtClassUser();
            user.setUserId(peopleStudent.getStudentId());
            user.setOrgId(peopleStudent.getOrgId());
            user.setEnable(true);
            user.setSchoolYear(MetaUtils.getSchoolYearProvider().getCurrentSchoolYear(peopleStudent.getOrgId()));
            PtClassUser classUser = classUserMapper.selectOne(user);
            if (classUser != null) {
                PtClass classInfo = classMapper.selectById(classUser.getClassId());
                if (classInfo != null) {
                    Meta meta = MetaUtils.getMeta(classInfo.getGradeId());
                    peopleStudent.setClassName(classInfo.getName());
                    peopleStudent.setPeriod(classInfo.getPeriod());
                    peopleStudent.setClassId(classInfo.getId());
                    peopleStudent.setGrade(meta.getName());// 年级名称
                }
            }
        }
    }
}
