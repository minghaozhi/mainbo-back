package com.mainbo.modular.system.service;

import com.mainbo.modular.system.model.PtFriendlink;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author stylefeng
 * @since 2020-03-11
 */
public interface IPtFriendlinkService extends IService<PtFriendlink> {

}
