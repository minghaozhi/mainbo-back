package com.mainbo.modular.system.service.impl;

import com.mainbo.core.shiro.ShiroKit;
import com.mainbo.core.util.Result;
import com.mainbo.modular.jms.JmsService;
import com.mainbo.modular.jms.MessageTypes;
import com.mainbo.modular.jms.model.SchoolBuildingMessageVo;
import com.mainbo.modular.system.model.PtSchoolBuilding;
import com.mainbo.modular.system.dao.PtSchoolBuildingMapper;
import com.mainbo.modular.system.service.IPtSchoolBuildingService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mainbo.modular.system.service.IPtSchoolClassroomService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author moshang
 * @since 2020-03-09
 */
@Service
public class PtSchoolBuildingServiceImpl extends ServiceImpl<PtSchoolBuildingMapper, PtSchoolBuilding> implements IPtSchoolBuildingService {
  private static final Logger logger= LoggerFactory.getLogger(PtSchoolBuildingServiceImpl.class);

    @Resource
    private JmsService jmsService;
    @Resource
    private PtSchoolBuildingMapper schoolBuildingMapper;
    @Resource
    private IPtSchoolClassroomService classroomService;
    @Override
    public void sendMessage(MessageTypes messageTypes, PtSchoolBuilding ptSchoolBuilding) {
        try {
            SchoolBuildingMessageVo messageVo = new SchoolBuildingMessageVo(messageTypes.name(),ptSchoolBuilding);
            jmsService.sendMessage(messageVo);
        } catch (Exception e) {
            logger.error("发送消息失败，消息类型{}", messageTypes.name(), e);
        }
    }

    @Override
    public void updateDeleteFlag(String id) {
        PtSchoolBuilding building = new PtSchoolBuilding();
        building.setId(id);
        building.setDeleted(true);
        building.setEnable(false);
        building.setLastupDttm(new Date());
        building.setLastupId(ShiroKit.getUser().getId());
        int update = schoolBuildingMapper.updateById(building);
        this.sendMessage(MessageTypes.SCHOOLBUILDING_DELETE,building);
        //更新教学楼下教室得状态
        classroomService.batchUpdateDeleteFlag(id);
    }

    @Override
    public void updateEnable(PtSchoolBuilding schbuild) {
        if(null!=schbuild){
            PtSchoolBuilding building = new PtSchoolBuilding();
            building.setId(schbuild.getId());
            building.setEnable(schbuild.getEnable());
            building.setDisabledReason(schbuild.getDisabledReason());
            building.setNote(schbuild.getNote());
            schoolBuildingMapper.updateById(building);
            this.sendMessage(MessageTypes.SCHOOLBUILDING_UPDATE,building);
        }
    }
}
