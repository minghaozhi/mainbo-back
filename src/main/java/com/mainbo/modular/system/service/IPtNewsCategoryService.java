package com.mainbo.modular.system.service;

import com.mainbo.modular.system.model.PtNews;
import com.mainbo.modular.system.model.PtNewsCategory;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author moshang
 * @since 2020-03-11
 */
public interface IPtNewsCategoryService extends IService<PtNewsCategory> {

    List<PtNewsCategory> findAll(PtNews news);
}
