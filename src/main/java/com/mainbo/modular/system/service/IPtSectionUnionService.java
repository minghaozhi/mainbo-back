package com.mainbo.modular.system.service;

import com.mainbo.modular.system.model.PtSectionUnion;
import com.baomidou.mybatisplus.service.IService;
import io.swagger.models.auth.In;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author moshang
 * @since 2020-03-10
 */
public interface IPtSectionUnionService extends IService<PtSectionUnion> {

    void deleteByCategoryId(String categoryId);

    void updateDelflagByName(String name);

    void disableByName(Integer enable,String name);
}
