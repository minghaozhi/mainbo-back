package com.mainbo.modular.system.service;

import com.mainbo.modular.system.model.PtUserManagescope;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author moshang
 * @since 2020-03-04
 */
public interface IPtUserManagescopeService extends IService<PtUserManagescope> {

}
