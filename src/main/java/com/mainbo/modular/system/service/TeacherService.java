package com.mainbo.modular.system.service;

import com.baomidou.mybatisplus.service.IService;
import com.mainbo.modular.system.model.CommUser;
import com.mainbo.modular.system.model.UserSearchModel;
import com.mainbo.modular.system.model.Teacher;
import com.mainbo.modular.system.model.vo.ExportInfoVo;

import java.util.List;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/2/26   14:16
 **/
public interface TeacherService extends IService<Teacher> {
    CommUser getOne(String name, String orgId);
    /**
     * 根据区域、机构查询教师用户信息
     */
    List<Teacher> findTeacherList(Teacher teacher, UserSearchModel um);

    Teacher saveTeac(Integer userType, Teacher teacher);

    List<ExportInfoVo> exportTeacherList(UserSearchModel um);
}
