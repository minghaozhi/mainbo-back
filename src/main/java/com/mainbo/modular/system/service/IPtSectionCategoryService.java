package com.mainbo.modular.system.service;

import com.mainbo.modular.system.model.PtSectionCategory;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author moshang
 * @since 2020-03-10
 */
public interface IPtSectionCategoryService extends IService<PtSectionCategory> {

}
