package com.mainbo.modular.system.service;

import com.mainbo.modular.system.model.PtRole;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author moshang
 * @since 2020-02-26
 */
public interface IPtRoleService extends IService<PtRole> {

    List<PtRole> getRoleByScope(String id, Integer applicationSchool);


    /**
     *
     * @param orgId
     *          机构id
     * @return List
     */
    List<PtRole> getDelRoleByOrgId(String orgId);
}
