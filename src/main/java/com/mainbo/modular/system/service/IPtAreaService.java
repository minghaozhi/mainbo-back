package com.mainbo.modular.system.service;

import com.mainbo.core.common.node.ZTreeNode;
import com.mainbo.modular.system.model.PtArea;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 地域表 服务类
 * </p>
 *
 * @author moshang
 * @Date 2020-02-17 21:23:57
 */
public interface IPtAreaService extends IService<PtArea> {

    List<ZTreeNode> tree();
}
