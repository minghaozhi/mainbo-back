package com.mainbo.modular.system.service;

import com.mainbo.modular.system.model.PtPeopleStudent;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author moshang
 * @since 2020-03-02
 */
public interface IPtPeopleStudentService extends IService<PtPeopleStudent> {

    List<PtPeopleStudent> findAll(PtPeopleStudent ptPeopleStudent);
}
