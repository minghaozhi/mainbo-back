package com.mainbo.modular.system.service;

import com.mainbo.modular.system.model.PtOrg;
import com.mainbo.modular.system.model.meta.vo.Meta;
import com.mainbo.modular.system.model.vo.SchoolYearVo;

import java.util.List;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/3/9   16:01
 **/
public interface IPtSchoolYearTermManageService {

    List<SchoolYearVo> getSchoolYearAndTermByOrg(String orgId);

    List<SchoolYearVo> getSchoolYearAndTermByArea(Integer areaId);

    void saveSchoolYear(SchoolYearVo vo);

    void updateSchoolYear(SchoolYearVo vo);
    /**
     * 根据学年id获取学期数据
     *
     * @param schoolYearId
     *          学年id
     * @return List of SchoolYearVo
     */
    List<SchoolYearVo> getSchoolTerm(Integer schoolYearId);

    /**
     * 根据学期id获取学期数据
     *
     * @param termId
     *          学期id
     * @return SchoolYearVo
     */
    SchoolYearVo getTerm(Integer termId);
    /**
     * 更新学期数据
     *
     * @param vo
     *          SchoolYearVo
     */
    void updateSchoolTerm(SchoolYearVo vo);

    /**
     * 保存学期数据
     *
     * @param vo
     *          SchoolYearVo
     */
    boolean saveSchoolTerm(SchoolYearVo vo);

    /**
     * 取学校配置的学年配置信息
     * @param orgId
     * @return
     */
    List<Meta> getSchoolYear(String orgId);
}
