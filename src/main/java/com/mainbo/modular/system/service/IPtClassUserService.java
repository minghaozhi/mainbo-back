package com.mainbo.modular.system.service;

import com.mainbo.modular.jms.model.ClassUserMessage;
import com.mainbo.modular.system.model.PtClassUser;
import com.baomidou.mybatisplus.service.IService;
import com.mainbo.modular.system.model.Teacher;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 班级用户表 服务类
 * </p>
 *
 * @author moshang
 * @since 2020-02-28
 */
public interface IPtClassUserService extends IService<PtClassUser> {
    /**
     * 获取学科教师map
     *
     * @param classId
     *          班级ID
     * @return map
     */
    Map<Integer, Teacher> getSubjectTeacherMap(String classId);

    void saveOrUpdateClassUser(ClassUserMessage classUserVo, Integer userType);

    List<PtClassUser> findStudents(PtClassUser classUser);
    /**
     * 拷贝班级用户
     *
     * @param classId
     *          班级ID
     * @param userType
     *          用户类型
     * @param schoolyear
     *          学年
     * @return List
     */
    List<PtClassUser> copyClassUsers(String oldClassId, Integer userType, Integer schoolYear);

    List<PtClassUser> findClassStudentListByClassId(PtClassUser um, Integer graduateState, String classIds);
}
