package com.mainbo.modular.system.service;

import com.mainbo.modular.system.model.PtConfig;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author moshang
 * @since 2020-02-25
 */
public interface IPtConfigService extends IService<PtConfig> {

}
