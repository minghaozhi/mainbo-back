package com.mainbo.modular.system.service.impl;

import com.mainbo.modular.system.model.PtOrgRelationship;
import com.mainbo.modular.system.dao.PtOrgRelationshipMapper;
import com.mainbo.modular.system.service.IPtOrgRelationshipService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author moshang
 * @since 2020-03-04
 */
@Service
public class PtOrgRelationshipServiceImpl extends ServiceImpl<PtOrgRelationshipMapper, PtOrgRelationship> implements IPtOrgRelationshipService {

    @Resource
    private PtOrgRelationshipMapper ptOrgRelationshipMapper;
    @Override
    public void deleteByOrgId(String orgId) {
        ptOrgRelationshipMapper.deleteByOrgId(orgId);
    }
}
