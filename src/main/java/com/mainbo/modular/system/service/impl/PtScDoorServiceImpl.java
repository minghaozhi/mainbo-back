package com.mainbo.modular.system.service.impl;

import com.mainbo.modular.system.model.PtScDoor;
import com.mainbo.modular.system.dao.PtScDoorMapper;
import com.mainbo.modular.system.service.IPtScDoorService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 墨殇
 * @since 2020-03-15
 */
@Service
public class PtScDoorServiceImpl extends ServiceImpl<PtScDoorMapper, PtScDoor> implements IPtScDoorService {

}
