package com.mainbo.modular.system.service;

import com.mainbo.modular.system.model.PtAnnouncement;
import com.baomidou.mybatisplus.service.IService;

import java.io.UnsupportedEncodingException;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author moshang
 * @since 2020-03-11
 */
public interface IPtAnnouncementService extends IService<PtAnnouncement> {

    void saveAnnunciate(PtAnnouncement ptAnnouncement);

    void deleteAnnunciate(Integer id);
}
