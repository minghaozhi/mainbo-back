package com.mainbo.modular.system.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mainbo.core.common.constant.Const;
import com.mainbo.modular.system.model.PtNews;
import com.mainbo.modular.system.model.PtNewsCategory;
import com.mainbo.modular.system.dao.PtNewsCategoryMapper;
import com.mainbo.modular.system.service.IPtNewsCategoryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author moshang
 * @since 2020-03-11
 */
@Service
public class PtNewsCategoryServiceImpl extends ServiceImpl<PtNewsCategoryMapper, PtNewsCategory> implements IPtNewsCategoryService {

    @Resource
    private PtNewsCategoryMapper newsCategoryMapper;
    @Override
    public List<PtNewsCategory> findAll(PtNews news) {
        List<String> orgIds=new ArrayList<>();
        orgIds.add(news.getOrgId());
        orgIds.add(Const.DEFAULT_ORG_ID);
        EntityWrapper<PtNewsCategory> wrapper=new EntityWrapper<>();
        wrapper.eq("enable",1);
        wrapper.orderBy("sort",true);
        wrapper.eq("parent_code",news.getCode());
        wrapper.in("org_id",orgIds);

        return this.selectList(wrapper);
    }
}
