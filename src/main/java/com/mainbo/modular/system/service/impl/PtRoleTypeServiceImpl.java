package com.mainbo.modular.system.service.impl;

import cn.hutool.core.convert.Convert;
import com.mainbo.modular.system.dao.RelationMapper;
import com.mainbo.modular.system.model.PtRoleType;
import com.mainbo.modular.system.dao.PtRoleTypeMapper;
import com.mainbo.modular.system.model.Relation;
import com.mainbo.modular.system.service.IPtRoleTypeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author moshang
 * @since 2020-02-23
 */
@Service
public class PtRoleTypeServiceImpl extends ServiceImpl<PtRoleTypeMapper, PtRoleType> implements IPtRoleTypeService {

    @Resource
    private  PtRoleTypeMapper ptRoleTypeMapper;
    @Resource
    private RelationMapper relationMapper;
    @Override
    public void setAuthority(Integer roleId, String ids) {

        // 删除该角色所有的权限
        this.ptRoleTypeMapper.deleteRolesById(roleId);

        // 添加新的权限
        for (Long id : Convert.toLongArray(ids.split(","))) {
            Relation relation = new Relation();
            relation.setRoleid(roleId);
            relation.setMenuid(id);
            this.relationMapper.insert(relation);
        }    }
}
