package com.mainbo.modular.system.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mainbo.core.shiro.ShiroKit;
import com.mainbo.modular.jms.JmsService;
import com.mainbo.modular.jms.MessageTypes;
import com.mainbo.modular.jms.model.ClassRoomMessage;
import com.mainbo.modular.system.model.PtSchoolClassroom;
import com.mainbo.modular.system.dao.PtSchoolClassroomMapper;
import com.mainbo.modular.system.service.IPtSchoolClassroomService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author moshang
 * @since 2020-03-05
 */
@Service
public class PtSchoolClassroomServiceImpl extends ServiceImpl<PtSchoolClassroomMapper, PtSchoolClassroom> implements IPtSchoolClassroomService {

    private static final Logger logger= LoggerFactory.getLogger(PtSchoolClassroomServiceImpl.class);
    @Resource
    private PtSchoolClassroomMapper classroomMapper;
    @Resource
    private JmsService jmsService;
    @Override
    public void batchUpdateDeleteFlag(String buildingId) {
       EntityWrapper<PtSchoolClassroom> classRoom = new EntityWrapper<>();
        classRoom.eq("building_id",buildingId);
        List<PtSchoolClassroom> classRooms = classroomMapper.selectList(classRoom);
        if(!CollectionUtils.isEmpty(classRooms))
            for (PtSchoolClassroom cr:classRooms
            ) {
                cr.setDeleted(1);
                cr.setEnable(false);
                cr.setLastupId(ShiroKit.getUser().getId());
                cr.setLastupDttm(new Date());
                int update = classroomMapper.updateById(cr);
                if(update>0){
                    this.sendMessage(MessageTypes.CLASSROOM_DELETE,cr);
                }
            }
    }
    @Override
    public void sendMessage(MessageTypes type, PtSchoolClassroom cr) {
        try {
            ClassRoomMessage roomMessage = new ClassRoomMessage(type.name(),cr);
            jmsService.sendMessage(roomMessage);
        } catch (Exception e) {
            logger.error("发送消息失败，消息类型{}", type.name(), e);
        }
    }

    @Override
    public void updateDeleteFlag(String id) {
        PtSchoolClassroom classRoom = new PtSchoolClassroom();
        classRoom.setId(id);
        classRoom.setDeleted(1);
        classRoom.setEnable(false);
        classRoom.setLastupDttm(new Date());
        classRoom.setLastupId(ShiroKit.getUser().getId());
        int update = classroomMapper.updateById(classRoom);
        if(update>0){
            this.sendMessage(MessageTypes.CLASSROOM_DELETE,classRoom);
        }
    }

    @Override
    public List<PtSchoolClassroom> listByBuildingId(List<String> idList) {
        return classroomMapper.listByBuildingId(idList);
    }
}
