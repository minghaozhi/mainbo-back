package com.mainbo.modular.system.service;

import com.baomidou.mybatisplus.service.IService;
import com.mainbo.modular.system.model.CommUser;
import com.mainbo.modular.system.model.excelUser.EduEmployee;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/2/26   14:06
 **/
public interface EduEmpService extends IService<EduEmployee> {
    CommUser getOne(String name,String orgId);
}
