package com.mainbo.modular.system.service.impl;

import com.mainbo.modular.system.model.PtUserManagescope;
import com.mainbo.modular.system.dao.PtUserManagescopeMapper;
import com.mainbo.modular.system.service.IPtUserManagescopeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author moshang
 * @since 2020-03-04
 */
@Service
public class PtUserManagescopeServiceImpl extends ServiceImpl<PtUserManagescopeMapper, PtUserManagescope> implements IPtUserManagescopeService {

}
