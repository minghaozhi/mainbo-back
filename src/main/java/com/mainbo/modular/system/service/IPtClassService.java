package com.mainbo.modular.system.service;


import com.baomidou.mybatisplus.service.IService;
import com.mainbo.core.util.Result;
import com.mainbo.modular.system.model.PtClass;

import java.util.List;
import java.util.Set;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author moshang
 * @since 2020-02-24
 */
public interface IPtClassService extends IService<PtClass> {

    List<PtClass> getClassList(PtClass ptClass);

    Result saveOrUpdateClass(PtClass ptClass, Result result);
    /**
     * 查询当前机构&学段&年级下是否有该名称的班级
     *
     * @param clss
     *          班级
     * @return ClassInfo
     */
    PtClass getClassByName(PtClass clss);


    /**
     * 删除班级
     *
     * @param id
     *          班级id
     */
    void deleteClass(String id);

    List<PtClass> getAllclasses(String orgId, Integer phaseId, Integer gradeId, Integer schoolYear);

    List<PtClass> getSelectedclasses(String orgId, Integer phaseId, Integer gradeId,  String classIds, Integer schoolYear);

    void changeClassStatus(String classIds, Integer status);
    /**
     * 修改用户毕业状态
     *
     * @param ids
     *          用户id集合
     * @param graduationStatus
     *          毕业状态
     */
    void chanageStudentGraduationStatus(List<String> userIds, Integer graduationStatus);
    /**
     * 修改班级毕业状态
     *
     * @param classIds
     *          逗号分隔classId
     * @param status
     *          班级id
     * @return List
     */
    List<String> changeClassinfoGraduation(Set<String> classIds, Integer status);
}
