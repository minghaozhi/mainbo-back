package com.mainbo.modular.system.service;

import com.mainbo.modular.jms.MessageTypes;
import com.mainbo.modular.system.model.PtSchoolBuilding;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author moshang
 * @since 2020-03-09
 */
public interface IPtSchoolBuildingService extends IService<PtSchoolBuilding> {

    void sendMessage(MessageTypes messageTypes, PtSchoolBuilding ptSchoolBuilding);
    /**
     * 更新学校删除标识
     * @param id
     */
    void updateDeleteFlag(String id);

    void updateEnable(PtSchoolBuilding schbuild);
}
