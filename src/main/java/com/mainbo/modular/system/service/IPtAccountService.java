package com.mainbo.modular.system.service;

import com.mainbo.core.util.Result;
import com.mainbo.modular.system.model.PtAccount;
import com.baomidou.mybatisplus.service.IService;
import com.mainbo.modular.system.model.PtUserRole;
import com.mainbo.modular.system.model.Teacher;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author moshang
 * @Date 2020-02-17 21:23:57
 */
public interface IPtAccountService extends IService<PtAccount> {

    boolean ExcelCheckStudentCode(String name, String studentCode, String orgId);

    boolean ExcelCheckCard(Teacher teacher, String orgId, Integer userType);

    PtAccount getByAccountName(String accountName);

    Integer resetPassword(String accountId);

    Result updatePwd(PtAccount account);

    void updateEnable(PtAccount account);

    void deleteUser(String id, Boolean deleted);

    Result updateByUserType(PtAccount ptAccount);
   /* 发送用户消息
   *
           * @param messageType
   *          消息类型
   * @param uid
   *          用户id
   */
    void sendUserMessege(String messageType, String id);
    /**
     * 用户账号信息更改消息 包括密码修改， 邮箱绑定，手机绑定
     *
     * @param uid
     *          用户id
     */
    void sendAccountModifyMessage(String uid);

    String exportUserRole(String id);

    PtAccount findAccountWithInfo(String accountId);

    /**
     * 发送用户角色消息
     *
     * @param messageType
     *          消息类型
     * @param ur
     *          职务
     */
    void sendUserRoleMessege(String messageType, PtUserRole ur);
    /**
     * 修改用户禁用状态
     *
     * @param userIds
     *          用户id集合
     * @param enable
     *          是否可用
     * @param disableReason
     *          禁用原因
     */
    void changeAccountEnableStatus(List<String> userIds, boolean enable, String disableReason);
}
