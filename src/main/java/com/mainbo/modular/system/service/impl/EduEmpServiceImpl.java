package com.mainbo.modular.system.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mainbo.modular.system.dao.PtEduEmpMapper;
import com.mainbo.modular.system.model.CommUser;
import com.mainbo.modular.system.model.excelUser.EduEmployee;
import com.mainbo.modular.system.service.EduEmpService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/2/26   14:07
 **/
@Service
public class EduEmpServiceImpl extends ServiceImpl<PtEduEmpMapper, EduEmployee> implements EduEmpService {
    @Resource
    private  PtEduEmpMapper eduEmpMapper;


    @Override
    public CommUser getOne(String name, String orgId) {
        return eduEmpMapper.getOne(name,orgId);
    }
}
