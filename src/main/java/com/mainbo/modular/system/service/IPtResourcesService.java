package com.mainbo.modular.system.service;

import com.mainbo.modular.system.model.PtResources;
import com.baomidou.mybatisplus.service.IService;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author moshang
 * @since 2020-02-24
 */
public interface IPtResourcesService extends IService<PtResources> {

    PtResources saveWebResources(MultipartFile myfile, String relativePath);


    PtResources saveTmptResources(MultipartFile myfile, String relativePath);

    boolean deleteResources(String resId);

    PtResources updateTmptResources(String oldResId);
}
