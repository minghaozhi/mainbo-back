package com.mainbo.modular.system.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mainbo.core.common.constant.Const;
import com.mainbo.core.util.AccountGenerator;
import com.mainbo.core.util.EncryptPassword;
import com.mainbo.jms.MessageTypes;
import com.mainbo.modular.system.dao.PtAccountMapper;
import com.mainbo.modular.system.dao.PtOrgMapper;
import com.mainbo.modular.system.dao.PtPeopleMapper;
import com.mainbo.modular.system.model.*;
import com.mainbo.modular.system.service.IPtAccountService;
import com.mainbo.modular.system.service.IPtResourcesService;
import com.mainbo.modular.system.service.PeopleService;
import com.mainbo.utils.SecurityCode;
import com.mainbo.utils.StringUtils;
import org.apache.shiro.authc.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/2/26   14:18
 **/
@Service
public class PeopleServiceImpl extends ServiceImpl<PtPeopleMapper, People> implements PeopleService {
    @Resource
    private PtPeopleMapper ptPeopleMapper;
    @Resource
    private PtAccountMapper accountMapper;
    @Resource
    private AccountGenerator accountGenerator;
    @Resource
    private PtOrgMapper orgMapper;
    @Resource
    private IPtResourcesService resourcesService;
    @Resource
    private IPtAccountService accountService;
    @Override
    public CommUser getOne(String orgId, String name, String cellphone) {
        return ptPeopleMapper.getOne(orgId,name,cellphone);
    }

    @Override
    public Object findPeopleByCondition(UserSearchModel um) {
        return ptPeopleMapper.findPeopleByCondition(um);
    }

    @Override
    public People saveParents(People people) {
        People saveOne = null;
        People findByPhone=null;
        if (StringUtils.isNotBlank(people.getCellphone())) {
            findByPhone=new People();
            findByPhone.setCellphone(people.getCellphone());
            findByPhone.setUserType(PtAccount.PARENTS_USER);
            findByPhone = ptPeopleMapper.findOne(findByPhone);
        }
        if (people.getId()==null) { // 先通过手机号查找，没有再从account表找，没有正常添加，有则account表中不保存手机号
            PtAccount findOneByPhone = new PtAccount();
            PtAccount acc = new PtAccount();
            findOneByPhone.setCellphone(people.getCellphone());
            findOneByPhone.setDeleted(false);
            acc.setUserType(PtAccount.PARENTS_USER);
            findOneByPhone = accountMapper.selectOne(findOneByPhone);
            if (findOneByPhone == null) {
                acc.setCellphone(people.getCellphone());
            }
            // 初始化账号数据 机构必填
            String orgId = people.getOrgId();
            acc.setUserType(SysRole.PARRENT.getId());
            String accountName = accountGenerator.validate(acc);// 自动生成用户名
            String salt = SecurityCode.getSecurityCode(8, SecurityCode.SecurityCodeLevel.Hard, false);
            String password = EncryptPassword.encryptPassword(Const.DEFAULT_PWD);
            acc.setAccount(accountName);
            acc.setId(people.getId());
            acc.setEnable(true);
            acc.setPassword(password);
            acc.setSalt(salt);
            acc.setDeleted(false);
            acc.setOrgId(orgId);
             accountMapper.insert(acc);
            // 初始化学生数据
            people.setId(acc.getId());
            people.setCrtId(acc.getId());
            people.setCrtDttm(new Date());
            people.setLastupId(acc.getId());
            people.setLastupDttm(new Date());
            people.setUserType(people.getUserType());
            PtOrg org = orgMapper.selectById(orgId);
            people.setOrgName(org.getName());
            people.setEnable(true);
            if (StringUtils.isNotBlank(people.getPhoto())) {
                resourcesService.updateTmptResources(people.getPhoto());
            }
             insert(people);
            saveOne=people;
            accountService.sendUserMessege(MessageTypes.USER_ADD.name(), acc.getId());
            //LoggerUtils.insertLogger(LoggerModule.YHGL, "用户管理——添加家长用户，用户ID：{}", people.getId());
        } else {
            saveOne = findByPhone;
            people.setId(saveOne.getId());
            this.updateById(people);
            accountService.sendUserMessege(MessageTypes.USER_UPDATE.name(), people.getId());
           // LoggerUtils.updateLogger(LoggerModule.YHGL, "用户管理——修改家长用户，用户ID：{}", people.getId());
        }
        return saveOne;
    }

    @Override
    public List<People> exportPeoList(UserSearchModel um) {
        return ptPeopleMapper.getPeopleDetailInfo(um.getOrgId(), um.getAreaId());
    }
}
