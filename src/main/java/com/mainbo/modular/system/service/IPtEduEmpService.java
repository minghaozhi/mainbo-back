package com.mainbo.modular.system.service;

import com.mainbo.modular.system.model.PtEduEmp;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 机构职工表 服务类
 * </p>
 *
 * @author moshang
 * @since 2020-02-22
 */
public interface IPtEduEmpService extends IService<PtEduEmp> {

}
