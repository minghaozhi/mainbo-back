package com.mainbo.modular.system.service;

import com.mainbo.modular.jms.MessageTypes;
import com.mainbo.modular.system.model.PtSchoolClassroom;
import com.baomidou.mybatisplus.service.IService;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author moshang
 * @since 2020-03-05
 */
public interface IPtSchoolClassroomService extends IService<PtSchoolClassroom> {

    void batchUpdateDeleteFlag(String buildingId);
    void sendMessage(MessageTypes classroomAdd, PtSchoolClassroom cr);

    void updateDeleteFlag(String id);

    List<PtSchoolClassroom> listByBuildingId(List<String> idList);
}
