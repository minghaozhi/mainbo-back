package com.mainbo.modular.system.service.impl;

import com.mainbo.modular.system.dao.PtSectionCategoryMapper;
import com.mainbo.modular.system.model.PtSectionTime;
import com.mainbo.modular.system.model.PtSectionUnion;
import com.mainbo.modular.system.dao.PtSectionUnionMapper;
import com.mainbo.modular.system.service.IPtSectionUnionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author moshang
 * @since 2020-03-10
 */
@Service
public class PtSectionUnionServiceImpl extends ServiceImpl<PtSectionUnionMapper, PtSectionUnion> implements IPtSectionUnionService {
    @Resource
    private PtSectionUnionMapper sectionUnionMapper;

    @Override
    public void deleteByCategoryId(String categoryId) {
        sectionUnionMapper.deleteByCategoryId(categoryId);

    }

    @Override
    public void updateDelflagByName(String name) {
        sectionUnionMapper.updateDelflagByName(name);
    }

    @Override
    public void disableByName(Integer enable,String name) {
        sectionUnionMapper.disableByName(enable,name);
    }
}
