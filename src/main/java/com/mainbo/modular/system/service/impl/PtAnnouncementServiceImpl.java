package com.mainbo.modular.system.service.impl;

import com.mainbo.core.shiro.SessionKey;
import com.mainbo.core.shiro.ShiroKit;
import com.mainbo.core.util.StringUtil;
import com.mainbo.modular.system.model.PtAccount;
import com.mainbo.modular.system.model.PtAnnouncement;
import com.mainbo.modular.system.dao.PtAnnouncementMapper;
import com.mainbo.modular.system.model.PtOrg;
import com.mainbo.modular.system.service.IPtAnnouncementService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mainbo.modular.system.service.IPtResourcesService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author moshang
 * @since 2020-03-11
 */
@Service
public class PtAnnouncementServiceImpl extends ServiceImpl<PtAnnouncementMapper, PtAnnouncement> implements IPtAnnouncementService {
    @Resource
    private PtAnnouncementMapper ptAnnouncementMapper;
    @Resource
    private IPtResourcesService resourcesService;

    @Override
    public void saveAnnunciate(PtAnnouncement ptAnnouncement) {
        PtAccount currentUser = ShiroKit.getSessionAttr(SessionKey.CURRENT_USER);
        if (ptAnnouncement.getId() != null) {
            ptAnnouncement.setLastupDttm(new Date());
            ptAnnouncement.setLastupId(currentUser.getId());
            ptAnnouncementMapper.updateById(ptAnnouncement);
           // LoggerUtils.updateLogger(LoggerModule.PTGG, "平台公告——修改通知公告，公告id：{}", ciate.getId());
        } else {
            if (currentUser != null) {
                PtOrg org =ShiroKit.getSessionAttr(SessionKey.CURRENT_ORG);
                if (StringUtils.isEmpty(ptAnnouncement.getOrgId())) {

                    ptAnnouncement.setOrgId(org.getId());
                }
                ptAnnouncement.setCrtId(currentUser.getId());
                if (StringUtils.isEmpty(ptAnnouncement.getUserName()) && currentUser.getUserInfo() != null) {
                    ptAnnouncement.setUserName(currentUser.getUserInfo().getName());// 用户姓名
                }
                if (org == null) {
                    ptAnnouncement.setType(0);// 系统用户设置成系统通知公告
                } else if (Integer.valueOf(PtOrg.TP_ORG).equals(org.getType())) {
                    ptAnnouncement.setType(1);// 区域公告
                } else if (Integer.valueOf(PtOrg.TP_SCHOOL).equals(org.getType())) {
                    ptAnnouncement.setType(2);// 学校公告
                }
                if (StringUtils.isNotBlank(ptAnnouncement.getOrgId())&&(!ptAnnouncement.getOrgId().equals("0"))) {
                }else {
                    ptAnnouncement.setOrgId(currentUser.getOrgId());
                }
            }
            ptAnnouncement.setEnable(1);// 可用
            ptAnnouncement.setIsTop(0);
            ptAnnouncement.setCrtDttm(new Date());
             ptAnnouncementMapper.insert(ptAnnouncement);
          //  LoggerUtils.insertLogger(LoggerModule.PTGG, "平台公告——新增通知公告，公告id：{}", saveOne.getId());
        }
        if (ptAnnouncement.getAttachs() != null) {
            for (String resourceId : ptAnnouncement.getAttachs().split("#")) {
                resourcesService.updateTmptResources(resourceId);
            }
        }
        if (ptAnnouncement.getFlags() != null) {
            for (String resourceId : ptAnnouncement.getFlags().split("#")) {
                resourcesService.deleteResources(resourceId);
            }
        }
    }

    @Override
    public void deleteAnnunciate(Integer id) {
        PtAnnouncement announcement = ptAnnouncementMapper.selectById(id);
        if (!StringUtils.isEmpty(announcement.getAttachs())) {
            for (String resId : announcement.getAttachs().split("#")) {
                resourcesService.deleteResources(resId);
            }
        }
        announcement.deleteById(id);
    }
}
