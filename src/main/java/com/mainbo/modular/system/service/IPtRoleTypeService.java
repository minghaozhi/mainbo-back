package com.mainbo.modular.system.service;

import com.mainbo.modular.system.model.PtRoleType;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author moshang
 * @since 2020-02-23
 */
public interface IPtRoleTypeService extends IService<PtRoleType> {

    void setAuthority(Integer roleId, String ids);
}
