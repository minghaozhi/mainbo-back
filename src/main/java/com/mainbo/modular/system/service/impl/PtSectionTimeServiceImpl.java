package com.mainbo.modular.system.service.impl;

import com.mainbo.modular.system.model.PtSectionTime;
import com.mainbo.modular.system.dao.PtSectionTimeMapper;
import com.mainbo.modular.system.service.IPtSectionTimeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author moshang
 * @since 2020-03-10
 */
@Service
public class PtSectionTimeServiceImpl extends ServiceImpl<PtSectionTimeMapper, PtSectionTime> implements IPtSectionTimeService {
    @Resource
    private PtSectionTimeMapper sectionTimeMapper;

    @Override
    public void deleteByScId(String id) {
        sectionTimeMapper.deleteByScId(id);
    }
}
