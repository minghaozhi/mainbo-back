package com.mainbo.modular.system.service;

import com.mainbo.modular.system.model.PtNews;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author moshang
 * @since 2020-03-11
 */
public interface IPtNewsService extends IService<PtNews> {

    void saveNews(PtNews ptNews);

    void deleteNews(String batchDeleteIds);
}
