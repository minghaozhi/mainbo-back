package com.mainbo.modular.system.service;

import com.mainbo.core.util.Result;
import com.mainbo.modular.system.model.PtUserRole;
import com.baomidou.mybatisplus.service.IService;
import org.springframework.ui.Model;

import java.util.List;

/**
 * <p>
 * 切换工作空间 服务类
 * </p>
 *
 * @author moshang
 * @since 2020-02-22
 */
public interface IPtUserRoleService extends IService<PtUserRole> {

    void addOrEditPostManage(String accountId, String spaceId, Model model);

    Result saveAreaPostManage(PtUserRole us, Result result);
    /**
     * 查询班主任
     * @param userRole
     * @return
     */
    List<PtUserRole> findHeadmaster(PtUserRole userRole);

    List<PtUserRole> listTeacherByOrg_Phase_Subject(PtUserRole userRole);
}
