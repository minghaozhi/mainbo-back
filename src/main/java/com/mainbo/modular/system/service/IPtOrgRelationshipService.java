package com.mainbo.modular.system.service;

import com.mainbo.modular.system.model.PtOrgRelationship;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author moshang
 * @since 2020-03-04
 */
public interface IPtOrgRelationshipService extends IService<PtOrgRelationship> {
    /**
     * 根据orgId删除关系数据
     *
     * @param orgId
     *          机构id
     */
    void deleteByOrgId(String orgId);
}
