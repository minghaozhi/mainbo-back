package com.mainbo.modular.system.controller.xymh;

import cn.stylefeng.roses.core.base.controller.BaseController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mainbo.core.common.constant.ApolloConstants;
import com.mainbo.core.shiro.SessionKey;
import com.mainbo.core.shiro.ShiroKit;
import com.mainbo.modular.system.model.PtNews;
import com.mainbo.modular.system.model.PtOrg;
import com.mainbo.modular.system.service.IPtNewsCategoryService;
import com.mainbo.modular.system.service.IPtNewsService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/3/13   14:05
 **/
@Controller
@RequestMapping("/achieveShow")
public class AchieveShowController extends BaseController {
    private static final Logger logger= LoggerFactory.getLogger(AchieveShowController.class);

    private String PREFIX = "/system/xymh/achieve/";
    @Resource
    private IPtNewsService newsService;
    @Resource
    private IPtNewsCategoryService newsCategoryService;
    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index(Model model) {

        PtOrg org = ShiroKit.getSessionAttr(SessionKey.CURRENT_ORG);
        if ((org == null)) {
            model.addAttribute("orgId", "0");
            return PREFIX + "achieve_org.html";
        }
        model.addAttribute("orgId", org.getId());
        return PREFIX + "achieve_index.html";
    }

    @RequestMapping("achieveList")
    public String noteList(Model model,String orgId) {
        model.addAttribute("orgId", orgId);
        model.addAttribute("code", PtNews.CODE_SC_ARCHIEVE);
        return PREFIX + "achieve_list.html";
    }
    @RequestMapping("achieveDraft")
    public String noteDraft(Model model,String orgId) {
        model.addAttribute("orgId", orgId);
        model.addAttribute("code", PtNews.CODE_SC_ARCHIEVE);
        return PREFIX + "achieve_draft.html";
    }


    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(PtNews news) {
        EntityWrapper<PtNews> wrapper=new EntityWrapper<>();
        if (StringUtils.isNotBlank(news.getTitle())){
            wrapper.like("title",news.getTitle());
        }
        wrapper.eq("org_id",news.getOrgId());
        wrapper.eq("is_draft",news.getIsDraft());
        wrapper.eq("code",news.getCode());
        wrapper.orderBy("is_top desc,crt_dttm desc");
        List<PtNews> list= newsService.selectList(wrapper);
        for (PtNews achieve:list){
            if (StringUtils.isNotBlank(achieve.getCategory())) {
                achieve.setTypeName(newsCategoryService.selectById(achieve.getCategory()).getName());
            }
        }

        return list;
    }
    @RequestMapping(value = "/toAdd")
    public String add(PtNews news, Model m) {

        m.addAttribute("categorys", newsCategoryService.findAll(news));
        m.addAttribute("model", news);
        m.addAttribute("uploadUrl", ApolloConstants.readUploadUrl());
        m.addAttribute("storageUrl", ApolloConstants.readStorageUrl());
        return PREFIX+"achieve_add.html";
    }
    @RequestMapping(value = "/toUpdate")
    public String toUpdate(PtNews news, Model m) {
        PtNews ptNews=newsService.selectById(news.getId());
        m.addAttribute("categorys", newsCategoryService.findAll(ptNews));
        m.addAttribute("model", ptNews);
        m.addAttribute("uploadUrl", ApolloConstants.readUploadUrl());
        m.addAttribute("storageUrl", ApolloConstants.readStorageUrl());
        return PREFIX+"achieve_edit.html";
    }
    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{ptNewsId}")
    public Object detail(@PathVariable("ptNewsId") String ptNewsId,Model model) {
        PtNews ptNews=newsService.selectById(ptNewsId);
        model.addAttribute("categorys", newsCategoryService.findAll(ptNews));
        model.addAttribute("model", ptNews);
        model.addAttribute("uploadUrl", ApolloConstants.readUploadUrl());
        model.addAttribute("storageUrl", ApolloConstants.readStorageUrl());
        return PREFIX+"achieve_info.html";
    }
}
