/**
 * Copyright 2018-2020 stylefeng & fengshuonan (https://gitee.com/stylefeng)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mainbo.modular.system.controller;

import com.mainbo.core.common.constant.ApolloConstants;
import com.mainbo.core.common.exception.InvalidKaptchaException;
import com.mainbo.core.common.node.MenuNode;
import com.mainbo.core.log.LogManager;
import com.mainbo.core.log.factory.LogTaskFactory;
import com.mainbo.core.shiro.SessionKey;
import com.mainbo.core.shiro.ShiroKit;
import com.mainbo.core.shiro.ShiroUser;
import com.mainbo.core.shiro.service.UserAuthService;
import com.mainbo.core.util.ApiMenuFilter;
import com.mainbo.core.util.KaptchaUtil;
import com.mainbo.modular.system.model.PtAccount;
import com.mainbo.modular.system.model.PtOrg;
import com.mainbo.modular.system.service.IMenuService;
import com.mainbo.modular.system.service.IPtAccountService;
import com.mainbo.modular.system.service.IPtOrgService;
import com.mainbo.modular.system.service.IUserService;
import cn.stylefeng.roses.core.base.controller.BaseController;
import cn.stylefeng.roses.core.util.ToolUtil;
import com.google.code.kaptcha.Constants;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static cn.stylefeng.roses.core.util.HttpContext.getIp;
import static com.mainbo.core.common.constant.ApolloConstants.MAINBO_PUBLIC_NAMESACE;
import static com.mainbo.core.common.constant.ApolloConstants.UCENTER__HOST;

/**
 * 登录控制器
 *
 * @author moshang
 * @Date 2020-02-17 21:23:57
 */
@Controller
public class LoginController extends BaseController {
    public String getRedirecturl() {
        return ApolloConstants.readConfig(MAINBO_PUBLIC_NAMESACE, UCENTER__HOST) + "/logout";
    }
    @Autowired
    private IMenuService menuService;
    @Resource
    private IPtOrgService orgService;

    @Autowired
    private IPtAccountService userService;
    @Autowired
    private UserAuthService userAuthService;

    /**
     * 跳转到主页
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        PtOrg org = orgService.listOrgList();
        if (org != null && ShiroKit
                .getSessionAttr(SessionKey.CURRENT_ORG) == null) {
            ShiroKit.setSessionAttr(SessionKey.CURRENT_ORG,
                    org);
        }
        //获取菜单列表
        List<Integer> roleList = ShiroKit.getUser().getRoleList();
        if (roleList == null || roleList.size() == 0) {
            ShiroKit.getSubject().logout();
            model.addAttribute("tips", "该用户没有角色，无法登陆");
            return "/login.html";
        }
        List<MenuNode> menus=new ArrayList<>();
        if (ShiroKit.isAdmin()){
            menus = menuService.getAllMenus();
        }else {
            menus = menuService.getMenusByRoleIds(roleList);
        }
        List<MenuNode> titles = MenuNode.buildTitle(menus);
        titles = ApiMenuFilter.build(titles);

        model.addAttribute("titles", titles);
        model.addAttribute("logoutUrl",getRedirecturl());
        //获取用户头像
        PtAccount account = ShiroKit.getSessionAttr(SessionKey.CURRENT_USER);
        String avatar="";
        assert account != null;
        if(StringUtils.isNoneBlank(account.getUserInfo().getPhoto())){
             avatar = account.getUserInfo().getPhoto();
        }
       model.addAttribute("storageUrl", ApolloConstants.readConfig(ApolloConstants.MAINBO_PUBLIC_NAMESACE,ApolloConstants.STORSGESERVER_URL));
        model.addAttribute("avatar", avatar);

        return "/index.html";
    }

    /**
     * 跳转到登录页面
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        PtAccount current=ShiroKit.getSessionAttr(SessionKey.CURRENT_USER);
        if (ShiroKit.isAuthenticated() || current!= null) {
            return REDIRECT + "/";
        } else {
            return "/login.html";
        }
    }

    /**
     * 点击登录执行的动作
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String loginVali() {

        String username = super.getPara("username").trim();
        String password = super.getPara("password").trim();
        String remember = super.getPara("remember");

        //验证验证码是否正确
        if (KaptchaUtil.getKaptchaOnOff()) {
            String kaptcha = super.getPara("kaptcha").trim();
            String code = (String) super.getSession().getAttribute(Constants.KAPTCHA_SESSION_KEY);
            if (ToolUtil.isEmpty(kaptcha) || !kaptcha.equalsIgnoreCase(code)) {
                throw new InvalidKaptchaException();
            }
        }

        Subject currentUser = ShiroKit.getSubject();

        UsernamePasswordToken token = new UsernamePasswordToken(username, password.toCharArray());

        if ("on".equals(remember)) {
            token.setRememberMe(true);
        } else {
            token.setRememberMe(false);
        }

        currentUser.login(token);

        ShiroUser shiroUser = ShiroKit.getUser();

        super.getSession().setAttribute("shiroUser", shiroUser);
        super.getSession().setAttribute("username", shiroUser.getAccount());

        LogManager.me().executeLog(LogTaskFactory.loginLog(shiroUser.getId(), getIp()));

        ShiroKit.getSession().setAttribute("sessionFlag", true);

        return REDIRECT + "/";
    }

//    /**
//     * 退出登录
//     */
//    @RequestMapping(value = "/logout", method = RequestMethod.GET)
//    public String logOut() {
//        LogManager.me().executeLog(LogTaskFactory.exitLog(ShiroKit.getUser().getId(), getIp()));
//        ShiroKit.getSubject().logout();
//        deleteAllCookie();
//        ShiroKit.removeSessionAttr(SessionKey.CURRENT_USER);
//        return getRedirecturl();
//    }
}
