package com.mainbo.modular.system.controller.xymh;

import cn.stylefeng.roses.core.base.controller.BaseController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mainbo.core.common.constant.ApolloConstants;
import com.mainbo.core.log.LogObjectHolder;
import com.mainbo.core.shiro.SessionKey;
import com.mainbo.core.shiro.ShiroKit;
import com.mainbo.core.util.Result;
import com.mainbo.modular.system.model.PtFriendlink;
import com.mainbo.modular.system.model.PtOrg;
import com.mainbo.modular.system.service.IPtFriendlinkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * banner配置
 * @author moshang
 * @date 2020-03-15
 **/
@Controller
@RequestMapping("/banner")
public class BannerController extends BaseController {

    private static final Logger logger= LoggerFactory.getLogger(MenHuController.class);


    private String PREFIX = "/system/xymh/banner/";

    @Resource
    private IPtFriendlinkService ptFriendlinkService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index(Model model,PtFriendlink ptFriendlink) {

        PtOrg org = ShiroKit.getSessionAttr(SessionKey.CURRENT_ORG);
        if ((org == null)) {
            model.addAttribute("orgId", "0");
            return PREFIX + "banner_org.html";
        }
        ptFriendlink.setOrgId(org.getId());
        ptFriendlink.setIsOrg(1);
        ptFriendlink.setCode(PtFriendlink.CODE_SC_BANNER);
        model.addAttribute("model", ptFriendlink);
        return PREFIX + "banner_list.html";
    }
    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(PtFriendlink ptFriendlink) {
        EntityWrapper<PtFriendlink> entityWrapper=new EntityWrapper<>();
        entityWrapper.eq("org_id",ptFriendlink.getOrgId());
        entityWrapper.eq("code",ptFriendlink.getCode());
        return ptFriendlinkService.selectList(entityWrapper);
    }
    /**
     * 跳转到添加
     */
    @RequestMapping("/banner_add")
    public String ptFriendlinkAdd(PtFriendlink friendlink,Model model) {
        model.addAttribute("model",friendlink);
        model.addAttribute("uploadUrl", ApolloConstants.readUploadUrl());
        model.addAttribute("storageUrl", ApolloConstants.readStorageUrl());
        return PREFIX + "banner_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/banner_update/{bannerId}")
    public String ptFriendlinkUpdate(@PathVariable Integer bannerId, Model model) {
        PtFriendlink ptFriendlink = ptFriendlinkService.selectById(bannerId);
        model.addAttribute("item",ptFriendlink);
        model.addAttribute("uploadUrl", ApolloConstants.readUploadUrl());
        model.addAttribute("storageUrl", ApolloConstants.readStorageUrl());
        LogObjectHolder.me().set(ptFriendlink);
        return PREFIX + "banner_edit.html";
    }


    /**
     * 保存配置
     *
     * @param model
     *          配置
     */
    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public Result saveOrUpdate(PtFriendlink model) {
        Result result=new Result();
        result.setCode(Result.SUCCESS);
        if (model.getId() == null) {
            ptFriendlinkService.insert(model);
//            LoggerUtils.insertLogger("保存门户配置成功：code: {} ,name:{}, value : {}", model.getCode(), model.getName(),
//                    model.getWebsite());
            result.setMsg("保存banner成功");
        } else {
            ptFriendlinkService.updateById(model);
//            LoggerUtils.insertLogger("更新门户配置成功：code: {} ,name:{}, value : {}", model.getCode(), model.getName(),
//                    model.getWebsite());
            result.setMsg("更新banner成功");
        }
        return result;
    }

    /**
     * 删除配置
     *
     * @param id
     *          配置id
     */
    @RequestMapping("/delete/{id}")
    @ResponseBody
    public Result delete(@PathVariable("id") Integer id) {
        Result result = new Result();
        result.setCode(Result.SUCCESS);
        try {
            ptFriendlinkService.deleteById(id);
        //    LoggerUtils.deleteLogger("删除门户配置数据成功，id : {}", id);
            result.setMsg("删除成功");
        } catch (Exception e) {
            result.setCode(Result.FAIL);
            result.setMsg("删除失败");
            logger.error("删除元数据失败", e);
        }
        return result;
    }
}
