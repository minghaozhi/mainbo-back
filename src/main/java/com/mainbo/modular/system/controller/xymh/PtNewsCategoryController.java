package com.mainbo.modular.system.controller.xymh;

import cn.stylefeng.roses.core.base.controller.BaseController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mainbo.core.common.constant.Const;
import com.mainbo.core.util.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.mainbo.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.mainbo.modular.system.model.PtNewsCategory;
import com.mainbo.modular.system.service.IPtNewsCategoryService;

import java.util.ArrayList;
import java.util.List;

/**
 * 控制器
 *
 * @author fengshuonan
 * @Date 2020-03-11 11:17:58
 */
@Controller
@RequestMapping("/ptNewsCategory")
public class PtNewsCategoryController extends BaseController {

    private static final Logger logger= LoggerFactory.getLogger(PtNewsCategoryController.class);
    private String PREFIX = "/system/xymh/category/";

    @Autowired
    private IPtNewsCategoryService ptNewsCategoryService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index(Model model,PtNewsCategory newsCategory) {
        model.addAttribute("model", newsCategory);
        return PREFIX + "category_list.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/ptNewsCategory_add")
    public String ptNewsCategoryAdd(PtNewsCategory newsCategory,Model model) {
        model.addAttribute("item",newsCategory);
        return PREFIX + "category_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/ptNewsCategory_update/{ptNewsCategoryId}")
    public String ptNewsCategoryUpdate(@PathVariable Integer ptNewsCategoryId, Model model) {
        PtNewsCategory ptNewsCategory = ptNewsCategoryService.selectById(ptNewsCategoryId);
        model.addAttribute("item",ptNewsCategory);
        LogObjectHolder.me().set(ptNewsCategory);
        return PREFIX + "category_edit.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(PtNewsCategory model) {
        List<String> orgIds=new ArrayList<>();
        orgIds.add(model.getOrgId());
        orgIds.add(Const.DEFAULT_ORG_ID);
        EntityWrapper<PtNewsCategory> wrapper=new EntityWrapper<>();
        wrapper.eq("parent_code",model.getParentCode());
        wrapper.in("org_id",orgIds);
        wrapper.orderBy("sort asc,crt_dttm desc");
        return ptNewsCategoryService.selectList(wrapper);
    }

    /**
     * 新增或修改
     */
    @RequestMapping(value = "/addOrUpdate")
    @ResponseBody
    public Result add(PtNewsCategory ptNewsCategory) {
        Result re = new Result();
        re.setCode(Result.SUCCESS);
        if (isNameExist(ptNewsCategory)) {
            re.setMsg("名称重复");
            re.setCode(Result.FAIL);
            return re;
        }
        if (ptNewsCategory.getId() == null) {
            ptNewsCategory.setEnable(1);
             ptNewsCategoryService.insert(ptNewsCategory);
            re.setMsg("保存成功");
           // LoggerUtils.insertLogger("保存成果分类成功,id:{}", model.getId());
            logger.info("保存成果分类成功，id：{}", ptNewsCategory.getId());
        } else {
            ptNewsCategoryService.updateById(ptNewsCategory);
            //LoggerUtils.updateLogger("保存成果分类成功,id:{}", model.getId());
            re.setMsg("修改成功");
            logger.info("修改成果分类成功，id：{}", ptNewsCategory.getId());
        }
        return re;
    }

    private boolean isNameExist(PtNewsCategory model) {
        if (model == null) {
            return true;
        }
        if (model.getId() == null) {
            EntityWrapper<PtNewsCategory> cate = new EntityWrapper<>();
            cate.eq("name",model.getName());
            return null != ptNewsCategoryService.selectOne(cate);
        }
        if (model.getId() != null) {
            EntityWrapper<PtNewsCategory> cate = new EntityWrapper<>();
            cate.eq("name",model.getName());
            cate.eq("org_id",model.getOrgId());
            PtNewsCategory one = ptNewsCategoryService.selectOne(cate);
            return null != one && !one.getId().equals(model.getId());
        }
        return true;
    }
    /**
     * 删除
     */
    @RequestMapping(value = "/delete/{id}")
    @ResponseBody
    public Object delete(@PathVariable Integer id) {
        Result re = new Result();
        if (id != null) {
            ptNewsCategoryService.deleteById(id);
            re.setMsg("删除成功");
            re.setCode(Result.SUCCESS);
            //LoggerUtils.deleteLogger("删除成果分类成功，id：{}", id);
            logger.info("删除成果分类成功，id：{}", id);
        }
        return re;
    }
    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{ptNewsCategoryId}")
    @ResponseBody
    public Object detail(@PathVariable("ptNewsCategoryId") Integer ptNewsCategoryId) {
        return ptNewsCategoryService.selectById(ptNewsCategoryId);
    }
}
