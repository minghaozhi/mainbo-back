package com.mainbo.modular.system.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.mainbo.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.mainbo.modular.system.model.PtUserManagescope;
import com.mainbo.modular.system.service.IPtUserManagescopeService;

/**
 * 控制器
 *
 * @author fengshuonan
 * @Date 2020-03-04 20:04:24
 */
@Controller
@RequestMapping("/ptUserManagescope")
public class PtUserManagescopeController extends BaseController {

    private String PREFIX = "/system/ptUserManagescope/";

    @Autowired
    private IPtUserManagescopeService ptUserManagescopeService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "ptUserManagescope.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/ptUserManagescope_add")
    public String ptUserManagescopeAdd() {
        return PREFIX + "ptUserManagescope_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/ptUserManagescope_update/{ptUserManagescopeId}")
    public String ptUserManagescopeUpdate(@PathVariable Integer ptUserManagescopeId, Model model) {
        PtUserManagescope ptUserManagescope = ptUserManagescopeService.selectById(ptUserManagescopeId);
        model.addAttribute("item",ptUserManagescope);
        LogObjectHolder.me().set(ptUserManagescope);
        return PREFIX + "ptUserManagescope_edit.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return ptUserManagescopeService.selectList(null);
    }

    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(PtUserManagescope ptUserManagescope) {
        ptUserManagescopeService.insert(ptUserManagescope);
        return SUCCESS_TIP;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer ptUserManagescopeId) {
        ptUserManagescopeService.deleteById(ptUserManagescopeId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(PtUserManagescope ptUserManagescope) {
        ptUserManagescopeService.updateById(ptUserManagescope);
        return SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{ptUserManagescopeId}")
    @ResponseBody
    public Object detail(@PathVariable("ptUserManagescopeId") Integer ptUserManagescopeId) {
        return ptUserManagescopeService.selectById(ptUserManagescopeId);
    }
}
