package com.mainbo.modular.system.controller.xymh;

import cn.stylefeng.roses.core.base.controller.BaseController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mainbo.core.common.constant.ApolloConstants;
import com.mainbo.core.shiro.SessionKey;
import com.mainbo.core.shiro.ShiroKit;
import com.mainbo.core.util.Result;
import com.mainbo.modular.system.model.PtAccount;
import com.mainbo.modular.system.model.PtOrg;
import com.mainbo.modular.system.model.PtScDoor;
import com.mainbo.modular.system.service.IPtScDoorService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 门户信息定制
 * @author moshang
 * @date 2020-03-15
 **/
@Controller
@RequestMapping("/menHu")
public class MenHuController extends BaseController {
    private static final Logger logger= LoggerFactory.getLogger(MenHuController.class);


    private String PREFIX = "/system/xymh/menHu/";
    @Resource
    private IPtScDoorService scDoorService;


    /**
     * 门户信息定制首页
     *
     * @param m
     *          Model
     *          搜索名称
     * @return String
     */
    @RequestMapping("")
    public String getSchoolTree(Model m) {
        PtOrg org = ShiroKit.getSessionAttr(SessionKey.CURRENT_ORG);

        if ((org == null)) {
            return PREFIX+"index.html";
        }else {
            EntityWrapper<PtScDoor> param=new EntityWrapper<>();
            param.eq("org_id",org.getId());
            //param.eq(1);
            m.addAttribute("uploadUrl", ApolloConstants.readUploadUrl());
            m.addAttribute("storageUrl", ApolloConstants.readStorageUrl());
            PtScDoor scDoor=scDoorService.selectOne(param);
             if (scDoor==null){
                 m.addAttribute("scDoor",new PtScDoor());
             }else {
                 m.addAttribute("scDoor",scDoor);
             }

            m.addAttribute("orgId",org.getId());
            return PREFIX+"list.html";
        }
    }

    /**
     * 保存首页配置
     *
     *          配置内容
     */
    @ResponseBody
    @RequestMapping("/saveOrUpdate")
    public Result save(PtScDoor model) {
        PtAccount account=ShiroKit.getSessionAttr(SessionKey.CURRENT_USER);
        Result result = new Result();
        result.setCode(Result.SUCCESS);
        boolean flag;
        try {
            if (model.getId() != null) {
                model.setLastupId(account.getId());
                model.setLastupDttm(new Date());
              flag=scDoorService.updateById(model);
             if (flag) {
                 result.setMsg("修改成功");
             }else {
                 result.setMsg("修改失败");
             }
            } else {
                model.setEnable(1);
                model.setCrtId(account.getId());
                model.setCrtDttm(new Date());
             flag=scDoorService.insert(model);
                if (flag) {
                    result.setMsg("添加成功");
                }else {
                    result.setMsg("添加失败");
                }

            }

        } catch (Exception e) {
            logger.error("", e);

        }

        return result;
    }
}
