package com.mainbo.modular.system.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.mainbo.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.mainbo.modular.system.model.PtClassUser;
import com.mainbo.modular.system.service.IPtClassUserService;

/**
 * 控制器
 *
 * @author fengshuonan
 * @Date 2020-02-28 13:39:13
 */
@Controller
@RequestMapping("/ptClassUser")
public class PtClassUserController extends BaseController {

    private String PREFIX = "/system/ptClassUser/";

    @Autowired
    private IPtClassUserService ptClassUserService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "ptClassUser.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/ptClassUser_add")
    public String ptClassUserAdd() {
        return PREFIX + "ptClassUser_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/ptClassUser_update/{ptClassUserId}")
    public String ptClassUserUpdate(@PathVariable Integer ptClassUserId, Model model) {
        PtClassUser ptClassUser = ptClassUserService.selectById(ptClassUserId);
        model.addAttribute("item",ptClassUser);
        LogObjectHolder.me().set(ptClassUser);
        return PREFIX + "ptClassUser_edit.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return ptClassUserService.selectList(null);
    }

    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(PtClassUser ptClassUser) {
        ptClassUserService.insert(ptClassUser);
        return SUCCESS_TIP;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer ptClassUserId) {
        ptClassUserService.deleteById(ptClassUserId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(PtClassUser ptClassUser) {
        ptClassUserService.updateById(ptClassUser);
        return SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{ptClassUserId}")
    @ResponseBody
    public Object detail(@PathVariable("ptClassUserId") Integer ptClassUserId) {
        return ptClassUserService.selectById(ptClassUserId);
    }
}
