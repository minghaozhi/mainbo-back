package com.mainbo.modular.system.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mainbo.core.common.constant.ApolloConstants;
import com.mainbo.core.shiro.SessionKey;
import com.mainbo.core.shiro.ShiroKit;
import com.mainbo.core.util.Result;
import com.mainbo.modular.jms.MessageTypes;
import com.mainbo.modular.system.model.UserSearchModel;
import com.mainbo.modular.system.model.meta.ConfigConstants;
import com.mainbo.modular.system.model.meta.MetaUtils;
import com.mainbo.modular.system.model.meta.vo.Meta;
import com.mainbo.modular.system.model.vo.OrgVo;
import com.mainbo.modular.system.service.impl.PtOrgServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.mainbo.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.mainbo.modular.system.model.PtOrg;
import com.mainbo.modular.system.service.IPtOrgService;

import java.util.Date;
import java.util.List;

/**
 * 控制器
 *
 * @author moshang
 * @Date 2020-02-21 21:05:19
 */
@Controller
@RequestMapping("/ptOrg")
public class PtOrgController extends BaseController {
    private final static Logger logger = LoggerFactory.getLogger(PtOrgController.class);
    private String PREFIX = "/system/ptOrg/";

    @Autowired
    private IPtOrgService ptOrgService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index(Model model) {
        PtOrg org = ShiroKit.getSessionAttr(SessionKey.CURRENT_ORG);
        if ((org == null)) {
            model.addAttribute("orgId","0");
            return PREFIX + "ptOrg_org.html";
        }
        model.addAttribute("orgId",org.getId());
        model.addAttribute("org",org);
        String areaName=ptOrgService.getAreaNameById(org.getAreaId());
        org.setAreaName(areaName);
        List<Meta> orgTypes= MetaUtils.listMeta(ConfigConstants.ORG_TYPE_CODE);
        model.addAttribute("orgTypes",orgTypes);
        model.addAttribute("uploadUrl", ApolloConstants.readUploadUrl());
        model.addAttribute("storageUrl", ApolloConstants.readConfig(ApolloConstants.MAINBO_PUBLIC_NAMESACE,ApolloConstants.STORSGESERVER_URL));
        model.addAttribute("schModels", MetaUtils.listMeta(ConfigConstants.SCHOOL_MODEL_CODE));
        return PREFIX + "ptOrg_update.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/ptOrg_add")
    public String ptOrgAdd() {
        return PREFIX + "ptOrg_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/ptOrg_update/{ptOrgId}")
    public String ptOrgUpdate(@PathVariable Integer ptOrgId, Model model) {
        PtOrg ptOrg = ptOrgService.selectById(ptOrgId);
        model.addAttribute("item",ptOrg);
        LogObjectHolder.me().set(ptOrg);
        return PREFIX + "ptOrg_edit.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String areaId) {
        EntityWrapper<PtOrg> wrapper=new EntityWrapper<>();
        wrapper.eq("area_id",areaId);
        wrapper.eq("enable",1);
        List<PtOrg> list = ptOrgService.selectList(wrapper);
        return list;
    }
    /**
     * 获取列表
     */
    @RequestMapping(value = "/findList")
    @ResponseBody
    public Object findList(PtOrg org) {
        List<OrgVo> list = ptOrgService.findList(org);
        return list;
    }
    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(PtOrg ptOrg) {
        ptOrgService.insert(ptOrg);
        return SUCCESS_TIP;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer ptOrgId) {
        ptOrgService.deleteById(ptOrgId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Result update(PtOrg org) {
        Result rs = new Result();
        org.setLastupDttm(new Date());
        try {
            Integer update = ptOrgService.updateOrg(org);
            if (update != null && update > 0) {
                ShiroKit.setSessionAttr(SessionKey.CURRENT_ORG,ptOrgService.selectById(org.getId()));
                ptOrgService.sendMessage(MessageTypes.ORG_UPDATE, ptOrgService.selectById(org.getId()));
            }
            rs.setMsg("保存成功！");
            rs.setCode(Result.SUCCESS);
        } catch (Exception e) {
            rs.setCode(Result.FAIL);
            rs.setMsg("保存失败！");
            logger.error("机构保存失败", e);
        }
        return rs;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{ptOrgId}")
    @ResponseBody
    public Object detail(@PathVariable("ptOrgId") Integer ptOrgId) {
        return ptOrgService.selectById(ptOrgId);
    }
}
