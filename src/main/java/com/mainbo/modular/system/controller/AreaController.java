package com.mainbo.modular.system.controller;

import com.mainbo.core.common.node.ZTreeNode;
import com.mainbo.core.log.LogObjectHolder;
import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import com.mainbo.modular.system.model.PtArea;
import com.mainbo.modular.system.service.IPtAreaService;

import java.util.List;

/**
 * 控制器
 *
 * @author moshang
 * @Date 2020-02-17 21:23:57
 */
@Controller
@RequestMapping("/back/sys/area")
public class AreaController extends BaseController {

    private String PREFIX = "/system/ptArea/";

    @Autowired
    private IPtAreaService ptAreaService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "ptArea.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/ptArea_add")
    public String ptAreaAdd() {
        return PREFIX + "ptArea_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/ptArea_update/{ptAreaId}")
    public String ptAreaUpdate(@PathVariable Integer ptAreaId, Model model) {
        PtArea ptArea = ptAreaService.selectById(ptAreaId);
        model.addAttribute("item",ptArea);
        LogObjectHolder.me().set(ptArea);
        return PREFIX + "ptArea_edit.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        List<ZTreeNode> tree = this.ptAreaService.tree();
        tree.add(ZTreeNode.createParent());
        return tree;
    }

    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(PtArea ptArea) {
        ptAreaService.insert(ptArea);
        return SUCCESS_TIP;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer ptAreaId) {
        ptAreaService.deleteById(ptAreaId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(PtArea ptArea) {
        ptAreaService.updateById(ptArea);
        return SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{ptAreaId}")
    @ResponseBody
    public Object detail(@PathVariable("ptAreaId") Integer ptAreaId) {
        return ptAreaService.selectById(ptAreaId);
    }
}
