package com.mainbo.modular.system.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import cn.stylefeng.roses.core.reqres.response.ResponseData;
import cn.stylefeng.roses.core.util.ToolUtil;
import cn.stylefeng.roses.kernel.model.exception.ServiceException;
import com.mainbo.core.common.annotion.BussinessLog;
import com.mainbo.core.common.annotion.Permission;
import com.mainbo.core.common.constant.Const;
import com.mainbo.core.common.constant.dictmap.RoleDict;
import com.mainbo.core.common.constant.factory.ConstantFactory;
import com.mainbo.core.common.exception.BizExceptionEnum;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.mainbo.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.mainbo.modular.system.model.PtRoleType;
import com.mainbo.modular.system.service.IPtRoleTypeService;

/**
 * 控制器
 *
 * @author fengshuonan
 * @Date 2020-02-23 17:24:56
 */
@Controller
@RequestMapping("/ptRoleType")
public class PtRoleTypeController extends BaseController {

    private String PREFIX = "/system/ptRoleType/";

    @Autowired
    private IPtRoleTypeService ptRoleTypeService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "ptRoleType.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/ptRoleType_add")
    public String ptRoleTypeAdd() {
        return PREFIX + "ptRoleType_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/ptRoleType_update/{ptRoleTypeId}")
    public String ptRoleTypeUpdate(@PathVariable Integer ptRoleTypeId, Model model) {
        PtRoleType ptRoleType = ptRoleTypeService.selectById(ptRoleTypeId);
        model.addAttribute("item",ptRoleType);
        LogObjectHolder.me().set(ptRoleType);
        return PREFIX + "ptRoleType_edit.html";
    }
    /**
     * 跳转到角色分配
     */
    @Permission
    @RequestMapping(value = "/role_assign/{roleId}")
    public String roleAssign(@PathVariable("roleId") Integer roleId, Model model) {
        if (ToolUtil.isEmpty(roleId)) {
            throw new ServiceException(BizExceptionEnum.REQUEST_NULL);
        }
        model.addAttribute("roleId", roleId);
        model.addAttribute("roleName", ConstantFactory.me().getSingleRoleName(roleId));
        return PREFIX + "ptRoleType_assign.html";
    }
    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return ptRoleTypeService.selectList(null);
    }

    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(PtRoleType ptRoleType) {
        ptRoleTypeService.insert(ptRoleType);
        return SUCCESS_TIP;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer ptRoleTypeId) {
        ptRoleTypeService.deleteById(ptRoleTypeId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(PtRoleType ptRoleType) {
        ptRoleTypeService.updateById(ptRoleType);
        return SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{ptRoleTypeId}")
    @ResponseBody
    public Object detail(@PathVariable("ptRoleTypeId") Integer ptRoleTypeId) {
        return ptRoleTypeService.selectById(ptRoleTypeId);
    }
    /**
     * 配置权限
     */
    @RequestMapping("/setAuthority")
    @BussinessLog(value = "配置权限", key = "roleId,ids", dict = RoleDict.class)
    @Permission(Const.ADMIN_NAME)
    @ResponseBody
    public ResponseData setAuthority(@RequestParam("roleId") Integer roleId, @RequestParam("ids") String ids) {
        if (ToolUtil.isOneEmpty(roleId)) {
            throw new ServiceException(BizExceptionEnum.REQUEST_NULL);
        }
        this.ptRoleTypeService.setAuthority(roleId, ids);
        return SUCCESS_TIP;
    }
}
