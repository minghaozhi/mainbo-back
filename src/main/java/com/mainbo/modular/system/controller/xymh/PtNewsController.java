package com.mainbo.modular.system.controller.xymh;

import cn.stylefeng.roses.core.base.controller.BaseController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mainbo.core.common.constant.ApolloConstants;
import com.mainbo.core.shiro.SessionKey;
import com.mainbo.core.shiro.ShiroKit;
import com.mainbo.core.util.Result;
import com.mainbo.modular.system.model.PtOrg;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.mainbo.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.mainbo.modular.system.model.PtNews;
import com.mainbo.modular.system.service.IPtNewsService;

import java.util.Date;
import java.util.List;

/**
 * 控制器
 *
 * @author fengshuonan
 * @Date 2020-03-11 11:17:42
 */
@Controller
@RequestMapping("/ptNews")
public class PtNewsController extends BaseController {
    private static final Logger logger= LoggerFactory.getLogger(PtNewsController.class);

    private String PREFIX = "/system/xymh/news/";

    @Autowired
    private IPtNewsService ptNewsService;
    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index(Model model) {

        PtOrg org = ShiroKit.getSessionAttr(SessionKey.CURRENT_ORG);
        if ((org == null)) {
            model.addAttribute("orgId", "0");
            return PREFIX + "news_org.html";
        }
        model.addAttribute("orgId", org.getId());
        return PREFIX + "news_index.html";
    }

    @RequestMapping("newsList")
    public String noteList(Model model,String orgId) {
        model.addAttribute("orgId", orgId);
        model.addAttribute("code", PtNews.CODE_SC_NEWS);
        return PREFIX + "news_list.html";
    }
    @RequestMapping("newsDraft")
    public String noteDraft(Model model,String orgId) {
        model.addAttribute("orgId", orgId);
        model.addAttribute("code", PtNews.CODE_SC_NEWS);
        return PREFIX + "news_draft.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/ptNews_add")
    public String ptNewsAdd(PtNews news,Model model) {
        model.addAttribute("model", news);
        model.addAttribute("uploadUrl", ApolloConstants.readUploadUrl());
        model.addAttribute("storageUrl", ApolloConstants.readStorageUrl());
        return PREFIX + "news_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/ptNews_update/{ptNewsId}")
    public String ptNewsUpdate(@PathVariable String ptNewsId, Model model) {
        PtNews ptNews = ptNewsService.selectById(ptNewsId);
        model.addAttribute("model",ptNews);
        model.addAttribute("uploadUrl", ApolloConstants.readUploadUrl());
        model.addAttribute("storageUrl", ApolloConstants.readStorageUrl());
        LogObjectHolder.me().set(ptNews);
        return PREFIX + "news_edit.html";
    }


    /**
     * 新增
     */
    @RequestMapping(value = "/addOrUpdate")
    @ResponseBody
    public Object add(PtNews ptNews) {
        Result juiResult = new Result();
        juiResult.setMsg("操作成功");
        try {
            juiResult.setCode(Result.SUCCESS);
            ptNewsService.saveNews(ptNews);
        } catch (Exception e) {
            juiResult.setCode(Result.FAIL);
            juiResult.setMsg("保存失败");
            logger.error(ptNews.getCode() + "保存失败", e);
        }
        return juiResult;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer ptNewsId) {
        ptNewsService.deleteById(ptNewsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(PtNews ptNews) {
        ptNewsService.updateById(ptNews);
        return SUCCESS_TIP;
    }
    /**
     * 刪除新闻信息
     *
     * @param batchDeleteIds
     *          待删除的新闻ids
     */
    @RequestMapping("/batchdelete")
    @ResponseBody
    public Result batchdelete(String batchDeleteIds) {
        Result result = new Result();
        try {
            ptNewsService.deleteNews(batchDeleteIds);
            result.setMsg("删除成功！");
        } catch (Exception e) {
            result.setCode(Result.FAIL);
            result.setMsg("删除失败！");
            logger.error("刪除平台新闻/学校成果失败", e);
        }
        return result;
    }
    /**
     * 添加置顶
     *
     */
    @ResponseBody
    @RequestMapping("/isTop")
    public Result isTop(PtNews news) {
        Result result = new Result();
        result.setCode(Result.SUCCESS);
        try {
                if (news.getIsTop()==0) {
                    result.setMsg("取消置顶成功！");
                } else {
                    result.setMsg("置顶成功！");
                }
                news.setLastupDttm(new Date());
                ptNewsService.updateById(news);
        } catch (Exception e) {
            result.setCode(Result.FAIL);
            result.setMsg("置顶失败！");
            logger.error("置顶新闻信息/学校成果失败", e);
        }
        return result;
    }
    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{ptNewsId}")
    public String detail(@PathVariable("ptNewsId") String ptNewsId,Model model) {
        PtNews ptNews=ptNewsService.selectById(ptNewsId);
        model.addAttribute("model", ptNews);
        model.addAttribute("uploadUrl", ApolloConstants.readUploadUrl());
        model.addAttribute("storageUrl", ApolloConstants.readStorageUrl());
        return PREFIX+"news_info.html";
    }
}
