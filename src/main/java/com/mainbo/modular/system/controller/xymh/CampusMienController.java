package com.mainbo.modular.system.controller.xymh;

import cn.stylefeng.roses.core.base.controller.BaseController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mainbo.core.common.constant.ApolloConstants;
import com.mainbo.core.shiro.SessionKey;
import com.mainbo.core.shiro.ShiroKit;
import com.mainbo.core.util.Result;
import com.mainbo.modular.system.model.PtFriendlink;
import com.mainbo.modular.system.model.PtOrg;
import com.mainbo.modular.system.service.IPtFriendlinkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * 校园风采
 * @author moshang
 * @date 2020-03-14
 **/
@Controller
@RequestMapping("/campusMien")
public class CampusMienController extends BaseController {


    private static final Logger logger = LoggerFactory.getLogger(CampusMienController.class);

    @Resource
    private IPtFriendlinkService ptFriendlinkService;
    private String PREFIX = "/system/xymh/campusMien/";
    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index(Model model) {

        PtOrg org = ShiroKit.getSessionAttr(SessionKey.CURRENT_ORG);
        if ((org == null)) {
            model.addAttribute("orgId", "0");
            return PREFIX + "campusMien_org.html";
        }
        model.addAttribute("orgId", org.getId());
        model.addAttribute("code", PtFriendlink.CODE_SC_CAMPUSMEIN);
        return PREFIX + "campusMien_list.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/campusMien_add")
    public String CampusMienAdd(Model model,PtFriendlink ptFriendlink) {
        model.addAttribute("model",ptFriendlink);
        model.addAttribute("uploadUrl", ApolloConstants.readUploadUrl());
        model.addAttribute("storageUrl", ApolloConstants.readStorageUrl());
        return PREFIX + "campusMien_add.html";
    }
    /**
     * 跳转到添加
     */
    @RequestMapping("/campusMien_update/{id}")
    public String CampusMienAdd(Model model,@PathVariable  String id) {
        model.addAttribute("model",ptFriendlinkService.selectById(id));
        model.addAttribute("uploadUrl", ApolloConstants.readUploadUrl());
        model.addAttribute("storageUrl", ApolloConstants.readStorageUrl());
        return PREFIX + "campusMien_edit.html";
    }
    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(PtFriendlink ptFriendlink) {
        EntityWrapper<PtFriendlink> entityWrapper=new EntityWrapper<>();
        entityWrapper.eq("org_id",ptFriendlink.getOrgId());
        entityWrapper.eq("code",ptFriendlink.getCode());
        return ptFriendlinkService.selectList(entityWrapper);
    }

    /**
     * 保存
     */
    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public Result save(PtFriendlink model) {
        Result result=new Result();
        result.setCode(Result.SUCCESS);
        if (model.getId() == null) {
            model.setType(0);
            ptFriendlinkService.insert(model);
            result.setMsg("保存学校风采成功");
            //LoggerUtils.insertLogger("保存学校风采成功");
        } else {
            ptFriendlinkService.updateById(model);
            result.setMsg("更新学校风采成功");
            //LoggerUtils.insertLogger("更新学校风采成功");
        }
        return result;
    }

    /**
     * 删除
     *
     * @param id
     *          id
     */
    @RequestMapping("/delete/{id}")
    @ResponseBody
    public Result delete(@PathVariable("id") Integer id) {
        Result result = new Result();
        result.setCode(Result.SUCCESS);
        try {
            ptFriendlinkService.deleteById(id);
           // LoggerUtils.deleteLogger("删除学校风采成功，id : {}", id);
            result.setMsg("删除成功");
        } catch (Exception e) {
            result.setCode(Result.FAIL);
            result.setMsg("删除失败");
            logger.error("删除学校风采数据失败", e);
        }
        return result;
    }

}
