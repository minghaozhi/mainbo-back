package com.mainbo.modular.system.controller.xymh;

import cn.stylefeng.roses.core.base.controller.BaseController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mainbo.core.common.constant.ApolloConstants;
import com.mainbo.core.shiro.SessionKey;
import com.mainbo.core.shiro.ShiroKit;
import com.mainbo.core.util.Result;
import com.mainbo.modular.system.model.PtFriendlink;
import com.mainbo.modular.system.model.PtNews;
import com.mainbo.modular.system.model.PtOrg;
import com.mainbo.modular.system.service.IPtFriendlinkService;
import com.mainbo.modular.system.service.IPtNewsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * 校园专栏
 * @author moshang
 * @date 2020-03-14
 **/
@Controller
@RequestMapping("/campusColumn")
public class CampusColumnController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(CampusColumnController.class);

    @Resource
    private IPtNewsService newsService;
    private String PREFIX = "/system/xymh/campusColumn/";

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index(Model model) {

        PtOrg org = ShiroKit.getSessionAttr(SessionKey.CURRENT_ORG);
        if ((org == null)) {
            model.addAttribute("orgId", "0");
            return PREFIX + "campusColumn_org.html";
        }
        model.addAttribute("orgId", org.getId());
        return PREFIX + "campusColumn_index.html";
    }
    @RequestMapping("campusColumnList")
    public String noteList(Model model,String orgId) {
        model.addAttribute("orgId", orgId);
        model.addAttribute("code", PtNews.CODE_CAMPUS_COLUMN);
        return PREFIX + "campusColumn_list.html";
    }
    @RequestMapping("campusColumnDraft")
    public String noteDraft(Model model,String orgId) {
        model.addAttribute("orgId", orgId);
        model.addAttribute("code", PtNews.CODE_CAMPUS_COLUMN);
        return PREFIX + "campusColumn_draft.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/campusColumn_add")
    public String CampusMienAdd(Model model,PtFriendlink ptFriendlink) {
        model.addAttribute("model",ptFriendlink);
        model.addAttribute("uploadUrl", ApolloConstants.readUploadUrl());
        model.addAttribute("storageUrl", ApolloConstants.readStorageUrl());
        return PREFIX + "campusColumn_add.html";
    }
    /**
     * 跳转到添加
     */
    @RequestMapping("/campusColumn_update/{id}")
    public String CampusMienAdd(Model model,@PathVariable String id) {
        model.addAttribute("model",newsService.selectById(id));
        model.addAttribute("uploadUrl", ApolloConstants.readUploadUrl());
        model.addAttribute("storageUrl", ApolloConstants.readStorageUrl());
        return PREFIX + "campusColumn_edit.html";
    }
    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(PtNews news) {
        EntityWrapper<PtNews> entityWrapper=new EntityWrapper<>();
        entityWrapper.eq("org_id",news.getOrgId());
        entityWrapper.eq("code",news.getCode());
        return newsService.selectList(entityWrapper);
    }

    /**
     * 保存
     */
    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public Result save(PtNews model) {
        Result result=new Result();
        result.setCode(Result.SUCCESS);
        if (model.getId() == null) {
            newsService.insert(model);
            result.setMsg("保存学校风采成功");
            //LoggerUtils.insertLogger("保存学校风采成功");
        } else {
            newsService.updateById(model);
            result.setMsg("更新学校风采成功");
            //LoggerUtils.insertLogger("更新学校风采成功");
        }
        return result;
    }
    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{id}")
    public String detail(@PathVariable("id") String id,Model model) {
        PtNews ptNews=newsService.selectById(id);
        model.addAttribute("model", ptNews);
        model.addAttribute("uploadUrl", ApolloConstants.readUploadUrl());
        model.addAttribute("storageUrl", ApolloConstants.readStorageUrl());
        return PREFIX+"campusColumn_info.html";
    }
    /**
     * 删除
     *
     * @param id
     *          id
     */
    @RequestMapping("/delete/{id}")
    @ResponseBody
    public Result delete(@PathVariable("id") Integer id) {
        Result result = new Result();
        result.setCode(Result.SUCCESS);
        try {
            newsService.deleteById(id);
            // LoggerUtils.deleteLogger("删除学校风采成功，id : {}", id);
            result.setMsg("删除成功");
        } catch (Exception e) {
            result.setCode(Result.FAIL);
            result.setMsg("删除失败");
            logger.error("删除学校风采数据失败", e);
        }
        return result;
    }
}
