package com.mainbo.modular.system.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.mainbo.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.mainbo.modular.system.model.PtRole;
import com.mainbo.modular.system.service.IPtRoleService;

/**
 * 控制器
 *
 * @author fengshuonan
 * @Date 2020-02-26 10:13:28
 */
@Controller
@RequestMapping("/ptRole")
public class PtRoleController extends BaseController {

    private String PREFIX = "/system/ptRole/";

    @Autowired
    private IPtRoleService ptRoleService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "ptRole.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/ptRole_add")
    public String ptRoleAdd() {
        return PREFIX + "ptRole_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/ptRole_update/{ptRoleId}")
    public String ptRoleUpdate(@PathVariable Integer ptRoleId, Model model) {
        PtRole ptRole = ptRoleService.selectById(ptRoleId);
        model.addAttribute("item",ptRole);
        LogObjectHolder.me().set(ptRole);
        return PREFIX + "ptRole_edit.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return ptRoleService.selectList(null);
    }

    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(PtRole ptRole) {
        ptRoleService.insert(ptRole);
        return SUCCESS_TIP;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer ptRoleId) {
        ptRoleService.deleteById(ptRoleId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(PtRole ptRole) {
        ptRoleService.updateById(ptRole);
        return SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{ptRoleId}")
    @ResponseBody
    public Object detail(@PathVariable("ptRoleId") Integer ptRoleId) {
        return ptRoleService.selectById(ptRoleId);
    }
}
