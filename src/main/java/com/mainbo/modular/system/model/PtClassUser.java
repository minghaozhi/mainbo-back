package com.mainbo.modular.system.model;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 班级用户表
 * </p>
 *
 * @author moshang
 * @since 2020-02-28
 */
@TableName("pt_class_user")
public class PtClassUser extends Model<PtClassUser> {

    private static final long serialVersionUID = 1L;
    public static final int TYPE_TEACHER = 0;
    public static final int TYPE_STUDENT = 1;
    @TableId(type = IdType.UUID)
    private String id;
    @TableField("org_id")
    private String orgId;
    /**
     * 班级id
     */
    @TableField("class_id")
    private String classId;
    /**
     * 科目id
     */
    @TableField("subject_id")
    private Integer subjectId;
    /**
     * 用户姓名
     */
    @TableField("user_name")
    private String userName;
    /**
     * 用户id
     */
    @TableField("user_id")
    private String userId;
    /**
     * 账号
     */
    private String account;
    /**
     * 0  普通教师，1 学生
     */
    private Integer type;
    @TableField("crt_dttm")
    private Date crtDttm;
    @TableField("crt_id")
    private String crtId;
    private Boolean enable;
    @TableField("school_year")
    private Integer schoolYear;
    @TableField(exist = false)
    private String  peopleId;
    @TableField(exist = false)
    private String  className;
    @TableField(exist = false)
    private String  grade;

    @TableField(exist = false)
    private Integer  graduationStatus;

    public Integer getGraduationStatus() {
        return graduationStatus;
    }

    public void setGraduationStatus(Integer graduationStatus) {
        this.graduationStatus = graduationStatus;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getPeopleId() {
        return peopleId;
    }

    public void setPeopleId(String peopleId) {
        this.peopleId = peopleId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public Integer getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getCrtDttm() {
        return crtDttm;
    }

    public void setCrtDttm(Date crtDttm) {
        this.crtDttm = crtDttm;
    }

    public String getCrtId() {
        return crtId;
    }

    public void setCrtId(String crtId) {
        this.crtId = crtId;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Integer getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(Integer schoolYear) {
        this.schoolYear = schoolYear;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PtClassUser{" +
        ", id=" + id +
        ", orgId=" + orgId +
        ", classId=" + classId +
        ", subjectId=" + subjectId +
        ", userName=" + userName +
        ", userId=" + userId +
        ", account=" + account +
        ", type=" + type +
        ", crtDttm=" + crtDttm +
        ", crtId=" + crtId +
        ", enable=" + enable +
        ", schoolYear=" + schoolYear +
        "}";
    }
}
