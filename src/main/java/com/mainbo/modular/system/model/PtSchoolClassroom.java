package com.mainbo.modular.system.model;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author moshang
 * @since 2020-03-05
 */
@TableName("pt_school_classroom")
public class PtSchoolClassroom extends Model<PtSchoolClassroom> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = IdType.UUID)
    private String id;
    /**
     * 教学楼名称
     */
    private String name;
    /**
     * 容纳人数
     */
    private Integer galleryful;
    /**
     * 机构id
     */
    @TableField("org_id")
    private String orgId;
    /**
     * 教学楼ID
     */
    @TableField("building_id")
    private String buildingId;
    @TableField("building_name")
    private String buildingName;
    /**
     * 楼层
     */
    private Integer floor;
    /**
     * 编号
     */
    @TableField("serial_no")
    private String serialNo;
    /**
     * 教室位置
     */
    @TableField("classroom_position")
    private String classroomPosition;
    /**
     * 教室类型
     */
    @TableField("classroom_type_id")
    private Integer classroomTypeId;
    /**
     * 备注
     */
    private String note;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 修改时间
     */
    @TableField("modify_time")
    private Date modifyTime;
    /**
     * 是否被占用，1已被占用 0未被占用
     */
    @TableField("is_used")
    private Integer isUsed;
    /**
     * 是否可用 1 可用， 0禁用
     */
    private Boolean enable;
    /**
     * 删除标识 0未删除 ，1已删除
     */
    private Integer deleted;
    @TableField("lastup_id")
    private String lastupId;
    @TableField("lastup_dttm")
    private Date lastupDttm;

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGalleryful() {
        return galleryful;
    }

    public void setGalleryful(Integer galleryful) {
        this.galleryful = galleryful;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(String buildingId) {
        this.buildingId = buildingId;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getClassroomPosition() {
        return classroomPosition;
    }

    public void setClassroomPosition(String classroomPosition) {
        this.classroomPosition = classroomPosition;
    }

    public Integer getClassroomTypeId() {
        return classroomTypeId;
    }

    public void setClassroomTypeId(Integer classroomTypeId) {
        this.classroomTypeId = classroomTypeId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Integer getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(Integer isUsed) {
        this.isUsed = isUsed;
    }


    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public String getLastupId() {
        return lastupId;
    }

    public void setLastupId(String lastupId) {
        this.lastupId = lastupId;
    }

    public Date getLastupDttm() {
        return lastupDttm;
    }

    public void setLastupDttm(Date lastupDttm) {
        this.lastupDttm = lastupDttm;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PtSchoolClassroom{" +
        ", id=" + id +
        ", name=" + name +
        ", galleryful=" + galleryful +
        ", orgId=" + orgId +
        ", buildingId=" + buildingId +
        ", buildingName=" + buildingName +
        ", floor=" + floor +
        ", serialNo=" + serialNo +
        ", classroomPosition=" + classroomPosition +
        ", classroomTypeId=" + classroomTypeId +
        ", note=" + note +
        ", createTime=" + createTime +
        ", modifyTime=" + modifyTime +
        ", isUsed=" + isUsed +
        ", enable=" + enable +
        ", deleted=" + deleted +
        ", lastupId=" + lastupId +
        ", lastupDttm=" + lastupDttm +
        "}";
    }
}
