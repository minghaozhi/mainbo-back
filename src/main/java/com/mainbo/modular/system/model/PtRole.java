package com.mainbo.modular.system.model;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author moshang
 * @since 2020-02-26
 */
@TableName("pt_role")
@Data
public class PtRole extends Model<PtRole> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    @TableField("role_name")
    private String roleName;
    /**
     * 权限代码
     */
    @TableField("role_code")
    private String roleCode;
    /**
     * 组织机构
     */
    @TableField("org_id")
    private String orgId;
    @TableField("role_desc")
    private String roleDesc;
    /**
     * 状态 0正常，1删除
     */
    @TableField("is_del")
    private Integer isDel;
    /**
     * 关联id 系统--方案
     */
    @TableField("rel_id")
    private Integer relId;
    /**
     * 应用方向-角色类型--冗余
     */
    private Integer scope;
    private Integer sort;
    /**
     * 是否显示学段/单位 0-不显示 1-显示
     */
    @TableField("show_phase")
    private Boolean showPhase;
    /**
     * 是否显示年级 0-不显示 1-显示
     */
    @TableField("show_grade")
    private Boolean showGrade;
    /**
     * 是否显示学科/学校 0-不显示 1-显示
     */
    @TableField("show_subject")
    private Boolean showSubject;

    @Override
    protected Serializable pkVal() {
       return this.id;
    }
}
