package com.mainbo.modular.system.model.meta.vo;

import java.util.Date;

/**
 * @author moshang
 * @date 2020-02-25
 **/
public interface Meta {


    /**
     * id
     *
     * @return Integer
     */
    Integer getId();

    /**
     * 配置名称
     *
     * @return String
     */
    String getName();

    /**
     * 配置值
     *
     * @return String
     */
    String getValue();

    /**
     * 是否可以修改
     *
     * @return Boolean
     */
    Boolean getCanOverride();

    Date getCrtDttm();
    Date getLastupDttm();
    String getCrtId();
    String getOrgId();
}
