package com.mainbo.modular.system.model.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/3/26   14:51
 **/
@Data
public class SubjectTeacherVo {
    private Integer subjectId;
    private String subjectName;
    private List<UserVo> teacherList;
}
