package com.mainbo.modular.system.model.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/3/6   11:05
 **/
@Setter
@Getter
public class UserVo {
    private String userId;
    private String userName;
}
