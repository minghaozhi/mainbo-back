package com.mainbo.modular.system.model;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author moshang
 * @since 2020-03-11
 */
@TableName("pt_news")
public class PtNews extends Model<PtNews> {

    private static final long serialVersionUID = 1L;
    public static final String TYPE_JYXW = "1";// 教育新闻
    public static final String TYPE_CGZS = "2";// 成果展示
    //系统新闻
    public static final String CODE_SYS_NEWS = "sys_news";
    //学校新闻
    public static final String CODE_SC_NEWS = "sc_news";
    //系统成果
    public static final String CODE_SYS_ARCHIEVE = "sys_archieve";
    //校园成果
    public static final String CODE_SC_ARCHIEVE = "archieve";
    //校园专栏
    public static final String CODE_CAMPUS_COLUMN = "campus_column";
    /**
     * 主键id
     */
    @TableId(type = IdType.UUID)
    private String id;
    /**
     * 机构id
     */
    @TableField("org_id")
    private String orgId;
    /**
     * 展示图
     */
    private String img;
    /**
     * 新闻标题
     */
    private String title;
    /**
     * 内容摘要
     */
    private String brief;
    /**
     * 新闻内容
     */
    private String content;
    private String attachs;
    /**
     * 是否置顶 0：非置顶 1：置顶
     */
    @TableField("is_top")
    private Integer isTop;
    /**
     * 是否草稿 0：否 1：是
     */
    @TableField("is_draft")
    private Integer isDraft;
    /**
     * 是否推荐（首页展示） 0：不推荐  1：推荐
     */
    @TableField("is_comm")
    private Integer isComm;
    /**
     * 创建人id
     */
    @TableField("crt_id")
    private String crtId;
    /**
     * 作者
     */
    private String author;
    /**
     * 创建时间
     */
    @TableField("crt_dttm")
    private Date crtDttm;
    /**
     * 最后修改人id
     */
    @TableField("lastup_id")
    private String lastupId;
    /**
     * 最后修改时间
     */
    @TableField("lastup_dttm")
    private Date lastupDttm;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 新闻分类
     */
    private String category;
    /**
     * code--news新闻，achieve成果
     */
    private String code;
    @TableField(exist = false)
    private String typeName;

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAttachs() {
        return attachs;
    }

    public void setAttachs(String attachs) {
        this.attachs = attachs;
    }

    public Integer getIsTop() {
        return isTop;
    }

    public void setIsTop(Integer isTop) {
        this.isTop = isTop;
    }

    public Integer getIsDraft() {
        return isDraft;
    }

    public void setIsDraft(Integer isDraft) {
        this.isDraft = isDraft;
    }

    public Integer getIsComm() {
        return isComm;
    }

    public void setIsComm(Integer isComm) {
        this.isComm = isComm;
    }

    public String getCrtId() {
        return crtId;
    }

    public void setCrtId(String crtId) {
        this.crtId = crtId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getCrtDttm() {
        return crtDttm;
    }

    public void setCrtDttm(Date crtDttm) {
        this.crtDttm = crtDttm;
    }

    public String getLastupId() {
        return lastupId;
    }

    public void setLastupId(String lastupId) {
        this.lastupId = lastupId;
    }

    public Date getLastupDttm() {
        return lastupDttm;
    }

    public void setLastupDttm(Date lastupDttm) {
        this.lastupDttm = lastupDttm;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PtNews_list{" +
        ", id=" + id +
        ", orgId=" + orgId +
        ", img=" + img +
        ", title=" + title +
        ", brief=" + brief +
        ", content=" + content +
        ", attachs=" + attachs +
        ", isTop=" + isTop +
        ", isDraft=" + isDraft +
        ", isComm=" + isComm +
        ", crtId=" + crtId +
        ", author=" + author +
        ", crtDttm=" + crtDttm +
        ", lastupId=" + lastupId +
        ", lastupDttm=" + lastupDttm +
        ", sort=" + sort +
        ", category=" + category +
        ", code=" + code +
        "}";
    }
}
