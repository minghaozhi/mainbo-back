package com.mainbo.modular.system.model.meta;

/**
 * @author moshang
 * @date 2020-02-25
 **/
public abstract class MetaKeys {
    /**
     * 用户批量导入模板内容所在sheet,默认0 配置code @see MetaCodeTree.PLATFORM_EXT_CONFIG_CODE
     */
    public static final String BACK_USER_BATCH_SHEET_INDEX = "back_userbatch_sheet_index";

    /**
     * 用户批量导入模板标题所在行,默认3
     */
    public static final String BACK_USER_BATCH_TITLE_LINE = "back_userbatch_title_line";
    /**
     * 用户批量导入模板内容所在sheet,默认0
     */
    public static final String BACK_SCH_BATCH_SHEET_INDEX = "back_schbatch_sheet_index";

    /**
     * 用户批量导入模板标题所在行,默认3
     */
    public static final String BACK_SCH_BATCH_TITLE_LINE = "back_schbatch_title_line";

    /**
     * 前台url
     */
    public static final String BACK_MONITOR_FRONT_URL = "front_web_url";

    /**
     * 门户logo
     */
    public static final String PORTAL_INFO_LOGO = "portal_info_logo";
    /**
     * 网站名称
     */
    public static final String PORTAL_INFO_NAME = "portal_info_name";
    /**
     * 底部信息
     */
    public static final String PORTAL_INFO_BOTTOM = "portal_info_bottom";
    /**
     * 联系方式
     */
    public static final String PORTAL_INFO_CONTACT = "portal_info_contact";

    /**
     * 首页对接接口配置前缀
     */
    public static final String HOME_API_PRE_KEY = "api_pre";

    public static final String UCENTER_FRONT_PATH = "ucenter.front.path";

    /**
     * 是否是静态配置，默认为true
     */
    public static final String UCENTER_FRONT_USEFILE = "ucenter.front.usefile";

    public static final String UCENTER_FRONT_CONFIG_CHARSET = "ucenter.front.config.charset";

    // ---- open api 配置

    /**
     * api 分页最大条数,默认100
     */
    public static final String API_MAX_PAGESIZE = "api.maxPageSize";

    /**
     * 平台消息初始化结束开关
     */
    public static final String SYS_INIT_OVER = "sys_init_over";

    /**
     * 平台id 标识，字母开头，不多于三位，用户自动生成用户名
     */
    public static final String PLATFORM_ID_KEY = "platform_id";

    public static final String ADMIN_USER_ID_KEY = "admin_user_id";

    /**
     * 优课消息配置
     */

    public static final String UCLASS_JMS_ENABLE_KEY = "uclass_jms_enable";

    public static final String UCLASS_JMS_APPID_KEY = "uclass_jms_appid";

    /**
     * 短信发送开关
     */
    public static final String SMS_SWITCH_KEY = "sms_switch";

    /**
     * 批量使用手机登录
     */
    public static final String PHONE_LOGIN_SWITCH_KEY = "phone_login_switch";

    /**
     * 批量时强制使用自动生成用户名
     */
    public static final String FORCE_LOGINNAME_KEY = "force_loginname";

    /**
     * 平台后台定制配置
     */
    /**
     * 后台logo
     */
    public static final String PF_BACK_LOGO_KEY = "platform.back.logo";
    /**
     * 后台标题
     */
    public static final String PF_BACK_TITLE_KEY = "platform.back.title";
    /**
     * 后台底部
     */
    public static final String PF_BACK_BOTTOM_KEY = "platform.back.bottom";

    /**
     * 后台用户初始密码
     */
    public static final String PF_BACK_UC_PASSWORD_KEY = "platform.back.uc.psd";

    /**
     * 后台学校管理员职务ID
     */
    public static final String PF_BACK_ROLE_SCHOOLMANAGER_ID_KEY = "platform.back.role.schoolManager";

    // 单点配置
    /**
     * 单点密匙
     */
    public static final String UCENTER_JWT_SECRET_KEY = "ucenter.jwt.secret";

    /**
     * jwt 过期时间
     */
    public static final String UCENTER_JWT_EXPIRATION_KEY = "ucenter.jwt.expiration";

    /**
     * jwt 签发者
     */
    public static final String UCENTER_JWT_ISS_KEY = "ucenter.jwt.iss";
    /**
     * jwt 压缩方式，支持flat,gzip
     */
    public static final String UCENTER_JWT_COMPRESS_KEY = "ucenter.jwt.compress";

    /**
     * 平台首页配置
     */
    public static final String UCENTER_INDEX_KEY = "ucenter.index";
    /**
     * 平台登录页配置
     */
    public static final String UCENTER_LOGIN_KEY = "ucenter.login";
    /**
     * 是否是云校园模式
     */
    public static final String IS_YUNXIAOYUAN = "is_yunxiaoyuanModel";
    /**
     * 模板1地址
     */
    public static final String SCHMODEL1 = "schModel1";
    /**
     * 模板2地址
     */
    public static final String SCHMODEL2 = "schModel2";
    /**
     * 模板3地址
     */
    public static final String SCHMODEL3= "schModel3";
    /**
     * 模板4地址
     */
    public static final String SCHMODEL4= "schModel4";
    /**
     * 系统邮箱配置
     */

    public static final String PF_MAIL_PROTOCOL_KEY = "pf.mail.protocol";

    public static final String PF_MAIL_PORT_KEY = "pf.mail.port";

    public static final String PF_MAIL_USERNAME_KEY = "pf.mail.username";

    public static final String PF_MAIL_HOST_KEY = "pf.mail.host";

    public static final String PF_MAIL_PASSWORD_KEY = "pf.mail.password";
    /**
     * 邮箱发送者名称
     */
    public static final String PF_MAIL_SENDER_KEY = "pf.mail.sender";
    /**
     * 首页跳转地址
     */
    public static final String PF_MAIL_TOINDEX = "pf.mail.toIndex";
    /**
     * 邮箱属性
     */
    public static final String PF_MAIL_PROP_AUTH_KEY = "mail.smtp.auth";
    public static final String PF_MAIL_PROP_STARTTTLS_ENABLE_KEY = "mail.smtp.starttls.enable";
    public static final String PF_MAIL_PROP_DEBUG_KEY = "mail.debug";
    public static final String PF_MAIL_PROP_SF_CLASS_KEY = "mail.smtp.socketFactory.class";
    public static final String PF_MAIL_RECEIVE = "pf.mail.receive";
    /**
     * 邮箱验证邮件内容配置：
     */
    public static final String PF_MAIL_VALIDATE_CONTENT_KEY = "mail.validate.content";

    /**
     * 邮箱验证时间限制
     */
    public static final String PF_MAIL_VALIDATE_TIMELIMIT_KEY = "mail.validate.timelimit";
    /**
     * rmi 配置
     */
    public static final String UCENTER_RMI_HOST_KEY = "ucenter.rmi.host";

    /**
     * 学年
     */
    public static final String SCHOOL_YEAR_SPLIT = "school.year.split";

    /**
     * 学期
     */
    public static final String TERM_SPLIT = "term.split";

    /**
     * 文件异步开关名称
     */
    public static final String RESSYNC_SWITCH = "resSync.switch";

    /**
     * 开关开启
     */
    public static final String ON = "on";
    /**
     * 开关关闭
     */
    public static final String OFF = "off";

    public static final String APPKEY = "appKey";

    public static final String APPSECRET = "appSecret";
}
