package com.mainbo.modular.system.model;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author moshang
 * @since 2020-03-04
 */
@TableName("pt_org_relationship")
public class PtOrgRelationship extends Model<PtOrgRelationship> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    @TableField("org_id")
    private String orgId;
    @TableField("phase_id")
    private Integer phaseId;
    private Integer schooling;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Integer getPhaseId() {
        return phaseId;
    }

    public void setPhaseId(Integer phaseId) {
        this.phaseId = phaseId;
    }

    public Integer getSchooling() {
        return schooling;
    }

    public void setSchooling(Integer schooling) {
        this.schooling = schooling;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PtOrgRelationship{" +
        ", id=" + id +
        ", orgId=" + orgId +
        ", phaseId=" + phaseId +
        ", schooling=" + schooling +
        "}";
    }
}
