package com.mainbo.modular.system.model.meta;

/**
 * <pre>
 * 国标枚举类型
 * </pre>
 *
 */
public enum GBPapersType {

  IDCARD("身份证", 11), DRIVERCARD("驾驶证", 15), OFFICERCARD("军官证", 90), POLICECARD(
      "警官证", 91), SOLDIERCARD("士兵证", 92), BOOKLET("户口簿", 93), PASSPORT("护照", 94), OTHER(
      "其他证件", 99);
  /**
   * 中文名称
   */
  private String cname;

  private Integer code;

  GBPapersType(String cname, Integer code) {
    this.cname = cname;
    this.code = code;
  }

  /**
   * 中文名称转代码
   * 
   * @param cname
   *          中文名称
   * @return Integer
   */
  public static Integer forCode(String cname) {
    for (GBPapersType paper : GBPapersType.values()) {
      if (paper.cname.equals(cname)) {
        return paper.getCode();
      }
    }
    return null;
  }

  /**
   * 代码转中文名称
   * 
   * @param code
   *          代码
   * @return String
   */
  public static String forCname(Integer code) {
    for (GBPapersType paper : GBPapersType.values()) {
      if (paper.code.equals(code)) {
        return paper.getCname();
      }
    }

    return null;
  }

  /**
   * Getter method for property <tt>cname</tt>.
   * 
   * @return property value of cname
   */
  public String getCname() {
    return cname;
  }

  /**
   * Getter method for property <tt>code</tt>.
   * 
   * @return property value of code
   */
  public Integer getCode() {
    return code;
  }

}
