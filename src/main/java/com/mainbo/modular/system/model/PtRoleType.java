package com.mainbo.modular.system.model;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author moshang
 * @since 2020-02-23
 */
@TableName("pt_role_type")
public class PtRoleType extends Model<PtRoleType> {

    private static final long serialVersionUID = 1L;
    public static final Integer APPLICATION_AREA = 1;
    public static final Integer APPLICATION_SCHOOL = 2;
    public static final Integer APPLICATION_SYSTEM = 3;

    public static final Integer SHOW = 1;// 显示
    public static final Integer NOT_SHOW = 0;// 不显示
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    @TableField("role_type_name")
    private String roleTypeName;
    @TableField("role_type_desc")
    private String roleTypeDesc;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 菜单应用范围。1区域、2学校、3系统
     */
    private Integer scope;
    /**
     * dic_id
     */
    private String code;
    @TableField("home_url")
    private String homeUrl;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleTypeName() {
        return roleTypeName;
    }

    public void setRoleTypeName(String roleTypeName) {
        this.roleTypeName = roleTypeName;
    }

    public String getRoleTypeDesc() {
        return roleTypeDesc;
    }

    public void setRoleTypeDesc(String roleTypeDesc) {
        this.roleTypeDesc = roleTypeDesc;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getScope() {
        return scope;
    }

    public void setScope(Integer scope) {
        this.scope = scope;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHomeUrl() {
        return homeUrl;
    }

    public void setHomeUrl(String homeUrl) {
        this.homeUrl = homeUrl;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PtRoleType{" +
        ", id=" + id +
        ", roleTypeName=" + roleTypeName +
        ", roleTypeDesc=" + roleTypeDesc +
        ", sort=" + sort +
        ", scope=" + scope +
        ", code=" + code +
        ", homeUrl=" + homeUrl +
        "}";
    }
}
