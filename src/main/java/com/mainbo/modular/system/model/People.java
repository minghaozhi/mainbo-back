/**
 * Mainbo.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */

package com.mainbo.modular.system.model;


import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;

/**
 * 通用用户 Entity
 * 
 * <pre>
 *
 * </pre>
 *
 * @author Generate Tools
 * @version $Id: People.java, v 1.0 2016-08-17 Generate Tools Exp $
 */
@SuppressWarnings("serial")
@TableName(value = People.TABLE_NAME)
@Getter
@Setter
public class People extends CommUser {
  public static final String TABLE_NAME = "pt_people";

  @TableField("regis_filed")
  private String regisFiled;

  /**
   * 教师状态(1:禁用，0：启用)
   **/
  /*
   * @TableField(name="teacher_status") private Integer teacherStatus;
   */

  /**
   * 用户类型(1:系统，5：家长)
   **/
  @TableField("user_type")
  private Integer userType;
}
