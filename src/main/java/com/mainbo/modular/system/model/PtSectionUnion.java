package com.mainbo.modular.system.model;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author moshang
 * @since 2020-03-10
 */
@TableName("pt_section_union")
public class PtSectionUnion extends Model<PtSectionUnion> {

    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.UUID)
    private String id;
    /**
     * 节次方案名称
     */
    private String name;
    /**
     * 节次方案id
     */
    @TableField("category_id")
    private String categoryId;
    @TableField("org_id")
    private String orgId;
    @TableField("org_name")
    private String orgName;
    /**
     * 所属学期id
     */
    @TableField("term_id")
    private String termId;
    /**
     * 所属学期
     */
    @TableField("term_name")
    private String termName;
    /**
     * 应用年级id
     */
    @TableField("grade_id")
    private String gradeId;
    /**
     * 应用年级
     */
    @TableField("grade_name")
    private String gradeName;
    /**
     * 应用开始时间
     */
    @DateTimeFormat(pattern = "yyyy.MM.dd")
    @TableField("start_time")
    private Date startTime;
    /**
     * 应用结束时间
     */
    @DateTimeFormat(pattern = "yyyy.MM.dd")
    @TableField("end_time")
    private Date endTime;
    /**
     * 每周应用范围
     */
    private String week;
    /**
     * 节次名称
     */
    @TableField("st_name")
    private String stName;
    /**
     * 第几节
     */
    @TableField("sc_num")
    private Integer scNum;
    /**
     * 开始时间
     */
    @TableField("st_start_time")
    @DateTimeFormat(pattern ="HH:mm" )
    private String stStartTime;
    /**
     * 结束时间
     */
    @DateTimeFormat(pattern ="HH:mm" )
    @TableField("st_end_time")
    private String stEndTime;
    /**
     * 创建者id
     */
    @TableField("creator_id")
    private String creatorId;
    /**
     * 创建者
     */
    private String creator;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 可用标识：0 不可用，1 可用
     */
    private Integer enable;
    /**
     * 删除标识 1删除 0未删除
     */
    private Integer deleted;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getTermId() {
        return termId;
    }

    public void setTermId(String termId) {
        this.termId = termId;
    }

    public String getTermName() {
        return termName;
    }

    public void setTermName(String termName) {
        this.termName = termName;
    }

    public String getGradeId() {
        return gradeId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public String getStName() {
        return stName;
    }

    public void setStName(String stName) {
        this.stName = stName;
    }

    public Integer getScNum() {
        return scNum;
    }

    public void setScNum(Integer scNum) {
        this.scNum = scNum;
    }

    public String getStStartTime() {
        return stStartTime;
    }

    public void setStStartTime(String stStartTime) {
        this.stStartTime = stStartTime;
    }

    public String getStEndTime() {
        return stEndTime;
    }

    public void setStEndTime(String stEndTime) {
        this.stEndTime = stEndTime;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PtSectionUnion{" +
        ", id=" + id +
        ", name=" + name +
        ", categoryId=" + categoryId +
        ", orgId=" + orgId +
        ", orgName=" + orgName +
        ", termId=" + termId +
        ", termName=" + termName +
        ", gradeId=" + gradeId +
        ", gradeName=" + gradeName +
        ", startTime=" + startTime +
        ", endTime=" + endTime +
        ", week=" + week +
        ", stName=" + stName +
        ", scNum=" + scNum +
        ", stStartTime=" + stStartTime +
        ", stEndTime=" + stEndTime +
        ", creatorId=" + creatorId +
        ", creator=" + creator +
        ", createTime=" + createTime +
        ", enable=" + enable +
        ", deleted=" + deleted +
        "}";
    }
}
