package com.mainbo.modular.system.model.meta;

/**
 * <pre>
 * 国标枚举类型
 * </pre>
 */
public enum GBPolitical {

  GCDDY("中共党员", 1), GCDYBDY("中共预备党员", 2), GCZYQNTTY("共青团员", 3), GMDGMWYH(
      "民革党员", 4), MZTMMY("民盟盟员", 5), MZJGHHY("民建会员", 6), MZCJHHY("民进会员", 7), NGMZDDY(
      "农工党党员", 8), ZGDDY("致公党党员", 9), JSXSSY("九三学社社员", 10), TWMZZZTMMY("台盟盟员",
      11), WDPMZRS("无党派人士", 12), PEOPLE("群众", 13);
  /**
   * 中文名称
   */
  private String cname;

  private Integer code;

  GBPolitical(String cname, Integer code) {
    this.cname = cname;
    this.code = code;
  }

  /**
   * 中文名称转代码
   * 
   * @param cname
   *          中文名称
   * @return Integer
   */
  public static Integer forCode(String cname) {
    for (GBPolitical paper : GBPolitical.values()) {
      if (paper.cname.equals(cname)) {
        return paper.getCode();
      }
    }
    return null;
  }

  /**
   * 代码转中文名称
   * 
   * @param code
   *          代码
   * @return String
   */
  public static String forCname(Integer code) {
    for (GBPolitical paper : GBPolitical.values()) {
      if (paper.code.equals(code)) {
        return paper.getCname();
      }
    }

    return null;
  }

  /**
   * Getter method for property <tt>cname</tt>.
   * 
   * @return property value of cname
   */
  public String getCname() {
    return cname;
  }

  /**
   * Getter method for property <tt>code</tt>.
   * 
   * @return property value of code
   */
  public Integer getCode() {
    return code;
  }

}
