package com.mainbo.modular.system.model.meta;

/**
 * @author moshang
 * @date 2020-02-25
 **/
public class ConfigConstants {
    public static final String SYS_PHASE_CODE = "phase";
    public static final String SYS_SUBJECT_CODE = "subject";
    public static final String SYS_FASCICULE_CODE = "fascicule";
    public static final String PHASE_GRADE_CODE = "phase_grade";
    public static final String PHASE_SUBJECT_CODE = "phase_subject";
    public static final String ORG_TYPE_CODE = "org_type";
    public static final String SYS_PUBLISHER_CODE = "publisher";
    public static final String PHASE_SUBJECT_PUBLISHER_CODE = "phase_subject_publisher";
    public static final String SYS_BOOKEDITION_CODE = "book_edition";
    public static final String SYS_EDITION_CODE = "edition";
    public static final String XZ_GRADE_CODE = "xz_grade";
    public static final String SCHOOLYEAR_CODE = "schoolyear";
    public static final String TERM_CODE = "term";
    public static final String RESOURCES_TYPE_CODE = "resource_type";
    public static final String SYS_TZGG_TYPE_CODE = "tzgg_type";
    public static final String SCHOOL_MODEL_CODE = "school_model";
    public static final String SYS_TYPE = "sys";
    public static final String ORG_TYPE = "org";
    public static final String AREA_TYPE = "area";
    public static final String DOMIN_TYPE = "domain";
    public static final String CLASS_TYPE="class_type";
    public static final String CLASSROOM_TYPE="classroom_type";
    public static final String WEEK="week";
    /**
     * 年级
     */
    public static final String SYS_GRADE_CODE = "grade";

    /**
     * 应用类型
     */
    public static final String SYS_APPTYPE_CODE = "app_type";
     /**
      * * 单位类型
   */
    public static final String SYS_UNIT_CODE = "unit_type";
    /**
     * 平台扩展配置项
     */
    public static final String PLATFORM_EXT_CONFIG_CODE = "platform_ext_config";

    /**
     * 贫困生类型
     */
    public static final String SYS_POOR_STUDENT_CODE = "poor_student_type";
    /**
     * 文理科code
     */
    public static final String SYS_DEP_SYSTEM_CODE = "department_system";
    /**
     * 应用对接类型
     */
    public static final String SYS_ACCESS_TYPE_CODE = "access_type";
    /**
     * 应用对接类型
     */
    public static final String APP_EXT_FIELD_TYPE_CODE = "app_field_type";
    /**
     * 首页对接接口配置
     */
    public static final String HOME_INTERFACE_CODE = "home_interface";
    /**
     * 平台邮箱配置项
     */
    public static final String PLATFORM_MAIL_CONFIG_CODE = "platform_mail_config";
    public static final String SMS_TEMPLATE_CODE = "sms_template";
    /**
     * 系统初始化
     */
    public static final String SYS_INITED_CODE = "sys_inited";
    public static enum MetaCodeTree {
        SYS_SUBJECT_CODE(ConfigConstants.SYS_SUBJECT_CODE, "系统学科", false, true),
        SYS_GRADE_CODE(ConfigConstants.SYS_GRADE_CODE, "系统年级", false, true),
        SYS_APPTYPE_CODE(ConfigConstants.SYS_APPTYPE_CODE, "应用类型",false),
        SYS_FASCICULE_CODE(ConfigConstants.SYS_FASCICULE_CODE, "册别", false, true),
        SYS_EDITION_CODE(ConfigConstants.SYS_EDITION_CODE, "版次", false,true),
        SYS_PUBLISHER_CODE(ConfigConstants.SYS_PUBLISHER_CODE, "出版社", false, true),
        SYS_DEP_SYSTEM_CODE(ConfigConstants.SYS_DEP_SYSTEM_CODE, "文理科", false,true),
        SYS_ACCESS_TYPE_CODE(ConfigConstants.SYS_ACCESS_TYPE_CODE, "应用对接类型", false),
        SYS_UNIT_CODE(ConfigConstants.SYS_UNIT_CODE,"机构类型"),
        SYS_POOR_STUDENT_CODE(ConfigConstants.SYS_POOR_STUDENT_CODE, "贫困生类型",true),
        APP_EXT_FIELD_TYPE_CODE(ConfigConstants.APP_EXT_FIELD_TYPE_CODE,"应用扩展字段类型"),
        PLATFORM_EXT_CONFIG_CODE(ConfigConstants.PLATFORM_EXT_CONFIG_CODE,"平台扩展配置", true),
        HOME_INTERFACE_CODE(ConfigConstants.HOME_INTERFACE_CODE, "首页接口配置",true),
        PLATFORM_MAIL_CONFIG_CODE(ConfigConstants.PLATFORM_MAIL_CONFIG_CODE,"平台邮箱配置", true),
        SMS_TEMPLATE_CODE(ConfigConstants.SMS_TEMPLATE_CODE,"短信模板管理", true),
        TERM_CODE(ConfigConstants.TERM_CODE, "学期",false),
        RESOURCES_TYPE_CODE(ConfigConstants.RESOURCES_TYPE_CODE,"资源类型", false, false,true),
        SYS_INITED_CODE(ConfigConstants.SYS_INITED_CODE, "数据启用",true),
        SYS_TZGG_TYPE_CODE(ConfigConstants.SYS_TZGG_TYPE_CODE, "公告类型", false);

        private String cname;
        private String code;
        private boolean isDic = false;
        private boolean hasChild = false;
        private boolean needValue = false;

        private MetaCodeTree(String code, String cname) {
            this(code, cname, false);
        }

        private MetaCodeTree(String code, String cname, boolean needValue) {
            this(code, cname, needValue, false);
        }

        private MetaCodeTree(String code, String cname, boolean needValue, boolean isDic) {
            this(code, cname, needValue, isDic, false);
        }

        private MetaCodeTree(String code, String cname, boolean needValue, boolean isDic, boolean hasChild) {
            this.cname = cname;
            this.code = code;
            this.needValue = needValue;
            this.isDic = isDic;
            this.hasChild = hasChild;
        }

        /**
         * 获取树机构
         *
         * @param code
         *          类型码
         * @return MetaCodeTree
         */
        public static MetaCodeTree getTree(String code) {
            MetaCodeTree[] values = MetaCodeTree.values();
            for (int i = 0; i < values.length; i++) {
                if (values[i].getCode().equals(code)) {
                    return values[i];
                }
            }
            return null;
        }

        /**
         * Getter method for property <tt>cname</tt>.
         *
         * @return property value of cname
         */
        public String getCname() {
            return cname;
        }

        /**
         * Getter method for property <tt>code</tt>.
         *
         * @return property value of code
         */
        public String getCode() {
            return code;
        }

        /**
         * Getter method for property <tt>needValue</tt>.
         *
         * @return property value of needValue
         */
        public boolean isNeedValue() {
            return needValue;
        }

        /**
         * Getter method for property <tt>isDic</tt>.
         *
         * @return isDic boolean
         */
        public boolean isDic() {
            return isDic;
        }

        /**
         * Getter method for property <tt>hasChild</tt>.
         *
         * @return hasChild boolean
         */
        public boolean isHasChild() {
            return hasChild;
        }

    }
    }
