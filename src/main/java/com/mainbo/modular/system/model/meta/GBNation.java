package com.mainbo.modular.system.model.meta;

/**
 * <pre>
 * 国标枚举类型
 * </pre>
 *
 */
public enum GBNation {

  HAN("汉族", "HA"), MENGGU("蒙古族", "MG"), HUI("回族", "HU"), ZANG("藏族", "ZA"), WEIWUER(
      "维吾尔族", "UG"), MIAO("苗族", "MH"), YI("彝族", "YI"), ZHUANG("壮族", "ZH"), BUYI(
      "布依族", "BY"), CHAOXIAN("朝鲜族", "CS"), MAN("满族", "MA"), DONG("侗族", "DO"), YAO(
      "瑶族", "YA"), BAI("白族", "BA"), TUJIA("土家族", "TJ"), HANI("哈尼族", "HN"), HASAKE(
      "哈萨克族", "KZ"), DAI("傣族", "DA"), LI("黎族", "LI"), LISU("傈僳族", "LS"), WA(
      "佤族", "VA"), SHE("畲族", "SH"), GAOSHAN("高山族", "GS"), LAHU("拉祜族", "LH"), SHUI(
      "水族", "SU"), DONGXIANG("东乡族", "DX"), NAXI("纳西族", "NX"), JINGPO("景颇族",
      "JP"), KERKEZI("柯尔克孜族", "KG"), TU("土族", "TU"), DAWOER("达斡尔族", "DU"), MULAO(
      "仫佬族", "ML"), QIANG("羌族", "QI"), BLANG("布朗族", "BL"), SALA("撒拉族", "SL"), MAONAN(
      "毛南族", "MN"), GELAO("仡佬族", "GL"), XIBE("锡伯族", "XB"), ACHANG("阿昌族", "AC"), PUMI(
      "普米族", "PM"), TAJIKE("塔吉克族", "TA"), NU("怒族", "NU"), WUZIBIEKE("乌孜别克族",
      "UZ"), RUSS("俄罗斯族", "RS"), EWENKE("鄂温克族", "EW"), DEANG("德昂族", "DE"), BAOAN(
      "保安族", "BN"), YUGU("裕固族", "YG"), JING("汉京族", "GI"), TATAER("塔塔尔族", "TT"), DULONG(
      "独龙族", "DR"), ELUNCHUN("鄂伦春族", "OR"), HEZHE("赫哲族", "HZ"), MENBA("门巴族",
      "MB"), LUOBA("珞巴族", "LB"), JINUO("基诺族", "JN");
  /**
   * 中文名称
   */
  private String cname;

  private String code;

  GBNation(String cname, String code) {
    this.cname = cname;
    this.code = code;
  }

  /**
   * 中文名称转代码
   * 
   * @param cname
   *          中文名称
   * @return String
   */
  public static String forCode(String cname) {
    for (GBNation nation : GBNation.values()) {
      if (nation.cname.equals(cname)) {
        return nation.getCode();
      }
    }
    return null;
  }

  /**
   * 代码转中文名称
   * 
   * @param code
   *          代码
   * @return String
   */
  public static String forCname(String code) {
    for (GBNation nation : GBNation.values()) {
      if (nation.code.equals(code)) {
        return nation.getCname();
      }
    }

    return null;
  }

  /**
   * Getter method for property <tt>cname</tt>.
   * 
   * @return property value of cname
   */
  public String getCname() {
    return cname;
  }

  /**
   * Getter method for property <tt>code</tt>.
   * 
   * @return property value of code
   */
  public String getCode() {
    return code;
  }
}
