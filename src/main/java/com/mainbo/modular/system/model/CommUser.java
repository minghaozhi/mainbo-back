package com.mainbo.modular.system.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/2/26   12:04
 **/
public class CommUser {
    /**
     * <pre>
     *
     * </pre>
     */
    private static final long serialVersionUID = -6659145155423577306L;

    /**
     * 用户id
     **/
    @TableId
    @TableField(value = "id")
    private String id;

    /**
     * 机构数据类ID
     **/
    @TableField( "org_id")
    private String orgId;

    /**
     * 机构名称
     */
    @TableField( "org_name")
    private String orgName;

    /**
     * 姓名
     **/
    @TableField("name")
    private String name;

    /**
     * 用户头像
     **/
    @TableField("photo")
    private String photo;

    /**
     * 缩略图
     **/
    @TableField("original_photo")
    private String originalPhoto;

    /**
     * 英文姓名
     **/
    @TableField("english_name")
    private String englishName;

    /**
     * 姓名拼音
     **/
    @TableField("name_pell")
    private String namePell;

    /**
     * 性别码(GB T 2261_1 人的性别代码)
     **/
    @TableField( "sex")
    private Integer sex;

    /**
     * 出生日期
     **/
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JSONField(format = "yyyy-MM-dd ")
    @TableField("birthday")
    private Date birthday;

    /**
     * 出生地码(GB/T 2260)
     **/
    @TableField("birth_place_code")
    private String birthPlaceCode;

    /**
     * 籍贯
     **/
    @TableField("native_place")
    private String nativePlace;

    /**
     * 民族码(GB T 3304)
     **/
    @TableField("nation_code")
    private String nationCode;

    /**
     * 国籍/地区码(GB T 2659)
     **/
    @TableField( "country_id")
    private Integer countryId;

    /**
     * 身份证件类型码(身份证件类型代码表 JY_SFZJLX)
     **/
    @TableField( "papers_type")
    private Integer papersType;

    /**
     * 身份证件号
     **/
    @TableField("papers_number")
    private String papersNumber;

    /**
     * 血型码(血型代码 JY_XX)
     **/
    @TableField( "blood_type")
    private String bloodType;

    /**
     * 现住址
     **/
    @TableField("present_address")
    private String presentAddress;

    /**
     * 户口所在地
     **/
    @TableField("domicile_place")
    private String domicilePlace;

    /**
     * 家庭住址
     **/
    @TableField("family_address")
    private String familyAddress;

    /**
     * 联系电话
     **/
    @TableField("telephone")
    private String telephone;

    /**
     * 手机
     **/
    @TableField("cellphone")
    private String cellphone;

    /**
     * 邮政编码
     **/
    @TableField("post_code")
    private String postCode;

    /**
     * 电子信箱
     **/
    @TableField("email")
    private String email;

    /**
     * 来源应用id,
     **/
    @TableField( "app_id")
    private String appId;
    /**
     * 创建人Id
     */
    @TableField( "crt_id")
    private String crtId;

    /**
     * 创建时间
     */
    @TableField( "crt_dttm")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date crtDttm;

    /**
     * 最后修改人ID
     */
    @TableField("lastup_id")
    private String lastupId;

    /**
     * 最后修改时间
     */
    @TableField("lastup_dttm")
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date lastupDttm;

    /**
     * 有效性
     */
    @TableField("enable")
    private Boolean enable;

    @TableField(exist = false)
    private String accountName;
    @TableField(exist = false)
    private Integer userType;
    @TableField(exist = false)
    private String flago;
    @TableField(exist = false)
    private String account;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getOriginalPhoto() {
        return originalPhoto;
    }

    public void setOriginalPhoto(String originalPhoto) {
        this.originalPhoto = originalPhoto;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getNamePell() {
        return namePell;
    }

    public void setNamePell(String namePell) {
        this.namePell = namePell;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }
    @JSONField(format = "yyyy-MM-dd ")
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getBirthPlaceCode() {
        return birthPlaceCode;
    }

    public void setBirthPlaceCode(String birthPlaceCode) {
        this.birthPlaceCode = birthPlaceCode;
    }

    public String getNativePlace() {
        return nativePlace;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace;
    }

    public String getNationCode() {
        return nationCode;
    }

    public void setNationCode(String nationCode) {
        this.nationCode = nationCode;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public Integer getPapersType() {
        return papersType;
    }

    public void setPapersType(Integer papersType) {
        this.papersType = papersType;
    }

    public String getPapersNumber() {
        return papersNumber;
    }

    public void setPapersNumber(String papersNumber) {
        this.papersNumber = papersNumber;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getPresentAddress() {
        return presentAddress;
    }

    public void setPresentAddress(String presentAddress) {
        this.presentAddress = presentAddress;
    }

    public String getDomicilePlace() {
        return domicilePlace;
    }

    public void setDomicilePlace(String domicilePlace) {
        this.domicilePlace = domicilePlace;
    }

    public String getFamilyAddress() {
        return familyAddress;
    }

    public void setFamilyAddress(String familyAddress) {
        this.familyAddress = familyAddress;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getCrtId() {
        return crtId;
    }

    public void setCrtId(String crtId) {
        this.crtId = crtId;
    }

    public Date getCrtDttm() {
        return crtDttm;
    }

    public void setCrtDttm(Date crtDttm) {
        this.crtDttm = crtDttm;
    }

    public String getLastupId() {
        return lastupId;
    }

    public void setLastupId(String lastupId) {
        this.lastupId = lastupId;
    }

    public Date getLastupDttm() {
        return lastupDttm;
    }

    public void setLastupDttm(Date lastupDttm) {
        this.lastupDttm = lastupDttm;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getFlago() {
        return flago;
    }

    public void setFlago(String flago) {
        this.flago = flago;
    }
}
