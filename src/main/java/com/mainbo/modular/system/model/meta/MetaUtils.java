package com.mainbo.modular.system.model.meta;

import cn.stylefeng.roses.core.util.SpringContextHolder;
import com.mainbo.modular.system.model.meta.vo.Meta;
import com.mainbo.modular.system.model.meta.MetaProvider.PhaseGradeProvider;
import com.mainbo.modular.system.model.meta.MetaProvider.PhaseSubjectProvider;
import com.mainbo.modular.system.model.meta.MetaProvider.PhaseProvider;
import com.mainbo.modular.system.model.meta.MetaProvider.XZGradeProvider;
import com.mainbo.modular.system.model.meta.MetaProvider.OrgTypeProvider;
import com.mainbo.modular.system.model.meta.MetaProvider.PublisherProvider;
import com.mainbo.modular.system.model.meta.MetaProvider.SysConfigProvider;
import com.mainbo.modular.system.model.meta.MetaProvider.BookEditionProvider;
import com.mainbo.modular.system.model.meta.MetaProvider.AppTypeProvider;
import com.mainbo.modular.system.model.meta.MetaProvider.UnitTypeProvider;
import com.mainbo.modular.system.model.meta.MetaProvider.SchoolYearProvider;

import java.util.List;

/**
 * @author moshang
 * @date 2020-02-25
 **/
public abstract class MetaUtils {

    private static PhaseProvider phaseProvider;

    private static PhaseSubjectProvider phaseSubjectProvider;

    private static PhaseGradeProvider phaseGradeProvider;

    private static XZGradeProvider xzGradeProvider;

    private static OrgTypeProvider orgTypeProvider;

    private static PublisherProvider publisherProvider;

    private static SysConfigProvider sysConfigProvider;

    private static BookEditionProvider bookEditionProvider;

    private static AppTypeProvider appTypeProvider;

    private static UnitTypeProvider unitTypeProvider;

    private static SchoolYearProvider schoolYearProvider;

    /**
     * 通过id获取系统元数据及配置
     *
     * @param id
     *          元数据主键id
     * @return Meta
     */
    public static Meta getMeta(Integer id) {
        return getPhaseProvider().getMeta(id);
    }

    /**
     * 通过code获取一条系统元数据及配置
     *
     * @param code
     *          类型码
     * @return Meta
     */
    public static Meta getMeta(String code) {
        return getPhaseProvider().getMeta(code, ConfigConstants.SYS_TYPE);
    }
    public static Meta getMeta(String code,String value) {
        return getPhaseProvider().getMeta(code,value, ConfigConstants.SYS_TYPE);
    }
    /**
     * 通过code获取系统元数据及配置
     *
     * @param code
     *          类型码
     * @return List
     */
    public static List<Meta> listMeta(String code) {
        return getPhaseProvider().listMeta(code, ConfigConstants.SYS_TYPE);
    }
    public static List<Meta> listMeta(String code,String orgId) {
        return getPhaseProvider().listMeta(code, ConfigConstants.SYS_TYPE,orgId);
    }
    /**
     * 学段
     *
     * @return PhaseProvider
     */
    public static final PhaseProvider getPhaseProvider() {
        if (phaseProvider == null) {
            phaseProvider = SpringContextHolder.getBean("phaseProvider");
        }
        return phaseProvider;
    }

    /**
     * 学科
     *
     * @return PhaseSubjectProvider
     */
    public static final PhaseSubjectProvider getPhaseSubjectProvider() {
        if (phaseSubjectProvider == null) {
            phaseSubjectProvider = SpringContextHolder
                    .getBean("phaseSubjectProvider");
        }
        return phaseSubjectProvider;
    }

    /**
     * 年级
     *
     * @return PhaseGradeProvider
     */
    public static final PhaseGradeProvider getPhaseGradeProvider() {
        if (phaseGradeProvider == null) {
            phaseGradeProvider = SpringContextHolder.getBean("phaseGradeProvider");
        }
        return phaseGradeProvider;
    }

    /**
     * 行政年级
     *
     * @return XZGradeProvider
     */
    public static final XZGradeProvider getXzGradeProvider() {
        if (xzGradeProvider == null) {
            xzGradeProvider = SpringContextHolder.getBean("xzGradeProvider");
        }
        return xzGradeProvider;
    }

    /**
     * 机构类型
     *
     * @return OrgTypeProvider
     */
    public static final OrgTypeProvider getOrgTypeProvider() {
        if (orgTypeProvider == null) {
            orgTypeProvider = SpringContextHolder.getBean("orgTypeProvider");
        }
        return orgTypeProvider;
    }

    /**
     * 出版社
     *
     * @return PublisherProvider
     */
    public static final PublisherProvider getPublisherProvider() {
        if (publisherProvider == null) {
            publisherProvider = SpringContextHolder.getBean("publisherProvider");
        }
        return publisherProvider;
    }

    /**
     * 系统Config
     *
     * @return SysConfigProvider
     */
    public static final SysConfigProvider getSysConfigProvider() {
        if (sysConfigProvider == null) {
            sysConfigProvider = SpringContextHolder.getBean("sysConfigProvider");
        }
        return sysConfigProvider;
    }

    /**
     * 书籍版本
     *
     * @return BookEditionProvider
     */
    public static final BookEditionProvider getBookEditionProvider() {
        if (bookEditionProvider == null) {
            bookEditionProvider = SpringContextHolder.getBean("bookEditionProvider");
        }
        return bookEditionProvider;
    }

    /**
     * 应用类型
     *
     * @return AppTypeProvider
     */
    public static final AppTypeProvider getAppTypeProvider() {
        if (appTypeProvider == null) {
            appTypeProvider = SpringContextHolder.getBean("appTypeProvider");
        }
        return appTypeProvider;
    }

    /**
     * 单位类型
     *
     * @return UnitTypeProvider
     */
    public static final UnitTypeProvider getUnitTypeProvider() {
        if (unitTypeProvider == null) {
            unitTypeProvider = SpringContextHolder.getBean("unitTypeProvider");
        }
        return unitTypeProvider;
    }

    /**
     * 学年
     *
     * @return SchoolYearProvider
     */
    public static final SchoolYearProvider getSchoolYearProvider() {
        if (schoolYearProvider == null) {
            schoolYearProvider = SpringContextHolder.getBean("schoolYearProvider");
        }
        return schoolYearProvider;
    }

}
