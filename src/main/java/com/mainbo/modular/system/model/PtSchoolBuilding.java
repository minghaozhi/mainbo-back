package com.mainbo.modular.system.model;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author moshang
 * @since 2020-03-09
 */
@TableName("pt_school_building")
public class PtSchoolBuilding extends Model<PtSchoolBuilding> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = IdType.UUID)
    private String id;
    /**
     * 教学楼名称
     */
    private String name;
    /**
     * 机构Id
     */
    @TableField("org_id")
    private String orgId;
    /**
     * 机构名称
     */
    @TableField("org_name")
    private String orgName;
    /**
     * 楼层数
     */
    @TableField("floor_count")
    private Integer floorCount;
    /**
     * 备注
     */
    private String note;
    @TableField("create_time")
    private Date createTime;
    @TableField("modify_time")
    private Date modifyTime;
    /**
     * 是否可用 1可用 0禁用
     */
    private Boolean enable;
    private Boolean deleted;
    /**
     * 禁用原因
     */
    @TableField("disabled_reason")
    private String disabledReason;
    @TableField("lastup_id")
    private String lastupId;
    @TableField("lastup_dttm")
    private Date lastupDttm;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Integer getFloorCount() {
        return floorCount;
    }

    public void setFloorCount(Integer floorCount) {
        this.floorCount = floorCount;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getDisabledReason() {
        return disabledReason;
    }

    public void setDisabledReason(String disabledReason) {
        this.disabledReason = disabledReason;
    }

    public String getLastupId() {
        return lastupId;
    }

    public void setLastupId(String lastupId) {
        this.lastupId = lastupId;
    }

    public Date getLastupDttm() {
        return lastupDttm;
    }

    public void setLastupDttm(Date lastupDttm) {
        this.lastupDttm = lastupDttm;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PtSchoolBuilding{" +
        ", id=" + id +
        ", name=" + name +
        ", orgId=" + orgId +
        ", orgName=" + orgName +
        ", floorCount=" + floorCount +
        ", note=" + note +
        ", createTime=" + createTime +
        ", modifyTime=" + modifyTime +
        ", enable=" + enable +
        ", deleted=" + deleted +
        ", disabledReason=" + disabledReason +
        ", lastupId=" + lastupId +
        ", lastupDttm=" + lastupDttm +
        "}";
    }
}
