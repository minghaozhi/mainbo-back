package com.mainbo.modular.system.model;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 地域表
 * </p>
 *
 * @author moshang
 * @Date 2020-02-17 21:23:57
 */
@TableName("pt_area")
public class PtArea extends Model<PtArea> {

    private static final long serialVersionUID = 1L;

    /**
     * 地域ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 地域名称
     */
    @TableField("area_name")
    private String areaName;
    /**
     * 父级地域ID
     */
    private Integer pid;
    /**
     * 邮编
     */
    @TableField("post_code")
    private String postCode;
    /**
     * 区域编码
     */
    private String code;
    /**
     * 层级
     */
    private String level;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 0  行政区域， 1 自定义区域
     */
    private Integer type;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PtArea{" +
        ", id=" + id +
        ", areaName=" + areaName +
        ", pid=" + pid +
        ", postCode=" + postCode +
        ", code=" + code +
        ", level=" + level +
        ", sort=" + sort +
        ", type=" + type +
        "}";
    }
}
