package com.mainbo.modular.system.model;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author moshang
 * @since 2020-03-10
 */
@TableName("pt_section_category")
public class PtSectionCategory extends Model<PtSectionCategory> {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.UUID)
    private String id;
    /**
     * 节次方案名称
     */
    private String name;
    @TableField("org_id")
    private String orgId;
    @TableField("org_name")
    private String orgName;
    /**
     * 所属学期id
     */
    @TableField("term_id")
    private String termId;
    /**
     * 所属学期
     */
    @TableField("term_name")
    private String termName;
    /**
     * 应用年级id
     */
    @TableField("grade_id")
    private String gradeId;
    /**
     * 应用年级
     */
    @TableField("grade_name")
    private String gradeName;
    /**
     * 应用开始时间
     */
    @TableField("start_time")
    private Date startTime;
    /**
     * 应用结束时间
     */
    @TableField("end_time")
    private Date endTime;
    /**
     * 每周应用范围
     */
    private String week;
    /**
     * 节次数量
     */
    private Integer sections;
    /**
     * 创建者id
     */
    @TableField("creator_id")
    private String creatorId;
    /**
     * 创建者
     */
    private String creator;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
    @TableField("lastup_dttm")
    private Date lastupDttm;
    @TableField("lastup_id")
    private String lastupId;
    @TableField(exist = false)
    private String useTime;

    public String getUseTime() {
        return useTime;
    }

    public void setUseTime(String useTime) {
        this.useTime = useTime;
    }

    /**
     * 删除标识：0 不可用，1 可用
     */
    private Integer enable;
    private Integer deleted;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getTermId() {
        return termId;
    }

    public void setTermId(String termId) {
        this.termId = termId;
    }

    public String getTermName() {
        return termName;
    }

    public void setTermName(String termName) {
        this.termName = termName;
    }

    public String getGradeId() {
        return gradeId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public Integer getSections() {
        return sections;
    }

    public void setSections(Integer sections) {
        this.sections = sections;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastupDttm() {
        return lastupDttm;
    }

    public void setLastupDttm(Date lastupDttm) {
        this.lastupDttm = lastupDttm;
    }

    public String getLastupId() {
        return lastupId;
    }

    public void setLastupId(String lastupId) {
        this.lastupId = lastupId;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PtSectionCategory{" +
        ", id=" + id +
        ", name=" + name +
        ", orgId=" + orgId +
        ", orgName=" + orgName +
        ", termId=" + termId +
        ", termName=" + termName +
        ", gradeId=" + gradeId +
        ", gradeName=" + gradeName +
        ", startTime=" + startTime +
        ", endTime=" + endTime +
        ", week=" + week +
        ", sections=" + sections +
        ", creatorId=" + creatorId +
        ", creator=" + creator +
        ", createTime=" + createTime +
        ", lastupDttm=" + lastupDttm +
        ", lastupId=" + lastupId +
        ", enable=" + enable +
        ", deleted=" + deleted +
        "}";
    }
}
