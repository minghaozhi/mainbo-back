/**
 * Mainbo.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */

package com.mainbo.modular.system.model.excelUser;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.mainbo.modular.system.model.CommUser;
import com.mainbo.modular.system.model.PtUserRole;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

/**
 * 教育局职工 Entity
 * 
 * <pre>
 *
 * </pre>
 *
 */
@SuppressWarnings("serial")
@TableName(value = EduEmployee.TABLE_NAME)
@Getter
@Setter
public class EduEmployee extends CommUser {
  public static final String TABLE_NAME = "pt_edu_emp";

  /**
   * 工号
   **/
  @TableField("job_number")
  private String jobNumber;

  /**
   * 曾用名
   **/
  @TableField( "old_name")
  private String oldName;

  /**
   * 婚姻状况码(GB T 2261.2)
   **/
  @TableField( "marital_status")
  private String maritalStatus;

  /**
   * 港澳台侨外码(港澳台侨外代码表)
   **/
  @TableField( "gatqwm")
  private String gatqwm;

  /**
   * 政治面貌码(政治面貌代码表 GB/T 4762)
   **/
  @TableField( "political_status")
  private String politicalStatus;

  /**
   * 健康状况码(健康状况代码 采用1位数字代码 GB T 2261.3)
   **/
  @TableField( "health_status")
  private String healthStatus;

  /**
   * 信仰宗教码(信仰宗教码 GA 214.12)
   **/
  @TableField( "religious_code")
  private String religiousCode;

  /**
   * 身份证件有效期(格式： YYYYMMDD-YYYYMMDD)
   **/
  @TableField( "papers_expire")
  private String papersExpire;

  /**
   * 机构号
   **/
  @TableField( "org_number")
  private String orgNumber;

  /**
   * 户口性质码(户口类别代码 GA 324.1)
   **/
  @TableField( "domicile_nature")
  private String domicileNature;

  /**
   * 学历码(学历代码 GB/T 4658)
   **/
  @TableField( "education_code")
  private String educationCode;

  /**
   * 参加工作年月
   **/
  @TableField( "join_work_date")
  private String joinWorkDate;

  /**
   * 编制类别码(JY/T 1001 ZXXBZLB 中小学 编制类别代码)
   **/
  @TableField( "preparation_category")
  private String preparationCategory;

  /**
   * 档案编号(存档案部门为本人 档案确定的管理编 号)
   **/
  @TableField( "record_code")
  private String recordCode;

  /**
   * 档案文本
   **/
  @TableField( "record_content")
  private String recordContent;

  /**
   * 通信地址
   **/
  @TableField( "contact_address")
  private String contactAddress;

  /**
   * 主页地址
   **/
  @TableField( "home_page")
  private String homePage;

  /**
   * 教师简介
   **/
  @TableField( "introduction")
  private String introduction;

  /**
   * 特长
   **/
  @TableField( "specialty")
  private String specialty;

  /**
   * 教师状态(1:禁用，0：启用)
   **/
  @TableField( "status")
  private Integer status;

  @TableField( "regis_role")
  private String regisRole;

  @TableField(exist = false)
  private List<PtUserRole> userRoleList;


}
