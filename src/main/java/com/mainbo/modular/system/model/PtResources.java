package com.mainbo.modular.system.model;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author moshang
 * @since 2020-02-24
 */
@TableName("pt_resources")
public class PtResources extends Model<PtResources> {

    private static final long serialVersionUID = 1L;
    /**
     * 普通文件
     */
    public static final int S_NORMAL = 1;

    /**
     * 临时文件
     */
    public static final int S_TEMP = 0;
    /**
     * 主键资源id
     */
    private String id;
    /**
     * 资源存储的相对路径（包含文件名和后缀）
     */
    private String path;
    /**
     * 资源的真实名字（不包含后缀）
     */
    private String name;
    /**
     * 资源大小（字节）
     */
    private Long size;
    /**
     * 后缀（如：doc，不含点）
     */
    private String ext;
    /**
     * 服务器id
     */
    @TableField("device_id")
    private Integer deviceId;
    /**
     * 服务器名称
     */
    @TableField("device_name")
    private String deviceName;
    /**
     * 资源状态
     */
    private Integer state;
    @TableField("crt_dttm")
    private Date crtDttm;
    @TableField("rms_res_id")
    private String rmsResId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCrtDttm() {
        return crtDttm;
    }

    public void setCrtDttm(Date crtDttm) {
        this.crtDttm = crtDttm;
    }

    public String getRmsResId() {
        return rmsResId;
    }

    public void setRmsResId(String rmsResId) {
        this.rmsResId = rmsResId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PtResources{" +
        ", id=" + id +
        ", path=" + path +
        ", name=" + name +
        ", size=" + size +
        ", ext=" + ext +
        ", deviceId=" + deviceId +
        ", deviceName=" + deviceName +
        ", state=" + state +
        ", crtDttm=" + crtDttm +
        ", rmsResId=" + rmsResId +
        "}";
    }
}
