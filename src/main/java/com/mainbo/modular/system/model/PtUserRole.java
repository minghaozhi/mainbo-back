package com.mainbo.modular.system.model;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.omg.CORBA.IDLType;

import java.io.Serializable;

import static com.baomidou.mybatisplus.enums.IdType.*;

/**
 * <p>
 * 切换工作空间
 * </p>
 *
 * @author moshang
 * @since 2020-02-22
 */
@EqualsAndHashCode(callSuper = true)
@TableName("pt_user_role")
public class PtUserRole extends Model<PtUserRole> {

    private static final long serialVersionUID = 1L;

    @TableId(type = UUID)
    private String id;
    @TableField("account_id")
    private String accountId;
    /**
     * 真实姓名
     */
    private String name;
    /**
     * 所教书籍
     */
    @TableField("book_id")
    private String bookId;
    /**
     * 所属机构
     */
    @TableField("org_id")
    private String orgId;
    @TableField("phase_id")
    private Integer phaseId;
    @TableField("grade_id")
    private Integer gradeId;
    @TableField("subject_id")
    private Integer subjectId;
    @TableField("sys_role_id")
    private Integer sysRoleId;
    /**
     * 角色id
     */
    @TableField("role_id")
    private Integer roleId;
    @TableField("enable")
    private boolean enable;
    /**
     * 创建时间
     */
    @TableField("crt_dttm")
    private Date crtDttm;
    /**
     * 创建人ID
     */
    @TableField("crt_id")
    private String crtId;
    /**
     * 最后更新人ID
     */
    @TableField("lastup_id")
    private String lastupId;
    /**
     * 最后更新时间
     */
    @TableField("lastup_dttm")
    private Date lastupDttm;
    @TableField(exist = false)
    private String phaseName;
    @TableField(exist = false)
    private String gradeName;
    @TableField(exist = false)
    private String subjectName;
    @TableField(exist = false)
    private String roleName;
    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Integer getPhaseId() {
        return phaseId;
    }

    public void setPhaseId(Integer phaseId) {
        this.phaseId = phaseId;
    }

    public Integer getGradeId() {
        return gradeId;
    }

    public void setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
    }

    public Integer getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    public Integer getSysRoleId() {
        return sysRoleId;
    }

    public void setSysRoleId(Integer sysRoleId) {
        this.sysRoleId = sysRoleId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public boolean getEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public Date getCrtDttm() {
        return crtDttm;
    }

    public void setCrtDttm(Date crtDttm) {
        this.crtDttm = crtDttm;
    }

    public String getCrtId() {
        return crtId;
    }

    public void setCrtId(String crtId) {
        this.crtId = crtId;
    }

    public String getLastupId() {
        return lastupId;
    }

    public void setLastupId(String lastupId) {
        this.lastupId = lastupId;
    }

    public Date getLastupDttm() {
        return lastupDttm;
    }

    public void setLastupDttm(Date lastupDttm) {
        this.lastupDttm = lastupDttm;
    }

    public String getPhaseName() {
        return phaseName;
    }

    public void setPhaseName(String phaseName) {
        this.phaseName = phaseName;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
