/**
 * Mainbo.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */

package com.mainbo.modular.system.model.meta;

/**
 * <pre>
 * 国标枚举类型
 * </pre>
 *
 * @author tmser
 * @version $Id: GBSex.java, v 1.0 2017年3月23日 下午1:35:37 tmser Exp $
 */
public enum GBSex {
  MAN("男", 1), WOMEN("女", 2);

  /**
   * 中文名称
   */
  private String cname;

  private Integer code;

  GBSex(String cname, Integer code) {
    this.cname = cname;
    this.code = code;
  }

  /**
   * 中文名称转代码
   * 
   * @param cname
   *          中文名称
   * @return Integer
   */
  public static Integer forCode(String cname) {
    for (GBSex sex : GBSex.values()) {
      if (sex.cname.equals(cname)) {
        return sex.getCode();
      }
    }
    return null;
  }

  /**
   * 代码转中文名称
   * 
   * @param code
   *          代码
   * @return String
   */
  public static String forCname(Integer code) {
    for (GBSex sex : GBSex.values()) {
      if (sex.code.equals(code)) {
        return sex.getCname();
      }
    }

    return null;
  }

  /**
   * Getter method for property <tt>cname</tt>.
   * 
   * @return property value of cname
   */
  public String getCname() {
    return cname;
  }

  /**
   * Getter method for property <tt>code</tt>.
   * 
   * @return property value of code
   */
  public Integer getCode() {
    return code;
  }

}
