package com.mainbo.modular.system.model;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 机构职工表
 * </p>
 *
 * @author moshang
 * @since 2020-02-22
 */
@TableName("pt_edu_emp")
public class PtEduEmp extends Model<PtEduEmp> {

    private static final long serialVersionUID = 1L;

    /**
     * 教育局职工子类id
     */
    private String id;
    /**
     * 机构数据类ID
     */
    @TableField("org_id")
    private String orgId;
    @TableField("org_name")
    private String orgName;
    /**
     * 工号
     */
    @TableField("job_number")
    private String jobNumber;
    /**
     * 姓名
     */
    private String name;
    /**
     * 用户头像
     */
    private String photo;
    /**
     * 缩略图
     */
    @TableField("original_photo")
    private String originalPhoto;
    /**
     * 英文姓名
     */
    @TableField("english_name")
    private String englishName;
    /**
     * 姓名拼音
     */
    @TableField("name_pell")
    private String namePell;
    /**
     * 曾用名
     */
    @TableField("old_name")
    private String oldName;
    /**
     * 性别码(GB T 2261_1 人的性别代码)
     */
    private Integer sex;
    /**
     * 出生日期
     */
    private Date birthday;
    /**
     * 出生地码(GB/T 2260)
     */
    @TableField("birth_place_code")
    private String birthPlaceCode;
    /**
     * 籍贯
     */
    @TableField("native_place")
    private String nativePlace;
    /**
     * 民族码(GB T 3304)
     */
    @TableField("nation_code")
    private String nationCode;
    /**
     * 国籍/地区码(GB T 2659)
     */
    @TableField("country_id")
    private Integer countryId;
    /**
     * 身份证件类型码(身份证件类型代码表 JY_SFZJLX)
     */
    @TableField("papers_type")
    private Integer papersType;
    /**
     * 身份证件号
     */
    @TableField("papers_number")
    private String papersNumber;
    /**
     * 婚姻状况码(GB T 2261.2)
     */
    @TableField("marital_status")
    private String maritalStatus;
    /**
     * 港澳台侨外码(港澳台侨外代码表)
     */
    private String gatqwm;
    /**
     * 政治面貌码(政治面貌代码表 GB/T 4762)
     */
    @TableField("political_status")
    private String politicalStatus;
    /**
     * 健康状况码(健康状况代码 采用1位数字代码 GB T 2261.3)
     */
    @TableField("health_status")
    private String healthStatus;
    /**
     * 信仰宗教码(信仰宗教码 GA 214.12)
     */
    @TableField("religious_code")
    private String religiousCode;
    /**
     * 血型码(血型代码 JY_XX)
     */
    @TableField("blood_type")
    private String bloodType;
    /**
     * 照片
     */
    private String picture;
    /**
     * 身份证件有效期(格式： YYYYMMDD-YYYYMMDD)
     */
    @TableField("papers_expire")
    private String papersExpire;
    /**
     * 机构号
     */
    @TableField("org_number")
    private String orgNumber;
    /**
     * 家庭住址
     */
    @TableField("family_address")
    private String familyAddress;
    /**
     * 现住址
     */
    @TableField("present_address")
    private String presentAddress;
    /**
     * 户口所在地
     */
    @TableField("domicile_place")
    private String domicilePlace;
    /**
     * 户口性质码(户口类别代码 GA 324.1)
     */
    @TableField("domicile_nature")
    private String domicileNature;
    /**
     * 学历码(学历代码 GB/T 4658)
     */
    @TableField("education_code")
    private String educationCode;
    /**
     * 参加工作年月
     */
    @TableField("join_work_date")
    private String joinWorkDate;
    /**
     * 编制类别码(JY/T 1001 ZXXBZLB 中小学 编制类别代码)
     */
    @TableField("preparation_category")
    private String preparationCategory;
    /**
     * 档案编号(存档案部门为本人 档案确定的管理编 号)
     */
    @TableField("record_code")
    private String recordCode;
    /**
     * 档案文本
     */
    @TableField("record_content")
    private String recordContent;
    /**
     * 通信地址
     */
    @TableField("contact_address")
    private String contactAddress;
    /**
     * 联系电话
     */
    private String telephone;
    /**
     * 邮政编码
     */
    @TableField("post_code")
    private String postCode;
    private String cellphone;
    /**
     * 电子信箱
     */
    private String email;
    /**
     * 主页地址
     */
    @TableField("home_page")
    private String homePage;
    /**
     * 教师简介
     */
    private String introduction;
    /**
     * 特长
     */
    private String specialty;
    @TableField("app_id")
    private String appId;
    /**
     * 教师状态(1:禁用，0：启用)
     */
    private Integer status;
    /**
     * 创建时间
     */
    @TableField("crt_dttm")
    private Date crtDttm;
    /**
     * 创建人ID
     */
    @TableField("crt_id")
    private String crtId;
    /**
     * 最后更新时间
     */
    @TableField("lastup_dttm")
    private Date lastupDttm;
    /**
     * 最后更新人ID
     */
    @TableField("lastup_id")
    private String lastupId;
    /**
     * 有效性
     */
    private Integer enable;
    @TableField("regis_role")
    private String regisRole;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getOriginalPhoto() {
        return originalPhoto;
    }

    public void setOriginalPhoto(String originalPhoto) {
        this.originalPhoto = originalPhoto;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getNamePell() {
        return namePell;
    }

    public void setNamePell(String namePell) {
        this.namePell = namePell;
    }

    public String getOldName() {
        return oldName;
    }

    public void setOldName(String oldName) {
        this.oldName = oldName;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getBirthPlaceCode() {
        return birthPlaceCode;
    }

    public void setBirthPlaceCode(String birthPlaceCode) {
        this.birthPlaceCode = birthPlaceCode;
    }

    public String getNativePlace() {
        return nativePlace;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace;
    }

    public String getNationCode() {
        return nationCode;
    }

    public void setNationCode(String nationCode) {
        this.nationCode = nationCode;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public Integer getPapersType() {
        return papersType;
    }

    public void setPapersType(Integer papersType) {
        this.papersType = papersType;
    }

    public String getPapersNumber() {
        return papersNumber;
    }

    public void setPapersNumber(String papersNumber) {
        this.papersNumber = papersNumber;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getGatqwm() {
        return gatqwm;
    }

    public void setGatqwm(String gatqwm) {
        this.gatqwm = gatqwm;
    }

    public String getPoliticalStatus() {
        return politicalStatus;
    }

    public void setPoliticalStatus(String politicalStatus) {
        this.politicalStatus = politicalStatus;
    }

    public String getHealthStatus() {
        return healthStatus;
    }

    public void setHealthStatus(String healthStatus) {
        this.healthStatus = healthStatus;
    }

    public String getReligiousCode() {
        return religiousCode;
    }

    public void setReligiousCode(String religiousCode) {
        this.religiousCode = religiousCode;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPapersExpire() {
        return papersExpire;
    }

    public void setPapersExpire(String papersExpire) {
        this.papersExpire = papersExpire;
    }

    public String getOrgNumber() {
        return orgNumber;
    }

    public void setOrgNumber(String orgNumber) {
        this.orgNumber = orgNumber;
    }

    public String getFamilyAddress() {
        return familyAddress;
    }

    public void setFamilyAddress(String familyAddress) {
        this.familyAddress = familyAddress;
    }

    public String getPresentAddress() {
        return presentAddress;
    }

    public void setPresentAddress(String presentAddress) {
        this.presentAddress = presentAddress;
    }

    public String getDomicilePlace() {
        return domicilePlace;
    }

    public void setDomicilePlace(String domicilePlace) {
        this.domicilePlace = domicilePlace;
    }

    public String getDomicileNature() {
        return domicileNature;
    }

    public void setDomicileNature(String domicileNature) {
        this.domicileNature = domicileNature;
    }

    public String getEducationCode() {
        return educationCode;
    }

    public void setEducationCode(String educationCode) {
        this.educationCode = educationCode;
    }

    public String getJoinWorkDate() {
        return joinWorkDate;
    }

    public void setJoinWorkDate(String joinWorkDate) {
        this.joinWorkDate = joinWorkDate;
    }

    public String getPreparationCategory() {
        return preparationCategory;
    }

    public void setPreparationCategory(String preparationCategory) {
        this.preparationCategory = preparationCategory;
    }

    public String getRecordCode() {
        return recordCode;
    }

    public void setRecordCode(String recordCode) {
        this.recordCode = recordCode;
    }

    public String getRecordContent() {
        return recordContent;
    }

    public void setRecordContent(String recordContent) {
        this.recordContent = recordContent;
    }

    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHomePage() {
        return homePage;
    }

    public void setHomePage(String homePage) {
        this.homePage = homePage;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCrtDttm() {
        return crtDttm;
    }

    public void setCrtDttm(Date crtDttm) {
        this.crtDttm = crtDttm;
    }

    public String getCrtId() {
        return crtId;
    }

    public void setCrtId(String crtId) {
        this.crtId = crtId;
    }

    public Date getLastupDttm() {
        return lastupDttm;
    }

    public void setLastupDttm(Date lastupDttm) {
        this.lastupDttm = lastupDttm;
    }

    public String getLastupId() {
        return lastupId;
    }

    public void setLastupId(String lastupId) {
        this.lastupId = lastupId;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public String getRegisRole() {
        return regisRole;
    }

    public void setRegisRole(String regisRole) {
        this.regisRole = regisRole;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PtEduEmp{" +
        ", id=" + id +
        ", orgId=" + orgId +
        ", orgName=" + orgName +
        ", jobNumber=" + jobNumber +
        ", name=" + name +
        ", photo=" + photo +
        ", originalPhoto=" + originalPhoto +
        ", englishName=" + englishName +
        ", namePell=" + namePell +
        ", oldName=" + oldName +
        ", sex=" + sex +
        ", birthday=" + birthday +
        ", birthPlaceCode=" + birthPlaceCode +
        ", nativePlace=" + nativePlace +
        ", nationCode=" + nationCode +
        ", countryId=" + countryId +
        ", papersType=" + papersType +
        ", papersNumber=" + papersNumber +
        ", maritalStatus=" + maritalStatus +
        ", gatqwm=" + gatqwm +
        ", politicalStatus=" + politicalStatus +
        ", healthStatus=" + healthStatus +
        ", religiousCode=" + religiousCode +
        ", bloodType=" + bloodType +
        ", picture=" + picture +
        ", papersExpire=" + papersExpire +
        ", orgNumber=" + orgNumber +
        ", familyAddress=" + familyAddress +
        ", presentAddress=" + presentAddress +
        ", domicilePlace=" + domicilePlace +
        ", domicileNature=" + domicileNature +
        ", educationCode=" + educationCode +
        ", joinWorkDate=" + joinWorkDate +
        ", preparationCategory=" + preparationCategory +
        ", recordCode=" + recordCode +
        ", recordContent=" + recordContent +
        ", contactAddress=" + contactAddress +
        ", telephone=" + telephone +
        ", postCode=" + postCode +
        ", cellphone=" + cellphone +
        ", email=" + email +
        ", homePage=" + homePage +
        ", introduction=" + introduction +
        ", specialty=" + specialty +
        ", appId=" + appId +
        ", status=" + status +
        ", crtDttm=" + crtDttm +
        ", crtId=" + crtId +
        ", lastupDttm=" + lastupDttm +
        ", lastupId=" + lastupId +
        ", enable=" + enable +
        ", regisRole=" + regisRole +
        "}";
    }
}
