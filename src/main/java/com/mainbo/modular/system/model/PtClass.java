package com.mainbo.modular.system.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author moshang
 * @since 2020-02-24
 */
@TableName("pt_class")
public class PtClass extends Model<PtClass> {

    private static final long serialVersionUID = 1L;
    public static final Integer update_not_begin = 0;
    public static final Integer update_begin = 1;
    public static final Integer update_success = 2;
    public static final Integer update_error = 3;
    public static final Integer update_not_need = 4;

    public static final Integer not_graduation = 5;
    public static final Integer phase_graduation = 6;
    public static final Integer school_graduation = 7;

    public static enum ClassInfoStatus {
        // 升级状态
        update_not_begin(PtClass.update_not_begin, "尚未启动升级"), update_begin(PtClass.update_begin, "部分升级完成"), update_success(PtClass.update_success,
                "升级成功"), update_error(PtClass.update_error, "升级失败"), update_not_need(PtClass.update_not_need, "无需升级"),
        // 毕业状态
        not_graduation(PtClass.not_graduation, "未毕业"), phase_graduation(PtClass.phase_graduation, "毕业"), // 需要拼接当前学段名称
        // 如：小学毕业
        school_graduation(PtClass.school_graduation, "本校毕业");
        /**
         * 中文名
         */
        private String cname;
        /**
         * id 关联
         */
        private Integer id;

        /**
         * Getter method for property <tt>cname</tt>.
         *
         * @return property value of cname
         */
        public String getCname() {
            return cname;
        }

        /**
         * Setter method for property <tt>cname</tt>.
         *
         * @param cname
         *          value to be assigned to property cname
         */
        public void setCname(String cname) {
            this.cname = cname;
        }

        /**
         * Getter method for property <tt>id</tt>.
         *
         * @return property value of id
         */
        public Integer getId() {
            return id;
        }

        /**
         * Setter method for property <tt>id</tt>.
         *
         * @param id
         *          value to be assigned to property id
         */
        public void setId(Integer id) {
            this.id = id;
        }

        ClassInfoStatus(Integer id, String cname) {
            this.cname = cname;
            this.id = id;
        }

        /**
         * 通过角色id获取角色名称
         *
         * @param roleId
         *          角色id
         * @return ClassInfoStatus
         */
        public static ClassInfoStatus getValueById(Integer roleId) {
            ClassInfoStatus[] roles = ClassInfoStatus.values();
            for (int i = 0; i < roles.length; i++) {
                if (roles[i].id.equals(roleId)) {
                    return roles[i];
                }
            }
            return update_not_begin;
        }
    }
    /**
     * 主键id
     */
    @TableId(type = IdType.UUID)
    private String id;
    /**
     * 班级类型
     */
    @TableField("class_type")
    private Integer classType;
    private String name;
    /**
     * 学校id
     */
    @TableField("org_id")
    private String orgId;
    @TableField("org_name")
    private String orgName;
    /**
     * 级届，即年份
     */
    private Integer period;
    /**
     * 学段id
     */
    @TableField("phase_id")
    private Integer phaseId;
    /**
     * 文理科id
     */
    @TableField("subject_type")
    private Integer subjectType;
    /**
     * 年级id
     */
    @TableField("grade_id")
    private Integer gradeId;
    /**
     * 班主任id
     */
    @TableField("teacher_id")
    private String teacherId;
    /**
     * 班主任姓名
     */
    @TableField("teacher_name")
    private String teacherName;
    private Integer sort;
    /**
     * 有效性，0失效，1有效，毕业班即失效
     */
    private Boolean enable;
    @TableField("crt_id")
    private String crtId;
    @TableField("crt_dttm")
    private Date crtDttm;
    @TableField("lastup_id")
    private String lastupId;
    @TableField("lastup_dttm")
    private Date lastupDttm;
    @TableField("school_year")
    private Integer schoolYear;
    private Integer status;
    /**
     * 毕业状态
     */
    @TableField("graduation_status")
    private Integer graduationStatus;
    @TableField("from_class_id")
    private String fromClassId;
    @TableField("classroom_id")
    private String classroomId;
    @TableField("classroom_name")
    private String classroomName;
    /**
     * 升级状态
     */
    @TableField(exist = false)
    private String statusName;

    @TableField(exist = false)
    private String graduationStatusName;
   @TableField(exist = false)
     private Integer studentCount;
    @TableField(exist = false)
    private String phase;
    @TableField(exist = false)
    private String grade;
    @TableField(exist = false)
    private String classTypeName;
    @TableField(exist = false)
    private String stuName;
    @TableField(exist = false)
    private String periodName;

    @TableField(exist = false)
    private String classIds;
    @TableField(exist = false)
    private String classId;
    public String getClassIds() {
        return classIds;
    }

    public void setClassIds(String classIds) {
        this.classIds = classIds;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getPeriodName() {
        return periodName;
    }

    public void setPeriodName(String periodName) {
        this.periodName = periodName;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public String getClassTypeName() {
        return classTypeName;
    }

    public void setClassTypeName(String classTypeName) {
        this.classTypeName = classTypeName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getGraduationStatusName() {
        return graduationStatusName;
    }

    public void setGraduationStatusName(String graduationStatusName) {
        this.graduationStatusName = graduationStatusName;
    }

    public Integer getStudentCount() {
        return studentCount;
    }

    public void setStudentCount(Integer studentCount) {
        this.studentCount = studentCount;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getClassType() {
        return classType;
    }

    public void setClassType(Integer classType) {
        this.classType = classType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public Integer getPhaseId() {
        return phaseId;
    }

    public void setPhaseId(Integer phaseId) {
        this.phaseId = phaseId;
    }

    public Integer getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(Integer subjectType) {
        this.subjectType = subjectType;
    }

    public Integer getGradeId() {
        return gradeId;
    }

    public void setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getCrtId() {
        return crtId;
    }

    public void setCrtId(String crtId) {
        this.crtId = crtId;
    }

    public Date getCrtDttm() {
        return crtDttm;
    }

    public void setCrtDttm(Date crtDttm) {
        this.crtDttm = crtDttm;
    }

    public String getLastupId() {
        return lastupId;
    }

    public void setLastupId(String lastupId) {
        this.lastupId = lastupId;
    }

    public Date getLastupDttm() {
        return lastupDttm;
    }

    public void setLastupDttm(Date lastupDttm) {
        this.lastupDttm = lastupDttm;
    }

    public Integer getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(Integer schoolYear) {
        this.schoolYear = schoolYear;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getGraduationStatus() {
        return graduationStatus;
    }

    public void setGraduationStatus(Integer graduationStatus) {
        this.graduationStatus = graduationStatus;
    }

    public String getFromClassId() {
        return fromClassId;
    }

    public void setFromClassId(String fromClassId) {
        this.fromClassId = fromClassId;
    }

    public String getClassroomId() {
        return classroomId;
    }

    public void setClassroomId(String classroomId) {
        this.classroomId = classroomId;
    }

    public String getClassroomName() {
        return classroomName;
    }

    public void setClassroomName(String classroomName) {
        this.classroomName = classroomName;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PtClass{" +
        ", id=" + id +
        ", classType=" + classType +
        ", name=" + name +
        ", orgId=" + orgId +
        ", orgName=" + orgName +
        ", period=" + period +
        ", phaseId=" + phaseId +
        ", subjectType=" + subjectType +
        ", gradeId=" + gradeId +
        ", teacherId=" + teacherId +
        ", teacherName=" + teacherName +
        ", sort=" + sort +
        ", enable=" + enable +
        ", crtId=" + crtId +
        ", crtDttm=" + crtDttm +
        ", lastupId=" + lastupId +
        ", lastupDttm=" + lastupDttm +
        ", schoolYear=" + schoolYear +
        ", status=" + status +
        ", graduationStatus=" + graduationStatus +
        ", fromClassId=" + fromClassId +
        ", classroomId=" + classroomId +
        ", classroomName=" + classroomName +
        "}";
    }
}
