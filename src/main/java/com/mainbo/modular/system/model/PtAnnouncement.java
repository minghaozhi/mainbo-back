package com.mainbo.modular.system.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author moshang
 * @since 2020-03-11
 */
@TableName("pt_announcement")
public class PtAnnouncement extends Model<PtAnnouncement> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 通知类型：0 系统通知, 1 区域通知 , 2 学校通知
     */
    private Integer type;
    /**
     * 组织id
     */
    @TableField("org_id")
    private String orgId;
    /**
     * 显示的发布者名称
     */
    @TableField("user_name")
    private String userName;
    /**
     * 标题
     */
    private String title;
    /**
     * 内容
     */
    private String content;
    /**
     * 附件id列表，用#分割
     */
    private String attachs;
    /**
     * 是否是草稿： 0 否,1 是
     */
    @TableField("is_draft")
    private Integer isDraft;
    /**
     * 是否在学校首页显示：0 不显示，1 显示
     */
    @TableField("is_display")
    private Integer isDisplay;
    /**
     * 创建人
     */
    @TableField("crt_id")
    private String crtId;
    /**
     * 创建时间
     */
    @TableField("crt_dttm")
    private Date crtDttm;
    /**
     * 最后更新用户
     */
    @TableField("lastup_id")
    private String lastupId;
    /**
     * 是否可用：1、可用，0、不可用
     */
    private Integer enable;
    /**
     * 最后更新时间
     */
    @TableField("lastup_dttm")
    private Date lastupDttm;
    /**
     * 是否置顶
     */
    @TableField("is_top")
    private Integer isTop;
    /**
     * 摘要
     */
    private String brief;
    /**
     * 自定义通知公告类型
     */
    @TableField("tzgg_type")
    private Integer tzggType;
    @TableField(exist = false)
    private String flags;
    @TableField(exist = false)
    private String tzggTypeName;

    public String getTzggTypeName() {
        return tzggTypeName;
    }

    public void setTzggTypeName(String tzggTypeName) {
        this.tzggTypeName = tzggTypeName;
    }

    public String getFlags() {
        return flags;
    }

    public void setFlags(String flags) {
        this.flags = flags;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAttachs() {
        return attachs;
    }

    public void setAttachs(String attachs) {
        this.attachs = attachs;
    }

    public Integer getIsDraft() {
        return isDraft;
    }

    public void setIsDraft(Integer isDraft) {
        this.isDraft = isDraft;
    }

    public Integer getIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(Integer isDisplay) {
        this.isDisplay = isDisplay;
    }

    public String getCrtId() {
        return crtId;
    }

    public void setCrtId(String crtId) {
        this.crtId = crtId;
    }

    public Date getCrtDttm() {
        return crtDttm;
    }

    public void setCrtDttm(Date crtDttm) {
        this.crtDttm = crtDttm;
    }

    public String getLastupId() {
        return lastupId;
    }

    public void setLastupId(String lastupId) {
        this.lastupId = lastupId;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public Date getLastupDttm() {
        return lastupDttm;
    }

    public void setLastupDttm(Date lastupDttm) {
        this.lastupDttm = lastupDttm;
    }

    public Integer getIsTop() {
        return isTop;
    }

    public void setIsTop(Integer isTop) {
        this.isTop = isTop;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public Integer getTzggType() {
        return tzggType;
    }

    public void setTzggType(Integer tzggType) {
        this.tzggType = tzggType;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PtAnnouncementList{" +
        ", id=" + id +
        ", type=" + type +
        ", orgId=" + orgId +
        ", userName=" + userName +
        ", title=" + title +
        ", content=" + content +
        ", attachs=" + attachs +
        ", isDraft=" + isDraft +
        ", isDisplay=" + isDisplay +
        ", crtId=" + crtId +
        ", crtDttm=" + crtDttm +
        ", lastupId=" + lastupId +
        ", enable=" + enable +
        ", lastupDttm=" + lastupDttm +
        ", isTop=" + isTop +
        ", brief=" + brief +
        ", tzggType=" + tzggType +
        "}";
    }
}
