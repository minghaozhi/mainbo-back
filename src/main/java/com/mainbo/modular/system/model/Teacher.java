/**
 * Mainbo.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */

package com.mainbo.modular.system.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.mainbo.modular.system.model.vo.ExportInfoVo;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;


import java.util.Date;
import java.util.List;

/**
 * 教师 Entity
 * 
 * <pre>
 *
 * </pre>
 *
 * @author Generate Tools
 * @version $Id: Teacher.java, v 1.0 2016-08-17 Generate Tools Exp $
 */
@SuppressWarnings("serial")
@TableName(value = Teacher.TABLE_NAME)
@Getter
@Setter
public class Teacher extends CommUser {
  public static final String TABLE_NAME = "pt_teacher";

  /**
   * 工号
   **/
  @TableField("job_number")
  private String jobNumber;

  /**
   * 曾用名
   **/
  @TableField("old_name")
  private String oldName;

  /**
   * 婚姻状况码(GB T 2261.2)
   **/
  @TableField("marital_status")
  private String maritalStatus;

  /**
   * 港澳台侨外码(港澳台侨外代码表)
   **/
  @TableField("gatqwm")
  private String gatqwm;

  /**
   * 政治面貌码(政治面貌代码表 GB/T 4762)
   **/
  @TableField("political_status")
  private String politicalStatus;

  /**
   * 健康状况码(健康状况代码 采用1位数字代码 GB T 2261.3)
   **/
  @TableField("health_status")
  private String healthStatus;


  @TableField("last_school_card")
  private String lastSchoolCrad;

  /**
   * 校卡号
   */
  @TableField("school_card")
  private String schoolCard;

  /**
   * 信仰宗教码(信仰宗教码 GA 214.12)
   **/
  @TableField("religious_code")
  private String religiousCode;

  /**
   * 身份证件有效期(格式： YYYYMMDD-YYYYMMDD)
   **/
  @TableField("papers_expire")
  private String papersExpire;

  /**
   * 机构号
   **/
  @TableField("org_number")
  private String orgNumber;

  /**
   * 家庭住址
   **/
  @TableField("family_address")
  private String familyAddress;

  /**
   * 户口性质码(户口类别代码 GA 324.1)
   **/
  @TableField("domicile_nature")
  private String domicileNature;

  /**
   * 学历码(学历代码 GB/T 4658)
   **/
  @TableField("education_code")
  private String educationCode;

  /**
   * 参加工作年月
   **/
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  @TableField("join_work_date")
  private Date joinWorkDate;

  /**
   * 来校年月
   **/
  @TableField("join_school_date")
  private String joinSchoolDate;

  /**
   * 从教年月
   **/
  @TableField("teaching_date")
  private String teachingDate;

  /**
   * 编制类别码(JY/T 1001 ZXXBZLB 中小学 编制类别代码)
   **/
  @TableField("preparation_category")
  private String preparationCategory;

  /**
   * 档案编号(存档案部门为本人 档案确定的管理编 号)
   **/
  @TableField("record_code")
  private String recordCode;

  /**
   * 档案文本
   **/
  @TableField("record_content")
  private String recordContent;

  /**
   * 通信地址
   **/
  @TableField("contact_address")
  private String contactAddress;

  /**
   * 主页地址
   **/
  @TableField("home_page")
  private String homePage;

  /**
   * 教师简介
   **/
  @TableField("introduction")
  private String introduction;

  /**
   * 特长
   **/
  @TableField("specialty")
  private String specialty;

  /**
   * 教师资格证码
   **/
  @TableField("seniority_code")
  private String seniorityCode;

  @TableField("regis_role")
  private String regisRole;

  @TableField("regis_filed")
  private String regisFiled;

  @TableField("professional_titles")
  private String professionalTitles;

  @TableField(exist = false)
  private List<PtUserRole> userRoleList;
  //职务
@TableField(exist = false)
  private String job;


}
