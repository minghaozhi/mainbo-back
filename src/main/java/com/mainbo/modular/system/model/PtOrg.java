package com.mainbo.modular.system.model;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 机构数据类
 * </p>
 *
 * @author moshang
 * @since 2020-02-21
 */
@TableName("pt_org")
public class PtOrg extends Model<PtOrg> {

    private static final long serialVersionUID = 1L;
    /**
     * 学校
     */
    public static final int TP_SCHOOL = 1;

    /**
     * 区域机构
     */
    public static final int TP_ORG = 0;

    /**
     * 部门
     */
    public static final int TP_DEP = 2;
    /**
     * 机构数据类ID
     */
    private String id;
    /**
     * 地域ID
     */
    @TableField("area_id")
    private Integer areaId;
    /**
     * 机构数据类父ID
     */
    private String pid;
    /**
     * 机构名称
     */
    private String name;
    /**
     * 机构简称
     */
    @TableField("short_name")
    private String shortName;
    /**
     * 区域层级id
     */
    @TableField("area_ids")
    private String areaIds;
    /**
     * 机构地址
     */
    private String address;
    /**
     * 机构电话
     */
    private String phone;
    /**
     * 简介
     */
    private String note;
    /**
     * 分站点ID
     */
    @TableField("site_id")
    private String siteId;
    /**
     * 机构类型，0代表机构，1代表学校,2代表部门
     */
    private Integer type;
    /**
     * 机构号编码
     */
    private String code;
    /**
     * 邮编
     */
    @TableField("post_code")
    private String postCode;
    /**
     * 主页地址
     */
    private String website;
    /**
     * 负责人姓名
     */
    @TableField("leader_name")
    private String leaderName;
    /**
     * 负责人电话
     */
    @TableField("leader_phone")
    private String leaderPhone;
    /**
     * 机构状态(0:正常，1删除)
     */
    private Boolean deleted;
    /**
     * 创建时间
     */
    @TableField("crt_dttm")
    private Date crtDttm;
    /**
     * 创建人ID
     */
    @TableField("crt_id")
    private String crtId;
    /**
     * 最后更新时间
     */
    @TableField("lastup_dttm")
    private Date lastupDttm;
    /**
     * 最后更新人ID
     */
    @TableField("lastup_id")
    private String lastupId;
    /**
     * 有效性,状态(1:正常，0：禁用)
     */
    private Boolean enable;
    /**
     * 学校学制类型
     */
    private Integer schoolings;
    /**
     * 学段类型l列表，以','分隔
     */
    @TableField("phase_types")
    private String phaseTypes;
    private String image;
    /**
     * 禁用或冻结原因
     */
    @TableField("disable_reason")
    private String disableReason;
    /**
     * 备注
     */
    private String remark;
    /**
     * 校园门户是否使用的是自有的门户地址，1：是的； 0：不是，即系统提供校园门户模板
     */
    @TableField("self_website")
    private Integer selfWebsite;
    /**
     * 学校域名信息绑定
     */
    @TableField("sch_host")
    private String schHost;
    /**
     * 学校模板类型，与前端约定的模板，1、2、3...
     */
    @TableField("sch_model")
    private Integer schModel;
    /**
     * 学校排序设置
     */
    private Integer sort;
    @TableField("is_display")
    private Integer isDisplay;
    /**
     * 学校类型，0代表会员校，1代表体验校,2代表演示校
     */
    @TableField("org_type")
    private Integer orgType;
    /**
     * 是否自定义期限,0代表无限制,1代表限制
     */
    @TableField("is_choice")
    private Integer isChoice;
    /**
     * 学校结束时间
     */
    @TableField("end_time")
    private String endTime;
    /**
     * 学校类型，0代表云校园，1代表新高考
     */
    @TableField("school_type")
    private Integer schoolType;
    @TableField(exist = false)
    private String areaName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getAreaIds() {
        return areaIds;
    }

    public void setAreaIds(String areaIds) {
        this.areaIds = areaIds;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getLeaderName() {
        return leaderName;
    }

    public void setLeaderName(String leaderName) {
        this.leaderName = leaderName;
    }

    public String getLeaderPhone() {
        return leaderPhone;
    }

    public void setLeaderPhone(String leaderPhone) {
        this.leaderPhone = leaderPhone;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Date getCrtDttm() {
        return crtDttm;
    }

    public void setCrtDttm(Date crtDttm) {
        this.crtDttm = crtDttm;
    }

    public String getCrtId() {
        return crtId;
    }

    public void setCrtId(String crtId) {
        this.crtId = crtId;
    }

    public Date getLastupDttm() {
        return lastupDttm;
    }

    public void setLastupDttm(Date lastupDttm) {
        this.lastupDttm = lastupDttm;
    }

    public String getLastupId() {
        return lastupId;
    }

    public void setLastupId(String lastupId) {
        this.lastupId = lastupId;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Integer getSchoolings() {
        return schoolings;
    }

    public void setSchoolings(Integer schoolings) {
        this.schoolings = schoolings;
    }

    public String getPhaseTypes() {
        return phaseTypes;
    }

    public void setPhaseTypes(String phaseTypes) {
        this.phaseTypes = phaseTypes;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDisableReason() {
        return disableReason;
    }

    public void setDisableReason(String disableReason) {
        this.disableReason = disableReason;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getSelfWebsite() {
        return selfWebsite;
    }

    public void setSelfWebsite(Integer selfWebsite) {
        this.selfWebsite = selfWebsite;
    }

    public String getSchHost() {
        return schHost;
    }

    public void setSchHost(String schHost) {
        this.schHost = schHost;
    }

    public Integer getSchModel() {
        return schModel;
    }

    public void setSchModel(Integer schModel) {
        this.schModel = schModel;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(Integer isDisplay) {
        this.isDisplay = isDisplay;
    }

    public Integer getOrgType() {
        return orgType;
    }

    public void setOrgType(Integer orgType) {
        this.orgType = orgType;
    }

    public Integer getIsChoice() {
        return isChoice;
    }

    public void setIsChoice(Integer isChoice) {
        this.isChoice = isChoice;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getSchoolType() {
        return schoolType;
    }

    public void setSchoolType(Integer schoolType) {
        this.schoolType = schoolType;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PtOrg{" +
        ", id=" + id +
        ", areaId=" + areaId +
        ", pid=" + pid +
        ", name=" + name +
        ", shortName=" + shortName +
        ", areaIds=" + areaIds +
        ", address=" + address +
        ", phone=" + phone +
        ", note=" + note +
        ", siteId=" + siteId +
        ", type=" + type +
        ", code=" + code +
        ", postCode=" + postCode +
        ", website=" + website +
        ", leaderName=" + leaderName +
        ", leaderPhone=" + leaderPhone +
        ", deleted=" + deleted +
        ", crtDttm=" + crtDttm +
        ", crtId=" + crtId +
        ", lastupDttm=" + lastupDttm +
        ", lastupId=" + lastupId +
        ", enable=" + enable +
        ", schoolings=" + schoolings +
        ", phaseTypes=" + phaseTypes +
        ", image=" + image +
        ", disableReason=" + disableReason +
        ", remark=" + remark +
        ", selfWebsite=" + selfWebsite +
        ", schHost=" + schHost +
        ", schModel=" + schModel +
        ", sort=" + sort +
        ", isDisplay=" + isDisplay +
        ", orgType=" + orgType +
        ", isChoice=" + isChoice +
        ", endTime=" + endTime +
        ", schoolType=" + schoolType +
        "}";
    }
}
