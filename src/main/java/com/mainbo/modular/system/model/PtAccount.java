package com.mainbo.modular.system.model;

import java.beans.Transient;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author moshang
 * @Date 2020-02-17 21:23:57
 */
@TableName("pt_account")
@Data
public class PtAccount extends Model<PtAccount> {

    private static final long serialVersionUID = 1L;
    public static final String MOBILE_PHONE_NUMBER_PATTERN = "^0{0,1}(1[3-9])[0-9]{9}$";
    public static final String USERNAME_PATTERN = "^[\\u4E00-\\u9FA5\\uf900-\\ufa2d_a-zA-Z][\\u4E00-\\u9FA5\\uf900-\\ufa2d\\w]{1,19}$";
    /**
     * 系统用户
     */
    public static final int SYSTEM_USER = 1;

    /**
     * 区域用户
     */
    public static final int AREA_USER = 2;

    /**
     * 学校教职工用户
     */
    public static final int TEACHER_USER = 3;

    /**
     * 学校学生用户
     */
    public static final int STUDENT_USER = 4;

    /**
     * 通用用户
     */
    public static final int PARENTS_USER = 5;
    /**
     * 用户id
     */
    @TableId(type=IdType.UUID)
    private String id;
    @TableField("user_type")
    private Integer userType;
    /**
     * 验证邮箱
     */
    private String email;
    /**
     * 验证电话
     */
    private String cellphone;
    /**
     * 学校信息表id
     */
    @TableField("org_id")
    private String orgId;
    /**
     * 登录帐号
     */
    private String account;
    /**
     * 帐号密码
     */
    private String password;
    /**
     * 第三方登录代码
     */
    private String logincode;
    /**
     * 安全盐值
     */
    private String salt;
    /**
     * 最近登录时间
     */
    @TableField("last_login")
    private Date lastLogin;
    private Boolean deleted;
    /**
     * 帐号状态(0:正常，1：禁用 默认0)
     */
    private Boolean enable;
    /**
     * 禁用或冻结原因
     */
    @TableField("disable_reason")
    private String disableReason;
    /**
     * 备注
     */
    private String remark;
    /**
     * 用户信息
     */
    @TableField(exist = false)
    private CommUser userInfo;

    @TableField(exist = false)
    private String flago;

    @TableField(exist = false)
    private String flags;
    @TableField(exist = false)
    private String photo;
    @TableField(exist = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;
    @TableField(exist = false)
    private String name;
    @TableField(exist = false)
    private String telephone;
    @TableField(exist = false)
    private Integer sex;
    @Override
    protected Serializable pkVal() {
       return this.id;
    }
}
