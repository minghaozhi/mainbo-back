package com.mainbo.modular.system.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.mainbo.modular.system.model.meta.vo.Meta;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author moshang
 * @since 2020-02-25
 */
@TableName("pt_config")
public class PtConfig extends Model<PtConfig> implements Meta {

    private static final long serialVersionUID = 1L;

    /**
     * 扩展性及个性化站点配置id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 配置代码
     */
    private String code;
    /**
     * 配置名称
     */
    private String name;
    /**
     * 配置内容
     */
    private String value;
    /**
     * 配置应用的域名
     */
    private String domain;
    /**
     * 配置分组类型
     */
    private String type;
    private String descs;
    private Integer phase;
    /**
     * 所属机构id
     */
    @TableField("org_id")
    private String orgId;
    /**
     * 1 启用， 0 禁用
     */
    private Integer enable;
    /**
     * 是否允许个性化： 1 允许，0 不允许
     */
    @TableField("can_override")
    private Boolean canOverride;
    /**
     * 创建时间
     */
    @TableField("crt_dttm")
    private Date crtDttm;
    /**
     * 最后更新时间
     */
    @TableField("lastup_dttm")
    private Date lastupDttm;
    /**
     * 创建人ID
     */
    @TableField("crt_id")
    private String crtId;
    /**
     * 最后更新人ID
     */
    @TableField("lastup_id")
    private String lastupId;
    private Integer sort;
    @TableField("subject_id")
    private Integer subjectId;
    @TableField("area_id")
    private Integer areaId;
    @TableField("grade_id")
    private Integer gradeId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescs() {
        return descs;
    }

    public void setDescs(String descs) {
        this.descs = descs;
    }

    public Integer getPhase() {
        return phase;
    }

    public void setPhase(Integer phase) {
        this.phase = phase;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public Boolean getCanOverride() {
        return canOverride;
    }

    public void setCanOverride(Boolean canOverride) {
        this.canOverride = canOverride;
    }

    public Date getCrtDttm() {
        return crtDttm;
    }

    public void setCrtDttm(Date crtDttm) {
        this.crtDttm = crtDttm;
    }

    public Date getLastupDttm() {
        return lastupDttm;
    }

    public void setLastupDttm(Date lastupDttm) {
        this.lastupDttm = lastupDttm;
    }

    public String getCrtId() {
        return crtId;
    }

    public void setCrtId(String crtId) {
        this.crtId = crtId;
    }

    public String getLastupId() {
        return lastupId;
    }

    public void setLastupId(String lastupId) {
        this.lastupId = lastupId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getGradeId() {
        return gradeId;
    }

    public void setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PtConfig{" +
        ", id=" + id +
        ", code=" + code +
        ", name=" + name +
        ", value=" + value +
        ", domain=" + domain +
        ", type=" + type +
        ", descs=" + descs +
        ", phase=" + phase +
        ", orgId=" + orgId +
        ", enable=" + enable +
        ", canOverride=" + canOverride +
        ", crtDttm=" + crtDttm +
        ", lastupDttm=" + lastupDttm +
        ", crtId=" + crtId +
        ", lastupId=" + lastupId +
        ", sort=" + sort +
        ", subjectId=" + subjectId +
        ", areaId=" + areaId +
        ", gradeId=" + gradeId +
        "}";
    }
}
