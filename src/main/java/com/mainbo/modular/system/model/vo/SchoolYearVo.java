package com.mainbo.modular.system.model.vo;

import com.mainbo.modular.system.model.PtConfig;
import com.mainbo.modular.system.model.meta.vo.Meta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/3/9   16:21
 **/
public class SchoolYearVo {
    private static final Logger logger = LoggerFactory.getLogger(SchoolYearVo.class);
    private Integer id;
    private String name;
    private String startYear;
    private String endYear;
    private String startTime;
    private String endTime;
    private String orgId;
    private Integer termSize;
    private String termName;
    private Integer termId;
    private Integer sysTermId;
    private String termStartTime;
    private String termEndTime;
    private String teachStartTime;
    private String teachEndTime;
    private Integer weeks;
    private Integer areaId;
    private String crtId;
    private Date crtDttm;
    private String termScope;
    private String teacherScope;
    /**
     *
     */
    public SchoolYearVo() {

    }

    public String getCrtId() {
        return crtId;
    }

    public void setCrtId(String crtId) {
        this.crtId = crtId;
    }

    public Date getCrtDttm() {
        return crtDttm;
    }

    public void setCrtDttm(Date crtDttm) {
        this.crtDttm = crtDttm;
    }

    /**
     *
     * @param meta
     *          Meta
     */
    public SchoolYearVo(Meta meta) {
        try {
            PtConfig config = (PtConfig) meta;
            this.id = config.getId();
            String xnName = config.getName();
            String[] split = xnName.split("-");
            this.startYear = split[0];
            this.endYear = split[1];
            this.name = xnName.replace("-", "至").concat("学年");
            String value = config.getValue();
            String[] times = value.split("-");
            this.startTime = times[0];
            this.endTime = times[1];
            this.orgId=config.getOrgId();
        } catch (Exception e) {
            logger.info("parse schoolYear failed");
        }
    }

    public String getTermScope() {
        return termScope;
    }

    public void setTermScope(String termScope) {
        this.termScope = termScope;
    }

    public String getTeacherScope() {
        return teacherScope;
    }

    public void setTeacherScope(String teacherScope) {
        this.teacherScope = teacherScope;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>id</tt>.
     *
     * @return id Integer
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>id</tt>.
     *
     * @param id
     *          Integer value to be assigned to property id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>name</tt>.
     *
     * @return name String
     */
    public String getName() {
        return name;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>name</tt>.
     *
     * @param name
     *          String value to be assigned to property name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>startYear</tt>.
     *
     * @return startYear String
     */
    public String getStartYear() {
        return startYear;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>startYear</tt>.
     *
     * @param startYear
     *          String value to be assigned to property startYear
     */
    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>endYear</tt>.
     *
     * @return endYear String
     */
    public String getEndYear() {
        return endYear;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>endYear</tt>.
     *
     * @param endYear
     *          String value to be assigned to property endYear
     */
    public void setEndYear(String endYear) {
        this.endYear = endYear;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>startTime</tt>.
     *
     * @return startTime String
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>startTime</tt>.
     *
     * @param startTime
     *          String value to be assigned to property startTime
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>endTime</tt>.
     *
     * @return endTime String
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>endTime</tt>.
     *
     * @param endTime
     *          String value to be assigned to property endTime
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>orgId</tt>.
     *
     * @return orgId String
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>orgId</tt>.
     *
     * @param orgId
     *          String value to be assigned to property orgId
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>termSize</tt>.
     *
     * @return termSize Integer
     */
    public Integer getTermSize() {
        return termSize;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>termSize</tt>.
     *
     * @param termSize
     *          Integer value to be assigned to property termSize
     */
    public void setTermSize(Integer termSize) {
        this.termSize = termSize;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>termName</tt>.
     *
     * @return termName String
     */
    public String getTermName() {
        return termName;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>termName</tt>.
     *
     * @param termName
     *          String value to be assigned to property termName
     */
    public void setTermName(String termName) {
        this.termName = termName;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>termId</tt>.
     *
     * @return termId Integer
     */
    public Integer getTermId() {
        return termId;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>termId</tt>.
     *
     * @param termId
     *          Integer value to be assigned to property termId
     */
    public void setTermId(Integer termId) {
        this.termId = termId;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>sysTermId</tt>.
     *
     * @return sysTermId Integer
     */
    public Integer getSysTermId() {
        return sysTermId;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>sysTermId</tt>.
     *
     * @param sysTermId
     *          Integer value to be assigned to property sysTermId
     */
    public void setSysTermId(Integer sysTermId) {
        this.sysTermId = sysTermId;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>termStartTime</tt>.
     *
     * @return termStartTime String
     */
    public String getTermStartTime() {
        return termStartTime;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>termStartTime</tt>.
     *
     * @param termStartTime
     *          String value to be assigned to property termStartTime
     */
    public void setTermStartTime(String termStartTime) {
        this.termStartTime = termStartTime;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>termEndTime</tt>.
     *
     * @return termEndTime String
     */
    public String getTermEndTime() {
        return termEndTime;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>termEndTime</tt>.
     *
     * @param termEndTime
     *          String value to be assigned to property termEndTime
     */
    public void setTermEndTime(String termEndTime) {
        this.termEndTime = termEndTime;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>teachStartTime</tt>.
     *
     * @return teachStartTime String
     */
    public String getTeachStartTime() {
        return teachStartTime;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>teachStartTime</tt>.
     *
     * @param teachStartTime
     *          String value to be assigned to property teachStartTime
     */
    public void setTeachStartTime(String teachStartTime) {
        this.teachStartTime = teachStartTime;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>teachEndTime</tt>.
     *
     * @return teachEndTime String
     */
    public String getTeachEndTime() {
        return teachEndTime;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>teachEndTime</tt>.
     *
     * @param teachEndTime
     *          String value to be assigned to property teachEndTime
     */
    public void setTeachEndTime(String teachEndTime) {
        this.teachEndTime = teachEndTime;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>weeks</tt>.
     *
     * @return weeks Integer
     */
    public Integer getWeeks() {
        return weeks;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>weeks</tt>.
     *
     * @param weeks
     *          Integer value to be assigned to property weeks
     */
    public void setWeeks(Integer weeks) {
        this.weeks = weeks;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>areaId</tt>.
     *
     * @return areaId Integer
     */
    public Integer getAreaId() {
        return areaId;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>areaId</tt>.
     *
     * @param areaId
     *          Integer value to be assigned to property areaId
     */
    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

}

