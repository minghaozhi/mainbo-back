package com.mainbo.modular.system.model;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author moshang
 * @since 2020-03-02
 */
@TableName("pt_people_student")
public class PtPeopleStudent extends Model<PtPeopleStudent> {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.UUID)
    private String id;
    /**
     * 家长id
     */
    @TableField("people_id")
    private String peopleId;
    /**
     * 学生id
     */
    @TableField("student_id")
    private String studentId;
    /**
     * 机构id
     */
    @TableField("org_id")
    private String orgId;
    /**
     * 学生姓名
     */
    @TableField("student_name")
    private String studentName;
    @TableField("crt_dttm")
    private Date crtDttm;
    @TableField(exist = false)
    private Integer period;
    @TableField(exist = false)
    private String grade;
    @TableField(exist = false)
    private String className;
    @TableField(exist = false)
    private String classId;

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPeopleId() {
        return peopleId;
    }

    public void setPeopleId(String peopleId) {
        this.peopleId = peopleId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public Date getCrtDttm() {
        return crtDttm;
    }

    public void setCrtDttm(Date crtDttm) {
        this.crtDttm = crtDttm;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PtPeopleStudent{" +
        ", id=" + id +
        ", peopleId=" + peopleId +
        ", studentId=" + studentId +
        ", orgId=" + orgId +
        ", studentName=" + studentName +
        ", crtDttm=" + crtDttm +
        "}";
    }
}
