/**
 * Mainbo.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */

package com.mainbo.modular.system.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;

/**
 * 学生 Entity
 * 
 * <pre>
 *
 * </pre>
 *
 * @author Generate Tools
 * @version $Id: Student.java, v 1.0 2016-08-17 Generate Tools Exp $
 */
@SuppressWarnings("serial")
@TableName(value = Student.TABLE_NAME)
@Getter
@Setter
public class Student extends CommUser {
  public static final String TABLE_NAME = "pt_student";

  /**
   * 学籍号
   **/
  @TableField("student_code")
  private String studentCode;

  /**
   * 特长
   **/
  @TableField("specialty")
  private String specialty;

  /**
   * 政治面貌码(政治面貌代码表 GB/T 4762)
   **/
  @TableField("political_status")
  private String politicalStatus;

  /**
   * 家长姓名
   **/
  @TableField("parents_name")
  private String parentsName;

  /**
   * 家长联系方式
   **/
  @TableField("parents_phone")
  private String parentsPhone;

  @TableField("regis_role")
  private String regisRole;

  @TableField("regis_filed")
  private String regisFiled;

  @TableField("poor_type")
  private Integer poorType;

  @TableField("graduate_state")
  private Integer graduateState;

  @TableField(exist = false)
  private Integer schoolYear;

  @TableField(exist = false)
  private Integer phaseId;
  @TableField(exist = false)
  private String className;
  @TableField(exist = false)
  private Integer areaId;
  @TableField("student_card_id")
  private String studentCardId;

  @TableField("last_card_id")
  private String lastCardId;
  @TableField(exist = false)
  private Boolean flag;

}
