package com.mainbo.modular.system.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author moshang
 * @date 2020-02-22
 **/
public enum SysRole {
    TEACHER(2, 2, "教师"),
    STUDENT(3, 4, "学生"),
    PARRENT(5, 5, "家长"),
    SCHWORKER(301, 2, "学校职工"),
    SCHMANAGER(300, 2, "学校管理者"),
    AREAMANAGER(200, 1, "区域管理者"),
    AREAWORKER(201, 1, "区域职工"),
    ADMIN(1, 3, "管理员"),
    UNKNOWN(0, 0, "未知");

    /**
     * 中文名
     */
    private String cname;
    /**
     * id 关联
     */
    private Integer id;

    /**
     * 应用范围
     */
    private Integer scope;

    /**
     * getter role cn name
     *
     * @return cn name
     */
    public String getCname() {
        return cname;
    }

    /**
     * getter role id
     *
     * @return role id
     */
    public Integer getId() {
        return id;
    }

    SysRole(Integer id, Integer scope, String cname) {
        this.cname = cname;
        this.id = id;
        this.scope = scope;
    }

    /**
     * find role by cn name.
     *
     * @param cname
     *          cn name.
     * @return role
     */
    public static SysRole findByCname(String cname) {
        SysRole sr = UNKNOWN;
        for (SysRole s : values()) {
            if (s.cname.equals(cname)) {
                sr = s;
                break;
            }
        }
        return sr;
    }

    /**
     * Getter method for property <tt>scope</tt>.
     *
     * @return property value of scope
     */
    public Integer getScope() {
        return scope;
    }

    /**
     * 通过角色id获取角色名称
     *
     * @param roleId
     *          role id
     * @return role cn name
     */
    public static String forCname(Integer roleId) {
        SysRole[] roles = SysRole.values();
        for (int i = 0; i < roles.length; i++) {
            if (roles[i].id.equals(roleId)) {
                return roles[i].cname;
            }
        }
        return null;
    }

    /**
     * 通过角色id获取角色名称
     *
     * @param roleId
     *          role id
     * @return role
     */
    public static SysRole getRoleById(Integer roleId) {
        SysRole[] roles = SysRole.values();
        for (int i = 0; i < roles.length; i++) {
            if (roles[i].id.equals(roleId)) {
                return roles[i];
            }
        }
        return null;
    }

    /**
     * 通过角色角色应用范围获取角色类型
     *
     * @param scope
     *          role scope
     * @return scope roles
     */
    public static List<SysRole> listForScope(Integer scope) {
        List<SysRole> roleList = new ArrayList<SysRole>();
        SysRole[] roles = SysRole.values();
        for (int i = 0; i < roles.length; i++) {
            if (scope == 0 || roles[i].scope.equals(scope)) {
                roleList.add(roles[i]);
            }
        }
        return roleList;
    }

    /**
     * 检查角色
     *
     * @param roleid
     *          role id
     * @param roleName
     *          role name
     * @return true if role name equalse roleid 's role name
     */
    public static final boolean checkSysRole(Integer roleid, String roleName) {
        if (roleName != null) {
            SysRole role;
            try {
                role = SysRole.valueOf(roleName.toUpperCase());
            } catch (Exception e) {
                role = SysRole.UNKNOWN;
            }
            return role != SysRole.UNKNOWN && role.getId().equals(roleid);
        }
        return false;
    }

    /**
     * 获得所有角色集合
     *
     * @return all roles
     */
    public static final Map<Integer, String> getAllSysRole() {
        Map<Integer, String> map = new HashMap<Integer, String>();
        for (SysRole s : values()) {
            map.put(s.getId(), s.getCname());
        }
        return map;
    }
}
