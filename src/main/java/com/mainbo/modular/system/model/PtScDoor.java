package com.mainbo.modular.system.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 墨殇
 * @since 2020-03-15
 */
@TableName("pt_sc_door")
public class PtScDoor extends Model<PtScDoor> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    @TableField("org_id")
    private String orgId;
    /**
     * 网站名称
     */
    @TableField("portal_name")
    private String portalName;
    /**
     * 底部信息
     */
    @TableField("portal_bottom")
    private String portalBottom;
    /**
     * 联系方式
     */
    @TableField("portal_contact")
    private String portalContact;
    /**
     * logo
     */
    @TableField("portal_logo")
    private String portalLogo;
    /**
     * 1 启用， 0 禁用
     */
    private Integer enable;
    /**
     * 创建时间
     */
    @TableField("crt_dttm")
    private Date crtDttm;
    @TableField("lastup_dttm")
    private Date lastupDttm;
    @TableField("crt_id")
    private String crtId;
    @TableField("lastup_id")
    private String lastupId;
    @TableField(exist = false)
    private Integer isOrg;

    public Integer getIsOrg() {
        return isOrg;
    }

    public void setIsOrg(Integer isOrg) {
        this.isOrg = isOrg;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getPortalName() {
        return portalName;
    }

    public void setPortalName(String portalName) {
        this.portalName = portalName;
    }

    public String getPortalBottom() {
        return portalBottom;
    }

    public void setPortalBottom(String portalBottom) {
        this.portalBottom = portalBottom;
    }

    public String getPortalContact() {
        return portalContact;
    }

    public void setPortalContact(String portalContact) {
        this.portalContact = portalContact;
    }

    public String getPortalLogo() {
        return portalLogo;
    }

    public void setPortalLogo(String portalLogo) {
        this.portalLogo = portalLogo;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public Date getCrtDttm() {
        return crtDttm;
    }

    public void setCrtDttm(Date crtDttm) {
        this.crtDttm = crtDttm;
    }

    public Date getLastupDttm() {
        return lastupDttm;
    }

    public void setLastupDttm(Date lastupDttm) {
        this.lastupDttm = lastupDttm;
    }

    public String getCrtId() {
        return crtId;
    }

    public void setCrtId(String crtId) {
        this.crtId = crtId;
    }

    public String getLastupId() {
        return lastupId;
    }

    public void setLastupId(String lastupId) {
        this.lastupId = lastupId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PtScDoor{" +
        ", id=" + id +
        ", orgId=" + orgId +
        ", portalName=" + portalName +
        ", portalBottom=" + portalBottom +
        ", portalContact=" + portalContact +
        ", portalLogo=" + portalLogo +
        ", enable=" + enable +
        ", crtDttm=" + crtDttm +
        ", lastupDttm=" + lastupDttm +
        ", crtId=" + crtId +
        ", lastupId=" + lastupId +
        "}";
    }
}
