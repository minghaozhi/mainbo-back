package com.mainbo.modular.system.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author moshang
 * @date 2020-02-22
 **/
public class UserInfo {

    /**
     * <pre>
     *
     * </pre>
     */
    private static final long serialVersionUID = -6659145155423577306L;

    /**
     * 用户id
     **/
    @TableId
    @TableField(value = "id")
    private String id;

    /**
     * 机构数据类ID
     **/
    @TableField(value = "org_id")
    private String orgId;

    /**
     * 机构名称
     */
    @TableField(value = "org_name")
    private String orgName;

    /**
     * 姓名
     **/
    @TableField(value = "name")
    private String name;

    /**
     * 用户头像
     **/
    @TableField(value = "photo")
    private String photo;

    /**
     * 缩略图
     **/
    @TableField(value = "original_photo")
    private String originalPhoto;

    /**
     * 英文姓名
     **/
    @TableField(value = "english_name")
    private String englishName;

    /**
     * 姓名拼音
     **/
    @TableField(value = "name_pell")
    private String namePell;

    /**
     * 性别码(GB T 2261_1 人的性别代码)
     **/
    @TableField(value = "sex")
    private Integer sex;

    /**
     * 出生日期
     **/
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @TableField(value = "birthday")
    private Date birthday;

    /**
     * 出生地码(GB/T 2260)
     **/
    @TableField(value = "birth_place_code")
    private String birthPlaceCode;

    /**
     * 籍贯
     **/
    @TableField(value = "native_place")
    private String nativePlace;

    /**
     * 民族码(GB T 3304)
     **/
    @TableField(value = "nation_code")
    private String nationCode;

    /**
     * 国籍/地区码(GB T 2659)
     **/
    @TableField(value = "country_id")
    private Integer countryId;

    /**
     * 身份证件类型码(身份证件类型代码表 JY_SFZJLX)
     **/
    @TableField(value = "papers_type")
    private Integer papersType;

    /**
     * 身份证件号
     **/
    @TableField(value = "papers_number")
    private String papersNumber;

    /**
     * 血型码(血型代码 JY_XX)
     **/
    @TableField(value = "blood_type")
    private String bloodType;

    /**
     * 现住址
     **/
    @TableField(value = "present_address")
    private String presentAddress;

    /**
     * 户口所在地
     **/
    @TableField(value = "domicile_place")
    private String domicilePlace;

    /**
     * 家庭住址
     **/
    @TableField(value = "family_address")
    private String familyAddress;

    /**
     * 联系电话
     **/
    @TableField(value = "telephone")
    private String telephone;

    /**
     * 手机
     **/
    @TableField(value = "cellphone")
    private String cellphone;

    /**
     * 邮政编码
     **/
    @TableField(value = "post_code")
    private String postCode;

    /**
     * 电子信箱
     **/
    @TableField(value = "email")
    private String email;

    /**
     * 来源应用id,
     **/
    @TableField(value = "app_id")
    private String appId;

    @TableField(exist = false)
    private String accountName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getOriginalPhoto() {
        return originalPhoto;
    }

    public void setOriginalPhoto(String originalPhoto) {
        this.originalPhoto = originalPhoto;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getNamePell() {
        return namePell;
    }

    public void setNamePell(String namePell) {
        this.namePell = namePell;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getBirthPlaceCode() {
        return birthPlaceCode;
    }

    public void setBirthPlaceCode(String birthPlaceCode) {
        this.birthPlaceCode = birthPlaceCode;
    }

    public String getNativePlace() {
        return nativePlace;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace;
    }

    public String getNationCode() {
        return nationCode;
    }

    public void setNationCode(String nationCode) {
        this.nationCode = nationCode;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public Integer getPapersType() {
        return papersType;
    }

    public void setPapersType(Integer papersType) {
        this.papersType = papersType;
    }

    public String getPapersNumber() {
        return papersNumber;
    }

    public void setPapersNumber(String papersNumber) {
        this.papersNumber = papersNumber;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getPresentAddress() {
        return presentAddress;
    }

    public void setPresentAddress(String presentAddress) {
        this.presentAddress = presentAddress;
    }

    public String getDomicilePlace() {
        return domicilePlace;
    }

    public void setDomicilePlace(String domicilePlace) {
        this.domicilePlace = domicilePlace;
    }

    public String getFamilyAddress() {
        return familyAddress;
    }

    public void setFamilyAddress(String familyAddress) {
        this.familyAddress = familyAddress;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
}
