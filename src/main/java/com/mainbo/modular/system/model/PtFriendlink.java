package com.mainbo.modular.system.model;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author stylefeng
 * @since 2020-03-11
 */
@TableName("pt_friendlink")
public class PtFriendlink extends Model<PtFriendlink> {

    private static final long serialVersionUID = 1L;
    //系统banner
    public static final String CODE_SYS_BANNER = "banner";
    //学校banner
    public static final String CODE_SC_BANNER = "sc_banner";
    //校园风采
    public static final String CODE_SC_CAMPUSMEIN= "campus_mein";
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String name;
    private String website;
    private String pic;
    private Integer sort;
    /**
     * 类型：区分图片链接（0）和文字链接（1）
     */
    private Integer type;
    private String code;
    @TableField("org_id")
    private String orgId;
    @TableField(exist = false)
    private Integer isOrg;

    public Integer getIsOrg() {
        return isOrg;
    }

    public void setIsOrg(Integer isOrg) {
        this.isOrg = isOrg;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PtFriendlink{" +
        ", id=" + id +
        ", name=" + name +
        ", website=" + website +
        ", pic=" + pic +
        ", sort=" + sort +
        ", type=" + type +
        ", code=" + code +
        ", orgId=" + orgId +
        "}";
    }
}
