package com.mainbo.modular.system.model;

import lombok.Data;

import java.util.List;

/**
 * 用户通用查询实体
 *
 * @author moshang
 * @date 2020-02-25
 **/
@Data
public class UserSearchModel {

    private static final long serialVersionUID = 2897412539652458186L;

    private Integer areaId;
    private String orgId;
    /**
     * 区域名称
     */
    private String areaName;

    /**
     * 系统角色
     */
    private Integer sysRoleId;

    private Integer roleId;

    private Integer userType;

    /**
     * 机构类型
     */
    private Integer orgType;

    private Integer gradeId;

    private Integer phaseId;

    private Integer subjectId;

    /**
     * 状态
     */
    private Boolean state;

    private List<String> orgIds;
    private String schoolCard;
    private String accountName;
    private String id;
    private String name;
    private String studentName;
}
