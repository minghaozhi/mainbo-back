package com.mainbo.modular.system.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author moshang
 * @since 2020-03-11
 */
@TableName("pt_news_category")
public class PtNewsCategory extends Model<PtNewsCategory> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String name;
    private String descs;
    private Integer sort;
    @TableField("crt_id")
    private String crtId;
    @TableField("crt_dttm")
    private Date crtDttm;
    /**
     * 机构id
     */
    @TableField("org_id")
    private String orgId;
    /**
     * 父code
     */
    @TableField("parent_code")
    private String parentCode;
    @TableField("lastup_id")
    private String lastupId;
    @TableField("lastup_dttm")
    private Date lastupDttm;
    private Integer enable;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescs() {
        return descs;
    }

    public void setDescs(String descs) {
        this.descs = descs;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getCrtId() {
        return crtId;
    }

    public void setCrtId(String crtId) {
        this.crtId = crtId;
    }

    public Date getCrtDttm() {
        return crtDttm;
    }

    public void setCrtDttm(Date crtDttm) {
        this.crtDttm = crtDttm;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getLastupId() {
        return lastupId;
    }

    public void setLastupId(String lastupId) {
        this.lastupId = lastupId;
    }

    public Date getLastupDttm() {
        return lastupDttm;
    }

    public void setLastupDttm(Date lastupDttm) {
        this.lastupDttm = lastupDttm;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "PtNewsCategory{" +
        ", id=" + id +
        ", name=" + name +
        ", descs=" + descs +
        ", sort=" + sort +
        ", crtId=" + crtId +
        ", crtDttm=" + crtDttm +
        ", orgId=" + orgId +
        ", parentCode=" + parentCode +
        ", lastupId=" + lastupId +
        ", lastupDttm=" + lastupDttm +
        ", enable=" + enable +
        "}";
    }
}
