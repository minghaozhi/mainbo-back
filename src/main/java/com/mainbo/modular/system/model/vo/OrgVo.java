package com.mainbo.modular.system.model.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author moshang
 * @date 2020-03-04
 **/
@Getter
@Setter
public class OrgVo {
    private String id;
    private Integer areaId;
    private String pid;
    private String name;
    private String shortName;
    private String areaIds;
    private String address;
    private String phone;
    private String note;
    private String siteId;
    private Integer type;
    private String section;
    private String studyLength;
    private String orgCode;
    private String postCode;
    private String website;
    private Integer status;
    private String areaName;
    private Date crtDttm;
    private Boolean enable;
    private String accountId;
    private Integer orgType;
    private String leaderName;
}
