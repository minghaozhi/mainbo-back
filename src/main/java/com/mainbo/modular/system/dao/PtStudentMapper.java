package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.CommUser;
import com.mainbo.modular.system.model.PtOrg;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mainbo.modular.system.model.UserInfo;
import com.mainbo.modular.system.model.Student;
import com.mainbo.modular.system.model.vo.ExportInfoVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 学生表 Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-02-22
 */
public interface PtStudentMapper extends BaseMapper<Student> {

    CommUser getById(@Param("id")String id);

    List<Student> findStudentByCondition(@Param("student") Student student);

    CommUser getOne(@Param("name")String name, @Param("orgId")String orgId);

    List<ExportInfoVo> findAll( @Param("schoolYear")Integer schoolYear, @Param("orgIds")List<String> orgIds,@Param("orglist") List<PtOrg> orglist, @Param("orgId")String orgId);

    List<Student> findStudentList(@Param("student") Student student);

    void changeStudentGraduationStatus(@Param("userIds") List<String> userIds, @Param("graduationStatus") Integer graduationStatus);
}
