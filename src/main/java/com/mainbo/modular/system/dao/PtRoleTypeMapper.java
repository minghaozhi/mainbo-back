package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.PtRoleType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-02-23
 */
public interface PtRoleTypeMapper extends BaseMapper<PtRoleType> {

    int deleteRolesById(Integer roleId);
}
