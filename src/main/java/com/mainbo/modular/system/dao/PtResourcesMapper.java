package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.PtResources;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-02-24
 */
public interface PtResourcesMapper extends BaseMapper<PtResources> {

}
