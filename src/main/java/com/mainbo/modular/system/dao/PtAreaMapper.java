package com.mainbo.modular.system.dao;

import com.mainbo.core.common.node.ZTreeNode;
import com.mainbo.modular.system.model.PtArea;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 地域表 Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-02-11
 */
public interface PtAreaMapper extends BaseMapper<PtArea> {

    List<ZTreeNode> tree();
}
