package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.PtRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-02-26
 */
public interface PtRoleMapper extends BaseMapper<PtRole> {

}
