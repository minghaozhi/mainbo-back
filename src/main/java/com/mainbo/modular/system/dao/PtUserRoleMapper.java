package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.PtUserRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 切换工作空间 Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-02-22
 */
public interface PtUserRoleMapper extends BaseMapper<PtUserRole> {

    List<Integer> findUserRoles(String accountId);
}
