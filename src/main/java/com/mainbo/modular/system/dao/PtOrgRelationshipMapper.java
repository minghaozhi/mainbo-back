package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.PtOrgRelationship;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-03-04
 */
public interface PtOrgRelationshipMapper extends BaseMapper<PtOrgRelationship> {

    void deleteByOrgId(@Param("orgId") String orgId);
}
