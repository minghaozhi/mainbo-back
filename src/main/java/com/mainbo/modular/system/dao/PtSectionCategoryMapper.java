package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.PtSectionCategory;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-03-10
 */
public interface PtSectionCategoryMapper extends BaseMapper<PtSectionCategory> {

}
