package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.PtAnnouncement;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-03-11
 */
public interface PtAnnouncementMapper extends BaseMapper<PtAnnouncement> {

}
