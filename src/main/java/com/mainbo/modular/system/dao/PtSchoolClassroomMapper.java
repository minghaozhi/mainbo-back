package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.PtSchoolClassroom;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-03-05
 */
public interface PtSchoolClassroomMapper extends BaseMapper<PtSchoolClassroom> {

    List<PtSchoolClassroom> listByBuildingId(@Param("idList") List<String> idList);
}
