package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.*;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mainbo.modular.system.model.vo.ExportInfoVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 教职工表 Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-02-22
 */
public interface PtTeacherMapper extends BaseMapper<Teacher> {

    CommUser getById(@Param("id")String id);

    CommUser getOne(@Param("name") String name,@Param("orgId")  String orgId);

    List<Teacher> findTeacherList(@Param("teacher") Teacher teacher,@Param("um") UserSearchModel um,@Param("orgIds") List<String> orgIds);

    List<ExportInfoVo> findAllTeachs(@Param("orgIds") List<String> orgIds, @Param("orglist") List<PtOrg> orglist, @Param("orgId") String orgId);
}
