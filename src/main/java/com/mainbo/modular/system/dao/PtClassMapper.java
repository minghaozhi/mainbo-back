package com.mainbo.modular.system.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mainbo.modular.system.model.PtClass;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-02-24
 */
public interface PtClassMapper extends BaseMapper<PtClass> {

    List<PtClass> findList(@Param("ptClass") PtClass ptClass);
}
