package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.PtOrg;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 机构数据类 Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-02-21
 */
public interface PtOrgMapper extends BaseMapper<PtOrg> {

    List<PtOrg> listOrgList(@Param("currentUserId") String currentUserId);
}
