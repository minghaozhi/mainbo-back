package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.PtScDoor;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 墨殇
 * @since 2020-03-15
 */
public interface PtScDoorMapper extends BaseMapper<PtScDoor> {

}
