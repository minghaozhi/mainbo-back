package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.PtAccount;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-02-11
 */
public interface PtAccountMapper extends BaseMapper<PtAccount> {

    PtAccount getByAccount(String account);

    void changeAccountEnableStatus(List<String> userIds, boolean enable, String disableReason);
}
