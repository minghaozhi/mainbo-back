package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.PtClassUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 班级用户表 Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-02-28
 */
public interface PtClassUserMapper extends BaseMapper<PtClassUser> {

    List<PtClassUser> findStudents(@Param("classUser") PtClassUser classUser);

    List<PtClassUser> findClassStudentListByClassId(@Param("student") PtClassUser um, @Param("graduateState") Integer graduateState, @Param("classIds") List<String> classIdList);
}
