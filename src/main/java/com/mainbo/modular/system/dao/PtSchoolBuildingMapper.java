package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.PtSchoolBuilding;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-03-09
 */
public interface PtSchoolBuildingMapper extends BaseMapper<PtSchoolBuilding> {

}
