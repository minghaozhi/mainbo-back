package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.CommUser;
import com.mainbo.modular.system.model.PtEduEmp;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mainbo.modular.system.model.UserInfo;
import com.mainbo.modular.system.model.excelUser.EduEmployee;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 机构职工表 Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-02-22
 */
public interface PtEduEmpMapper extends BaseMapper<EduEmployee> {

    CommUser getById(String id);

    CommUser getOne(@Param("name") String name, @Param("orgId") String orgId);
}
