package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.PtUserManagescope;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-03-04
 */
public interface PtUserManagescopeMapper extends BaseMapper<PtUserManagescope> {

}
