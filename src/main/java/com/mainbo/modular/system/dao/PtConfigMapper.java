package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.PtConfig;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-02-25
 */
public interface PtConfigMapper extends BaseMapper<PtConfig> {

}
