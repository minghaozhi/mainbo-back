package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.PtNews;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-03-11
 */
public interface PtNewsMapper extends BaseMapper<PtNews> {

}
