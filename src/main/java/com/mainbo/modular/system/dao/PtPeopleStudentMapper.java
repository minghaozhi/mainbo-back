package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.PtPeopleStudent;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-03-02
 */
public interface PtPeopleStudentMapper extends BaseMapper<PtPeopleStudent> {

}
