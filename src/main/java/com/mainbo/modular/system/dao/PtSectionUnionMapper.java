package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.PtSectionUnion;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-03-10
 */
public interface PtSectionUnionMapper extends BaseMapper<PtSectionUnion> {

    void deleteByCategoryId(@Param("categoryId") String categoryId);

    void updateDelflagByName(@Param("name") String name);

    void disableByName(@Param("enable")Integer enable,@Param("name")String name);
}
