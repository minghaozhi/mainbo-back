package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.PtSectionTime;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-03-10
 */
public interface PtSectionTimeMapper extends BaseMapper<PtSectionTime> {

    void deleteByScId(@Param("sectionCategoryId") String id);
}
