package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.PtFriendlink;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author stylefeng
 * @since 2020-03-11
 */
public interface PtFriendlinkMapper extends BaseMapper<PtFriendlink> {

}
