package com.mainbo.modular.system.dao;

import com.mainbo.modular.system.model.CommUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mainbo.modular.system.model.UserInfo;
import com.mainbo.modular.system.model.People;
import com.mainbo.modular.system.model.UserSearchModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 通用用户 Mapper 接口
 * </p>
 *
 * @author moshang
 * @since 2020-02-22
 */
public interface PtPeopleMapper extends BaseMapper<People> {

    CommUser getById(String id);

    CommUser getOne(@Param("orgId") String orgId, @Param("name")String name, @Param("cellphone")String cellphone);

    List<People> findPeopleByCondition (@Param("um") UserSearchModel um);

    People findOne(@Param("people") People findByPhone);

    List<People> getPeopleDetailInfo(@Param("orgId") String orgId, @Param("areaId")Integer areaId);
}
