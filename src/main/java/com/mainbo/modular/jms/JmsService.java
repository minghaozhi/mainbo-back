package com.mainbo.modular.jms;

import com.mainbo.core.activemq.JmsP2pSender;
import com.mainbo.core.activemq.JmsSender;

/**
 * @author moshang
 * @date 2020-03-01
 **/
public interface JmsService extends JmsSender, JmsP2pSender {

}