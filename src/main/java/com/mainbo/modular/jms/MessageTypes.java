/**
 * Mainbo.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */

package com.mainbo.modular.jms;

/**
 * <pre>
 * 消息类型枚举
 * </pre>
 *
 */
public enum MessageTypes {
  SYS_INITED("sys", "系统初始化完成"),

  ACCOUNT_MODIFY("account", "账号信息变更"),

  USER_ADD("user", "用户添加"),
  USER_UPDATE("user", "用户更新"),
  USER_DELETE("user", "用户删除"),
  USER_BATCH_ADD("user", "用户批量增加"),
  USER_BATCH_UPDATE("user", "用户批量修改"),
  USER_LEAVE("user", "用户离职"),

  USER_ROLE_ADD("userRole", "用户职务添加"),
  USER_ROLE_UPDATE("userRole", "用户职务更新"),
  USER_ROLE_DELETE("userRole", "用户职务删除"),

  USER_MANAGESCOPE_UPDATE("userManagescope", "用户管辖范围更新"),

  ORG_ADD("org", "添加机构"),
  ORG_UPDATE("org", "修改机构"),
  ORG_DEL("org", "删除机构"),
  ORG_BATCH_ADD("org", "批量添加机构"),
  ORG_BATCH_UPDATE("org", "批量更新机构"),

  META_ADD("config", "新增元数据"),
  META_UPDATE("config", "修改元数据"),
  META_DELETE("config", "删除元数据"),

  GRADE_CONFIG_ADD("config", "添加年级"),
  GRADE_CONFIG_DELETE("config", "删除年级"),
  GRADE_CONFIG_UPDATE("config", "修改年级配置"),

  XZGRADE_CONFIG_ADD("config", "添加行政年级"),
  XZGRADE_CONFIG_DELETE("config", "删除行政年级"),
  XZGRADE_CONFIG_UPDATE("config", "修改行政年级配置"),

  SUBJECT_CONFIG_ADD("config", "添加学科"),
  SUBJECT_CONFIG_UPDATE("config", "修改学科配置"),
  SUBJECT_CONFIG_DELETE("config", "删除学科"),

  ORG_CUSTOM_SUBJECT_ADD("config", "学校新增自定义学科"),

  ORG_SUBJECT_CONFIG_ADD("config", "学校添加学科"),

  ORG_SUBJECT_CONFIG_DELETE("config", "学校删除学科"),

  AREA_CUSTOM_SUBJECT_ADD("config", "区域新增自定义学科"),

  AREA_SUBJECT_CONFIG_ADD("config", "区域添加学科"),

  AREA_SUBJECT_CONFIG_DELETE("config", "区域删除学科"),

  ORG_CUSTOM_PUBLISHER_ADD("config", "学校新增自定义出版社"),

  ORG_PUBLISHER_CONFIG_ADD("config", "学校添加出版社"),

  ORG_PUBLISHER_CONFIG_DELETE("config", "学校删除出版社"),

  AREA_CUSTOM_PUBLISHER_ADD("config", "区域新增自定义出版社"),

  AREA_PUBLISHER_CONFIG_ADD("config", "区域添加出版社"),

  AREA_PUBLISHER_CONFIG_DELETE("config", "区域删除出版社"),

  PUBLISHER_CONFIG_ADD("config", "修改出版社配置"),

  PUBLISHER_CONFIG_DELETE("config", "删除出版社"),

  SCHOOL_TYPE_UPDATE("config", "修改学校类型"),
  SCHOOL_TYPE_ADD("config", "新增学校类型"),
  SCHOOL_TYPE_DELETE("config", "删除学校类型"),

  BOOK_EDITION_ADD("config", "添加版本"),
  BOOK_EDITION_UPDATE("config", "修改版本"),
  BOOK_EDITION_DELETE("config", "删除版本"),

  ORG_BOOK_EDITION_ADD("config", "添加学校版本"),
  ORG_BOOK_EDITION_UPDATE("config", "修改学校版本"),
  ORG_BOOK_EDITION_DELETE("config", "学校删除非本校版本"),

  BOOKSYNC_UPDATE("config", "更新书籍同步"),
  BOOKSYNC_ADD("config", "添加书籍同步"),
  BOOKSYNC_DELETE("config", "删除书籍同步"),
  BOOKSYNC_SOFT_DELETE("config", "添加书籍同步删除记录"),
  BOOK_UPDATE("config", "更新书籍"),
  BOOK_ADD("config", "添加书籍"),
  CATALOG_ADD("config", "添加目录"),
  CATALOG_BATCH_ADD("config", "批量导入目录"),
  CATALOG_UPDATE("config", "修改目录"),
  CATALOG_DELETE("config", "删除目录"),

  CLASS_INSERT("class", "新增班级"),
  CLASS_UPDATE("class", "更新班级"),
  CLASS_DELETE("class", "删除班级"),
  CLASS_BATCH_INSERT("class", "批量新增班级"),
  CLASS_BATCH_UPDATE("class", "批量更新班级"),
  CLASS_UPDATE_XJ_CLASS("class", "班级升级"),
  CLASS_GRADUATION("class", "班级毕业"),
  CLASS_STUDENT_GRADUATION("class", "班级学生毕业"),

  CLASS_STUDENT_DELETE("class", "删除班级学生关联关系"),
  CLASS_STUDENT_INSERT("class", "增加班级学生关联关系"),
  CLASS_STUDENT_IMPORT("class", "批量导入班级学生关联关系"),
  CLASS_TEACHER_INSERT("class", "设置班级的任课教师"),
  CLASS_TEACHER_IMPORT("class", "批量导入班级的任课教师"),

  ROLE_ADD("role", "职务添加"),
  ROLE_UPDATE("role", "职务更新"),
  ROLE_DELETE("role", "职务删除"),

  APP_ADD("app", "应用添加"),
  APP_UPDATE("app", "应用更新"),
  APP_DELETE("app", "应用删除"),

  PEOPLE_STUDENT_ADD("peoplestudent", "家长学生添加"),
  PEOPLE_STUDENT_UPDATE("peoplestudent", "家长学生更新"),
  PEOPLE_STUDENT_DELETE("peoplestudent", "家长学生删除"),

  INFORMATION_ADD("information", "应用消息添加"),

  SCHOOLBUILDING_UPDATE("schoolbuilding","教学楼更新"),
  SCHOOLBUILDING_ADD("schoolbuilding","教学楼添加"),
  SCHOOLBUILDING_DELETE("schoolbuilding","教学楼删除"),

  CLASSROOM_UPDATE("classroom","教室更新"),
  CLASSROOM_ADD("classroom","教室添加"),
  CLASSROOM_DELETE("classroom","教室删除"),
  CLASSROOM_SET("classroom","教室设置"),


  SECTIONUNION_UPDATE("sectionunion","节次更新"),
  SECTIONUNION_ADD("sectionunion","节次添加"),
  SECTIONUNION_DELETE("sectionunion","节次删除"),
  SCHOOLYEAR_ADD("config", "学年添加"),
  SCHOOLYEAR_UPDATE("config", "学年修改"),
  SCHOOLYEAR_DELETE("config", "学年删除"),
  TERM_ADD("config", "学期添加"),
  TERM_UPDATE("config", "学期修改"),
  TERM_DELETE("config", "学期删除"),
  ;


  public String desc;
  public String module;

  MessageTypes(String module, String desc) {
    this.desc = desc;
    this.module = module;
  }

}
