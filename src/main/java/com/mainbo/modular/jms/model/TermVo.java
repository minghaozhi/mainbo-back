package com.mainbo.modular.jms.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/3/10   13:36
 **/
public class TermVo {
    private Integer id;
    private String schoolYear;
    private String name;
    private String termStartTime;
    private String termEndTime;
    private String teachStartTime;
    private String teachEndTime;
    private Integer weeks;
    private String crtId;
    private Date crtDttm;
    private String orgId;
    public TermVo() {

    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(String schoolYear) {
        this.schoolYear = schoolYear;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTermStartTime() {
        return termStartTime;
    }

    public void setTermStartTime(String termStartTime) {
        this.termStartTime = termStartTime;
    }

    public String getTermEndTime() {
        return termEndTime;
    }

    public void setTermEndTime(String termEndTime) {
        this.termEndTime = termEndTime;
    }

    public String getTeachStartTime() {
        return teachStartTime;
    }

    public void setTeachStartTime(String teachStartTime) {
        this.teachStartTime = teachStartTime;
    }

    public String getTeachEndTime() {
        return teachEndTime;
    }

    public void setTeachEndTime(String teachEndTime) {
        this.teachEndTime = teachEndTime;
    }

    public Integer getWeeks() {
        return weeks;
    }

    public void setWeeks(Integer weeks) {
        this.weeks = weeks;
    }

    public String getCrtId() {
        return crtId;
    }

    public void setCrtId(String crtId) {
        this.crtId = crtId;
    }

    public Date getCrtDttm() {
        return crtDttm;
    }

    public void setCrtDttm(Date crtDttm) {
        this.crtDttm = crtDttm;
    }
}
