package com.mainbo.modular.jms.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * @author moshang
 * @date 2020-03-04
 **/
public class UserRoleVo implements Serializable {

    private static final long serialVersionUID = -2387939915439681507L;

    private String id;

    private String accountId;

    /**
     * 真实姓名
     **/
    private String name;

    /**
     * 所属机构
     **/
    private String orgId;

    /**
     * 学段id
     **/
    private Integer phaseId;

    /**
     * 年级id
     **/
    private Integer gradeId;

    /**
     * 学科id
     **/
    private Integer subjectId;

    /**
     * 角色类型id
     **/
    private Integer sysRoleId;

    /**
     * 角色id
     **/
    private Integer roleId;

    /**
     * Getter method for property <tt>id</tt>.
     *
     * @return id String
     */
    public String getId() {
        return id;
    }

    /**
     * Setter method for property <tt>id</tt>.
     *
     * @param id
     *          String value to be assigned to property id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Getter method for property <tt>accountId</tt>.
     *
     * @return accountId String
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * Setter method for property <tt>accountId</tt>.
     *
     * @param accountId
     *          String value to be assigned to property accountId
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * Getter method for property <tt>name</tt>.
     *
     * @return name String
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for property <tt>name</tt>.
     *
     * @param name
     *          String value to be assigned to property name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter method for property <tt>orgId</tt>.
     *
     * @return orgId String
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * Setter method for property <tt>orgId</tt>.
     *
     * @param orgId
     *          String value to be assigned to property orgId
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * Getter method for property <tt>phaseId</tt>.
     *
     * @return phaseId Integer
     */
    public Integer getPhaseId() {
        return phaseId;
    }

    /**
     * Setter method for property <tt>phaseId</tt>.
     *
     * @param phaseId
     *          Integer value to be assigned to property phaseId
     */
    public void setPhaseId(Integer phaseId) {
        this.phaseId = phaseId;
    }

    /**
     * Getter method for property <tt>gradeId</tt>.
     *
     * @return gradeId Integer
     */
    public Integer getGradeId() {
        return gradeId;
    }

    /**
     * Setter method for property <tt>gradeId</tt>.
     *
     * @param gradeId
     *          Integer value to be assigned to property gradeId
     */
    public void setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
    }

    /**
     * Getter method for property <tt>subjectId</tt>.
     *
     * @return subjectId Integer
     */
    public Integer getSubjectId() {
        return subjectId;
    }

    /**
     * Setter method for property <tt>subjectId</tt>.
     *
     * @param subjectId
     *          Integer value to be assigned to property subjectId
     */
    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    /**
     * Getter method for property <tt>sysRoleId</tt>.
     *
     * @return sysRoleId Integer
     */
    public Integer getSysRoleId() {
        return sysRoleId;
    }

    /**
     * Setter method for property <tt>sysRoleId</tt>.
     *
     * @param sysRoleId
     *          Integer value to be assigned to property sysRoleId
     */
    public void setSysRoleId(Integer sysRoleId) {
        this.sysRoleId = sysRoleId;
    }

    /**
     * Getter method for property <tt>roleId</tt>.
     *
     * @return roleId Integer
     */
    public Integer getRoleId() {
        return roleId;
    }

    /**
     * Setter method for property <tt>roleId</tt>.
     *
     * @param roleId
     *          Integer value to be assigned to property roleId
     */
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @Override
    public boolean equals(final Object other) {
        if (!(other instanceof UserRoleVo)) {
            return false;
        }
        UserRoleVo castOther = (UserRoleVo) other;
        return new EqualsBuilder().append(id, castOther.id).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).toHashCode();
    }
}