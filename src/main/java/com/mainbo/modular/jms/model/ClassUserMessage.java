package com.mainbo.modular.jms.model;

import com.mainbo.core.activemq.model.PfMessage;
import com.mainbo.modular.system.model.PtClassUser;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author moshang
 * @date 2020-03-01
 **/
@Setter
@Getter
public class ClassUserMessage extends PfMessage<Serializable> {

    private static final long serialVersionUID = -3232162396603465338L;

    public static final Integer TYPE_TEACHER = 0;
    public static final Integer TYPE_STUDENT = 1;

    private String id;

    /**
     * 班级id
     **/
    private String classId;

    private Set<String> classIds;

    private String orgId;

    /**
     * 科目id
     **/
    private Integer subjectId;

    /**
     * 用户姓名
     **/
    private String userName;

    /**
     * 用户id
     **/
    private String userId;

    private String account;

    /**
     * 0 普通教师，1 学生
     **/
    private Integer type;

    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 结束时间
     */
    private Date endTime;

    /**
     * 创建时间
     */
    private Date crtDttm;

    /**
     * 最后修改时间
     */
    private Date lastupDttm;

    /**
     * 有效性
     */
    private Boolean enable;

    /**
     * 学年
     */
    private Integer schoolYear;

    /**
     * 毕业状态
     */
    private Integer graduateState;


    private List<PtClassUser> classUserList;
}