package com.mainbo.modular.jms.model;

import com.mainbo.core.activemq.model.PfMessage;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <pre>
 *
 * </pre>
 *
 */
@Getter
@Setter
public class UserMessage extends PfMessage<Serializable> {

  /**
   * <pre>
   *
   * </pre>
   */
  private static final long serialVersionUID = -4894520885031937908L;

  private AccountVo accountVo;

  private CommUserVo commUserVo;


}
