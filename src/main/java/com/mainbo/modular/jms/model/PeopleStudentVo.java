package com.mainbo.modular.jms.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author moshang
 * @date 2020-03-02
 **/
@Getter
@Setter
public class PeopleStudentVo implements Serializable {

    /**
     * <pre>
     *
     * </pre>
     */
    private static final long serialVersionUID = 2845434635667577300L;

    private String id;

    /**
     * 家长id
     **/
    private String peopleId;

    /**
     * 学生id
     **/
    private String studentId;

    /**
     * 机构id
     **/
    private String orgId;

    /**
     * 学生姓名
     **/
    private String studentName;
    /**
     * 创建时间
     **/
    private Date crtDttm;
}
