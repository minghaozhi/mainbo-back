package com.mainbo.modular.jms.model;

import java.io.Serializable;

/**
 * @author moshang
 * @date 2020-03-04
 **/
public class OrganizationVo implements Serializable {

    /**
     * <pre>
     *
     * </pre>
     */
    private static final long serialVersionUID = -299005195699120685L;

    private String id;

    /**
     * 地域ID
     **/
    private Integer areaId;

    /**
     * 机构数据类父ID
     **/
    private String pid;

    /**
     * 机构名称
     **/
    private String name;

    /**
     * 机构简称
     **/
    private String shortName;

    /**
     * 区域层级id
     **/
    private String areaIds;

    /**
     * 机构地址
     **/
    private String address;

    /**
     * 机构电话
     **/
    private String phone;

    /**
     * 简介
     **/
    private String note;

    /**
     * 分站点ID
     **/
    private String siteId;

    /**
     * 类型
     **/
    private Integer type;

    /**
     * 机构号
     **/
    private String code;

    /**
     * 邮编
     **/
    private String postCode;

    /**
     * 主页地址
     **/
    private String website;

    /**
     * 负责人姓名
     **/
    private String leaderName;

    /**
     * 负责人电话
     **/
    private String leaderPhone;

    /**
     * 学校学制信息
     */
    private Integer schoolings;
    /**
     * 所属学段类型列表
     **/
    private String phaseTypes;
    /**
     * 机构状态(0:正常，1：删除)
     **/
    private Boolean deleted;
    /**
     * 学校图片
     **/
    private String image;
    /**
     * 机构状态(1:正常，0：禁用)
     **/
    private Boolean enable;

    /**
     * <p>
     * </p>
     * Getter method for property <tt>id</tt>.
     *
     * @return id String
     */
    public String getId() {
        return id;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>id</tt>.
     *
     * @param id
     *          String value to be assigned to property id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>areaId</tt>.
     *
     * @return areaId Integer
     */
    public Integer getAreaId() {
        return areaId;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>areaId</tt>.
     *
     * @param areaId
     *          Integer value to be assigned to property areaId
     */
    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>pid</tt>.
     *
     * @return pid String
     */
    public String getPid() {
        return pid;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>pid</tt>.
     *
     * @param pid
     *          String value to be assigned to property pid
     */
    public void setPid(String pid) {
        this.pid = pid;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>name</tt>.
     *
     * @return name String
     */
    public String getName() {
        return name;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>name</tt>.
     *
     * @param name
     *          String value to be assigned to property name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>shortName</tt>.
     *
     * @return shortName String
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>shortName</tt>.
     *
     * @param shortName
     *          String value to be assigned to property shortName
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>areaIds</tt>.
     *
     * @return areaIds String
     */
    public String getAreaIds() {
        return areaIds;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>areaIds</tt>.
     *
     * @param areaIds
     *          String value to be assigned to property areaIds
     */
    public void setAreaIds(String areaIds) {
        this.areaIds = areaIds;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>address</tt>.
     *
     * @return address String
     */
    public String getAddress() {
        return address;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>address</tt>.
     *
     * @param address
     *          String value to be assigned to property address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>phone</tt>.
     *
     * @return phone String
     */
    public String getPhone() {
        return phone;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>phone</tt>.
     *
     * @param phone
     *          String value to be assigned to property phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>note</tt>.
     *
     * @return note String
     */
    public String getNote() {
        return note;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>note</tt>.
     *
     * @param note
     *          String value to be assigned to property note
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>siteId</tt>.
     *
     * @return siteId String
     */
    public String getSiteId() {
        return siteId;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>siteId</tt>.
     *
     * @param siteId
     *          String value to be assigned to property siteId
     */
    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>type</tt>.
     *
     * @return type Integer
     */
    public Integer getType() {
        return type;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>type</tt>.
     *
     * @param type
     *          Integer value to be assigned to property type
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>code</tt>.
     *
     * @return code String
     */
    public String getCode() {
        return code;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>code</tt>.
     *
     * @param code
     *          String value to be assigned to property code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>postCode</tt>.
     *
     * @return postCode String
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>postCode</tt>.
     *
     * @param postCode
     *          String value to be assigned to property postCode
     */
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>website</tt>.
     *
     * @return website String
     */
    public String getWebsite() {
        return website;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>website</tt>.
     *
     * @param website
     *          String value to be assigned to property website
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>leaderName</tt>.
     *
     * @return leaderName String
     */
    public String getLeaderName() {
        return leaderName;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>leaderName</tt>.
     *
     * @param leaderName
     *          String value to be assigned to property leaderName
     */
    public void setLeaderName(String leaderName) {
        this.leaderName = leaderName;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>leaderPhone</tt>.
     *
     * @return leaderPhone String
     */
    public String getLeaderPhone() {
        return leaderPhone;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>leaderPhone</tt>.
     *
     * @param leaderPhone
     *          String value to be assigned to property leaderPhone
     */
    public void setLeaderPhone(String leaderPhone) {
        this.leaderPhone = leaderPhone;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>schoolings</tt>.
     *
     * @return schoolings Integer
     */
    public Integer getSchoolings() {
        return schoolings;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>schoolings</tt>.
     *
     * @param schoolings
     *          Integer value to be assigned to property schoolings
     */
    public void setSchoolings(Integer schoolings) {
        this.schoolings = schoolings;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>phaseTypes</tt>.
     *
     * @return phaseTypes String
     */
    public String getPhaseTypes() {
        return phaseTypes;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>phaseTypes</tt>.
     *
     * @param phaseTypes
     *          String value to be assigned to property phaseTypes
     */
    public void setPhaseTypes(String phaseTypes) {
        this.phaseTypes = phaseTypes;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>deleted</tt>.
     *
     * @return deleted Boolean
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>deleted</tt>.
     *
     * @param deleted
     *          Boolean value to be assigned to property deleted
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>image</tt>.
     *
     * @return image String
     */
    public String getImage() {
        return image;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>image</tt>.
     *
     * @param image
     *          String value to be assigned to property image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>enable</tt>.
     *
     * @return enable Boolean
     */
    public Boolean getEnable() {
        return enable;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>enable</tt>.
     *
     * @param enable
     *          Boolean value to be assigned to property enable
     */
    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

}
