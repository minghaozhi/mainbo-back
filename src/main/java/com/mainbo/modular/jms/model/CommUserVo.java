package com.mainbo.modular.jms.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author moshang
 * @date 2020-03-01
 **/
@Getter
@Setter
public class CommUserVo implements Serializable {

    private static final long serialVersionUID = -6659145155423577306L;

    /**
     * 用户id
     **/
    private String id;

    /**
     * 机构数据类ID
     **/
    private String orgId;

    /**
     * 机构名称
     */
    private String orgName;

    /**
     * 姓名
     **/
    private String name;

    /**
     * 用户头像
     **/
    private String photo;

    /**
     * 缩略图
     **/
    private String originalPhoto;

    /**
     * 英文姓名
     **/
    private String englishName;

    /**
     * 姓名拼音
     **/
    private String namePell;

    /**
     * 性别码(GB T 2261_1 人的性别代码)
     **/
    private Integer sex;

    /**
     * 出生日期
     **/
    private Date birthday;

    /**
     * 出生地码(GB/T 2260)
     **/
    private String birthPlaceCode;

    /**
     * 籍贯
     **/
    private String nativePlace;

    /**
     * 民族码(GB T 3304)
     **/
    private String nationCode;

    /**
     * 国籍/地区码(GB T 2659)
     **/
    private Integer countryId;

    /**
     * 身份证件类型码(身份证件类型代码表 JY_SFZJLX)
     **/
    private Integer papersType;

    /**
     * 身份证件号
     **/
    private String papersNumber;

    /**
     * 血型码(血型代码 JY_XX)
     **/
    private String bloodType;

    /**
     * 现住址
     **/
    private String presentAddress;

    /**
     * 户口所在地
     **/
    private String domicilePlace;

    /**
     * 家庭住址
     **/
    private String familyAddress;

    /**
     * 联系电话
     **/
    private String telephone;

    /**
     * 手机
     **/
    private String cellphone;

    /**
     * 邮政编码
     **/
    private String postCode;

    /**
     * 电子信箱
     **/
    private String email;

    /**
     * 来源应用id,
     **/
    private String appId;

    private String accountName;

    /**
     * 工号
     **/
    private String jobNumber;

    /**
     * 曾用名
     **/
    private String oldName;

    /**
     * 婚姻状况码(GB T 2261.2)
     **/
    private String maritalStatus;

    /**
     * 港澳台侨外码(港澳台侨外代码表)
     **/
    private String gatqwm;

    /**
     * 政治面貌码(政治面貌代码表 GB/T 4762)
     **/
    private String politicalStatus;

    /**
     * 健康状况码(健康状况代码 采用1位数字代码 GB T 2261.3)
     **/
    private String healthStatus;

    /**
     * 信仰宗教码(信仰宗教码 GA 214.12)
     **/
    private String religiousCode;

    /**
     * 身份证件有效期(格式： YYYYMMDD-YYYYMMDD)
     **/
    private String papersExpire;

    /**
     * 机构号
     **/
    private String orgNumber;

    /**
     * 户口性质码(户口类别代码 GA 324.1)
     **/
    private String domicileNature;

    /**
     * 学历码(学历代码 GB/T 4658)
     **/
    private String educationCode;

    /**
     * 参加工作年月
     **/
    private String joinWorkDate;

    /**
     * 编制类别码(JY/T 1001 ZXXBZLB 中小学 编制类别代码)
     **/
    private String preparationCategory;

    /**
     * 档案编号(存档案部门为本人 档案确定的管理编 号)
     **/
    private String recordCode;

    /**
     * 档案文本
     **/
    private String recordContent;

    /**
     * 通信地址
     **/
    private String contactAddress;

    /**
     * 主页地址
     **/
    private String homePage;

    /**
     * 教师简介
     **/
    private String introduction;

    /**
     * 特长
     **/
    private String specialty;

    /**
     * 教师状态(1:禁用，0：启用)
     **/
    private Integer status;

    private String regisRole;

    /**
     * 用户类型(1:系统，5：家长)
     **/
    private Integer userType;

    /**
     * 来校年月
     **/
    private String joinSchoolDate;

    /**
     * 从教年月
     **/
    private String teachingDate;

    /**
     * 教师资格证码
     **/
    private String seniorityCode;

    private String regisFiled;

    /**
     * 学籍号
     **/
    private String studentCode;

    /**
     * 学生卡号
     **/
    private String studentCardId;

    /**
     * 家长姓名
     **/
    private String parentsName;

    /**
     * 家长联系方式
     **/
    private String parentsPhone;

    /**
     * 教师职称
     */
    private String professionalTitles;
}