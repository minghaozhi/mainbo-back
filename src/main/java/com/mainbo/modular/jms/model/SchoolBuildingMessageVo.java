package com.mainbo.modular.jms.model;

import com.mainbo.core.activemq.model.PfMessage;
import com.mainbo.modular.system.model.PtSchoolBuilding;

import java.io.Serializable;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/3/9   10:40
 **/
public class SchoolBuildingMessageVo extends PfMessage<Serializable> {

    private static final long serialVersionUID = 1L;

    private PtSchoolBuilding schoolBuilding;



    public PtSchoolBuilding getSchoolBuilding() {
        return schoolBuilding;
    }

    public void setSchoolBuilding(PtSchoolBuilding schoolBuilding) {
        this.schoolBuilding = schoolBuilding;
    }

    public SchoolBuildingMessageVo(){
        super();
    }

    /**
     *
     * @param msgType
     *          消息类型
     * @param schoolBuilding
     *          com.mainbo.model.Book
     */
    public SchoolBuildingMessageVo(String msgType, PtSchoolBuilding schoolBuilding) {
        super();
        setMessageType(msgType);
        this.schoolBuilding = schoolBuilding;
    }
}