package com.mainbo.modular.jms.model;

import com.mainbo.core.activemq.model.PfMessage;
import com.mainbo.modular.system.model.PtSchoolClassroom;

import java.io.Serializable;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/3/9   11:01
 **/
public class ClassRoomMessage extends PfMessage<Serializable> {

    private static final long serialVersionUID = 1L;

    private PtSchoolClassroom classRoom;

    public PtSchoolClassroom getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(PtSchoolClassroom classRoom) {
        this.classRoom = classRoom;
    }

    public ClassRoomMessage (String msgType, PtSchoolClassroom classRoom){
        super();
        setMessageType(msgType);
        this.classRoom = classRoom;
    }

    public ClassRoomMessage() {
        super();
    }
}

