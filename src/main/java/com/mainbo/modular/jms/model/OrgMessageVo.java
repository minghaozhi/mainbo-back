package com.mainbo.modular.jms.model;

import com.mainbo.core.activemq.model.PfMessage;
import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;

/**
 * @author moshang
 * @date 2020-03-04
 **/
@Getter
@Setter
public class OrgMessageVo extends PfMessage<Serializable> {

    /**
     * <pre>
     *
     * </pre>
     */
    private static final long serialVersionUID = 1L;

    private OrganizationVo organizationVo;
}
