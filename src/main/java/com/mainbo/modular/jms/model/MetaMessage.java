package com.mainbo.modular.jms.model;

import com.mainbo.core.activemq.model.PfMessage;

import java.io.Serializable;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/3/10   14:39
 **/
public class MetaMessage extends PfMessage<Serializable> {

    /**
     * <pre>
     *
     * </pre>
     */
    private static final long serialVersionUID = 1L;
    private MetaVo vo;

    /**
     *
     */
    public MetaMessage() {
        super();
    }

    /**
     * @param messageType 消息类型
     * @param vo          com.mainbo.model.MetaVo
     */
    public MetaMessage(String messageType, MetaVo vo) {
        setMessageType(messageType);
        this.vo = vo;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>vo</tt>.
     *
     * @return vo MetaVo
     */
    public MetaVo getVo() {
        return vo;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>vo</tt>.
     *
     * @param vo MetaVo value to be assigned to property vo
     */
    public void setVo(MetaVo vo) {
        this.vo = vo;
    }

}