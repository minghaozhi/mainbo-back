/**
 * Mainbo.com Inc.
 * Copyright (c) 2015-2017 All Rights Reserved.
 */

package com.mainbo.modular.jms.model;


import com.mainbo.core.activemq.model.PfMessage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 班级管理
 *
 * <pre>
 *
 * </pre>
 *
 */

public class ClassInfoMessage extends PfMessage<Serializable> {

    /**
     * <pre>
     *
     * </pre>
     */
    private static final long serialVersionUID = 8275954366544256919L;

    private String id;

    private Integer classType;

    private String name;

    private String orgId;

    private String orgName;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 届，即年份
     */
    private Integer period;

    private Integer phaseId;

    /**
     * 文理科id
     **/
    private Integer subjectType;

    /**
     * 年级id
     **/
    private Integer gradeId;

    /**
     * 班主任id
     **/
    private String teacherId;

    /**
     * 班主任姓名
     **/
    private String teacherName;

    /**
     * 创建人Id
     */
    private Integer crtId;

    /**
     * 创建时间
     */
    private Date crtDttm;

    /**
     * 最后修改人ID
     */
    private Integer lastupId;

    /**
     * 最后修改时间
     */
    private Date lastupDttm;

    /**
     * 有效性
     */
    private Boolean enable;

    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 结束时间
     */
    private Date endTime;

    /**
     * 学年
     */
    private Integer schoolYear;

    /**
     * 教室id
     */
    private String classroomId;
    /**
     * 从哪个班级升级
     */
    private String fromClassId;

    /**
     * 毕业状态
     */
    private Integer graduateState;

    private List<ClassInfoMessage> updates = new ArrayList<ClassInfoMessage>();

    /**
     * Getter method for property <tt>id</tt>.
     *
     * @return id String
     */
    public String getId() {
        return id;
    }

    /**
     * Setter method for property <tt>id</tt>.
     *
     * @param id String value to be assigned to property id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Getter method for property <tt>classType</tt>.
     *
     * @return classType Integer
     */
    public Integer getClassType() {
        return classType;
    }

    /**
     * Setter method for property <tt>classType</tt>.
     *
     * @param classType Integer value to be assigned to property classType
     */
    public void setClassType(Integer classType) {
        this.classType = classType;
    }

    /**
     * Getter method for property <tt>name</tt>.
     *
     * @return name String
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for property <tt>name</tt>.
     *
     * @param name String value to be assigned to property name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter method for property <tt>orgId</tt>.
     *
     * @return orgId String
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * Setter method for property <tt>orgId</tt>.
     *
     * @param orgId String value to be assigned to property orgId
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * Getter method for property <tt>orgName</tt>.
     *
     * @return orgName String
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * Setter method for property <tt>orgName</tt>.
     *
     * @param orgName String value to be assigned to property orgName
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * Getter method for property <tt>sort</tt>.
     *
     * @return sort Integer
     */
    public Integer getSort() {
        return sort;
    }

    /**
     * Setter method for property <tt>sort</tt>.
     *
     * @param sort Integer value to be assigned to property sort
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /**
     * Getter method for property <tt>period</tt>.
     *
     * @return period Integer
     */
    public Integer getPeriod() {
        return period;
    }

    /**
     * Setter method for property <tt>period</tt>.
     *
     * @param period Integer value to be assigned to property period
     */
    public void setPeriod(Integer period) {
        this.period = period;
    }

    /**
     * Getter method for property <tt>phaseId</tt>.
     *
     * @return phaseId Integer
     */
    public Integer getPhaseId() {
        return phaseId;
    }

    /**
     * Setter method for property <tt>phaseId</tt>.
     *
     * @param phaseId Integer value to be assigned to property phaseId
     */
    public void setPhaseId(Integer phaseId) {
        this.phaseId = phaseId;
    }

    /**
     * Getter method for property <tt>subjectType</tt>.
     *
     * @return subjectType Integer
     */
    public Integer getSubjectType() {
        return subjectType;
    }

    /**
     * Setter method for property <tt>subjectType</tt>.
     *
     * @param subjectType Integer value to be assigned to property subjectType
     */
    public void setSubjectType(Integer subjectType) {
        this.subjectType = subjectType;
    }

    /**
     * Getter method for property <tt>gradeId</tt>.
     *
     * @return gradeId Integer
     */
    public Integer getGradeId() {
        return gradeId;
    }

    /**
     * Setter method for property <tt>gradeId</tt>.
     *
     * @param gradeId Integer value to be assigned to property gradeId
     */
    public void setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
    }

    /**
     * Getter method for property <tt>teacherId</tt>.
     *
     * @return teacherId String
     */
    public String getTeacherId() {
        return teacherId;
    }

    /**
     * Setter method for property <tt>teacherId</tt>.
     *
     * @param teacherId String value to be assigned to property teacherId
     */
    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    /**
     * Getter method for property <tt>teacherName</tt>.
     *
     * @return teacherName String
     */
    public String getTeacherName() {
        return teacherName;
    }

    /**
     * Setter method for property <tt>teacherName</tt>.
     *
     * @param teacherName String value to be assigned to property teacherName
     */
    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    /**
     * Getter method for property <tt>crtId</tt>.
     *
     * @return crtId Integer
     */
    public Integer getCrtId() {
        return crtId;
    }

    /**
     * Setter method for property <tt>crtId</tt>.
     *
     * @param crtId Integer value to be assigned to property crtId
     */
    public void setCrtId(Integer crtId) {
        this.crtId = crtId;
    }

    /**
     * Getter method for property <tt>crtDttm</tt>.
     *
     * @return crtDttm Date
     */
    public Date getCrtDttm() {
        return crtDttm;
    }

    /**
     * Setter method for property <tt>crtDttm</tt>.
     *
     * @param crtDttm Date value to be assigned to property crtDttm
     */
    public void setCrtDttm(Date crtDttm) {
        this.crtDttm = crtDttm;
    }

    /**
     * Getter method for property <tt>lastupId</tt>.
     *
     * @return lastupId Integer
     */
    public Integer getLastupId() {
        return lastupId;
    }

    /**
     * Setter method for property <tt>lastupId</tt>.
     *
     * @param lastupId Integer value to be assigned to property lastupId
     */
    public void setLastupId(Integer lastupId) {
        this.lastupId = lastupId;
    }

    /**
     * Getter method for property <tt>lastupDttm</tt>.
     *
     * @return lastupDttm Date
     */
    public Date getLastupDttm() {
        return lastupDttm;
    }

    /**
     * Setter method for property <tt>lastupDttm</tt>.
     *
     * @param lastupDttm Date value to be assigned to property lastupDttm
     */
    public void setLastupDttm(Date lastupDttm) {
        this.lastupDttm = lastupDttm;
    }

    /**
     * Getter method for property <tt>enable</tt>.
     *
     * @return enable Boolean
     */
    public Boolean getEnable() {
        return enable;
    }

    /**
     * Setter method for property <tt>enable</tt>.
     *
     * @param enable Boolean value to be assigned to property enable
     */
    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    /**
     * Getter method for property <tt>startTime</tt>.
     *
     * @return startTime Date
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * Setter method for property <tt>startTime</tt>.
     *
     * @param startTime Date value to be assigned to property startTime
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * Getter method for property <tt>endTime</tt>.
     *
     * @return endTime Date
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * Setter method for property <tt>endTime</tt>.
     *
     * @param endTime Date value to be assigned to property endTime
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * Getter method for property <tt>schoolYear</tt>.
     *
     * @return schoolYear Integer
     */
    public Integer getSchoolYear() {
        return schoolYear;
    }

    /**
     * Setter method for property <tt>schoolYear</tt>.
     *
     * @param schoolYear Integer value to be assigned to property schoolYear
     */
    public void setSchoolYear(Integer schoolYear) {
        this.schoolYear = schoolYear;
    }

    /**
     * Getter method for property <tt>fromClassId</tt>.
     *
     * @return fromClassId String
     */
    public String getFromClassId() {
        return fromClassId;
    }

    /**
     * Setter method for property <tt>fromClassId</tt>.
     *
     * @param fromClassId String value to be assigned to property fromClassId
     */
    public void setFromClassId(String fromClassId) {
        this.fromClassId = fromClassId;
    }

    /**
     * Getter method for property <tt>graduateState</tt>.
     *
     * @return graduateState Integer
     */
    public Integer getGraduateState() {
        return graduateState;
    }

    /**
     * Setter method for property <tt>graduateState</tt>.
     *
     * @param graduateState Integer value to be assigned to property graduateState
     */
    public void setGraduateState(Integer graduateState) {
        this.graduateState = graduateState;
    }

    /**
     * Getter method for property <tt>updates</tt>.
     *
     * @return updates List&lt;ClassInfoMessage&gt;
     */
    public List<ClassInfoMessage> getUpdates() {
        return updates;
    }

    /**
     * Setter method for property <tt>updates</tt>.
     *
     * @param updates List&lt;ClassInfoMessage&gt; value to be assigned to property updates
     */
    public void setUpdates(List<ClassInfoMessage> updates) {
        this.updates = updates;
    }

    public String getClassroomId() {
        return classroomId;
    }

    public void setClassroomId(String classroomId) {
        this.classroomId = classroomId;
    }
}
