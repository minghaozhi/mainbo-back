package com.mainbo.modular.jms.model;

import com.mainbo.core.activemq.model.PfMessage;
import com.mainbo.modular.system.model.vo.SchoolYearVo;

import java.io.Serializable;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/3/9   17:29
 **/

public class SchoolYearMessage extends PfMessage<Serializable> {
    private static final long serialVersionUID = 1L;

    private SchoolYearVo schoolYearVo;

    public SchoolYearMessage(String type, SchoolYearVo SchoolYearVo) {
        setMessageType(type);
        this.schoolYearVo = SchoolYearVo;
    }

    public SchoolYearVo getSchoolYearVo() {
        return schoolYearVo;
    }

    public void setSchoolYearVo(SchoolYearVo schoolYearVo) {
        this.schoolYearVo = schoolYearVo;
    }
}