package com.mainbo.modular.jms.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author moshang
 * @date 2020-03-01
 **/
@Getter
@Setter
public class AccountVo implements Serializable {

    /**
     * <pre>
     *
     * </pre>
     */
    private static final long serialVersionUID = -3456279245493610774L;

    private String id;

    /**
     * 用户类型id
     **/
    private Integer userType;

    /**
     * 验证邮箱
     **/
    private String email;

    /**
     * 验证电话
     **/
    private String cellphone;

    /**
     * 学校信息表id
     **/
    private String orgId;

    /**
     * 登录帐号
     **/
    private String account;

    /**
     * 真实姓名
     **/
    private String name;

    /**
     * 帐号密码
     **/
    private String password;

    /**
     * 第三方登录代码
     **/
    private String logincode;

    private String salt;

    /**
     * 是否删除
     **/
    private Boolean deleted;

    /**
     * 用户信息
     */
    private CommUserVo userInfo;

    /**
     * 有效性
     */
    private Boolean enable;
}