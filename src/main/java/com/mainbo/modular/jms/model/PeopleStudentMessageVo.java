package com.mainbo.modular.jms.model;

import com.mainbo.core.activemq.model.PfMessage;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author moshang
 * @date 2020-03-02
 **/
@Getter
@Setter
public class PeopleStudentMessageVo extends PfMessage<Serializable> {

    /**
     * <pre>
     *
     * </pre>
     */
    private static final long serialVersionUID = 1L;

    private PeopleStudentVo peopleStudentVo;
}