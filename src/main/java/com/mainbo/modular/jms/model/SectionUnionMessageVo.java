package com.mainbo.modular.jms.model;

import com.mainbo.core.activemq.model.PfMessage;
import com.mainbo.modular.system.model.PtSectionUnion;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/3/10   17:32
 **/
public class SectionUnionMessageVo extends PfMessage<Serializable> {
    private static final long serialVersionUID = 1L;

    private PtSectionUnion sectionUnion;

    private Date beginTime;

    private Date endTime;

    public PtSectionUnion getSectionUnion() {
        return sectionUnion;
    }

    public void setSectionUnion(PtSectionUnion sectionUnion) {
        this.sectionUnion = sectionUnion;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }



    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public SectionUnionMessageVo(String msgType, PtSectionUnion sectionUnion) {
        super();
        setMessageType(msgType);
        this.sectionUnion = sectionUnion;
    }

    public SectionUnionMessageVo(String msgType, PtSectionUnion sectionUnion,Date beginTime,Date endTime) {
        super();
        setMessageType(msgType);
        setBeginTime(beginTime);
        setEndTime(endTime);
        this.sectionUnion = sectionUnion;
    }
    public SectionUnionMessageVo(){
        super();
    }
}
