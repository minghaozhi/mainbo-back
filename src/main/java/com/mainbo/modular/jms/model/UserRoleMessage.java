package com.mainbo.modular.jms.model;

import com.mainbo.core.activemq.model.PfMessage;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author moshang
 * @date 2020-03-04
 **/
@Getter
@Setter
public class UserRoleMessage extends PfMessage<Serializable> {

    /**
     * <pre>
     *
     * </pre>
     */
    private static final long serialVersionUID = 9090891091409716943L;

    private UserRoleVo userRole;
}