package com.mainbo.modular.jms.model;

import java.io.Serializable;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/3/10   14:39
 **/
public class MetaVo implements Serializable {

    public static final String T_SYS = "sys";
    public static final String T_ORG = "org";
    public static final String T_AREA = "area";
    /**
     * <pre>
     *
     * </pre>
     */
    private static final long serialVersionUID = -8441094496645814261L;

    private Integer id;
    private String name;
    private String value;
    private String descs;
    private Integer phase;
    private Integer gradeId;
    private Integer subjectId;
    private String type;
    private Integer areaId;
    private String orgId;
    private Integer sort;
    private String code;
    private String domain;

    /**
     * <p>
     * </p>
     * Getter method for property <tt>id</tt>.
     *
     * @return id Integer
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>id</tt>.
     *
     * @param id
     *          Integer value to be assigned to property id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>name</tt>.
     *
     * @return name String
     */
    public String getName() {
        return name;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>name</tt>.
     *
     * @param name
     *          String value to be assigned to property name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>value</tt>.
     *
     * @return value String
     */
    public String getValue() {
        return value;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>value</tt>.
     *
     * @param value
     *          String value to be assigned to property value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>descs</tt>.
     *
     * @return descs String
     */
    public String getDescs() {
        return descs;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>descs</tt>.
     *
     * @param descs
     *          String value to be assigned to property descs
     */
    public void setDescs(String descs) {
        this.descs = descs;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>phase</tt>.
     *
     * @return phase Integer
     */
    public Integer getPhase() {
        return phase;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>phase</tt>.
     *
     * @param phase
     *          Integer value to be assigned to property phase
     */
    public void setPhase(Integer phase) {
        this.phase = phase;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>gradeId</tt>.
     *
     * @return gradeId Integer
     */
    public Integer getGradeId() {
        return gradeId;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>gradeId</tt>.
     *
     * @param gradeId
     *          Integer value to be assigned to property gradeId
     */
    public void setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>subjectId</tt>.
     *
     * @return subjectId Integer
     */
    public Integer getSubjectId() {
        return subjectId;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>subjectId</tt>.
     *
     * @param subjectId
     *          Integer value to be assigned to property subjectId
     */
    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>type</tt>.
     *
     * @return type String
     */
    public String getType() {
        return type;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>type</tt>.
     *
     * @param type
     *          String value to be assigned to property type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>areaId</tt>.
     *
     * @return areaId Integer
     */
    public Integer getAreaId() {
        return areaId;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>areaId</tt>.
     *
     * @param areaId
     *          Integer value to be assigned to property areaId
     */
    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>orgId</tt>.
     *
     * @return orgId String
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>orgId</tt>.
     *
     * @param orgId
     *          String value to be assigned to property orgId
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>sort</tt>.
     *
     * @return sort Integer
     */
    public Integer getSort() {
        return sort;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>sort</tt>.
     *
     * @param sort
     *          Integer value to be assigned to property sort
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>code</tt>.
     *
     * @return code String
     */
    public String getCode() {
        return code;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>code</tt>.
     *
     * @param code
     *          String value to be assigned to property code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * <p>
     * </p>
     * Getter method for property <tt>domain</tt>.
     *
     * @return domain String
     */
    public String getDomain() {
        return domain;
    }

    /**
     * <p>
     * </p>
     * Setter method for property <tt>domain</tt>.
     *
     * @param domain
     *          String value to be assigned to property domain
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

}
