package com.mainbo.modular.jms.model;

import com.mainbo.core.activemq.model.PfMessage;

import java.io.Serializable;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/3/10   13:37
 **/
public class TermMessage extends PfMessage<Serializable> {
    private static final long serialVersionUID = 1L;

private TermVo termVo;

    public TermMessage(String type, TermVo termVo) {
        setMessageType(type);
        this.termVo = termVo;
    }

    public TermVo getTermVo() {
        return termVo;
    }

    public void setTermVo(TermVo termVo) {
        termVo = termVo;
    }
}