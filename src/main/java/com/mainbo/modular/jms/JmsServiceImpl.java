package com.mainbo.modular.jms;

import com.mainbo.core.activemq.JmsSenderImpl;
import com.mainbo.core.activemq.model.PfMessage;
import com.mainbo.core.activemq.model.PfP2pMessage;
import com.mainbo.core.common.constant.ConfigUtils;
import com.mainbo.modular.system.model.meta.ConfigConstants;
import com.mainbo.modular.system.model.meta.MetaKeys;
import com.mainbo.modular.system.model.meta.MetaUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Map;

/**
 * @author moshang
 * @date 2020-03-01
 **/
public class JmsServiceImpl implements JmsService {

    private static final Logger logger = LoggerFactory
            .getLogger(JmsSenderImpl.class);

    @Resource(name = "jmsSenderImpl")
    private JmsSenderImpl jmsSender;

    @Autowired(required = false)
    @Qualifier("uclassjmsSenderImpl")
    private JmsSenderImpl ucalssJmsSender;

    private String initedCode = ConfigConstants.SYS_INITED_CODE;

    private String initedKey = MetaKeys.SYS_INIT_OVER;

    @Override
    public void sendMessage(String type, Serializable content) {
        if (ConfigUtils.readBoolConfig(initedCode,initedKey,null)) {
            jmsSender.sendMessage(type, content);
            sendUclassMessage(new PfMessage<Serializable>(type, content));
            return;
        }

        logger.info("message sender switch closed!");
    }

    @Override
    public void sendMessage(PfMessage<Serializable> message) {
        if (ConfigUtils.readBoolConfig(initedCode, initedKey, false)) {
            jmsSender.sendMessage(message);
            sendUclassMessage(message);
            return;
        }
        logger.info("message sender switch closed!");
    }

    @Override
    public void sendMessage(PfMessage<Serializable> message,
                            Map<String, Object> params) {
        if (ConfigUtils.readBoolConfig(initedCode, initedKey, false)) {
            jmsSender.sendMessage(message, params);
            sendUclassMessage(message);
            return;
        }
        logger.info("message sender switch closed!");

    }

    @Override
    public void sendMessage(String type, Serializable content,
                            Map<String, Object> params) {
        if (ConfigUtils.readBoolConfig(initedCode, initedKey, false)) {
            jmsSender.sendMessage(type, content, params);
            sendUclassMessage(new PfMessage<Serializable>(type, content));
            return;
        }
        logger.info("message sender switch closed!");

    }

    @Override
    public void sendP2pMessage(String type, String to, Serializable content) {
        jmsSender.sendP2pMessage(type, to, content);
    }

    @Override
    public void sendP2pMessage(String type, String to, Serializable content,
                               Map<String, Object> params) {
        jmsSender.sendP2pMessage(type, to, content);
    }

    @Override
    public void sendP2pMessage(PfP2pMessage<Serializable> message) {
        jmsSender.sendP2pMessage(message);
    }

    @Override
    public void sendP2pMessage(PfP2pMessage<Serializable> message,
                               Map<String, Object> params) {
        jmsSender.sendP2pMessage(message, params);
    }

    private void sendUclassMessage(PfMessage<Serializable> message) {
        if (ConfigUtils.readBoolConfig(MetaKeys.UCLASS_JMS_ENABLE_KEY, false)
                && ucalssJmsSender != null) {
            message.setCorrelationID(
                    ConfigUtils.readConfig(MetaKeys.UCLASS_JMS_APPID_KEY, ""));
            try {
                ucalssJmsSender.sendMessage(message);
            } catch (Exception e) {
                logger.error("send message error!", e);
            }
        }
    }

}

