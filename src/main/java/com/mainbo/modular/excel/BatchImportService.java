package com.mainbo.modular.excel;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/2/26   9:16
 **/
public interface BatchImportService {

    void getRegisterTemplate(Integer templateType, Integer phaseId, String orgId, HttpServletResponse response) throws IOException;

    StringBuilder batchRegiterUser(Integer userType, String orgId, Integer phaseId, MultipartFile file);
}
