package com.mainbo.modular.excel.impl;

import com.mainbo.modular.excel.ExcelBatchService;
import com.mainbo.modular.system.model.PtAccount;
import com.mainbo.modular.system.model.PtAccount;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.springframework.stereotype.Service;

import javax.swing.*;
import java.io.File;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @desc    批量导出用户数据
 * @author moshang
 * @date 2020-02-27
 **/
@Service("batchExportUserServiceImpl")
public class BatchExportUserServiceImpl extends ExcelBatchService {


    @Override
    public int sheetIndex() {
        return 0;
    }

    @Override
    public int titleLine() {
        return 0;
    }

    @Override
    protected ExcelTitle[] titles() {
        return new ExcelTitle[0];
    }

    @Override
    protected void parseRow(Map<ExcelTitle, String> rowValueMap, Map<String, Object> params, Row row,
                            StringBuilder returnMsg) {

    }

    @Override
    public void exportData(OutputStream os, Map<String, Object> params, StringBuilder resultMsg, File template) {
        super.exportData(os, params, resultMsg, template);
    }

    @Override
    protected void fillSheetData(Sheet sheet, Map<String, Object> params, StringBuilder resultMsg) {
        @SuppressWarnings("unchecked")
        List<Map<Integer, String>> data = (List<Map<Integer, String>>) params.get("data");
        Integer userType = (Integer) params.get("userType");

        Row row = null;
        Cell cell = null;
        Workbook wb = sheet.getWorkbook();
        CellStyle style = creatStyle(wb, (short) 14, true);
        CellStyle style2 = creatStyle(wb, (short) 11, false);
        sheet.setDefaultColumnWidth(25);

        // 创建header
        Row title = sheet.createRow(0);

        if (userType != null && userType == PtAccount.STUDENT_USER || userType == PtAccount.TEACHER_USER) {
            if(userType == PtAccount.STUDENT_USER){
                StuExport[] stu = StuExport.values();
                for (int i = 0; i < stu.length; i++) {
                    cell = title.createCell(i);
                    cell.setCellStyle(style);
                    cell.setCellValue(stu[i].getMapNames().get(0));
                }
            }else{
                TeacherExport[] tea = TeacherExport.values();
                for (int i = 0; i < tea.length ; i++) {
                    cell = title.createCell(i);
                    cell.setCellStyle(style);
                    cell.setCellValue(tea[i].getMapNames().get(0));
                }
            }

        }  else {
            ExcelHeader[] headers = ExcelHeader.values();
            for (int i = 0; i < headers.length; i++) {
                if (i == headers.length - 1 && !(userType != null && userType == PtAccount.PARENTS_USER)) {
                    break;
                }

                if (i == headers.length - 5) {
                    if (userType != null && (userType == PtAccount.TEACHER_USER || userType == PtAccount.AREA_USER
                            || userType == PtAccount.SYSTEM_USER)) {
                        cell = title.createCell(i);
                        cell.setCellStyle(style);
                        cell.setCellValue(headers[i].getMapNames().get(0));
                    }
                } else if (i > headers.length - 5) {
                    if (userType != null && (userType == PtAccount.TEACHER_USER || userType == PtAccount.AREA_USER
                            || userType == PtAccount.SYSTEM_USER)) {
                        cell = title.createCell(i);
                        cell.setCellStyle(style);
                        cell.setCellValue(headers[i].getMapNames().get(0));
                    } else {
                        cell = title.createCell(i - 1);
                        cell.setCellStyle(style);
                        cell.setCellValue(headers[i].getMapNames().get(0));
                    }
                } else {
                    cell = title.createCell(i);
                    cell.setCellStyle(style);
                    cell.setCellValue(headers[i].getMapNames().get(0));
                }
            }

        }

        // 填充内容:map 数字为key, value为具体内容
        for (int i = 0; i < data.size(); i++) {
            Map<Integer, String> map = data.get(i);
            row = sheet.createRow(i + 1);
            filldata(map, cell, row, userType, style2);
        }

    }

    private void filldata(Map<Integer, String> map, Cell cell, Row row, Integer userType, CellStyle style2) {
        if(userType != null && userType==PtAccount.STUDENT_USER){
            for (int j = 0; j <= map.size(); j++) {
                if (j == 3) {
                    if (userType != null
                            && (userType == PtAccount.TEACHER_USER || userType == PtAccount.AREA_USER || userType == PtAccount.SYSTEM_USER)) {
                        cell = row.createCell(j);
                        cell.setCellStyle(style2);
                        cell.setCellValue(map.get(j));
                    }
                } else if (j > 3) {
                    if (userType != null
                            && (userType == PtAccount.TEACHER_USER || userType == PtAccount.AREA_USER || userType == PtAccount.SYSTEM_USER)) {
                        cell = row.createCell(j);
                        cell.setCellStyle(style2);
                        cell.setCellValue(map.get(j));
                    } else {
                        cell = row.createCell(j - 1);
                        cell.setCellStyle(style2);
                        cell.setCellValue(map.get(j));
                    }
                } else {
                    cell = row.createCell(j);
                    cell.setCellStyle(style2);
                    cell.setCellValue(map.get(j));
                }
            }
        }else{
            for (int j = 0; j < map.size(); j++) {
                if (j == 3) {
                    if (userType != null
                            && (userType == PtAccount.TEACHER_USER || userType == PtAccount.AREA_USER || userType == PtAccount.SYSTEM_USER)) {
                        cell = row.createCell(j);
                        cell.setCellStyle(style2);
                        cell.setCellValue(map.get(j));
                    }
                } else if (j > 3) {
                    if (userType != null
                            && (userType == PtAccount.TEACHER_USER || userType == PtAccount.AREA_USER || userType == PtAccount.SYSTEM_USER)) {
                        cell = row.createCell(j);
                        cell.setCellStyle(style2);
                        cell.setCellValue(map.get(j));
                    } else {
                        cell = row.createCell(j - 1);
                        cell.setCellStyle(style2);
                        cell.setCellValue(map.get(j));
                    }
                } else {
                    cell = row.createCell(j);
                    cell.setCellStyle(style2);
                    cell.setCellValue(map.get(j));
                }
            }
        }


    }

    @Override
    protected String sheetname() {
        return "用户数据";
    }

    enum ExcelHeader implements ExcelTitle {
        LOGINNAME(true, 25, "登录名"), USERNAME(true, 25, "真实姓名"), ROLETYPE(true, 25, "角色类型"), USERROLE(true, 25,
                "职务"), SEX(true, 25, "性别"), PASSWORD(true, 25, "初始密码"), ENABLE(true, 25, "是否冻结"), STUDENT(true, 25, "子女姓名");

        private String[] mapNames;

        private boolean required = false;

        private Integer size = null;

        private ExcelHeader(String... header) {
            this.mapNames = header;
        }

        private ExcelHeader(boolean required, String... header) {
            this.mapNames = header;
            this.required = required;
        }

        private ExcelHeader(Integer size, String... header) {
            this.mapNames = header;
            this.size = size;
        }

        private ExcelHeader(boolean required, Integer size, String... header) {
            this.mapNames = header;
            this.required = required;
            this.size = size;
        }

        @Override
        public List<String> getMapNames() {
            return Arrays.asList(mapNames);
        }

        @Override
        public boolean isRequired() {
            return required;
        }

        @Override
        public int length() {
            return size;
        }
    }

    enum StuExport {
        LOGINNAME(true, 25, "登录名"), USERNAME(true, 25, "真实姓名"), ROLETYPE(true, 25, "角色类型"), SEX(true, 25,
                "性别"), PASSWORD(true, 25, "初始密码"), ENABLE(true, 25, "是否冻结"), CLASSNAME(true, 25, "班级名称"),STUDENTCARDID(true,25,"校卡号");

        private String[] mapNames;

        private boolean required = false;

        private Integer size = null;

        private StuExport(String... header) {
            this.mapNames = header;
        }

        private StuExport(boolean required, String... header) {
            this.mapNames = header;
            this.required = required;
        }

        private StuExport(Integer size, String... header) {
            this.mapNames = header;
            this.size = size;
        }

        private StuExport(boolean required, Integer size, String... header) {
            this.mapNames = header;
            this.required = required;
            this.size = size;
        }

        public List<String> getMapNames() {
            return Arrays.asList(mapNames);
        }
    }


    enum TeacherExport {
        LOGINNAME(true, 25, "登录名"), USERNAME(true, 25, "真实姓名"), ROLETYPE(true, 25, "角色类型"), USERROLE(true, 25,
                "职务"), SEX(true, 25, "性别"), PASSWORD(true, 25, "初始密码"), ENABLE(true, 25, "是否冻结"),SCHOOLCARD(true,25,"校卡号");

        private String[] mapNames;

        private boolean required = false;

        private Integer size = null;

        private TeacherExport(String... header) {
            this.mapNames = header;
        }

        private TeacherExport(boolean required, String... header) {
            this.mapNames = header;
            this.required = required;
        }

        private TeacherExport(Integer size, String... header) {
            this.mapNames = header;
            this.size = size;
        }

        private TeacherExport(boolean required, Integer size, String... header) {
            this.mapNames = header;
            this.required = required;
            this.size = size;
        }

        public List<String> getMapNames() {
            return Arrays.asList(mapNames);
        }
    }

    /**
     * @param wb
     *          :工作簿
     * @param fontsize
     *          :字体大小
     * @param isbold
     *          :是否加粗
     *
     */
    private CellStyle creatStyle(Workbook wb, short fontsize, boolean isbold) {
        CellStyle style = wb.createCellStyle(); // 样式对象
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);// 垂直
        style.setAlignment(CellStyle.ALIGN_CENTER);// 水平
        style.setWrapText(true);// 能否换行
        style.setBorderTop((short) 1);
        style.setBorderRight((short) 1);
        style.setBorderBottom((short) 1);
        style.setBorderLeft((short) 1);
        // 设置标题字体格式
        Font font = wb.createFont();
        // 设置字体样式
        font.setFontHeightInPoints(fontsize); // --->设置字体大小
        font.setFontName("宋体"); // ---》设置字体，是什么类型例如：宋体
        font.setBold(isbold); // --->设置是否是加粗
        style.setFont(font); // --->将字体格式加入到style1中
        return style;
    }

}
