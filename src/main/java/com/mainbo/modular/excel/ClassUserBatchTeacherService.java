package com.mainbo.modular.excel;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author moshang
 * @date 2020-03-08
 **/
public interface ClassUserBatchTeacherService {


    /**
     * 下载班级的任课教师批量关联模板
     * @param orgId 机构ID
     * @param schoolYear 学年ID
     */
    void downloadBatchManageTeacherTemplate(String orgId,
                                            HttpServletResponse response, Integer schoolYear) throws IOException;

    /**
     * 批量导入教师
     * @param orgId 机构id
     * @param schoolYear 学年ID
     */
    StringBuilder batchImportTeacher(String orgId, MultipartFile registerFile, Integer schoolYear);
}
