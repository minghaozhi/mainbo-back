package com.mainbo.modular.excel;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author moshang
 * @date 2020-03-08
 **/
public interface ClassUserBatchStudentService {


    /**
     * 下载班级学生导入模板
     * @param orgId 机构ID
     * @param schoolYear 学年ID
     */
    void downloadBatchManageStudentTemplate(String orgId,
                                            HttpServletResponse response, Integer schoolYear) throws IOException;

    /**
     * 批量导入学生
     * @param orgId 机构ID
     * @param schoolYear 学年ID
     */
    StringBuilder batchImportStudent(String orgId, MultipartFile registerFile, Integer schoolYear);
}
