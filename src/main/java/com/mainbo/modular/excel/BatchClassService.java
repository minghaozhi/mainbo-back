package com.mainbo.modular.excel;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author moshang
 * @date 2020-03-07
 **/
public interface BatchClassService {

    /**
     * 下载班级导入模板
     * @param orgId 机构ID
     * @param schoolYear 学年ID
     */
    void downloadBatchClassTemplate(String orgId, HttpServletResponse response, Integer schoolYear) throws IOException;

    /**
     * 批量导入班级
     * @param orgId 机构ID
     * @param schoolYear 学年ID
     */
    StringBuilder batchImportClass(String orgId, MultipartFile file, Integer schoolYear);
}
