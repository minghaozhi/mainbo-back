package com.mainbo.core.util;

import org.apache.commons.codec.binary.Hex;

/**
 * 生成密码方法
 * @author moshang
 * @date 2020-02-23
 **/
public class EncryptPassword {

    public static String encryptPassword(String password) {
        return Encodes.encodeBase64(Encodes.md5Byte(password));
    }

    public static void main(String[] args) {
        String password="000000";
        System.out.println(encryptPassword(password));
    }
}
