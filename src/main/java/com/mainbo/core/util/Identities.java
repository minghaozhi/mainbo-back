package com.mainbo.core.util;

import java.security.SecureRandom;
import java.util.UUID;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/2/26   9:25
 **/
public class Identities {

    private static SecureRandom random = new SecureRandom();

    public Identities() {
    }

    public static String uuid() {
        return UUID.randomUUID().toString();
    }

    public static String uuid2() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static long randomLong() {
        return Math.abs(random.nextLong());
    }

    public static int randomInt() {
        return Math.abs(random.nextInt());
    }

    public static String randomBase62(int length) {
        byte[] randomBytes = new byte[length];
        random.nextBytes(randomBytes);
        return Encodes.encodeBase62(randomBytes);
    }
}
