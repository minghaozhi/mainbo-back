package com.mainbo.core.util;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/2/24   17:40
 **/
public class FileUtil {

    public final static   Integer WEB_DEVICE_ID = 0;
    public final static  String WEB_DEVICE_NAME = "web";
    public final static String CLOUD_DEVICE_NAME = "rmi";//第三方标识
    public static String getFileExt(String fileName) {
        if (fileName != null && !"".equals(fileName)) {
            int lastindex = fileName.lastIndexOf(".");
            return lastindex == -1 ? "" : fileName.substring(lastindex + 1, fileName.length());
        } else {
            return "";
        }
    }


    public static String getFileName(String fileName) {
        if (fileName != null && !"".equals(fileName)) {
            int lastindex = fileName.lastIndexOf(".");
            return lastindex == -1 ? fileName : fileName.substring(0, lastindex);
        } else {
            return "";
        }
    }
}
