package com.mainbo.core.util;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.io.Serializable;


@Data
public class Result implements Serializable {

    private static final long serialVersionUID = 4106493533535450543L;

    public static final Integer SUCCESS = 200;
    public static final Integer FAIL = 500;

    private int code;
    private Object data;
    private String msg="";
    private String error="";
    private int count;

    public static Result success() {
        return build(HttpStatus.OK.value(),"sucess");
    }

    public static Result success(Object data) {
        return build(HttpStatus.OK.value(),"sucess",data,0);
    }

    public static Result success(String msg,Object data) {
        return build(HttpStatus.OK.value(),msg,data,0);
    }

    public static Result fail(String msg) {
        return build(HttpStatus.BAD_REQUEST.value(),msg);
    }

    public static Result fail(int code, String msg) {
        return build(code,msg);
    }


    public static Result serviceUnavailable(){
        return build(HttpStatus.SERVICE_UNAVAILABLE.value(),"service unavailable");
    }

    public static Result build(int code,String msg){
        return build(code,msg,null,0);
    }

    public static Result build(int code,String msg,Object data,int count){
        Result r = new Result();
        r.setCode(code);
        r.setMsg(msg);
        r.setData(data);
        r.setCount(count);
        return r;
    }
}
