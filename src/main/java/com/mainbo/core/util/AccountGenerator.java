package com.mainbo.core.util;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mainbo.core.common.constant.Const;
import com.mainbo.modular.system.dao.PtAccountMapper;
import com.mainbo.modular.system.dao.PtOrgMapper;
import com.mainbo.modular.system.model.PtAccount;
import com.mainbo.modular.system.model.PtOrg;
import com.mainbo.modular.system.model.SysRole;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author xww
 * @Description  生成账号策略
 * @Date 2020/2/24   10:41
 **/
@Component
public class AccountGenerator {
    @Resource
    private PtOrgMapper orgMapper;
    @Resource
    private PtAccountMapper accountMapper;
    private Map<String, Integer> countCache = new ConcurrentHashMap<>();
    public String validate(PtAccount account) {
        StringBuilder accout = new StringBuilder().append(orgType(account.getOrgId()));
        accout.append(userType(account.getOrgId(), account.getUserType()));
        String activeAccount = accout.toString();
        PtAccount model = new PtAccount();
        model.setAccount(activeAccount);
        if (accountMapper.selectOne(model) != null) { // 已存在,重新生成一个
            activeAccount = validate(account);
        }
        return activeAccount;
    }

    private String orgType(String orgId) {
        String ot = "M0001";
        if (StringUtils.isNotBlank(orgId) && !Const.DEFAULT_ORG_ID.equals(orgId)) {
            PtOrg org = orgMapper.selectById(orgId);
            if (org != null && org.getType() != null) {
                int hash = orgId.hashCode();
                if (hash != Integer.MIN_VALUE) {
                    hash = Math.abs(hash) % 10000;
                } else {
                    hash = Math.abs(Integer.MIN_VALUE % 10000);
                }

                if (PtOrg.TP_ORG != org.getType()) {
                    ot = "S3";
                } else {
                    ot = "E2";
                }

                if (hash < 10) {
                    ot = ot + "00" + hash;
                } else if (hash < 100) {
                    ot = ot + "0" + hash;
                } else {
                    ot = ot + hash;
                }
            }
        }
        return ot;
    }

    private String userType(String orgId, Integer userType) {
        String ot = "M";
        if (StringUtils.isNotBlank(orgId)) {
            orgId = Const.DEFAULT_ORG_ID;
        }
        String key = "o-" + orgId + "-u-" + userType;
        Integer current = countCache.get(key);
        if (current == null) {
            EntityWrapper<PtAccount> model = new EntityWrapper<>();
            model.eq("org_id",orgId);
            model.eq("user_type",userType);
            current = accountMapper.selectCount(model);
        }

        current = current + 1;

        switch (SysRole.getRoleById(userType)) {
            case AREAMANAGER:
            case AREAWORKER:
                ot = "A";
                break;
            case STUDENT:
                ot = "S";
                break;
            case SCHMANAGER:
            case SCHWORKER:
            case TEACHER:
                ot = "T";
                break;
            case PARRENT:
                ot = "F";
                break;
            default:
                break;
        }

        if (current < 10) {
            ot = ot + "00" + current;
        } else if (current < 100) {
            ot = ot + "0" + current;
        } else {
            ot = ot + current;
        }

        countCache.put(key, current);

        return ot;
    }
}
