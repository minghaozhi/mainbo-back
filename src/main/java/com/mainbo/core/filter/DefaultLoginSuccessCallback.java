package com.mainbo.core.filter;

import com.mainbo.platform.uc.client.JwtToken;
import com.mainbo.platform.uc.client.LoginSuccessCallback;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.util.Map;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/3/16   10:29
 **/
public class DefaultLoginSuccessCallback implements LoginSuccessCallback {

    @Override
    public void onSuccess(Object principal, Map<String, Object> infos,
                          ServletRequest request, ServletResponse response) {

        Subject subject = SecurityUtils.getSubject();
        if (subject.getPrincipal() == null
                || !principal.equals(subject.getPrincipal())) {
            JwtToken token = new JwtToken(principal, infos);
            subject.login(token);
        }

        invokeSuccess(principal, infos, request, response);
    }

    /**
     * 登录回调 可用于记录应用系统的相关登录状态
     *
     * @param principal
     *          登录认证id，一般为用户id
     * @param infos
     *          平台登录后的用户信息，参考平台登录信息规范
     */
    protected void invokeSuccess(Object principal, Map<String, Object> infos,
                                 ServletRequest request, ServletResponse response) {

    }

}