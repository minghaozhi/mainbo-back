package com.mainbo.core.filter;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.LogoutFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/3/16   11:28
 **/
public class ShiroCasLogoutFilter extends LogoutFilter {

    /**
     * 退出 首先，判定是否在教研登录页登录，是直接用退出到默认地址； 否则，获取用户所属app,跳转到app 设置的登录页
     *
     * @param request
     *          the incoming Servlet request
     * @param response
     *          the outgoing ServletResponse
     * @param subject
     *          the not-yet-logged-out currently executing Subject
     * @return the redirect URL to send the user after logout.
     */
    @Override
    protected String getRedirectUrl(ServletRequest request, ServletResponse response, Subject subject) {
        onLogout(request, response, subject);
        return super.getRedirectUrl();
    }

    protected void onLogout(ServletRequest request, ServletResponse response, Subject subject) {

    }
}

