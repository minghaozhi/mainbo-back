package com.mainbo.core.common.exception;

/**
 * @author moshang
 * @date 2020-03-01
 **/
public class JmsSenderException extends RuntimeException {
    private static final long serialVersionUID = -6660159156933037589L;

    public JmsSenderException() {
        super();
    }

    public JmsSenderException(String message) {
        super(message);
    }

    public JmsSenderException(Throwable cause) {
        super(cause);
    }

    public JmsSenderException(String message, Throwable cause) {
        super(message, cause);
    }
}

