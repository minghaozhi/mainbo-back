package com.mainbo.core.common.exception;

import com.mainbo.core.util.Encodes;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * @author moshang
 * @date 2020-03-01
 **/
public class RsaSignException extends RuntimeException {
    private static final long serialVersionUID = -6660159156933037589L;

    public RsaSignException() {
        super();
    }

    public RsaSignException(String message) {
        super(message);
    }

    public RsaSignException(Throwable cause) {
        super(cause);
    }

    public RsaSignException(String message, Throwable cause) {
        super(message, cause);
    }
}
