package com.mainbo.core.common.constant;

import com.mainbo.modular.system.model.PtAccount;
import com.mainbo.platform.uc.client.JwtToken;

import java.util.Map;

/**
 * @author moshang
 * @date 2020-03-16
 **/
public class UserToken extends JwtToken {

    private PtAccount user;

    public UserToken(Object principal, Map<String, Object> infos, PtAccount user) {
        super(principal, infos);
        this.user =user;
    }

    @Override
    public Object getPrincipal() {
        return user;
    }
}
