package com.mainbo.core.common.constant;

/**
 * @Author xww
 * @Description //TODO
 * @Date 2020/3/16   9:18
 **/
public abstract class Constants {


    /**
     * 默认用户id
     */
    public static final String DEFAULT_USER_ID = "0";

    /**
     * 默认机构id
     */
    public static final String DEFAULT_ORG_ID = "0";

    /**
     * 消息key
     */
    public static final String MESSAGE = "message";

    /**
     * 错误key
     */
    public static final String ERROR = "error";

    /**
     * 当前用户MDC key
     */
    public static final String MDC_CURRENT_USER_ID = "_mdc_current_user_id";
    /**
     * 当前用户MDC key
     */
    public static final String MDC_CURRENT_USER_NAME = "_mdc_current_user_name";
    /**
     * 当前用户MDC key
     */
    public static final String MDC_CURRENT_USER_ROLE = "_mdc_current_user_role";
    /**
     * 当前用户MDC key
     */
    public static final String MDC_CURRENT_USER_ACCOUNT = "_mdc_current_user_account";
}
