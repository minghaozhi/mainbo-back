package com.mainbo.core.common.constant;

import com.ctrip.framework.apollo.ConfigService;
import com.mainbo.apollo.config.ApolloConfigUtil;

/**
 * @author moshang
 * @date 2020-02-29
 **/
public class ApolloConstants {
    public final static String MAINBO_PUBLIC_NAMESACE = "mainbo.public";
    public final static String UCENTER_FRONT_NAMESACE = "ucenter.front";
    public final static String APPLICATION = "application";
    public final static String BACK_NAMESACE = "back";
    public final static String JDBC_URL = "jdbc.url";

    public final static String JDBC_USER = "jdbc.username";
    public final static String JDBC_PASSWORD = "jdbc.password";
    public final static String JDBC_DRIVER = "jdbc.driver";
    public final static String JDBC_INITIALSIZE = "jdbc.initialSize";
    public final static String JDBC_MINIDLE = "jdbc.minIdle";
    public final static  String UCENTER_INTRANET_HOST = "ucenter.intranet.host";
    public final static  String UCENTER__HOST = "ucenter.host";

    public final static String JMS_URLS = "jms.urls";
    public final static String JMS_SECRETKEY = "jms.secretKey";
    public final static String JMS_SERVER_SECRETKEY = "jms.server.secretKey";
    public final static String UPLOAD_URL = "upload_url";
    public final static String DOWNLOAD_URL = "download_url";
    public final static String STORSGESERVER_URL = "storageServer.host";
    public final static String BACK_APPKEY = "back.appKey";
    public final static String BACK_APPID = "back.appId";
    public static String readConfig(String nameSpace,String key) {
      return  ApolloConfigUtil.getKeyByNameSpace(nameSpace,key);
    }
    public static String readStorageUrl() {
        return  ApolloConfigUtil.getKeyByNameSpace(ApolloConstants.MAINBO_PUBLIC_NAMESACE,ApolloConstants.STORSGESERVER_URL);
    }
    public static String readUploadUrl() {
        return  ConfigService.getConfig(ApolloConstants.UCENTER_FRONT_NAMESACE).getProperty(ApolloConstants.UPLOAD_URL,"");
    }
    public static String readActiveMqUrl() {
        return  ApolloConfigUtil.getKeyByNameSpace(ApolloConstants.MAINBO_PUBLIC_NAMESACE,ApolloConstants.JMS_URLS);
    }
    public static String readServerSecretKey() {
        return  ApolloConfigUtil.getKeyByNameSpace(ApolloConstants.BACK_NAMESACE,ApolloConstants.JMS_SERVER_SECRETKEY);
    }
}
