/**
 * Copyright 2018-2020 stylefeng & fengshuonan (https://gitee.com/stylefeng)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mainbo.core.common.constant;

/**
 * 系统常量
 *
 * @author moshang
 * @Date 2020-02-17 21:23:57
 */
public interface Const {

    /**
     * 系统默认的管理员密码
     */
    String DEFAULT_PWD = "000000";

    /**
     * 管理员角色的名字
     */
    String ADMIN_NAME = "admin";

    /**
     * 管理员id
     */
    Integer ADMIN_ID = 1;

    /**
     * 超级管理员角色id
     */
    Integer ADMIN_ROLE_ID = 1;

    /**
     * 接口文档的菜单名
     */
    String API_MENU_NAME = "接口文档";
    String DEFAULT_ORG_ID = "0";
    String TP_JSON = "json";
    String MESSAGE_SIGN = "_message_sign";

    String MESSAGE_SIGN_CONTENT = "_message_sign_content";

    String DEFAUT_SIGN_CONTENT = "MAINBO.COM";
    String MESSAGE_TYPE_NAME = "_MESSAGE_TYPE_";
    String MESSAGE_FORMATE_NAME = "_MESSAGE_FORMAT_";
    String RECEIVER_ID = "RECEIVER_ID";

    String EXCLUDE_RECEIVER_IDS = "EXCLUDE_RECEIVER_ID";
}
