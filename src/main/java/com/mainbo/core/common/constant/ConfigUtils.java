package com.mainbo.core.common.constant;

/**
 * @author moshang
 * @date 2020-03-01
 **/

import cn.stylefeng.roses.core.util.SpringContextHolder;
import com.mainbo.modular.system.model.meta.ConfigConstants;
import com.mainbo.modular.system.model.meta.MetaUtils;
import com.mainbo.modular.system.model.meta.vo.Meta;
import org.springframework.core.env.Environment;
        /* <pre>
 *   配置文件工具
         *   优先级顺序：
         *    1. 数据库配置，按domain加载 config 表中配置。
         *    2. 数据库配置，config 表中配置。
         *    3. 配置文件， config/init/文件夹下的properties 文件
        */
public final class ConfigUtils {

    private static Environment config;

    /**
     *
     * @param name
     *          名称
     * @param domain
     *          域
     * @return String
     */
    public static String readConfigWithDomain(String name, String domain) {
        return readConfigWithDomain(name, domain, "");
    }

    /**
     *
     * @param name
     *          名称
     * @param domain
     *          域
     * @param defaultValue
     *          默认值
     * @return String
     */
    public static String readConfigWithDomain(String name, String domain,
                                              Object defaultValue) {
        return readConfigWithDomain(
                ConfigConstants.MetaCodeTree.PLATFORM_EXT_CONFIG_CODE.getCode(), name,
                domain, defaultValue);
    }

    /**
     *
     * @param code
     *          类型码
     * @param name
     *          名称
     * @param domain
     *          domain 域
     * @param defaultValue
     *          默认值
     * @return String
     */
    public static String readConfigWithDomain(String code, String name,
                                              String domain, Object defaultValue) {
        Meta sysConfig = MetaUtils.getSysConfigProvider()
                .getConfigByNameWithDomain(code, name, domain);
        if (sysConfig != null) {
            return sysConfig.getValue();
        }
        return readStaticConfig(name, String.valueOf(defaultValue));
    }
    public static String readConfigWithDomain(String code, String name,
                                              String domain, Object defaultValue,String orgId) {
        Meta sysConfig = MetaUtils.getSysConfigProvider()
                .getConfigByNameWithDomain(code, name, domain,orgId);
        if (sysConfig != null) {
            return sysConfig.getValue();
        }
        return readStaticConfig(name, String.valueOf(defaultValue));
    }
    /**
     *
     * @param name
     *          名称
     * @return String
     */
    public static String readConfig(String name) {
        return readConfigWithDomain(name, null, "");
    }

    /**
     *
     * @param name
     *          名称
     * @param defaultValue
     *          默认值
     * @return String
     */
    public static String readConfig(String name, Object defaultValue) {
        return readConfigWithDomain(name, null, String.valueOf(defaultValue));
    }

    /**
     *
     * @param code
     *          类型码
     * @param name
     *          名称
     * @return String
     */
    public static String readConfigWithCode(String code, String name) {
        return readConfigWithDomain(code, name, null, "");
    }

    /**
     *
     * @param code
     *          类型码
     * @param name
     *          名称
     * @param defaultValue
     *          默认值
     * @return String
     */
    public static String readConfigWithCode(String code, String name,
                                            Object defaultValue) {
        return readConfigWithDomain(code, name, null, defaultValue);
    }

    /**
     * 获取数字类型配置
     *
     * @param name
     *          名称
     * @param defaultValue
     *          默认值
     * @return String
     */
    @SuppressWarnings("unchecked")
    public static <T extends Number> T readNumberConfig(String name,
                                                        T defaultValue) {
        String value = readConfigWithDomain(name, null,
                String.valueOf(defaultValue));
        T rs;
        try {
            if (defaultValue instanceof Long) {
                rs = (T) Long.valueOf(value);
            } else if (defaultValue instanceof Short) {
                rs = (T) Short.valueOf(value);
            } else if (defaultValue instanceof Double) {
                rs = (T) Double.valueOf(value);
            } else if (defaultValue instanceof Float) {
                rs = (T) Float.valueOf(value);
            } else {
                rs = (T) Integer.valueOf(value);
            }
        } catch (NumberFormatException e) {
            rs = defaultValue;
        }
        return rs;

    }

    /**
     * 获取数字类型配置
     *
     * @param name
     *          名称
     * @param defaultValue
     *          默认值
     * @return Boolean
     */
    public static Boolean readBoolConfig(String name, Boolean defaultValue) {
        String value = readConfigWithDomain(name, null,
                String.valueOf(defaultValue));
        if ("".equals(value)) {
            return defaultValue;
        }

        return Boolean.valueOf(value) || "1".equals(value);

    }

    /**
     * 获取数字类型配置
     *
     * @param name
     *          名称
     * @param defaultValue
     *          默认值
     * @return Boolean
     */
    public static Boolean readBoolConfig(String code, String name,
                                         Boolean defaultValue) {
        String value = readConfigWithDomain(code, name, null,
                String.valueOf(defaultValue));
        if ("".equals(value)) {
            return defaultValue;
        }

        return Boolean.valueOf(value) || "1".equals(value);

    }

    /**
     * 读取静态配置 目前是只读取properties 配置文件中的配置项
     *
     * @param name
     *          名称
     * @return String
     */

    public static String readStaticConfig(String name) {
        return readStaticConfig(name, "");
    }

    /**
     * 读取静态配置 目前是只读取properties 配置文件中的配置项
     *
     * @param name
     *          名称
     * @param defaultValue
     *          默认值
     * @return String
     */
    public static String readStaticConfig(String name, Object defaultValue) {
        String defaultString = String.valueOf(defaultValue);
        return getProperties().getProperty(name, defaultString);
    }

    private static Environment getProperties() {
        if (config == null) {
            config = SpringContextHolder.getApplicationContext().getEnvironment();
        }
        return config;
    }

}
