/**
 * Copyright 2018-2020 stylefeng & fengshuonan (https://gitee.com/stylefeng)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mainbo.core.shiro;

import com.mainbo.core.shiro.service.UserAuthService;
import com.mainbo.core.shiro.service.impl.UserAuthServiceServiceImpl;
import com.mainbo.modular.system.model.PtAccount;
import cn.stylefeng.roses.core.util.ToolUtil;
import com.mainbo.platform.uc.client.JwtToken;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.subject.PrincipalCollection;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ShiroDbRealm extends ShiroCasUserRealm {
    /**
     * 是否使用本地登录
     */
    private Boolean useLocalLogin = false;
    /**
     * 登录认证
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken)
            throws AuthenticationException {
        UserAuthService shiroFactory = UserAuthServiceServiceImpl.me();
        UsernamePasswordToken upToken = (UsernamePasswordToken) authcToken;
        JwtToken tokens = (JwtToken) authcToken;
        String account=(String) tokens.getInfos().get("account");
        PtAccount user = shiroFactory.user(account);
        ShiroUser shiroUser = shiroFactory.shiroUser(user);
        String password = String.valueOf(user.getPassword());
        upToken.setUsername(account);
        upToken.setPassword(password.toCharArray());
        SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(shiroUser, password.toCharArray(), getName());
        return simpleAuthenticationInfo;
    }

    /**
     * 权限认证
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        UserAuthService shiroFactory = UserAuthServiceServiceImpl.me();
        ShiroUser shiroUser = (ShiroUser) principals.getPrimaryPrincipal();
        List<Integer> roleList = shiroUser.getRoleList();

        Set<String> permissionSet = new HashSet<>();
        Set<String> roleNameSet = new HashSet<>();

        for (Integer roleId : roleList) {
            List<String> permissions = shiroFactory.findPermissionsByRoleId(roleId);
            if (permissions != null) {
                for (String permission : permissions) {
                    if (ToolUtil.isNotEmpty(permission)) {
                        permissionSet.add(permission);
                    }
                }
            }
            String roleName = shiroFactory.findRoleNameByRoleId(roleId);
            roleNameSet.add(roleName);
        }

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.addStringPermissions(permissionSet);
        info.addRoles(roleNameSet);

        return info;
    }
    public void setUseLocalLogin(Boolean useLocalLogin) {
        this.useLocalLogin = useLocalLogin == null ? Boolean.FALSE : useLocalLogin;
    }

    @Override
    public Class<?> getAuthenticationTokenClass() {
        return useLocalLogin ? super.getAuthenticationTokenClass() : JwtToken.class;
    }
    /**
     * 设置认证加密方式
     */
//    @Override
//    public void setCredentialsMatcher(CredentialsMatcher credentialsMatcher) {
//        HashedCredentialsMatcher md5CredentialsMatcher = new HashedCredentialsMatcher();
//        md5CredentialsMatcher.setHashAlgorithmName(ShiroKit.hashAlgorithmName);
//        md5CredentialsMatcher.setHashIterations(ShiroKit.hashIterations);
//        super.setCredentialsMatcher(md5CredentialsMatcher);
//    }
}
