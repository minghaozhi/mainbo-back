package com.mainbo.core.shiro;

/**
 * @author moshang
 * @date 2020-02-29
 **/
public abstract class SessionKey {

    /**
     * 当前用户
     */
    public static final String CURRENT_USER = "_CURRENT_USER_";

    /**
     * 当前用户角色，@see UserRole ， 后台一个用户只有一种角色。
     */
    public static final String CURRENT_USER_ROLE = "_CURRENT_USER_ROLE";

    /**
     * 当前使用的用户当前管理学校
     */
    public static final String CURRENT_ORG = "_CURRENT_ORG_";

}
