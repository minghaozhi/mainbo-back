package com.mainbo.core.activemq;

import com.mainbo.core.activemq.model.PfMessage;

import java.io.Serializable;
import java.util.Map;

/**
 * @author moshang
 * @date 2020-03-01
 **/
public interface JmsSender {
    /**
     * 发送Message消息
     *
     * @param type
     *          定义参数类型
     * @param content
     *          消息内容
     */
    public void sendMessage(String type, Serializable content);

    /**
     * 发送Message消息
     *
     * @param message
     *          消息体
     */
    public void sendMessage(PfMessage<Serializable> message);

    /**
     * 发送Message消息
     *
     * @param message
     *          消息体
     * @param params
     *          消息参数
     */
    public void sendMessage(PfMessage<Serializable> message,
                            Map<String, Object> params);

    /**
     * 发送Message消息
     *
     * @param type
     *          消息类型
     * @param content
     *          消息内容
     * @param params
     *          消息参数
     */
    public void sendMessage(String type, Serializable content,
                            Map<String, Object> params);
}
