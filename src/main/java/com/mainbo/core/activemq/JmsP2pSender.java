package com.mainbo.core.activemq;

import com.mainbo.core.activemq.model.PfP2pMessage;

import java.io.Serializable;
import java.util.Map;

/**
 * @author moshang
 * @date 2020-03-01
 **/
public interface JmsP2pSender {
    /**
     * 发送Message消息
     *
     * @param type
     *          自定义参数类型
     * @param to
     *          接收者标识
     * @param content
     *          消息内容
     */
    void sendP2pMessage(String type, String to, Serializable content);

    /**
     * 发送Message消息
     *
     * @param type
     *          自定义参数类型
     * @param to
     *          接收者标识
     * @param content
     *          消息内容
     * @param params
     *          消息参数
     */
    void sendP2pMessage(String type, String to, Serializable content,
                        Map<String, Object> params);

    /**
     * 发送Message消息
     *
     * @param message
     *          消息体
     */
    public void sendP2pMessage(PfP2pMessage<Serializable> message);

    /**
     * 发送Message消息
     *
     * @param message
     *          消息体
     * @param params
     *          消息参数
     */
    public void sendP2pMessage(PfP2pMessage<Serializable> message,
                               Map<String, Object> params);

}
