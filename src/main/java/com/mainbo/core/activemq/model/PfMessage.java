package com.mainbo.core.activemq.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.mainbo.core.activemq.ReplyAble;

import javax.jms.Destination;
import java.io.Serializable;

/**
 * 平台消息模型
 * @author moshang
 * @date 2020-03-01
 **/
public class PfMessage<T extends Serializable>
        implements ReplyAble, Serializable {

    /**
     * <pre>
     *
     * </pre>
     */
    private static final long serialVersionUID = 2702594558177053345L;

    /**
     * 消息类型,自定义的消息类型代码
     */
    private String messageType;

    /**
     * 消息描述信息
     */
    private String desc;

    /**
     * 消息内容
     */
    private T content;

    /**
     * 消息来源
     * 点对点消息时，为应用的appkey
     */
    private String from;

    /**
     * 排除接收者ids,多个用英文逗号分隔
     */
    @JSONField(serialize = false)
    private String excludeReceiverIds;

    @JSONField(serialize = false)
    private String correlationID;

    @JSONField(serialize = false)
    private Destination replyToDestination;

    @JSONField(serialize = false)
    private boolean needReply = false;

    /**
     * 消息格式，目前支持json , xml 两种
     */
    private String formatType;

    public PfMessage() {
    }

    public PfMessage(String messageType) {
        this.messageType = messageType;
    }

    public PfMessage(String messageType, T content) {
        this.messageType = messageType;
        this.content = content;
    }

    /**
     * Getter method for property <tt>messageType</tt>.
     *
     * @return property value of messageType
     */
    public String getMessageType() {
        return messageType;
    }

    /**
     * 消息类型,自定义的消息类型代码
     * Setter method for property <tt>messageType</tt>.
     *
     * @param messageType
     *          value to be assigned to property messageType
     */
    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    /**
     * Getter method for property <tt>desc</tt>.
     *
     * @return property value of desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * 消息描述信息
     * Setter method for property <tt>desc</tt>.
     *
     * @param desc
     *          value to be assigned to property desc
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * Getter method for property <tt>content</tt>.
     *
     * @return property value of content
     */
    public T getContent() {
        return content;
    }

    /**
     * 消息内容
     * Setter method for property <tt>content</tt>.
     *
     * @param content
     *          value to be assigned to property content
     */
    public void setContent(T content) {
        this.content = content;
    }

    /**
     * Getter method for property <tt>formatType</tt>.
     *
     * @return property value of formatType
     */
    public String getFormatType() {
        return formatType;
    }

    /**
     * Setter method for property <tt>formatType</tt>.
     *
     * @param formatType
     *          value to be assigned to property formatType
     */
    public void setFormatType(String formatType) {
        this.formatType = formatType;
    }

    /**
     * Getter method for property <tt>excludeReceiverId</tt>.
     *
     * @return property value of excludeReceiverId
     */
    public String getExcludeReceiverIds() {
        return excludeReceiverIds;
    }

    /**
     * 排除接收者ids,多个用英文逗号分隔
     * Setter method for property <tt>excludeReceiverId</tt>.
     *
     * @param excludeReceiverIds
     *          value to be assigned to property excludeReceiverId
     */
    public void setExcludeReceiverIds(String excludeReceiverIds) {
        this.excludeReceiverIds = excludeReceiverIds;
    }

    @Override
    public boolean needReply() {
        return needReply;
    }

    @Override
    public Destination getReplyToDestination() {
        return replyToDestination;
    }

    @Override
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * Setter method for property <tt>needReply</tt>.
     *
     * @param needReply boolean value to be assigned to property needReply
     */
    public void setNeedReply(boolean needReply) {
        this.needReply = needReply;
    }

    /**
     * Setter method for property <tt>correlationID</tt>.
     *
     * @param correlationID String value to be assigned to property correlationID
     */
    public void setCorrelationID(String correlationID) {
        this.correlationID = correlationID;
    }

    /**
     * Setter method for property <tt>replyToDestination</tt>.
     *
     * @param replyToDestination Destination value to be assigned to property replyToDestination
     */
    public void setReplyToDestination(Destination replyToDestination) {
        this.replyToDestination = replyToDestination;
    }

    /**
     * Getter method for property <tt>from</tt>.
     *
     * @return property value of from
     */
    public String getFrom() {
        return from;
    }

    /**
     * 消息来源
     * 点对点消息时，为应用的appkey
     * Setter method for property <tt>from</tt>.
     *
     * @param from
     *          value to be assigned to property from
     */
    public void setFrom(String from) {
        this.from = from;
    }

}

