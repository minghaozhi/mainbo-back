package com.mainbo.core.activemq.model;

import java.io.Serializable;

/**
 * 平台消息模型
 * @author moshang
 * @date 2020-03-01
 **/
public class PfP2pMessage<T extends Serializable> extends PfMessage<T> {

    public PfP2pMessage() {
    }

    public PfP2pMessage(String messageType) {
        super(messageType);
    }

    public PfP2pMessage(String messageType, T content, String to) {
        super(messageType, content);
        this.to = to;
    }

    private static final long serialVersionUID = 1222806024162175287L;

    /**
     * 消息发送者
     */
    private String author;

    /**
     * 消息接收者标识
     */
    private String to;

    /**
     * Getter method for property <tt>to</tt>.
     *
     * @return property value of to
     */
    public String getTo() {
        return to;
    }

    /**
     * Setter method for property <tt>to</tt>.
     *
     * @param to
     *          value to be assigned to property to
     */
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * Getter method for property <tt>author</tt>.
     *
     * @return property value of author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Setter method for property <tt>author</tt>.
     *
     * @param author
     *          value to be assigned to property author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

}

