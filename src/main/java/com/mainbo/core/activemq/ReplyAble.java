package com.mainbo.core.activemq;

import javax.jms.Destination;

/**
 * 可回复的消息
 * @author moshang
 * @date 2020-03-01
 **/
public interface ReplyAble {
    /**
     * message need reply ?
     * @return true or false
     */
    boolean needReply();

    /**
     * if need reply , must set reply destination.
     * @return reply destionation
     */
    Destination getReplyToDestination();

    /**
     * if need reply , correlation id must be set.
     * @return correlation id
     */
    String getCorrelationID();
}
