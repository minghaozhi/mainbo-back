package com.mainbo.core.activemq.parser;

/**
 * @author moshang
 * @date 2020-03-01
 **/
public interface MessageParser<T> {

    public T parse(String message) throws MessageParseException;
}