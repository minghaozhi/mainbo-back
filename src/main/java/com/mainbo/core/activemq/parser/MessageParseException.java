package com.mainbo.core.activemq.parser;

/**
 * @author moshang
 * @date 2020-03-01
 **/
public class MessageParseException extends Exception {
    private static final long serialVersionUID = -6660159156997037589L;

    public MessageParseException() {
        super();
    }

    public MessageParseException(String message) {
        super(message);
    }

    public MessageParseException(Throwable cause) {
        super(cause);
    }

    public MessageParseException(String message, Throwable cause) {
        super(message, cause);
    }
}