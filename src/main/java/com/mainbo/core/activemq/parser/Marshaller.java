package com.mainbo.core.activemq.parser;

import com.mainbo.core.activemq.model.PfMessage;

import java.io.Serializable;

/**
 * @author moshang
 * @date 2020-03-01
 **/
public interface Marshaller<R>{
    String marshall(PfMessage<Serializable> input) throws MessageParseException;
}
