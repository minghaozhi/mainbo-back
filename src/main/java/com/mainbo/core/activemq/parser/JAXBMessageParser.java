package com.mainbo.core.activemq.parser;

import com.mainbo.core.activemq.model.PfMessage;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.sax.SAXSource;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;

/**
 * @author moshang
 * @date 2020-03-01
 **/
public class JAXBMessageParser<T> implements MessageParser<T>, Marshaller<T> {

    private static final SAXParserFactory saxParserFactory = SAXParserFactory
            .newInstance();

    // It allows to specify the class type, if the class type is specified,
    // the contextPath will be ignored.
    private Class<T> modelClass;

    // Because JAXBContext.newInstance() is a very slow method,
    // it can improve performance a lot to cache the instances of JAXBContext
    // for used context paths or class types.
    private static HashMap<Object, JAXBContext> cachedContexts = new HashMap<Object, JAXBContext>();

    static {
        saxParserFactory.setNamespaceAware(true);
        saxParserFactory.setValidating(false);
    }

    public JAXBMessageParser(Class<T> modelClass) {
        assert (modelClass != null);
        this.modelClass = modelClass;
    }

    @Override
    public T parse(String message) throws MessageParseException {
        assert (message != null);
        return getObject(message);
    }

    @SuppressWarnings("unchecked")
    private T getObject(String responseContent) throws MessageParseException {
        try {
            if (!cachedContexts.containsKey(modelClass)) {
                initJAXBContext(modelClass);
            }

            assert (cachedContexts.containsKey(modelClass));
            JAXBContext jc = cachedContexts.get(modelClass);
            Unmarshaller um = jc.createUnmarshaller();
            // It performs better to call Unmarshaller#unmarshal(Source)
            // than to call Unmarshaller#unmarshall(InputStream)
            // if XMLReader is specified in the SAXSource instance.
            return (T) um.unmarshal(getSAXSource(responseContent));
        } catch (JAXBException e) {
            throw new MessageParseException("FailedToParseResponse", e);
        } catch (SAXException e) {
            throw new MessageParseException("FailedToParseResponse", e);
        } catch (ParserConfigurationException e) {
            throw new MessageParseException("FailedToParseResponse", e);
        }
    }

    @Override
    public String marshall(PfMessage<Serializable> object) throws MessageParseException {
        assert (object != null);
        try {
            if (!cachedContexts.containsKey(modelClass)) {
                initJAXBContext(modelClass);
            }
            assert (cachedContexts.containsKey(modelClass));
            JAXBContext jc = cachedContexts.get(modelClass);
            javax.xml.bind.Marshaller marshaller = jc.createMarshaller();
            Writer os = new StringWriter();
            marshaller.marshal(object, os);
            return os.toString();
        } catch (JAXBException e) {
            throw new MessageParseException("FailedToMarshallerOjbect", e);
        }
    }

    private static synchronized void initJAXBContext(Class<?> c)
            throws JAXBException {
        if (!cachedContexts.containsKey(c)) {
            JAXBContext jc = JAXBContext.newInstance(c);
            cachedContexts.put(c, jc);
        }
    }

    private static SAXSource getSAXSource(String content)
            throws SAXException, ParserConfigurationException {
        SAXParser saxParser = saxParserFactory.newSAXParser();
        return new SAXSource(saxParser.getXMLReader(), new InputSource(content));
    }

}