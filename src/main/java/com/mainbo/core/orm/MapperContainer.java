package com.mainbo.core.orm;

/**
 * @author moshang
 * @date 2020-03-07
 **/
public interface MapperContainer {

    /**
     * 根据 Bo 的 class 查找 Table信息
     *
     * @param className
     *          类简单名称，全系统必须保证bo 的简单名唯一
     */
    Table getTable(String className);

    /**
     * 注册Bo 映射
     *
     * @param className classname
     * @param table table
     */
    void addTable(String className, Table table);

    /**
     * 清空容器中的映射
     */
    void clear();

    /**
     * 包含的映射总数
     *
     * @return size
     */
    int size();
}
