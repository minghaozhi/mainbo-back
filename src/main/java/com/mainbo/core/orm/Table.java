package com.mainbo.core.orm;

/**
 * 表信息
 * @author moshang
 * @date 2020-03-07
 **/

import java.util.*;
public class Table {
    /**
     * 表bo中映射名全限定名
     */
    private String name;

    /**
     * bo 简单类名
     */
    private String simpleName;

    private String pkName;

    private String catalog = "";

    private String schema = "";
    /**
     * 表名
     */
    private String tableName = "";

    /**
     * Getter method for property <tt>name</tt>.
     *
     * @return name String
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for property <tt>name</tt>.
     *
     * @param name
     *          String value to be assigned to property name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter method for property <tt>simpleName</tt>.
     *
     * @return simpleName String
     */
    public String getSimpleName() {
        return simpleName;
    }

    /**
     * Setter method for property <tt>simpleName</tt>.
     *
     * @param simpleName
     *          String value to be assigned to property simpleName
     */
    public void setSimpleName(String simpleName) {
        this.simpleName = simpleName;
    }

    /**
     * Getter method for property <tt>pkName</tt>.
     *
     * @return pkName String
     */
    public String getPkName() {
        return pkName;
    }

    /**
     * Setter method for property <tt>pkName</tt>.
     *
     * @param pkName
     *          String value to be assigned to property pkName
     */
    public void setPkName(String pkName) {
        this.pkName = pkName;
    }

    /**
     * Getter method for property <tt>catalog</tt>.
     *
     * @return catalog String
     */
    public String getCatalog() {
        return catalog;
    }

    /**
     * Setter method for property <tt>catalog</tt>.
     *
     * @param catalog
     *          String value to be assigned to property catalog
     */
    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    /**
     * Getter method for property <tt>schema</tt>.
     *
     * @return schema String
     */
    public String getSchema() {
        return schema;
    }

    /**
     * Setter method for property <tt>schema</tt>.
     *
     * @param schema
     *          String value to be assigned to property schema
     */
    public void setSchema(String schema) {
        this.schema = schema;
    }

    /**
     * Getter method for property <tt>tableName</tt>.
     *
     * @return tableName String
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * Setter method for property <tt>tableName</tt>.
     *
     * @param tableName
     *          String value to be assigned to property tableName
     */
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    /**
     * Getter method for property <tt>columnMap</tt>.
     */
    public Map<String, Column> getColumnMap() {
        return columnMap;
    }

    /**
     * Setter method for property <tt>columnMap</tt>.
     */
    public void setColumnMap(Map<String, Column> columnMap) {
        this.columnMap = columnMap;
    }

    /**
     * Getter method for property <tt>attrMap</tt>.
     *
     */
    public Map<String, Column> getAttrMap() {
        return attrMap;
    }

    /**
     * Setter method for property <tt>attrMap</tt>.
     *
     */
    public void setAttrMap(Map<String, Column> attrMap) {
        this.attrMap = attrMap;
    }

    /**
     * 包含的字段名 - 字段信息 映射
     */
    private Map<String, Column> columnMap = new HashMap<String, Column>();

    /**
     * 表的bo 属性名称 - 字段信息 映射
     */
    private Map<String, Column> attrMap = new HashMap<String, Column>();

    /**
     * 添加字段包装类信息
     *
     * @param columnName
     *          字段名称
     * @param column
     *          字段对应的栏目
     */
    public void addColumn(String columnName, Column column) {
        columnMap.put(columnName, column);
        attrMap.put(column.getName(), column);
    }

    /**
     * 根据表字段名获取改字段信息
     *
     * @param columnName
     *          column name
     */
    public Column getColumn(String columnName) {
        if (columnName == null) {
            return null;
        }
        return columnMap.get(columnName.toUpperCase());
    }

    /**
     * 获取表包含的所有字段信息
     *
     */
    public List<Column> getColumns() {
        List<Column> columns = new ArrayList<Column>(
                Collections.unmodifiableCollection(columnMap.values()));
        return columns;
    }

    /**
     * 根据bo 属性名获取栏目
     *
     * @param attrName
     *          bo 属性名
     */
    public Column getColumnByAttrName(String attrName) {
        return attrMap.get(attrName);
    }

    /**
     * 获取表包含字段数
     *
     */
    public int size() {
        return columnMap.size();
    }
}
