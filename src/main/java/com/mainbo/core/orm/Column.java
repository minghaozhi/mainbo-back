package com.mainbo.core.orm;

import com.baomidou.mybatisplus.enums.IdType;

import java.lang.reflect.Type;

/**
 * @author moshang
 * @date 2020-03-07
 **/
public class Column {

    /**
     * 字段数据库名称
     */
    private String column;
    /**
     * 字段bo 中名称
     */
    private String name;
    /**
     * 字段是否允许为null,默认为true
     */
    private boolean nullable = true;
    /**
     * 字段是否是唯一标识,默认为false
     */
    private boolean unique;
    /**
     * length:表示该字段的大小,仅对String类型的字段有效
     */
    private int length;

    /**
     * 精度
     */
    private int scale;

    private boolean isPK = false;

    /**
     * 是否自动增长，目前只支持主键
     */
    private boolean isAutoIncrement = false;

    private IdType generationType;

    /**
     * bo 字段属性类型
     */
    private Type attrType;

    /**
     * 创建脚本生成时使用
     * 表示该字段在数据库中的实际类型.通常ORM框架可以根据属性类型自动判断数据库中字段的类型,
     * 但是对于Date类型仍无法确定数据库中字段类型究竟是DATE,TIME还是TIMESTAMP.此外,
     * String的默认映射类型为VARCHAR,如果要将String类型映射到特定数据库的BLOB或TEXT字段类型,
     * 该属性非常有用.
     */
    private String columnDefinition;

    /**
     * Getter method for property <tt>column</tt>.
     *
     * @return column String
     */
    public String getColumn() {
        return column;
    }

    /**
     * Setter method for property <tt>column</tt>.
     *
     * @param column String value to be assigned to property column
     */
    public void setColumn(String column) {
        this.column = column;
    }

    /**
     * Getter method for property <tt>name</tt>.
     *
     * @return name String
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for property <tt>name</tt>.
     *
     * @param name String value to be assigned to property name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter method for property <tt>nullable</tt>.
     *
     * @return nullable boolean
     */
    public boolean isNullable() {
        return nullable;
    }

    /**
     * Setter method for property <tt>nullable</tt>.
     *
     * @param nullable boolean value to be assigned to property nullable
     */
    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    /**
     * Getter method for property <tt>unique</tt>.
     *
     * @return unique boolean
     */
    public boolean isUnique() {
        return unique;
    }

    /**
     * Setter method for property <tt>unique</tt>.
     *
     * @param unique boolean value to be assigned to property unique
     */
    public void setUnique(boolean unique) {
        this.unique = unique;
    }

    /**
     * Getter method for property <tt>length</tt>.
     *
     * @return length int
     */
    public int getLength() {
        return length;
    }

    /**
     * Setter method for property <tt>length</tt>.
     *
     * @param length int value to be assigned to property length
     */
    public void setLength(int length) {
        this.length = length;
    }

    /**
     * Getter method for property <tt>scale</tt>.
     *
     * @return scale int
     */
    public int getScale() {
        return scale;
    }

    /**
     * Setter method for property <tt>scale</tt>.
     *
     * @param scale int value to be assigned to property scale
     */
    public void setScale(int scale) {
        this.scale = scale;
    }

    /**
     * Getter method for property <tt>isPK</tt>.
     *
     * @return isPK boolean
     */
    public boolean isPK() {
        return isPK;
    }

    /**
     * Setter method for property <tt>isPK</tt>.
     *
     * @param isPK boolean value to be assigned to property isPK
     */
    public void setPK(boolean isPK) {
        this.isPK = isPK;
    }

    /**
     * Getter method for property <tt>isAutoIncrement</tt>.
     *
     * @return isAutoIncrement boolean
     */
    public boolean isAutoIncrement() {
        return isAutoIncrement;
    }

    /**
     * Setter method for property <tt>isAutoIncrement</tt>.
     *
     * @param isAutoIncrement boolean value to be assigned to property isAutoIncrement
     */
    public void setAutoIncrement(boolean isAutoIncrement) {
        this.isAutoIncrement = isAutoIncrement;
    }

    /**
     * Getter method for property <tt>attrType</tt>.
     *
     * @return attrType Type
     */
    public Type getAttrType() {
        return attrType;
    }

    /**
     * Setter method for property <tt>attrType</tt>.
     *
     * @param attrType Type value to be assigned to property attrType
     */
    public void setAttrType(Type attrType) {
        this.attrType = attrType;
    }

    /**
     * Getter method for property <tt>columnDefinition</tt>.
     *
     * @return columnDefinition String
     */
    public String getColumnDefinition() {
        return columnDefinition;
    }

    /**
     * Setter method for property <tt>columnDefinition</tt>.
     *
     * @param columnDefinition String value to be assigned to property columnDefinition
     */
    public void setColumnDefinition(String columnDefinition) {
        this.columnDefinition = columnDefinition;
    }

    /**
     * Getter method for property <tt>generationType</tt>.
     *
     * @return property value of generationType
     */
    public IdType getGenerationType() {
        return generationType;
    }

    /**
     * Setter method for property <tt>generationType</tt>.
     *
     * @param generationType
     *          value to be assigned to property generationType
     */
    public void setGenerationType(IdType generationType) {
        this.generationType = generationType;
    }

}
