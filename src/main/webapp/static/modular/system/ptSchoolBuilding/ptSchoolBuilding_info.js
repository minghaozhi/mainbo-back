/**
 * 初始化场所管理详情对话框
 */
var PtSchoolBuildingInfoDlg = {
    ptSchoolBuildingInfoData : {},
    validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '名称不能为空'
                },
                stringLength: {
                    max: 10,
                    message: '长度必须小于10'
                }
            }
        },
        floorCount: {
            validators: {
                notEmpty: {
                    message: '楼层数不能为空'
                },
                regexp: {/* 只需加此键值对，包含正则表达式，和提示 */
                    regexp: /^[0-9]+$/,
                    message: '只能是数字'
                }, stringLength: {/*长度提示*/
                    max: 3,
                    message: '长度必须小于3'
                }
            }
        },
    }
};

/**
 * 清除数据
 */
PtSchoolBuildingInfoDlg.clearData = function() {
    this.ptSchoolBuildingInfoData = {};
}
PtSchoolBuildingInfoDlg.validate = function () {
    $('#schoolBuildingForm').data("bootstrapValidator").resetForm();
    $('#schoolBuildingForm').bootstrapValidator('validate');
    return $("#schoolBuildingForm").data('bootstrapValidator').isValid();
};
/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtSchoolBuildingInfoDlg.set = function(key, val) {
    this.ptSchoolBuildingInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtSchoolBuildingInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
PtSchoolBuildingInfoDlg.close = function() {
    parent.layer.close(window.parent.PtSchoolBuilding.layerIndex);
}

/**
 * 收集数据
 */
PtSchoolBuildingInfoDlg.collectData = function() {
    this
    .set('id')
    .set('name')
    .set('orgId')
    .set('orgName')
    .set('floorCount')
    .set('note')
    .set('createTime')
    .set('modifyTime')
    .set('enable')
    .set('deleted')
    .set('disabledReason')
    .set('lastupId')
    .set('lastupDttm');
};
/**
 * 收集数据
 */
PtSchoolBuildingInfoDlg.collectData1 = function() {
    this
        .set('id')
        .set('name')
        .set('disabledReason')
        .set('floorCount')
        .set('enable')
        .set('note');
};

/**
 * 提交添加
 */
PtSchoolBuildingInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptSchoolBuilding/add", function(data){
        Feng.success("添加成功!");
        window.parent.PtSchoolBuilding.table.refresh();
        PtSchoolBuildingInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.ptSchoolBuildingInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
PtSchoolBuildingInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData1();
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptSchoolBuilding/update", function(data){
        Feng.success("修改成功!");
        window.parent.PtSchoolBuilding.table.refresh();
        PtSchoolBuildingInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.ptSchoolBuildingInfoData);
    ajax.start();
}
/**
 * 提交修改
 */
PtSchoolBuildingInfoDlg.disableSchbuild = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptSchoolBuilding/updateEnable", function(data){
       if (data.code===200){
           Feng.success(data.msg);
           var index =parent.layer.getFrameIndex(window.name);//获取当前弹窗的Id
           parent.layer.close(index);//关闭
           window.parent.PtSchoolBuilding.table.refresh();
       }

    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.ptSchoolBuildingInfoData);
    ajax.start();
};
$(function() {
    Feng.initValidator("schoolBuildingForm", PtSchoolBuildingInfoDlg.validateFields);

});
