/**
 * 场所管理管理初始化
 */
var PtSchoolBuilding = {
    id: "PtSchoolBuildingTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    orgId:null
};

/**
 * 初始化表格的列
 */
PtSchoolBuilding.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '教学楼名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '所属机构', field: 'orgName', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
         {title: '状态', field: 'enable', visible: true, width: '50px', align: 'center', valign: 'middle',
            formatter: function (value, row, index) {
                var value = "";
                if (!row.enable) {
                    value = "禁用";
                } else {
                    value = "正常";
                }

                return value;
            }},
            {title: '状态', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
        {
            field: 'operate', title: '操作', halign: "center", align: "left", formatter: btnGroup(),    // 自定义方法，添加按钮组
            events: {               // 注册按钮组事件
                'click #edit': function (event, value, row, index) {
                    PtSchoolBuilding.openPtSchoolBuildingEdit(row.id)
                },
                'click #showUser': function (event, value, row, index) {
                    PtSchoolBuilding.openShowInfo(row.id)
                },
                'click #delUser': function (event, value, row, index) {
                    PtSchoolBuilding.delete(row.id);
                },
                'click #resetPwd': function (event, value, row, index) {
                    PtSchoolBuilding.updateEnable(row)
                }
            }
        }
    ];
};
function btnGroup() {   // 自定义方法，添加操作按钮
    var html =
        '<a href="####" class="btn btn-primary" id="edit"  data-target="#edit" title="修改">' +
        '<span class="glyphicon glyphicon-edit"></span></a>' +
        '<a href="####" class="btn btn-info" id="showUser"  data-target="#showUser" ' +
        'style="margin-left:2px" title="查看">' +
        '<span class="glyphicon glyphicon-info-sign"></span></a>' +
        '<a href="####" class="btn btn-danger" id="delUser"  data-target="#deleteuser" ' +
        'style="margin-left:2px" title="删除">' +
        '<span class="glyphicon glyphicon-trash"></span></a>' +
        '<a href="####" class="btn btn-warning" id="resetPwd"  data-target="#resetPwd" ' +
        'style="margin-left:2px" title="禁用/启用">' +
        '<span class="glyphicon glyphicon-off"></span></a>';
    return html
};
/**
 * 检查是否选中
 */
PtSchoolBuilding.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        PtSchoolBuilding.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加场所管理
 */
PtSchoolBuilding.openAddPtSchoolBuilding = function () {
    var index = layer.open({
        type: 2,
        title: '添加场所管理',
        area: ['800px', '460px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptSchoolBuilding/ptSchoolBuilding_add?orgId='+PtSchoolBuilding.orgId
    });
    this.layerIndex = index;
};

/**
 * 打开查看场所管理详情
 */
PtSchoolBuilding.openPtSchoolBuildingEdit = function (id) {
     if (id==null) {
         layer.alert("请选择一行");
         return;
     }
        var index = layer.open({
            type: 2,
            title: '场所管理详情',
            area: ['800px', '460px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptSchoolBuilding/ptSchoolBuilding_update/' +id
        });
        this.layerIndex = index;
};
/**
 * 打开查看场所管理详情
 */
PtSchoolBuilding.openShowInfo = function (id) {
    if (id==null) {
        layer.alert("请选择一行");
        return;
    }
    var index = layer.open({
        type: 2,
        title: '场所管理详情',
        area: ['800px', '460px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptSchoolBuilding/openShowInfo/' +id
    });
    this.layerIndex = index;
};
/**
 * 删除场所管理
 */
PtSchoolBuilding.delete = function (id) {
    if (id==null) {
        layer.alert("请选择一行");
        return;
    }
        var ajax = new $ax(Feng.ctxPath + "/ptSchoolBuilding/delete/"+id, function (data) {
            if (data.code===200){
            Feng.success(data.msg);
            PtSchoolBuilding.table.refresh();
            }else {
                Feng.error(data.msg);
            }
        }, function (data) {
            Feng.error(data.msg);
        });
        // ajax.set("ptSchoolBuildingId",this.seItem.id);
        ajax.start();
};
PtSchoolBuilding.updateEnable=function(row){
    if (row.id===null){
        layer.alert("请选择学校!");

    }
    if (!row.enable){
        //启用
        var ajax = new $ax(Feng.ctxPath + "/ptSchoolBuilding/updateEnable", function (data) {
            if (data.code===200){
                Feng.success(data.msg);
                PtSchoolBuilding.table.refresh();
            }else {
                Feng.error(data.msg);
            }
        }, function (data) {
            Feng.error(data.msg);
        });
        ajax.set("id",row.id);
        ajax.set("enable",1);
        ajax.start();
    } else {
     layer.confirm('确定要禁用教学楼吗？', {
            btn: ['确定', '取消'],
            shade: false //不显示遮罩
        }, function (index) {
            layer.close(index);
            var index = layer.open({
                type: 2,
                title: '禁用教学楼',
                area: ['800px', '460px'], //宽高
                fix: false, //不固定
                maxmin: true,
                content: Feng.ctxPath + '/ptSchoolBuilding/disableSchbuild/' +row.id
            });
            this.layerIndex = index;
        });
    }


};
/**
 * 管理教室
 */
PtSchoolBuilding.managerClassRoom = function () {
   if(this.check()) {
       var index = layer.open({
           type: 2,
           title: '管理教室',
           area: ['900px', '600px'], //宽高
           fix: false, //不固定
           maxmin: true,
           content: Feng.ctxPath + '/ptSchoolClassroom/toManagerClassRoom?buildingId=' + this.seItem.id
       });
       this.layerIndex = index;
   }
};
/**
 * 查询场所管理列表
 */
PtSchoolBuilding.search = function () {
    var queryData = {};
    queryData['name'] = $("#name").val();
    PtSchoolBuilding.table.refresh({query: queryData});
};
PtSchoolBuilding.formParams = function() {
    var queryData = {};
    queryData['orgId'] = PtSchoolBuilding.orgId;
    return queryData;
};
$(function () {

        PtSchoolBuilding.orgId=$("#orgId").val();
    var defaultColunms = PtSchoolBuilding.initColumn();
    var table = new BSTable(PtSchoolBuilding.id, "/ptSchoolBuilding/list", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams(PtSchoolBuilding.formParams());
    PtSchoolBuilding.table = table.init();
    if (PtSchoolBuilding.orgId==="0"){
        PtTeacher.orgId=orgId;
        var ztree = new $ZTree("areaTree", "/back/sys/area/list");
        ztree.bindOnClick(PtSchoolBuilding.onClickOrg);
        ztree.init();
    }
});
