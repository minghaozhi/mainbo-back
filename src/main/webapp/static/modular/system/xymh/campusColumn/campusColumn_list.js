/**
 * 管理初始化
 */
var CampusColumnList = {
    id: "CampusColumnListTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    orgId:null,
    code:null
};

/**
 * 初始化表格的列
 */
CampusColumnList.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
        {title: '编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '作者', field: 'author', visible: true, align: 'center', valign: 'middle'},
        {title: '新闻标题', field: 'title', visible: true, align: 'center', valign: 'middle'},
        {title: '发布时间', field: 'crtDttm', visible: true, align: 'center', valign: 'middle'},
        {title: '置顶', field: 'isTop', visible: true, align: 'left',width:'70px',halign:"center", valign: 'middle',
            formatter: function (value, row, index) {
                var value = "";
                if (row.isTop===1) {
                    value = "已置顶";
                } else  {
                    value = "未置顶";
                }
                return value;
            }},
        {title: '排序', field: 'sort', visible: true, align: 'center', valign: 'middle'},
        {
            field: 'operate',
            title: '操作',
            halign: "center",
            align: "left",
            formatter: btnGroupCampusColumnList,    // 自定义方法，添加按钮组
            events: {               // 注册按钮组事件
                'click #edit': function (event, value, row, index) {
                    CampusColumnList.openEditNews(row.id)
                },
                'click #showUser': function (event, value, row, index) {
                    CampusColumnList.openShowInfo(row.id)
                },
                'click #delUser': function (event, value, row, index) {
                    CampusColumnList.delete(row);
                },
                'click #resetPwd': function (event, value, row, index) {
                    CampusColumnList.updateTop(row)
                }
            }
        }
    ];
};
function btnGroupCampusColumnList() {   // 自定义方法，添加操作按钮
                        // data-target="xxx" 为点击按钮弹出指定名字的模态框
    var html =
        '<a href="####" class="btn btn-primary" id="edit"  data-target="#edit" title="修改">' +
        '<span class="glyphicon glyphicon-edit"></span></a>' +
        '<a href="####" class="btn btn-info" id="showUser"  data-target="#showUser" ' +
        'style="margin-left:2px" title="查看">' +
        '<span class="glyphicon glyphicon-info-sign"></span></a>' +
        '<a href="####" class="btn btn-danger" id="delUser"  data-target="#deleteuser" ' +
        'style="margin-left:2px" title="删除">' +
        '<span class="glyphicon glyphicon-trash"></span></a>' +
        '<a href="####" class="btn btn-warning" id="resetPwd"  data-target="#resetPwd" ' +
        'style="margin-left:2px" title="置顶/取消置顶">' +
        '<span class="glyphicon glyphicon-off"></span></a>';
    return html
};

/**
 * 检查是否选中
 */
CampusColumnList.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        CampusColumnList.seItem = selected[0];
        return true;
    }
};
function getSelections() {
    var selected = $('#' + CampusColumnList.id).bootstrapTable('getSelections');
    var ids = new Array();
    for (var i = 0; i < selected.length; i++) {
        ids[i] = selected[i].id;
    }
    return ids.join(",");
}
/**
 * 点击添加
 */
CampusColumnList.openAddNews = function () {
    var index = layer.open({
        type: 2,
        title: '发布内容',
        area: ['870px', '600px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/campusColumn/campusColumn_add?orgId='+CampusColumnList.orgId+"&code="+CampusColumnList.code
    });
    this.layerIndex = index;
};

/**
 * 打开修改
 */
CampusColumnList.openEditNews = function (id) {

    var index = layer.open({
        type: 2,
        title: '修改',
        area: ['870px', '600px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/campusColumn/campusColumn_update/' + id
    });
    this.layerIndex = index;
};
CampusColumnList.openShowInfo=function(id){
    var index = layer.open({
        type: 2,
        title: '详情页',
        area: ['870px', '600px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/campusColumn/detail/'+id
    });
    this.layerIndex = index;
};
/**
 * 删除
 */
CampusColumnList.delete=function(row){

    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/ptNews/batchdelete", function (data) {
            Feng.success("删除成功!");
            CampusColumnList.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("batchDeleteIds",row.id);
        ajax.start();
    };

    Feng.confirm("是否刪除 " + row.title + "?", operation);
};
CampusColumnList.batchDelete=function(){
    const ids = getSelections();
    if (ids.length === 0) {
        layer.msg("请至少选择一条数据", {icon: 5});
        return;
    }
    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/ptNews/batchdelete", function (data) {
            Feng.success("删除成功!");
            CampusColumnList.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("batchDeleteIds",ids);
        ajax.start();
    };

    Feng.confirm("是否批量刪除?", operation);
};
/**
 * 置顶状态
 * @param row
 */
CampusColumnList.updateTop = function (row) {
    let status='';
    let enable;
    if (row.isTop===1){
        status="取消置顶";
        enable=0;
    }else {
        status="置顶";
        enable=1;
    }
    var message = "您确定要"+status+"吗？";
    parent.layer.confirm(message, {
        btn: ['确定', '取消'],
        shade: false //不显示遮罩
    }, function () {
        var ajax = new $ax(Feng.ctxPath + "/ptNews/isTop", function (data) {
            if (data.code===200){
                Feng.success(data.msg);
                var index =parent.layer.getFrameIndex(window.name);//获取当前弹窗的Id
                layer.close(index);//关闭
                CampusColumnList.table.refresh();
            }else {
                Feng.error(data.msg);
            }

        }, function (data) {
            Feng.error(data.msg);
        });
        ajax.set("id", row.id);
        ajax.set("isTop", enable);
        ajax.start();
    });

};
CampusColumnList.search = function () {
    var queryData = {};
    queryData['orgId'] = $("#orgId").val();
    queryData['title'] = $("#title").val();
    queryData['code'] = CampusColumnList.code;
    queryData['isDraft'] =0;
    CampusColumnList.table.refresh({query: queryData});
};
CampusColumnList.formParams = function () {
    var queryData = {};
    queryData['orgId'] = CampusColumnList.orgId;
    queryData['code'] = CampusColumnList.code;
    queryData['isDraft'] =0;
    return queryData;
};

$(function () {
    CampusColumnList.orgId=$("#orgId").val();
    CampusColumnList.code=$("#code").val();
    var defaultColunms = CampusColumnList.initColumn();
    var table = new BSTable(CampusColumnList.id, "/achieveShow/list", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams(CampusColumnList.formParams());
    CampusColumnList.table = table.init();
});
