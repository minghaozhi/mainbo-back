/**
 * 管理初始化
 */
var CampusColumnDraftList = {
    id: "CampusColumnDraftListTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};
CampusColumnDraftList.getSelections=function (){
    var selected = $('#' + CampusColumnDraftList.id).bootstrapTable('getSelections');
    var ids = new Array();
    for (var i = 0; i < selected.length; i++) {
        ids[i] = selected[i].id;
    }
    return ids.join(",");
}
/**
 * 初始化表格的列
 */
CampusColumnDraftList.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
        {title: '编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '作者', field: 'author', visible: true, align: 'center', valign: 'middle'},
        {title: '新闻标题', field: 'title', visible: true, align: 'center', valign: 'middle'},

        {title: '发布时间', field: 'crtDttm', visible: true, align: 'center', valign: 'middle'},
        {title: '置顶', field: 'isTop', visible: true, align: 'left',width:'70px',halign:"center", valign: 'middle',
            formatter: function (value, row, index) {
                var value = "";
                if (row.isTop===1) {
                    value = "已置顶";
                } else  {
                    value = "未置顶";
                }
                return value;
            }},
        {title: '排序', field: 'sort', visible: true, align: 'center', valign: 'middle'},
        {
            field: 'operate',
            title: '操作',
            halign: "center",
            align: "left",
            formatter: btnGroupCampusColumnDraft,    // 自定义方法，添加按钮组
            events: {               // 注册按钮组事件
                'click #edit': function (event, value, row, index) {
                    CampusColumnDraftList.openEditNews(row.id)
                },
                'click #showUser': function (event, value, row, index) {
                    CampusColumnDraftList.openShowInfo(row.id)
                },
                'click #delUser': function (event, value, row, index) {
                    CampusColumnDraftList.delete(row);
                }
            }
        }
    ];
};
function btnGroupCampusColumnDraft() {   // 自定义方法，添加操作按钮
                        // data-target="xxx" 为点击按钮弹出指定名字的模态框
    var html =
        '<a href="####" class="btn btn-primary" id="edit"  data-target="#edit" title="修改">' +
        '<span class="glyphicon glyphicon-edit"></span></a>' +
        '<a href="####" class="btn btn-info" id="showUser"  data-target="#showUser" ' +
        'style="margin-left:2px" title="查看">' +
        '<span class="glyphicon glyphicon-info-sign"></span></a>' +
        '<a href="####" class="btn btn-danger" id="delUser"  data-target="#deleteuser" ' +
        'style="margin-left:2px" title="删除">' +
        '<span class="glyphicon glyphicon-trash"></span></a>';
    return html
};
/**
 * 打开修改
 */
CampusColumnDraftList.openEditNews = function (id) {
    var index = layer.open({
        type: 2,
        title: '修改',
        area: ['850px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/campusColumn/campusColumn_update/' +id
    });
    this.layerIndex = index;
};
CampusColumnDraftList.openShowInfo=function(id){
    var index = layer.open({
        type: 2,
        title: '详情页',
        area: ['850px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/campusColumn/detail/'+id
    });
    this.layerIndex = index;
};
/**
 * 删除
 */
CampusColumnDraftList.delete=function(row){

    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/ptNews/batchdelete", function (data) {
            Feng.success("删除成功!");
            CampusColumnDraftList.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("batchDeleteIds",row.id);
        ajax.start();
    };

    Feng.confirm("是否刪除 " + row.title + "?", operation);
};


CampusColumnDraftList.batchDelete=function(){
    const ids =this.getSelections();
    if (ids.length === 0) {
        layer.msg("请至少选择一条数据", {icon: 5});
        return;
    }
    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/ptNews/batchdelete", function (data) {
            Feng.success("删除成功!");
            CampusColumnDraftList.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("batchDeleteIds",ids);
        ajax.start();
    };

    Feng.confirm("是否批量刪除?", operation);
};
CampusColumnDraftList.search = function () {
    var queryData = {};
    queryData['orgId'] = $("#orgId").val();
    queryData['title'] = $("#title1").val();
    queryData['code'] = CampusColumnDraftList.code;
    queryData['isDraft'] =1;
    CampusColumnDraftList.table.refresh({query: queryData});
};
CampusColumnDraftList.formParams = function () {
    var queryData = {};
    queryData['orgId'] = CampusColumnDraftList.orgId;
    queryData['code'] = CampusColumnDraftList.code;
    queryData['isDraft'] =1;
    return queryData;
};

$(function () {
    CampusColumnDraftList.orgId=$("#orgId").val();
    CampusColumnDraftList.code=$("#code").val();
    var defaultColunms = CampusColumnDraftList.initColumn();
    var table = new BSTable(CampusColumnDraftList.id, "/achieveShow/list", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams(CampusColumnDraftList.formParams());
    CampusColumnDraftList.table = table.init();
});
