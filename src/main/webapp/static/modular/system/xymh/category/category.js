/**
 * 管理初始化
 */
var CategoryList = {
    id: "CategoryListTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    orgId:null,
    parentCode:null
};

/**
 * 初始化表格的列
 */
CategoryList.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '编号', field: 'id', visible: true, align: 'center',halign:"center",  valign: 'middle'},
        {title: '作者', field: 'name', visible: true, align: 'center',halign:"center", valign: 'middle'},
        {title: '描述', field: 'descs', visible: true, align: 'center', valign: 'middle'}
        ];
};
/**
* 检查是否选中
*/
CategoryList.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        CategoryList.seItem = selected[0];
        return true;
    }
};
/**
 * 添加分类
 */
CategoryList.openAddCategory=function(){
    var index = layer.open({
        type: 2,
        title: '新增',
        area: ['800px', '450px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptNewsCategory/ptNewsCategory_add?orgId='+CategoryList.orgId+"&parentCode="+CategoryList.parentCode
    });
    this.layerIndex = index;
};
/**
 * 修改分类
 */
CategoryList.openEditCategory=function(){
    if (this.check()){
    var index = layer.open({
        type: 2,
        title: '修改',
        area: ['800px', '450px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptNewsCategory/ptNewsCategory_update/'+this.seItem.id
    });
    this.layerIndex = index;
    }
};

/**
 * 删除分类
 */
CategoryList.delete = function () {
    if (this.check()) {

        var operation = function(){
            var ajax = new $ax(Feng.ctxPath + "/ptNewsCategory/delete/"+ CategoryList.seItem.id, function (data) {
                Feng.success("删除成功!");
                CategoryList.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.start();
        };

        Feng.confirm("是否刪除分类 " + CategoryList.seItem.name + "?", operation);
    }
};
CategoryList.search = function () {
    var queryData = {};
    queryData['orgId'] = $("#orgId").val();
    queryData['title'] = $("#title").val();
    queryData['parentCode'] = CategoryList.parentCode;
    CategoryList.table.refresh({query: queryData});
};
CategoryList.formParams = function () {
    var queryData = {};
    queryData['orgId'] = CategoryList.orgId;
    queryData['parentCode'] = CategoryList.parentCode;
    return queryData;
};
$(function () {
    CategoryList.orgId=$("#orgId").val();
    CategoryList.parentCode=$("#parentCode").val();
    var defaultColunms = CategoryList.initColumn();
    var table = new BSTable(CategoryList.id, "/ptNewsCategory/list", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams(CategoryList.formParams());
    CategoryList.table = table.init();
});
