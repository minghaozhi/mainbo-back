/**
 * 初始化字典详情对话框
 */
var CategoryInfoDlg = {
    categoryInfoData : {},
    validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '名称不能为空'
                },
                stringLength: {
                    max: 30,
                    message: '长度必须小于30'
                }
            }
        },
        sort: {
            validators: {
                notEmpty: {
                    message: '排序不能为空'
                },
                regexp: {/* 只需加此键值对，包含正则表达式，和提示 */
                    regexp: /^[0-9]+$/,
                    message: '只能是数字'
                }, stringLength: {/*长度提示*/
                    max: 4,
                    message: '长度必须小于4'
                }
            }
        },
    }
};
/**
 * 验证数据是否为空
 */
CategoryInfoDlg.validate = function () {
    $('#categoryInfoForm').data("bootstrapValidator").resetForm();
    $('#categoryInfoForm').bootstrapValidator('validate');
    return $("#categoryInfoForm").data('bootstrapValidator').isValid();
};
/**
 * 清除数据
 */
CategoryInfoDlg.clearData = function() {
    this.categoryInfoData = {};
};

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CategoryInfoDlg.set = function(key, val) {
    this.categoryInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
};
/**
 * 关闭此对话框
 */
CategoryInfoDlg.close = function() {
    parent.layer.close(window.parent.CategoryList.layerIndex);
};
/**
 * 收集数据
 */
CategoryInfoDlg.collectData= function() {
    this
        .set('id')
        .set('name')
        .set('sort')
        .set("parentCode")
        .set("orgId")
        .set('descs');
};
CategoryInfoDlg.addSubmit=function () {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptNewsCategory/addOrUpdate", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            window.parent.CategoryList.table.refresh();
            CategoryInfoDlg.close();
        }else {
            Feng.error(data.msg);
        }
    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.categoryInfoData);
    ajax.start();
};

CategoryInfoDlg.editSubmit=function () {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptNewsCategory/addOrUpdate", function(data){
        if (data.code===200){
            Feng.success("修改成功");
            window.parent.CategoryList.table.refresh();
            CategoryInfoDlg.close();
        }else {
            Feng.error(data.msg);
        }
    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.categoryInfoData);
    ajax.start();
};
$(function () {
    Feng.initValidator("categoryInfoForm", CategoryInfoDlg.validateFields);
});
