/**
 * 管理初始化
 */
var Banner = {
    id: "BannerTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Banner.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '网址', field: 'website', visible: true, align: 'center', valign: 'middle'},
        {title: '类型', field: 'type', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row, index) {
                var value = "";
                if (row.type===0) {
                    value = "图片链接";
                } else  {
                    value = "文字链接";
                }
                return value;
            }},
        {title: '显示顺序', field: 'sort', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Banner.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Banner.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
Banner.openAddPtBanner = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/banner/banner_add?orgId='+Banner.orgId+"&code="+Banner.code
    });
    this.layerIndex = index;
};

/**
 * 打开查看详情
 */
Banner.openEditBanner = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '修改',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/banner/banner_update/' + Banner.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除
 */
Banner.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/banner/delete/"+this.seItem.id, function (data) {
            if (data.code===200){
                Feng.success(data.msg);
                Banner.table.refresh();
            }else {
                Feng.error(data.msg);
            }


        }, function (data) {

        });
        ajax.start();
    }
};
Banner.search = function () {
    var queryData = {};
    queryData['orgId'] = Banner.orgId;
    queryData['code'] = Banner.code;
    queryData['isOrg'] = Banner.isOrg;
    return queryData;
};
/**
 * 查询列表
 */
Banner.formParams = function () {
    var queryData = {};
    queryData['orgId'] = Banner.orgId;
    queryData['code'] = Banner.code;
    queryData['isOrg'] = Banner.isOrg;
    return queryData;
};
$(function () {
    Banner.orgId=$("#orgId").val();
    Banner.code=$("#code").val();
    Banner.isOrg=$("#isOrg").val();
    var defaultColunms = Banner.initColumn();
    var table = new BSTable(Banner.id, "/banner/list", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams(Banner.formParams());
    Banner.table = table.init();


});
