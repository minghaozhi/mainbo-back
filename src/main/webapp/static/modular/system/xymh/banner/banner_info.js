/**
 * 初始化详情对话框
 */
var BannerInfoDlg = {
    bannerInfoData : {},
    validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '名称不能为空'
                },
                stringLength: {/*长度提示*/
                    max: 30,
                    message: '长度必须小于30'
                }/*最后一个没有逗号*/
            }
        },
        sort: {
            validators: {
                notEmpty: {
                    message: '排序值不能为空'
                },
                regexp: {/* 只需加此键值对，包含正则表达式，和提示 */
                    regexp: /^[0-9]+$/,
                    message: '只能是数字'
                },
                stringLength: {/*长度提示*/
                    max: 30,
                    message: '长度必须小于30'
                }/*最后一个没有逗号*/
            }
        },
        website: {
            validators: {
                url: {
                    message: '链接格式有误'
                }
            },

        }
    }
};
/**
 * 验证数据是否为空
 */
BannerInfoDlg.validate = function () {
    $('#bannerInfoForm').data("bootstrapValidator").resetForm();
    $('#bannerInfoForm').bootstrapValidator('validate');
    return $("#bannerInfoForm").data('bootstrapValidator').isValid();
};
/**
 * 清除数据
 */
BannerInfoDlg.clearData = function() {
    this.bannerInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
BannerInfoDlg.set = function(key, val) {
    this.bannerInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
BannerInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
BannerInfoDlg.close = function() {
    parent.layer.close(window.parent.Banner.layerIndex);
}

/**
 * 收集数据
 */
BannerInfoDlg.collectData = function() {
    this
        .set('id')
        .set('name')
        .set('pic',$("#avatar").val())
        .set('sort')
        .set("website")
        .set('code')
        .set("type")
        .set('orgId');
}

/**
 * 提交添加
 */
BannerInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/banner/saveOrUpdate", function(data){
        if (data.code===200){
        Feng.success(data.msg);
        window.parent.Banner.table.refresh();
        BannerInfoDlg.close();
        }else {
            Feng.success(data.msg);
        }
    },function(data){
        Feng.success(data.msg);
    });
    ajax.set(this.bannerInfoData);
    ajax.start();
};

/**
 * 提交修改
 */
BannerInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/banner/saveOrUpdate", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            window.parent.Banner.table.refresh();
            BannerInfoDlg.close();
        }else {
            Feng.success(data.msg);
        }
    },function(data){
        Feng.success(data.msg);
    });
    ajax.set(this.bannerInfoData);
    ajax.start();
};


