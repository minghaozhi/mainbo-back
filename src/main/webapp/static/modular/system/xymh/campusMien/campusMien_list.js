/**
 * 管理初始化
 */
var CampusMienList = {
    id: "CampusMienTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    orgId:null,
    code:null
};


/**
 * 初始化表格的列
 */
CampusMienList.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '显示顺序', field: 'sort', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
CampusMienList.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        CampusMienList.seItem = selected[0];
        return true;
    }
};

/**
* 点击添加
*/
CampusMienList.openAddCampusMien = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '450px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/campusMien/campusMien_add?orgId='+CampusMienList.orgId+"&code="+CampusMienList.code
    });
    this.layerIndex = index;
};
/**
 * 点击添加
 */
CampusMienList.openEditCampusMien = function () {
    if (this.check()){
    var index = layer.open({
        type: 2,
        title: '修改',
        area: ['800px', '450px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/campusMien/campusMien_update/'+this.seItem.id
    });
    this.layerIndex = index;
    }
};
/**
 * 删除
 */
CampusMienList.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/campusMien/delete/"+this.seItem.id, function (data) {
            if (data.code===200){
                Feng.success(data.msg);
                CampusMienList.table.refresh();
            }else {
                Feng.error(data.msg);
            }

        }, function (data) {
            Feng.error(data.msg);
        });
        ajax.start();
    }
};

/**
 * 查询列表
 */
CampusMienList.search = function () {
    var queryData = {};
    queryData['code'] = $("#condition").val();
    CampusMienList.table.refresh({query: queryData});
};
CampusMienList.formParams = function () {
    var queryData = {};
    queryData['orgId'] = CampusMienList.orgId;
    queryData['code'] = CampusMienList.code;
    return queryData;
};
$(function () {
    CampusMienList.orgId=$("#orgId").val();
    CampusMienList.code=$("#code").val();
    var defaultColunms = CampusMienList.initColumn();
    var table = new BSTable(CampusMienList.id, "/campusMien/list", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams(CampusMienList.formParams());
    CampusMienList.table = table.init();
});