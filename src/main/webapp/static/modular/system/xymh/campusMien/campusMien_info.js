/**
 * 初始化详情对话框
 */
var CampusMienInfoDlg = {
    campusMienInfoData : {},
    validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '标题不能为空'
                },
                stringLength: {
                    max: 40,
                    message: '长度必须小于40'
                }
            }
        },
        sort: {
            validators: {
                notEmpty: {
                    message: '排序值不能为空'
                }, regexp: {/* 只需加此键值对，包含正则表达式，和提示 */
                    regexp: /^[0-9]+$/,
                    message: '只能是数字'
                },
                stringLength: {
                    max: 4,
                    message: '长度必须小于4'
                }
            }
        }
    }
};
/**
 * 验证数据是否为空
 */
CampusMienInfoDlg.validate = function () {
    $('#campusMienInfoForm').data("bootstrapValidator").resetForm();
    $('#campusMienInfoForm').bootstrapValidator('validate');
    return $("#campusMienInfoForm").data('bootstrapValidator').isValid();
};
/**
 * 清除数据
 */
CampusMienInfoDlg.clearData = function() {
    this.campusMienInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CampusMienInfoDlg.set = function(key, val) {
    this.campusMienInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CampusMienInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
CampusMienInfoDlg.close = function() {
    parent.layer.close(window.parent.CampusMienList.layerIndex);
}

/**
 * 收集数据
 */
CampusMienInfoDlg.collectData = function() {
    this
        .set('id')
        .set('name')
        .set('pic',$("#avatar").val())
        .set('sort')
        .set('code')
        .set('orgId');
}

/**
 * 提交添加
 */
CampusMienInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/campusMien/saveOrUpdate", function(data){
        if (data.code===200){
        Feng.success(data.msg);
        window.parent.CampusMienList.table.refresh();
        CampusMienInfoDlg.close();
        }else {
            Feng.success(data.msg);
        }
    },function(data){
        Feng.success(data.msg);
    });
    ajax.set(this.campusMienInfoData);
    ajax.start();
};

/**
 * 提交修改
 */
CampusMienInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/campusMien/saveOrUpdate", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            window.parent.CampusMienList.table.refresh();
            CampusMienInfoDlg.close();
        }else {
            Feng.success(data.msg);
        }
    },function(data){
        Feng.success(data.msg);
    });
    ajax.set(this.campusMienInfoData);
    ajax.start();
}

$(function() {
    Feng.initValidator("campusMienInfoForm", CampusMienInfoDlg.validateFields);
    var avatarUp = new $WebUpload("avatar");
    avatarUp.setUploadBarId("progressBar");
    avatarUp.setUploadUrl($("#uploadUrl").val());
    avatarUp.init();
});
