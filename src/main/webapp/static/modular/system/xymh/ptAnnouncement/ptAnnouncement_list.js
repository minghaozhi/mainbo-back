/**
 * 管理初始化
 */
var PtAnnouncementlist = {
    id: "PtAnnouncementTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    orgId:null
};

/**
 * 初始化表格的列
 */
PtAnnouncementlist.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
            {title: '编号', field: 'id', visible: true, align: 'center',width:'70px',halign:"center",  valign: 'middle'},
            {title: '作者', field: 'userName', visible: true, align: 'center',width:'90px',halign:"center", valign: 'middle'},
            {title: '标题', field: 'title', visible: true, align: 'center', valign: 'middle'},
            {title: '通知范围', field: 'type', visible: true, align: 'center', valign: 'middle',formatter: function (value, row, index) {
                    var value = "";
                    if (row.type===0) {
                        value = "系统通知";
                    } else if (row.type===1) {
                        value = "区域通知";
                    }else if (row.type===2){
                        value = "学校通知";
                    }
                    return value;
                }},
           {title: '公告类型', field: 'tzggTypeName', visible: true, align: 'center',width:'100px', valign: 'middle'},
         {title: '发布时间', field: 'crtDttm', visible: true, align: 'left',width:'150px', valign: 'middle'},
        {title: '置顶', field: 'isTop', visible: true, align: 'left',width:'70px',halign:"center", valign: 'middle',
            formatter: function (value, row, index) {
                var value = "";
                if (row.isTop===1) {
                    value = "已置顶";
                } else  {
                    value = "未置顶";
                }
                return value;
            }},
        {
            field: 'operate',
            title: '操作',
            halign: "center",
            align: "left",
            formatter: btnGroupPtAnnouncementlist,    // 自定义方法，添加按钮组
            events: {               // 注册按钮组事件
                'click #edit': function (event, value, row, index) {
                    PtAnnouncementlist.openPtAnnouncementDetail(row.id)
                },
                'click #showUser': function (event, value, row, index) {
                    PtAnnouncementlist.openShowInfo(row.id)
                },
                'click #delUser': function (event, value, row, index) {
                    PtAnnouncementlist.delete(row.id);
                },
                'click #resetPwd': function (event, value, row, index) {
                    PtAnnouncementlist.updateTop(row)
                }
            }
        }

    ];
};
function btnGroupPtAnnouncementlist() {   // 自定义方法，添加操作按钮
    // data-target="xxx" 为点击按钮弹出指定名字的模态框
    var html =
        '<a href="####" class="btn btn-primary" id="edit"  data-target="#edit" title="修改">' +
        '<span class="glyphicon glyphicon-edit"></span></a>' +
        '<a href="####" class="btn btn-info" id="showUser"  data-target="#showUser" ' +
        'style="margin-left:2px" title="查看">' +
        '<span class="glyphicon glyphicon-info-sign"></span></a>' +
        '<a href="####" class="btn btn-danger" id="delUser"  data-target="#deleteuser" ' +
        'style="margin-left:2px" title="删除">' +
        '<span class="glyphicon glyphicon-trash"></span></a>' +
        '<a href="####" class="btn btn-warning" id="resetPwd"  data-target="#resetPwd" ' +
        'style="margin-left:2px" title="置顶/取消置顶">' +
        '<span class="glyphicon glyphicon-off"></span></a>';
    return html
};
/**
 * 检查是否选中
 */
PtAnnouncementlist.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        PtAnnouncementlist.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
PtAnnouncementlist.openAddPtAnnouncement = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['1100px', '700px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptAnnouncement/ptAnnouncement_add?orgId='+PtAnnouncementlist.orgId
    });
    this.layerIndex = index;
};

/**
 * 打开修改页面
 */
PtAnnouncementlist.openPtAnnouncementDetail = function (id) {

        var index = layer.open({
            type: 2,
            title: '修改公告',
            area: ['1100px', '700px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptAnnouncement/ptAnnouncement_update/' +id
        });
        this.layerIndex = index;
};
/**
 * 打开查看详情
 */
PtAnnouncementlist.openShowInfo = function (id) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['1100px', '700px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptAnnouncement/detail/' +id
        });
        this.layerIndex = index;
};
/**
 * 删除
 */
PtAnnouncementlist.delete = function (id) {

        var ajax = new $ax(Feng.ctxPath + "/ptAnnouncement/delete/"+id, function (data) {
            if(data.code===200){
                Feng.success(data.msg);
                PtAnnouncementlist.table.refresh();
            }else {
                Feng.error(data.msg);
            }

        }, function (data) {
            Feng.error(data.msg);
        });
        ajax.start();
};
/**
 * 置顶状态
 * @param row
 */
PtAnnouncementlist.updateTop = function (row) {
        let status='';
        let enable;
        if (row.isTop===1){
            status="取消置顶";
            enable=0;
        }else {
            status="置顶";
            enable=1;
        }
        var message = "您确定要"+status+"该职务吗？";
        parent.layer.confirm(message, {
            btn: ['确定', '取消'],
            shade: false //不显示遮罩
        }, function () {
            var ajax = new $ax(Feng.ctxPath + "/ptAnnouncement/isTop", function (data) {
                if (data.code===200){
                    Feng.success(data.msg);
                    var index =parent.layer.getFrameIndex(window.name);//获取当前弹窗的Id
                    layer.close(index);//关闭
                    PtAnnouncementlist.table.refresh();
                }else {
                    Feng.error(data.msg);
                }

            }, function (data) {
                Feng.error(data.msg);
            });
            ajax.set("id", row.id);
            ajax.set("isTop", enable);
            ajax.start();
        });

};
PtAnnouncementlist.getSelections=function() {
    var selected = $('#' + PtAnnouncementlist.id).bootstrapTable('getSelections');

    for (var i = 0; i < selected.length; i++) {
        ids[i] = selected[i].id;
    }
    return ids;
}
/**
 * 删除
 */
PtAnnouncementlist.batchDelete = function () {
    const ids =this.getSelections();
    if (ids.length === 0) {
        layer.msg("请至少选择一条数据", {icon: 5});
        return;
    }
    var ajax = new $ax(Feng.ctxPath + "/ptAnnouncement/batchdelete", function (data) {
        if (data.code === 200) {
            Feng.success(data.msg);
        }
        PtAnnouncementlist.table.refresh();
    }, function (data) {
        Feng.error(data.msg);
    });
    ajax.set("ids", ids.join(","));
    ajax.start();
};

/**
 * 查询列表
 */
PtAnnouncementlist.search = function () {
    var queryData = {};
    queryData['orgId'] = $("#orgId").val();
    queryData['title'] = $("#title").val();
    PtAnnouncementlist.table.refresh({query: queryData});
};
PtAnnouncementlist.formParams = function () {
    var queryData = {};
    queryData['orgId'] = PtAnnouncementlist.orgId;
   return queryData;
};
$(function () {
    PtAnnouncementlist.orgId=$("#orgId").val();
    var defaultColunms = PtAnnouncementlist.initColumn();
    var table = new BSTable(PtAnnouncementlist.id, "/ptAnnouncement/list", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams(PtAnnouncementlist.formParams());
    PtAnnouncementlist.table = table.init();
});
