/**
 * 管理初始化
 */
var PtAnnouncementDraft = {
    id: "PtAnnouncementDraftTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
PtAnnouncementDraft.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
        {title: '编号', field: 'id', visible: true, align: 'center',width:'70px',halign:"center",  valign: 'middle'},
        {title: '作者', field: 'userName', visible: true, align: 'center',width:'120px',halign:"center", valign: 'middle'},
        {title: '标题', field: 'title', visible: true, align: 'center', valign: 'middle'},
        {title: '通知范围', field: 'type', visible: true, align: 'center', valign: 'middle',formatter: function (value, row, index) {
                var value = "";
                if (row.type===0) {
                    value = "系统通知";
                } else if (row.type===1) {
                    value = "区域通知";
                }else if (row.type===2){
                    value = "学校通知";
                }
                return value;
            }},
        {title: '公告类型', field: 'tzggTypeName', visible: true, align: 'center',width:'100px', valign: 'middle'},
        {title: '发布时间', field: 'crtDttm', visible: true, align: 'left',width:'150px', valign: 'middle'},
        {title: '置顶', field: 'isTop', visible: true, align: 'left',width:'70px',halign:"center", valign: 'middle',
            formatter: function (value, row, index) {
                var value = "";
                if (row.isTop===1) {
                    value = "已置顶";
                } else  {
                    value = "未置顶";
                }
                return value;
            }},
        {
            field: 'operate',
            title: '操作',
            halign: "center",
            align: "left",
            formatter: btnGroupPtAnnouncementDraft,    // 自定义方法，添加按钮组
            events: {               // 注册按钮组事件
                'click #edit': function (event, value, row, index) {
                    PtAnnouncementDraft.openEditPtAnnouncement(row.id)
                },
                'click #delUser': function (event, value, row, index) {
                    PtAnnouncementDraft.delete(row.id);
                },
            }
        }

    ];
};
function btnGroupPtAnnouncementDraft() {   // 自定义方法，添加操作按钮
                        // data-target="xxx" 为点击按钮弹出指定名字的模态框
    var html =
        '<a href="####" class="btn btn-primary" id="edit"  data-target="#edit" title="修改">' +
        '<span class="glyphicon glyphicon-edit"></span></a>' +
        '<a href="####" class="btn btn-danger" id="delUser"  data-target="#deleteuser" ' +
        'style="margin-left:2px" title="删除">' +
        '<span class="glyphicon glyphicon-trash"></span></a>' ;
    return html
};
/**
 * 检查是否选中
 */
PtAnnouncementDraft.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        PtAnnouncementDraft.seItem = selected[0];
        return true;
    }
};
/**
 * 打开查看详情
 */
PtAnnouncementDraft.openEditPtAnnouncement = function (id) {
    var index = layer.open({
        type: 2,
        title: '详情',
        area: ['1100px', '700px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptAnnouncement/ptAnnouncement_update/' +id
    });
    this.layerIndex = index;
};
var ids = new Array(0);
function getSelections() {
    var selected = $('#' + PtAnnouncementDraft.id).bootstrapTable('getSelections');

    for (var i = 0; i < selected.length; i++) {
        ids[i] = selected[i].id;
    }
    return ids;
}
/**
 * 删除
 */
PtAnnouncementDraft.delete = function (id) {
    var ajax = new $ax(Feng.ctxPath + "/ptAnnouncement/delete/"+id, function (data) {
        if(data.code===200){
            Feng.success(data.msg);
            PtAnnouncementDraft.table.refresh();
        }else {
            Feng.error(data.msg);
        }

    }, function (data) {
        Feng.error(data.msg);
    });
    ajax.start();
};
/**
 * 批量删除
 */
PtAnnouncementDraft.batchDelete = function () {
    const ids = getSelections();
    if (ids.length === 0) {
        layer.msg("请至少选择一条数据", {icon: 5});
        return;
    }
    var ajax = new $ax(Feng.ctxPath + "/ptAnnouncement/batchdelete", function (data) {
        if (data.code === 200) {
            Feng.success(data.msg);
        }
        PtAnnouncementDraft.table.refresh();
    }, function (data) {
        Feng.error(data.msg);
    });
    ajax.set("ids", ids.join(","));
    ajax.start();
};
/**
 * 查询列表
 */
PtAnnouncementDraft.search = function () {
    var queryData = {};
    queryData['orgId'] = $("#orgId").val();
    queryData['title'] = $("#title1").val();
    queryData['isDraft'] =1;
    PtAnnouncementDraft.table.refresh({query: queryData});
};
PtAnnouncementDraft.formParams = function () {
    var queryData = {};
    queryData['orgId'] = PtAnnouncementDraft.orgId;
    queryData['isDraft'] =1;
    return queryData;
};
$(function () {
    PtAnnouncementDraft.orgId=$("#orgId").val();
    var defaultColunms = PtAnnouncementDraft.initColumn();
    var table = new BSTable(PtAnnouncementDraft.id, "/ptAnnouncement/draftList", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams(PtAnnouncementDraft.formParams());
    PtAnnouncementDraft.table = table.init();
});
