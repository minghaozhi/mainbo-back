/**
 * 初始化详情对话框
 */
var PtAnnouncementInfoDlg = {
    ptAnnouncementInfoData : {},
    editor: null,
    validateFields: {
        userName: {
            validators: {
                stringLength: {
                    max: 10,
                    message: '长度必须小于10'
                }
            }
            },
        title: {
            validators: {
                notEmpty: {
                    message: '标题不能为空'
                },
                stringLength: {
                    max: 50,
                    message: '长度必须小于50'
                }
            }
        },
        editor: {
            validators: {
                notEmpty: {
                    message: '内容不能为空'
                },
                stringLength: {
                    max: 5000,
                    message: '长度必须小于5000'
                }
            }
        },
        brief: {
            validators: {
                notEmpty: {
                    message: '内容摘要不能为空'
                }, stringLength: {/*长度提示*/
                    max: 100,
                    message: '长度必须小于100'
                }/*最后一个没有逗号*/
            }
        }
    }
};
/**
 * 验证数据是否为空
 */
PtAnnouncementInfoDlg.validate = function () {
    $('#announcementInfoForm').data("bootstrapValidator").resetForm();
    $('#announcementInfoForm').bootstrapValidator('validate');
    return $("#announcementInfoForm").data('bootstrapValidator').isValid();
};
/**
 * 清除数据
 */
PtAnnouncementInfoDlg.clearData = function() {
    this.ptAnnouncementInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtAnnouncementInfoDlg.set = function(key, val) {
    this.ptAnnouncementInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtAnnouncementInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
PtAnnouncementInfoDlg.close = function() {
    parent.layer.close(window.parent.PtAnnouncementlist.layerIndex);
}

/**
 * 收集数据
 */
PtAnnouncementInfoDlg.collectData0 = function() {
    this.ptAnnouncementInfoData['content'] = PtAnnouncementInfoDlg.editor.txt.html();
    // this.set("content",PtAnnouncementInfoDlg.editor.txt.html());
    this.set('id').set('orgId').set('userName').set('title').set('attachs').set("isDraft",0).set('brief').set('tzggType').set("isDisplay", $('input:radio:checked').val());
};
/**
 * 收集数据
 */
PtAnnouncementInfoDlg.collectData1 = function() {
    this.ptAnnouncementInfoData['content'] = PtAnnouncementInfoDlg.editor.txt.html();
    // this.set("content",PtAnnouncementInfoDlg.editor.txt.html());
    this.set('id').set('orgId').set('userName').set("isDisplay",$('input:radio:checked').val()).set('title').set('attachs').set("isDraft",1).set('brief').set('tzggType');
};
/**
 * 提交添加
 */
PtAnnouncementInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData0();
    if (!this.validate()) {
        return;
    }
    if (PtAnnouncementInfoDlg.editor.txt.html()==='<p><br></p>'){
        Feng.error("内容不能为空");
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptAnnouncement/saveOrUpdate", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            window.parent.PtAnnouncementlist.table.refresh();
            window.parent.PtAnnouncementDraft.table.refresh();
            PtAnnouncementInfoDlg.close();
        }else {
            Feng.error(data.msg);
        }
    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.ptAnnouncementInfoData);
    ajax.start();
}
/**
 * 存草稿
 */
PtAnnouncementInfoDlg.draftSubmit = function() {

    this.clearData();
    this.collectData1();
    if (!this.validate()) {
        return;
    }
    if (PtAnnouncementInfoDlg.editor.txt.html()==='<p><br></p>'){
        Feng.error("内容不能为空");
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptAnnouncement/saveOrUpdate", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            window.parent.PtAnnouncementlist.table.refresh();
            window.parent.PtAnnouncementDraft.table.refresh();
            var index = parent.layer.getFrameIndex(window.name);
            parent.layer.close(index);
        }else {
            Feng.error(data.msg);
        }
    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.ptAnnouncementInfoData);
    ajax.start();
}
/**
 * 提交修改
 */
PtAnnouncementInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData0();
    if (!this.validate()) {
        return;
    }
    if (PtAnnouncementInfoDlg.editor.txt.html()==='<p><br></p>'){
        Feng.error("内容不能为空");
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptAnnouncement/saveOrUpdate", function(data){
        Feng.success("修改成功!");
        window.parent.location.reload();
        var index = parent.layer.getFrameIndex(window.name);
        window.parent.PtAnnouncementDraft.table.refresh();
        window.parent.PtAnnouncementlist.table.refresh();
        parent.layer.close(index);
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.ptAnnouncementInfoData);
    ajax.start();
}

$(function () {
    Feng.initValidator("announcementInfoForm", PtAnnouncementInfoDlg.validateFields);
    var storageUrl=$("#storageUrl").val();
    //初始化编辑器
    var E = window.wangEditor;
    var editor = new E('#editor');
    // 自定义菜单配置
    editor.customConfig.menus = [
        'head',  // 标题
        'bold',  // 粗体
        'fontSize',  // 字号
        'fontName',  // 字体
        'italic',  // 斜体
        'underline',  // 下划线
        'strikeThrough',  // 删除线
        'foreColor',  // 文字颜色
        'backColor',  // 背景颜色
        'link',  // 插入链接
        'list',  // 列表
        'justify',  // 对齐方式
        'quote',  // 引用
        'emoticon',  // 表情
        'image',  // 插入图片
        'table',  // 表格
        'undo',  // 撤销
        'redo'  // 重复
    ],
    editor.customConfig.uploadImgServer = $("#uploadUrl").val(); //上传URL
    editor.customConfig.uploadImgParamsWithUrl = true;
    editor.customConfig.customUploadImg = function (files, insert) {//对上传的图片进行处理，图片上传的方式
        var data = new FormData()
        data.append("file", files[0])
        data.append("name",files[0].name,)
        data.append("relativePath", 'photo/ico')
        data.append("isWebRes", false)
        $.ajax({
            url:$("#uploadUrl").val(),
            type: "post",
            data: data,
            processData: false,
            contentType: false,
            success: function (data) {
                $.ajax({
                    type:"post",
                    async : false,
                    url:Feng.ctxPath+"/file/saveResource",
                    data:{'name':files[0].name,'rmsResId':data.key,'size':files[0].size,'path':"photo/ico","ext":files[0].ext},
                    dataType:"json",
                    cache: false,
                    success: function(obj){
                    },
                });
                //这里是对图片格式进行处理，我这里因为是要保存到本地服务器上，将图片前缀修改
                editor.cmd.do('insertHTML', '<img style="width: 200px;height: 200px;" src="'+storageUrl+'/resource/' + data.key +"/preview"+ '" alt="">')


            },
            error: function () {
                alert("图片上传错误")
            }
        })
    },
        editor.create();
    editor.txt.html($("#contentVal").val());
    PtAnnouncementInfoDlg.editor = editor;
});
