/**
 * 初始化详情对话框
 */
var PtNewsInfoDlg = {
    ptNewsInfoData : {},
    editor: null,
    validateFields: {
        author: {
            validators: {
                stringLength: {
                    max: 16,
                    message: '长度必须小于16'
                }
            }
        },
        title: {
            validators: {
                notEmpty: {
                    message: '标题不能为空'
                },
                stringLength: {/*长度提示*/
                    max: 100,
                    message: '长度必须小于100'
                }
            }
        },
        brief: {
            validators: {
                notEmpty: {
                    message: '内容摘要不能为空'
                }
            },
            stringLength: {/*长度提示*/
                max: 100,
                message: '长度必须小于100'
            }
        },
        sort: {
            validators: {
                notEmpty: {
                    message: '排序不能为空'
                },
                regexp: {/* 只需加此键值对，包含正则表达式，和提示 */
                    regexp: /^[0-9]+$/,
                    message: '只能是数字'
                }, stringLength: {/*长度提示*/
                    max: 4,
                    message: '长度必须小于4'
                }
            }
        },
        editor: {
            validators: {
                notEmpty: {
                    message: '内容不能为空'
                },
                stringLength: {
                    max: 5000,
                    message: '长度必须小于5000'
                }
            }
        }
    }
};
/**
 * 验证数据是否为空
 */
PtNewsInfoDlg.validate = function () {
    $('#newsInfoForm').data("bootstrapValidator").resetForm();
    $('#newsInfoForm').bootstrapValidator('validate');
    return $("#newsInfoForm").data('bootstrapValidator').isValid();
};
/**
 * 清除数据
 */
PtNewsInfoDlg.clearData = function() {
    this.ptNewsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtNewsInfoDlg.set = function(key, val) {
    this.ptNewsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtNewsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
PtNewsInfoDlg.close = function() {
    parent.layer.close(window.parent.NewsList.layerIndex);
}

/**
 * 收集数据
 */
PtNewsInfoDlg.collectData = function() {
    this.ptNewsInfoData['content'] = PtNewsInfoDlg.editor.txt.html();
    this
    .set('id')
    .set('orgId')
        .set('img',$("#avatar").val())
    .set('title')
    .set('brief')
    .set('isTop',$("input[name='isTop']:checked").val())
    .set('isDraft',0)
    .set('isComm',$("input[name='isComm']:checked").val())
    .set('author')
    .set('sort')
    .set('category')
    .set('code');
};
PtNewsInfoDlg.collectData1 = function() {
    this.ptNewsInfoData['content'] = PtNewsInfoDlg.editor.txt.html();
    this
        .set('id')
        .set('orgId')
        .set('img',$("#avatar").val())
        .set('title')
        .set('brief')
        .set('isTop',$("input[name='isTop']:checked").val())
        .set('isDraft',1)
        .set('isComm',$("input[name='isComm']:checked").val())
        .set('author')
        .set('sort')
        .set('category')
        .set('code');
};
/**
 * 提交添加
 */
PtNewsInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    if (PtNewsInfoDlg.editor.txt.html()==='<p><br></p>'){
        Feng.error("内容不能为空");
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptNews/addOrUpdate", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            window.parent.NewsDraftList.table.refresh();
            window.parent.NewsList.table.refresh();
            var index = parent.layer.getFrameIndex(window.name);
            parent.layer.close(index);
        }else {
            Feng.error(data.msg);
        }
    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.ptNewsInfoData);
    ajax.start();
};

/**
 * 提交修改
 */
PtNewsInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    if (PtNewsInfoDlg.editor.txt.html()==='<p><br></p>'){
        Feng.error("内容不能为空");
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptNews/addOrUpdate", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            window.parent.NewsList.table.refresh();
            window.parent.NewsDraftList.table.refresh();
            var index = parent.layer.getFrameIndex(window.name);
            parent.layer.close(index);
            PtNewsInfoDlg.close();
        }else {
            Feng.error(data.msg);
        }
    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.ptNewsInfoData);
    ajax.start();
};

/**
 * 存草稿
 */
PtNewsInfoDlg.draftSubmit = function() {

    this.clearData();
    this.collectData1();
    if (!this.validate()) {
        return;
    }
    if (PtNewsInfoDlg.editor.txt.html()==='<p><br></p>'){
        Feng.error("内容不能为空");
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptNews/addOrUpdate", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            window.parent.NewsDraftList.table.refresh();
            window.parent.NewsList.table.refresh();
            var index = parent.layer.getFrameIndex(window.name);
            parent.layer.close(index);
        }else {
            Feng.error(data.msg);
        }
    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.ptNewsInfoData);
    ajax.start();
};

$(function() {

    Feng.initValidator("newsInfoForm", PtNewsInfoDlg.validateFields);
    var avatarUp = new $WebUpload("avatar");
    avatarUp.setUploadBarId("progressBar");
    avatarUp.setUploadUrl($("#uploadUrl").val());
    avatarUp.init();

    var storageUrl=$("#storageUrl").val();
    //初始化编辑器
    var E = window.wangEditor;
    var editor = new E('#editor');
    // 自定义菜单配置
    editor.customConfig.menus = [
        'head',  // 标题
        'bold',  // 粗体
        'fontSize',  // 字号
        'fontName',  // 字体
        'italic',  // 斜体
        'underline',  // 下划线
        'strikeThrough',  // 删除线
        'foreColor',  // 文字颜色
        'backColor',  // 背景颜色
        'link',  // 插入链接
        'list',  // 列表
        'justify',  // 对齐方式
        'quote',  // 引用
        'emoticon',  // 表情
        'image',  // 插入图片
        'table',  // 表格
        'undo',  // 撤销
        'redo'  // 重复
    ];
    editor.customConfig.uploadImgServer = $("#uploadUrl").val(); //上传URL
    editor.customConfig.uploadImgParamsWithUrl = true;
    editor.customConfig.customUploadImg = function (files, insert) {//对上传的图片进行处理，图片上传的方式
        var data = new FormData();
        data.append("file", files[0]);
        data.append("name",files[0].name,);
        data.append("relativePath", 'photo/ico');
        data.append("isWebRes", false);
        $.ajax({
            url:$("#uploadUrl").val(),
            type: "post",
            data: data,
            processData: false,
            contentType: false,
            success: function (data) {
                $.ajax({
                    type:"post",
                    async : false,
                    url:Feng.ctxPath+"/file/saveResource",
                    data:{'name':files[0].name,'rmsResId':data.key,'size':files[0].size,'path':"photo/ico","ext":files[0].ext},
                    dataType:"json",
                    cache: false,
                    success: function(obj){
                    },
                });
                //这里是对图片格式进行处理，我这里因为是要保存到本地服务器上，将图片前缀修改
                editor.cmd.do('insertHTML', '<img style="width: 200px;height: 200px;" src="'+storageUrl+'/resource/' + data.key +"/preview"+ '" alt="">')


            },
            error: function () {
                alert("图片上传错误")
            }
        })
    };
    editor.create();
    editor.txt.html($("#contentVal").val());
    PtNewsInfoDlg.editor = editor;
});
