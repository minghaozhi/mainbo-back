/**
 * 初始化详情对话框
 */
var MenHu = {
    MenHuInfoData : {}
};
/**
 * 清除数据
 */
MenHu.clearData = function() {
    this.menuInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
MenHu.set = function(key, val) {
    this.menuInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
MenHu.get = function(key) {
    return $("#" + key).val();
}
/**
* 收集数据
*/
MenHu.collectData = function() {
    this
        .set('id',$("#id").val())
        .set('orgId')
        .set('portalName')
        .set('portalBottom')
        .set('portalContact')
        .set('portalLogo',$("#avatar").val());
};
/**
 * 提交添加
 */
MenHu.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/menHu/saveOrUpdate", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            location.reload();
            MenHu.close();
        }else {
            Feng.success(data.msg);
        }
    },function(data){
        Feng.success(data.msg);
    });
    ajax.set(this.menuInfoData);
    ajax.start();
};
$(function() {
    var avatarUp = new $WebUpload("avatar");
    avatarUp.setUploadBarId("progressBar");
    avatarUp.setUploadUrl($("#uploadUrl").val());
    avatarUp.init();
});