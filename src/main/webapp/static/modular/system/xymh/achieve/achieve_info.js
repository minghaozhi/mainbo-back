/**
 * 初始化详情对话框
 */
var AchieveInfoDlg = {
    achieveInfoData : {},
    editor: null,
    validateFields: {
        author: {
            validators: {
                stringLength: {
                    max: 16,
                    message: '长度必须小于16'
                }
            }
        },
        title: {
            validators: {
                notEmpty: {
                    message: '标题不能为空'
                },
                stringLength: {/*长度提示*/
                    max: 100,
                    message: '长度必须小于100'
                }
            }
        },
        editor: {
            validators: {
                notEmpty: {
                    message: '内容不能为空'
                },
                stringLength: {
                    max: 5000,
                    message: '长度必须小于5000'
                }
            }
        },
    }
};

/**
 * 验证数据是否为空
 */
AchieveInfoDlg.validate = function () {
    $('#achieveInfoForm').data("bootstrapValidator").resetForm();
    $('#achieveInfoForm').bootstrapValidator('validate');
    return $("#achieveInfoForm").data('bootstrapValidator').isValid();
};
/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AchieveInfoDlg.set = function(key, val) {
    this.achieveInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
};
/**
 * 清除数据
 */
AchieveInfoDlg.clearData = function() {
    this.achieveInfoData = {};
};

/**
 * 关闭此对话框
 */
AchieveInfoDlg.close = function() {
    parent.layer.close(window.parent.Achievelist.layerIndex);
};
/**
 * 收集数据
 */
AchieveInfoDlg.collectData= function() {
    this.achieveInfoData['content'] = AchieveInfoDlg.editor.txt.html();
    this
        .set('id')
        .set("orgId")
        .set("code")
        .set('category')
        .set('author')
        .set('title')
        .set("isDraft",0)
        .set('isTop',$('input:radio:checked').val());
};
/**
 * 收集数据
 */
AchieveInfoDlg.collectData1= function() {
    this.achieveInfoData['content'] = AchieveInfoDlg.editor.txt.html();
    this
        .set('id')
        .set("orgId")
        .set("code")
        .set('category')
        .set('author')
        .set('title')
        .set("isDraft",1)
        .set('isTop',$('input:radio:checked').val());
};
AchieveInfoDlg.addSubmit=function () {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    if (AchieveInfoDlg.editor.txt.html()==='<p><br></p>'){
        Feng.error("内容不能为空");
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptNews/addOrUpdate", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            window.parent.Achievelist.table.refresh();
            AchieveInfoDlg.close();
        }else {
            Feng.error(data.msg);
        }
    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.achieveInfoData);
    ajax.start();
};

AchieveInfoDlg.editSubmit=function () {
    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    if (AchieveInfoDlg.editor.txt.html()==='<p><br></p>'){
        Feng.error("内容不能为空");
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptNews/addOrUpdate", function (data) {
        if (data.code === 200) {
            Feng.success("修改成功");
            window.parent.location.reload();
            var index = parent.layer.getFrameIndex(window.name);
            parent.layer.close(index);
            window.parent.Achievelist.table.refresh();
            AchieveInfoDlg.close();
        } else {
            Feng.error(data.msg);
        }
    }, function (data) {
        Feng.error(data.msg);
    });
    ajax.set(this.achieveInfoData);
    ajax.start();
};

/**
 * 存草稿
 */
AchieveInfoDlg.draftSubmit = function() {

    this.clearData();
    this.collectData1();
    if (!this.validate()) {
        return;
    }
    if (AchieveInfoDlg.editor.txt.html()==='<p><br></p>'){
        Feng.error("内容不能为空");
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptNews/addOrUpdate", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            window.parent.Achievelist.table.refresh();
            window.parent.AchieveDraftlist.table.refresh();
            var index = parent.layer.getFrameIndex(window.name);
            parent.layer.close(index);
            AchieveInfoDlg.close();
        }else {
            Feng.error(data.msg);
        }
    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.achieveInfoData);
    ajax.start();
};
$(function () {
    Feng.initValidator("achieveInfoForm", AchieveInfoDlg.validateFields);
    var storageUrl=$("#storageUrl").val();
    //初始化编辑器
    var E = window.wangEditor;
    var editor = new E('#editor');
    // 自定义菜单配置
    editor.customConfig.menus = [
        'head',  // 标题
        'bold',  // 粗体
        'fontSize',  // 字号
        'fontName',  // 字体
        'italic',  // 斜体
        'underline',  // 下划线
        'strikeThrough',  // 删除线
        'foreColor',  // 文字颜色
        'backColor',  // 背景颜色
        'link',  // 插入链接
        'list',  // 列表
        'justify',  // 对齐方式
        'quote',  // 引用
        'emoticon',  // 表情
        'image',  // 插入图片
        'table',  // 表格
        'undo',  // 撤销
        'redo'  // 重复
    ];
    editor.customConfig.uploadImgServer = $("#uploadUrl").val(); //上传URL
    editor.customConfig.uploadImgParamsWithUrl = true;
    editor.customConfig.customUploadImg = function (files, insert) {//对上传的图片进行处理，图片上传的方式
        var data = new FormData();
        data.append("file", files[0]);
        data.append("name",files[0].name,);
        data.append("relativePath", 'photo/ico');
        data.append("isWebRes", false);
        $.ajax({
            url:$("#uploadUrl").val(),
            type: "post",
            data: data,
            processData: false,
            contentType: false,
            success: function (data) {
                $.ajax({
                    type:"post",
                    async : false,
                    url:Feng.ctxPath+"/file/saveResource",
                    data:{'name':files[0].name,'rmsResId':data.key,'size':files[0].size,'path':"photo/ico","ext":files[0].ext},
                    dataType:"json",
                    cache: false,
                    success: function(obj){
                    },
                });
                //这里是对图片格式进行处理，我这里因为是要保存到本地服务器上，将图片前缀修改
                editor.cmd.do('insertHTML', '<img style="width: 200px;height: 200px;" src="'+storageUrl+'/resource/' + data.key +"/preview"+ '" alt="">')


            },
            error: function () {
                alert("图片上传错误")
            }
        })
    },
        editor.create();
    editor.txt.html($("#contentVal").val());
    AchieveInfoDlg.editor = editor;
});