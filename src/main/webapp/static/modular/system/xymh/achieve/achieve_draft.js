/**
 * 管理初始化
 */
var AchieveDraftlist = {
    id: "AchieveDraftTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    orgId:null,
    code:null
};

/**
 * 初始化表格的列
 */
AchieveDraftlist.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
        {title: '编号', field: 'id', visible: true, align: 'center',width:'70px',halign:"center",  valign: 'middle'},
        {title: '作者', field: 'author', visible: true, align: 'center',width:'90px',halign:"center", valign: 'middle'},
        {title: '标题', field: 'title', visible: true, align: 'center', valign: 'middle'},
        {title: '成果类型', field: 'typeName', visible: true, align: 'center', valign: 'middle'},
        {title: '发布时间', field: 'crtDttm', visible: true, align: 'left',width:'150px', valign: 'middle'},
        {title: '置顶', field: 'isTop', visible: true, align: 'left',width:'70px',halign:"center", valign: 'middle',
            formatter: function (value, row, index) {
                var value = "";
                if (row.isTop===1) {
                    value = "已置顶";
                } else  {
                    value = "未置顶";
                }
                return value;
            }},
        {
            field: 'operate',
            title: '操作',
            halign: "center",
            align: "left",
            formatter: btnGroupAchieveDraftlist,    // 自定义方法，添加按钮组
            events: {               // 注册按钮组事件
                'click #edit': function (event, value, row, index) {
                    AchieveDraftlist.openEditAchieve(row.id)
                },
                'click #showUser': function (event, value, row, index) {
                    AchieveDraftlist.openShowInfo(row.id)
                },
                'click #delUser': function (event, value, row, index) {
                    AchieveDraftlist.delete(row);
                }
            }
        }

    ];
};
function btnGroupAchieveDraftlist() {   // 自定义方法，添加操作按钮
                        // data-target="xxx" 为点击按钮弹出指定名字的模态框
    var html =
        '<a href="####" class="btn btn-primary" id="edit"  data-target="#edit" title="修改">' +
        '<span class="glyphicon glyphicon-edit"></span></a>' +
        '<a href="####" class="btn btn-info" id="showUser"  data-target="#showUser" ' +
        'style="margin-left:2px" title="查看">' +
        '<span class="glyphicon glyphicon-info-sign"></span></a>' +
        '<a href="####" class="btn btn-danger" id="delUser"  data-target="#deleteuser" ' +
        'style="margin-left:2px" title="删除">' +
        '<span class="glyphicon glyphicon-trash"></span></a>';
    return html
};
/*
* 检查是否选中
*/
AchieveDraftlist.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        Achievelist.seItem = selected[0];
        return true;
    }
};
/**
 * 进入到修改页面
 */
AchieveDraftlist.openEditAchieve=function(id){
    var index = layer.open({
        type: 2,
        title: '修改',
        area: ['900px', '500px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/achieveShow/toUpdate?id='+id
    });
    this.layerIndex = index;
};
AchieveDraftlist.openShowInfo=function(id){
    var index = layer.open({
        type: 2,
        title: '详情页',
        area: ['900px', '500px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/achieveShow/detail/'+id
    });
    this.layerIndex = index;
};
AchieveDraftlist.delete=function(row){

    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/ptNews/batchdelete", function (data) {
            Feng.success("删除成功!");
            AchieveDraftlist.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("batchDeleteIds",row.id)
        ajax.start();
    };

    Feng.confirm("是否刪除 " + row.title + "?", operation);
};
function getSelections() {
    var selected = $('#' + AchieveDraftlist.id).bootstrapTable('getSelections');
    var ids = new Array();
    for (var i = 0; i < selected.length; i++) {
        ids[i] = selected[i].id;
    }
    return ids.join(",");
}
AchieveDraftlist.batchDelete=function(){
    const ids = getSelections();
    if (ids.length === 0) {
        layer.msg("请至少选择一条数据", {icon: 5});
        return;
    }
    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/ptNews/batchdelete", function (data) {
            Feng.success("删除成功!");
            AchieveDraftlist.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("batchDeleteIds",ids);
        ajax.start();
    };

    Feng.confirm("是否批量刪除?", operation);
};
AchieveDraftlist.search = function () {
    var queryData = {};
    queryData['orgId'] = $("#orgId").val();
    queryData['title'] = $("#title").val();
    queryData['code'] = AchieveDraftlist.code;
    queryData['isDraft'] =1;
    AchieveDraftlist.table.refresh({query: queryData});
};
AchieveDraftlist.formParams = function () {
    var queryData = {};
    queryData['orgId'] = AchieveDraftlist.orgId;
    queryData['code'] = AchieveDraftlist.code;
    queryData['isDraft'] =1;
    return queryData;
};
$(function () {
    AchieveDraftlist.orgId=$("#orgId").val();
    AchieveDraftlist.code=$("#code").val();
    var defaultColunms = AchieveDraftlist.initColumn();
    var table = new BSTable(AchieveDraftlist.id, "/achieveShow/list", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams(AchieveDraftlist.formParams());
    AchieveDraftlist.table = table.init();
});
