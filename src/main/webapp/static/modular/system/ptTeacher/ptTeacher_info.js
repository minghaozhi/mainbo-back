/**
 * 初始化详情对话框
 */
var PtTeacherInfoDlg = {
    ptTeacherInfoData: {},
    validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '姓名不能为空'
                },
                stringLength: {/*长度提示*/
                    max: 10,
                    message: '长度必须小于10'
                }
            }
        },
        cellphone: {
            validators: {
                regexp: {
                    regexp: /^1\d{10}$/,
                    message: '手机号格式错误'
                },
                stringLength: {/*长度提示*/
                    max: 11,
                    message: '长度必须小于11'
                },/*最后一个没有逗号*/
            }
        },
        telephone: {
            validators: {
                regexp: {
                    regexp: /^1\d{10}$/,
                    message: '手机号格式错误'
                },
                stringLength: {/*长度提示*/
                    max: 11,
                    message: '长度必须小于11'
                },/*最后一个没有逗号*/
            }
        },
        schoolCard: {
            validators: {
                stringLength: {/*长度提示*/
                    max: 20,
                    message: '长度必须小于20'
                },/*最后一个没有逗号*/
                regexp: {/* 只需加此键值对，包含正则表达式，和提示 */
                    regexp: /^[0-9]+$/,
                    message: '只能是数字'
                }
            }
        },
        papersNumber: {
            validators: {
                stringLength: {/*长度提示*/
                    max: 18,
                    message: '长度必须小于18'
                }
            }
        },
        postCode: {
            validators: {
                stringLength: {/*长度提示*/
                    max: 6,
                    message: '长度必须小于6'
                }
            }
        },
        familyAddress: {
            validators: {
                stringLength: {/*长度提示*/
                    max: 50,
                    message: '长度必须小于50'
                }
            }
        },
        email: {
            validators: {
                regexp: {
                    regexp: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/,
                    message: '请输入正确的邮箱地址'
                }

            }
        },
    }
};
/**
 * 验证数据是否为空
 */
PtTeacherInfoDlg.validate = function () {
    $('#teacherInfoForm').data("bootstrapValidator").resetForm();
    $('#teacherInfoForm').bootstrapValidator('validate');
    return $("#teacherInfoForm").data('bootstrapValidator').isValid();
};
/**
 * 清除数据
 */
PtTeacherInfoDlg.clearData = function () {
    this.ptTeacherInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtTeacherInfoDlg.set = function (key, val) {
    this.ptTeacherInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtTeacherInfoDlg.get = function (key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
PtTeacherInfoDlg.close = function () {
    parent.layer.close(window.parent.PtTeacher.layerIndex);
}

/**
 * 收集数据
 */
PtTeacherInfoDlg.collectData = function () {
    this
        .set('id')
        .set('orgId')
        .set('orgName')
        .set('name')
        .set('photo', $("#avatar").val())
        .set('sex')
        .set('birthday')
        .set('birthPlaceCode')
        .set('nativePlace')
        .set('papersType')
        .set('papersNumber')
        .set('politicalStatus')
        .set('familyAddress')
        .set("nationCode")
        .set("politicalStatus")
        .set('telephone')
        .set('cellphone')
        .set('postCode')
        .set('email')
        .set('schoolCard');
}

/**
 * 提交添加
 */
PtTeacherInfoDlg.addSubmit = function () {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptTeacher/add", function (data) {

        if (data.code === 200) {
            Feng.success(data.msg);
            window.parent.PtTeacher.table.refresh();
            PtTeacherInfoDlg.close();
        } else {
            Feng.error(data.msg);
        }
    }, function (data) {
        Feng.error(data.msg);
    });
    ajax.set(this.ptTeacherInfoData);
    ajax.set("userType", $("#userType").val());
    ajax.start();
};

/**
 * 提交修改
 */
PtTeacherInfoDlg.editSubmit = function () {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptTeacher/update", function (data) {
        Feng.success("修改成功!");
        window.parent.PtTeacher.table.refresh();
        PtTeacherInfoDlg.close();
    }, function (data) {
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.ptTeacherInfoData);
    ajax.start();
}

$(function () {
    Feng.initValidator("teacherInfoForm", PtTeacherInfoDlg.validateFields);
    var sex = $("#sexValue").val();
    $("#sex").val(sex);
    // document.getElementById('sex1').innerHTML = sex;
    // $("#sex1").text(sex);
    // 初始化头像上传
    var avatarUp = new $WebUpload("avatar");
    avatarUp.setUploadBarId("progressBar");
    avatarUp.setUploadUrl($("#uploadUrl").val());
    avatarUp.init();
});
