/**
 * 管理初始化
 */
var PtTeacher = {
    id: "PtTeacherTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    orgId:"0",
    areaId:0
};
var ids = new Array(0);
function getSelections() {
    var selected = $('#' + PtTeacher.id).bootstrapTable('getSelections');

    for (var i = 0; i < selected.length; i++) {
        ids[i] = selected[i].id;
    }
    return ids;
}
function removeSelections() {
    $('#' + PtTeacher.id).bootstrapTable('uncheckAll');
    ids=new Array(0);
}
/**
 * 初始化表格的列
 */
PtTeacher.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
        {
            title: '编号',
            field: 'id',
            visible: true,
            halign: "center",
            align: "left",
            width: '90px',
            cellStyle: formatTableUnit,
            formatter: paramsMatter
        },
        {title: '账号', field: 'accountName', visible: true, width: '98px', align: 'center', valign: 'middle'},
        {title: '姓名', field: 'name', visible: true, halign: "center", align: "left", width: '100px', valign: 'middle'},
        {title: '手机号', field: 'cellphone', visible: true, width: '100px', align: 'center', valign: 'middle'},
        {title: '校卡号', field: 'schoolCard', visible: true, width: '90px', align: 'center', valign: 'middle'},
        {title: '学校', field: 'orgName', visible: true, halign: "center", align: "left", width: '90px', valign: 'middle'},
        {title: '角色类型', field: 'userType', visible: true, halign: "center", align: "left", width: '90px', valign: 'middle',formatter: function (value, row, index) {
                var value = "";
                if (row.userType===2) {
                    value = "教师";
                } else if (row.userType===300) {
                    value = "学校管理者";
                }else if (row.userType===301){
                    value="学校职工";
                }
                return value;
            }},
        {title: '职务', field: 'job', visible: true, halign: "center", align: "left", width: '140px', valign: 'middle'},
        {title: '状态', field: 'enable', visible: true, width: '50px', align: 'center', valign: 'middle', formatter: function (value, row, index) {
                var value = "";
                if (!row.enable) {
                    value = "冻结";
                } else {
                    value = "启用";
                }
                return value;
            }},
        {title: '创建时间', field: 'crtDttm', visible: true, halign: "center", align: "left", width: '160px', valign: 'middle'},
        {
            field: 'operate', title: '操作', halign: "center", align: "left", formatter: btnGroup,    // 自定义方法，添加按钮组
            events: {               // 注册按钮组事件
                'click #edit': function (event, value, row, index) {
                    PtTeacher.openEditTeacher(row.id)
                },
                'click #showUser': function (event, value, row, index) {
                    PtTeacher.openShowInfo(row.id)
                },
                'click #delUser': function (event, value, row, index) {
                    PtTeacher.delete(row.id);
                },
                'click #resetPwd': function (event, value, row, index) {
                    PtTeacher.resetPwd(row.id)
                }
            }
        }
    ];
};
//td宽度以及内容超过宽度隐藏
function formatTableUnit(value, row, index) {
    return {
        css: {
            "white-space": 'nowrap',
            "text-overflow": 'ellipsis',
            "overflow": 'hidden',
            "max-width": '150px'
        }
    }
}
function paramsMatter(value, row, index, field) {
    var span = document.createElement('span');
    span.setAttribute('title', value);
    span.innerHTML = value;
    return span.outerHTML;
}
function btnGroup() {   // 自定义方法，添加操作按钮
    // data-target="xxx" 为点击按钮弹出指定名字的模态框
    var html =
        '<a href="####" class="btn btn-primary" id="edit"  data-target="#edit" title="修改">' +
        '<span class="glyphicon glyphicon-edit"></span></a>' +
        '<a href="####" class="btn btn-info" id="showUser"  data-target="#showUser" ' +
        'style="margin-left:2px" title="查看">' +
        '<span class="glyphicon glyphicon-info-sign"></span></a>' +
        '<a href="####" class="btn btn-danger" id="delUser"  data-target="#deleteuser" ' +
        'style="margin-left:2px" title="删除">' +
        '<span class="glyphicon glyphicon-trash"></span></a>' +
        '<a href="####" class="btn btn-warning" id="resetPwd"  data-target="#resetPwd" ' +
        'style="margin-left:2px" title="重置密码">' +
        '<span class="glyphicon glyphicon-off"></span></a>';
    return html
};
/**
 * 修改教师
 */
PtTeacher.openEditTeacher = function (id) {
    if (PtTeacher.orgId === "0") {
        layer.alert("请选择学校!");
        return;
    }
    if (id === "") {
        layer.alert("请选中一条!");
        return;
    }
    var index = layer.open({
        type: 2,
        title: '教师修改',
        area: ['800px', '700px'], //宽高
        fix: false, //不固定
        maxmin: true,
        shade: 0,
        content: Feng.ctxPath + '/ptTeacher/ptTeacher_update/' + id
    });
    this.layerIndex = index;
};


/**
 * 批量导入
 */
PtTeacher.import = function () {
    if (PtTeacher.orgId === "0") {
        layer.alert("请选择学校!");
        return;
    }
    var index = layer.open({
        type: 2,
        title: '批量导入教师',
        area: ['800px', '460px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptAccount/batch/add?orgId=' + PtTeacher.orgId + '&userType=3'
    });
    this.layerIndex = index;
};
PtTeacher.export = function () {

    window.location.href = Feng.ctxPath + "/ptTeacher/exportTeacher?templateType=xxyh&orgId=" + PtTeacher.orgId + "&areaId=" + PtTeacher.areaId;
};
/**
 * 删除
 */
PtTeacher.delete = function (id) {
    var ajax = new $ax(Feng.ctxPath + "/ptTeacher/delete/" + id, function (data) {
        if (data.code === 200) {
            Feng.success(data.msg);
        }
        PtTeacher.table.refresh();
    }, function (data) {
        Feng.error(data.msg);
    });
    ajax.start();
};
/**
 * 删除
 */
PtTeacher.batchDelete = function () {
    const ids = getSelections();
    if (ids.length === 0) {
        layer.msg("请至少选择一条数据", {icon: 5});
        return;
    }
    var ajax = new $ax(Feng.ctxPath + "/ptTeacher/batchdelete", function (data) {
        if (data.code === 200) {
            Feng.success(data.msg);
        }
        PtTeacher.table.refresh();
    }, function (data) {
        Feng.error(data.msg);
    });
    ajax.set("accountIds", ids.join(","));
    ajax.start();
};
/**
 *查看
 */
PtTeacher.openShowInfo = function (id) {
    if (PtTeacher.orgId === "0") {
        layer.alert("请选择学校!");
        return;
    }
    if (id === "") {
        layer.alert("请选中一条!");
        return;
    }
    var index = layer.open({
        type: 2,
        title: '查看',
        area: ['800px', '650px'], //宽高
        fix: false, //不固定
        maxmin: true,
        shade: 0,
        content: Feng.ctxPath + '/ptTeacher/ptTeacher_showInfo/' + id
    });
    this.layerIndex = index;
};
/**
 * 重置密码
 * @param id
 */
PtTeacher.resetPwd = function (id) {
    var ajax = new $ax(Feng.ctxPath + "/ptAccount/resetPwd?accountId=" + id, function (data) {
        if (data.code === 200) {
            Feng.success(data.msg);
        }
        PtTeacher.table.refresh();
    }, function (data) {
        Feng.error(data.msg);
    });
    ajax.start();
};
/**
 * 检查是否选中
 */
PtTeacher.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        PtTeacher.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
PtTeacher.openAddPtTeacher = function () {
    if (PtTeacher.orgId === "0") {
        layer.alert("请选择学校!");
        return;
    }
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '700px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptTeacher/ptTeacher_add?orgId=' + PtTeacher.orgId
    });
    this.layerIndex = index;
};

/**
 * 打开查看详情
 */
PtTeacher.openPtTeacherDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptTeacher/ptTeacher_update/' + PtTeacher.seItem.id
        });
        this.layerIndex = index;
    }
};


/**
 * 批量冻结
 */
PtTeacher.updateEnable = function (status) {
    const ids = getSelections();
    if (ids.length === 0) {
        layer.msg("请至少选择一条数据", {icon: 5});
        return;
    }
    var ajaxUrl;
    if (status == 0) {
        //批量冻结
        ajaxUrl = "/ptAccount/batchDisableAccount";
    } else {
        ajaxUrl = "/ptAccount/batchUpdateEnable";
    }
    var ajax = new $ax(Feng.ctxPath + ajaxUrl, function (data) {
        if (data.code === 200) {
            Feng.success(data.msg);
        }
        PtTeacher.table.refresh();
    }, function (data) {
        Feng.error(data.msg);
    });
    ajax.set("accountIds", ids.join(","));
    ajax.start();
};
/**
 * 职务管理
 */
PtTeacher.addOrEditPostManage=function(){
    const ids = getSelections();
    if (PtTeacher.orgId === "0") {
        layer.alert("请选择学校!");
        return;
    }
    if (ids.length===0) {
        layer.alert("请选择一条数据!");

        return;
    }
    if (ids.length>1) {
        layer.alert("只能选中一条哟!");
        removeSelections();
        return;
    }
    var index = layer.open({
        type: 2,
        title: '职务',
        area: ['850px', '600px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptAccount/toPostManage?accountId=' + ids
    });
    this.layerIndex = index;
};
/**
 * 查询列表
 */
PtTeacher.search = function () {
    var queryData = {};
    queryData['id'] = $("#id").val();
    queryData['name'] = $("#userName").val();
    queryData['accountName'] = $("#account").val();
    queryData['schoolCard'] = $("#schoolCard").val();
    queryData['sysRoleId'] = $("#sysRoleId").val();
    queryData['roleId'] = $("#roleId").val();
    queryData['phaseId'] = $("#phaseId").val();
    queryData['enable'] = $("#status").val();
    queryData['orgId'] = PtTeacher.orgId;
    queryData['areaId'] = PtTeacher.areaId;
    PtTeacher.table.refresh({query: queryData});
};
PtTeacher.onClickOrg = function (e, treeId, treeNode) {
    PtTeacher.orgId = '0';
    PtTeacher.areaId = 0;
    if (treeNode.flag == "area") {
        pid = treeNode.id;
        var data = {"areaId": pid};

        $.ajax({
            url: "/ptOrg/list",
            type: "post",
            dataType: "json",
            data: data,
            cache: false,

            success: function (data) {
                $.each(data, function (index, obj) {
                    var treeObj = $.fn.zTree.getZTreeObj("areaTree");
                    var nodes = treeObj.getSelectedNodes();
                    if (nodes && nodes.length > 0) {
                        treeObj.removeChildNodes(nodes[0]);
                    }

                    treeObj.addNodes(treeNode, {
                        id: obj.id,
                        pId: treeNode.id,
                        name: obj.shortName,
                        title: obj.name,
                        flag: "school",
                        open: true
                    });
                });
            }
        });
        PtTeacher.areaId = treeNode.id;
        PtTeacher.search();
    } else {
        PtTeacher.orgId = treeNode.id;
        PtTeacher.search();
    }
};
PtTeacher.formParams = function () {
    var queryData = {};
    queryData['orgId'] = PtTeacher.orgId;
    queryData['areaId'] = PtTeacher.areaId;
    return queryData;
};
$(function () {
    PtTeacher.orgId=$("#orgId").val();
    PtTeacher.areaId=$("#areaId").val();
    var defaultColunms = PtTeacher.initColumn();
    var table = new BSTable(PtTeacher.id, "/ptTeacher/list", defaultColunms);

    table.setQueryParams(PtTeacher.formParams());
    if (PtTeacher.orgId==="0"){
        table.setQueryParams("");
        PtTeacher.orgId=orgId;
        var ztree = new $ZTree("areaTree", "/back/sys/area/list");
        ztree.bindOnClick(PtTeacher.onClickOrg);
        ztree.init();
    }
    table.setPaginationType("client");
     PtTeacher.table = table.init();
});
