/**
 * 管理初始化
 */
var PtStudent = {
    id: "PtStudentTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    orgId: "0",
    areaId: 0,
};

function getSelections() {
    var selected = $('#' + PtStudent.id).bootstrapTable('getSelections');
    var ids = new Array();
    for (var i = 0; i < selected.length; i++) {
        ids[i] = selected[i].id;
    }
    return ids.join(",");
}

/**
 * 初始化表格的列
 */
PtStudent.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
        {
            title: '编号',
            field: 'id',
            visible: true,
            halign: "center",
            align: "left",
            width: '120px',
            cellStyle: formatTableUnit,
            formatter: paramsMatter
        },
        {title: '账号', field: 'account', visible: true, width: '98px', align: 'center', valign: 'middle'},
        {title: '姓名', field: 'name', visible: true, halign: "center", align: "left", width: '100px', valign: 'middle'},
        {title: '班级名称', field: 'className', visible: true, width: '98px', align: 'center', valign: 'middle'},
        {title: '手机号', field: 'cellphone', visible: true, width: '100px', align: 'center', valign: 'middle'},
        {title: '学生卡号', field: 'studentCardId', visible: true, width: '90px', align: 'center', valign: 'middle'},
        {
            title: '学校',
            field: 'orgName',
            visible: true,
            halign: "center",
            align: "left",
            width: '90px',
            valign: 'middle'
        },
        {
            title: '毕业状态', field: 'graduateState', visible: true, width: '70px', align: 'center', valign: 'middle',
            formatter: function (value, row, index) {
                var value = "";
                if (row.graduateState === 5) {
                    value = "未毕业";
                } else if (row.graduateState === 6) {
                    value = "毕业";
                } else {
                    value = "学校毕业";
                }

                return value;
            }
        },
        {title: '状态', field: 'enable', visible: true, width: '50px', align: 'center', valign: 'middle',
            formatter: function (value, row, index) {
                var value = "";
                if (!row.enable) {
                    value = "冻结";
                } else {
                    value = "启用";
                }

                return value;
            }},
        {
            title: '创建时间',
            field: 'crtDttm',
            visible: true,
            halign: "center",
            align: "left",
            width: '138px',
            valign: 'middle'
        }
        , {
            field: 'operate',
            title: '操作',
            halign: "center",
            align: "left",
            formatter: btnGroupStudent,    // 自定义方法，添加按钮组
            events: {               // 注册按钮组事件
                'click #edit': function (event, value, row, index) {
                    PtStudent.openEditPtStudent(row.id)
                },
                'click #showUser': function (event, value, row, index) {
                    PtStudent.openShowInfo(row.id)
                },
                'click #delUser': function (event, value, row, index) {
                    PtStudent.delete(row.id);
                },
                'click #resetPwd': function (event, value, row, index) {
                    PtStudent.resetPwd(row.id)
                }
            }
        }
    ];
};

function paramsMatter(value, row, index, field) {
    var span = document.createElement('span');
    span.setAttribute('title', value);
    span.innerHTML = value;
    return span.outerHTML;
}

//td宽度以及内容超过宽度隐藏
function formatTableUnit(value, row, index) {
    return {
        css: {
            "white-space": 'nowrap',
            "text-overflow": 'ellipsis',
            "overflow": 'hidden',
            "max-width": '150px'
        }
    }
}

function btnGroupStudent() {   // 自定义方法，添加操作按钮
                        // data-target="xxx" 为点击按钮弹出指定名字的模态框
    var html =
        '<a href="####" class="btn btn-primary" id="edit"  data-target="#edit" title="修改">' +
        '<span class="glyphicon glyphicon-edit"></span></a>' +
        '<a href="####" class="btn btn-info" id="showUser"  data-target="#showUser" ' +
        'style="margin-left:2px" title="查看">' +
        '<span class="glyphicon glyphicon-info-sign"></span></a>' +
        '<a href="####" class="btn btn-danger" id="delUser"  data-target="#deleteuser" ' +
        'style="margin-left:2px" title="删除">' +
        '<span class="glyphicon glyphicon-trash"></span></a>' +
        '<a href="####" class="btn btn-warning" id="resetPwd"  data-target="#resetPwd" ' +
        'style="margin-left:2px" title="重置密码">' +
        '<span class="glyphicon glyphicon-off"></span></a>';
    return html
};
/**
 * 检查是否选中
 */
PtStudent.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        PtStudent.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
PtStudent.openAddPtStudent = function () {
    if (PtStudent.orgId === "0") {
        layer.alert("请选择学校!");
        return;
    }
    var index = layer.open({
        type: 2,
        title: '学生添加',
        area: ['870px', '650px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptStudent/ptStudent_add?orgId=' + PtStudent.orgId
    });
    this.layerIndex = index;
};
/**
 * 修改学生
 */
PtStudent.openEditPtStudent = function (id) {
    if (PtStudent.orgId === "0") {
        layer.alert("请选择学校!");
        return;
    }
    if (id === "") {
        layer.alert("请选中一条!");
        return;
    }
    var index = layer.open({
        type: 2,
        title: '学生修改',
        area: ['870px', '650px'], //宽高
        fix: false, //不固定
        maxmin: true,
        shade: 0,
        content: Feng.ctxPath + '/ptStudent/ptStudent_update/' + id
    });
    this.layerIndex = index;
};
/**
 *查看
 */
PtStudent.openShowInfo = function (id) {
    if (PtStudent.orgId === "0") {
        layer.alert("请选择学校!");
        return;
    }
    if (id === "") {
        layer.alert("请选中一条!");
        return;
    }
    var index = layer.open({
        type: 2,
        title: '看看',
        area: ['800px', '650px'], //宽高
        fix: false, //不固定
        maxmin: true,
        shade: 0,
        content: Feng.ctxPath + '/ptStudent/ptStudent_showInfo/' + id
    });
    this.layerIndex = index;
};
/**
 * 批量导入
 */
PtStudent.import = function () {
    if (PtStudent.orgId === "0") {
        layer.alert("请选择学校!");
        return;
    }
    var index = layer.open({
        type: 2,
        title: '批量导入学生',
        area: ['800px', '460px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptAccount/batch/add?orgId=' + PtStudent.orgId + '&userType=4'
    });
    this.layerIndex = index;
};
PtStudent.export = function () {

    window.location.href = Feng.ctxPath + "/ptStudent/exportStudent?templateType=xxyh&orgId=" + PtStudent.orgId + "&areaId=" + PtStudent.areaId;
};
/**
 * 打开查看详情
 */
PtStudent.openPtStudentDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptStudent/ptStudent_update/' + PtStudent.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除
 */
PtStudent.delete = function (id) {
    var ajax = new $ax(Feng.ctxPath + "/ptStudent/delete/" + id, function (data) {
        if (data.code === 200) {
            Feng.success(data.msg);
        }
        PtStudent.table.refresh();
    }, function (data) {
        Feng.error(data.msg);
    });
    ajax.start();
};
/**
 * 删除
 */
PtStudent.batchDelete = function () {
    var ids = getSelections();
    if (ids.length === 0) {
        layer.msg("请至少选择一条数据", {icon: 5});
        return;
    }
    var ajax = new $ax(Feng.ctxPath + "/ptStudent/batchdelete", function (data) {
        if (data.code === 200) {
            Feng.success(data.msg);
        }
        PtStudent.table.refresh();
    }, function (data) {
        Feng.error(data.msg);
    });
    ajax.set("accountIds", ids);
    ajax.start();
};
/**
 * 重置密码
 * @param id
 */
PtStudent.resetPwd = function (id) {
    var ajax = new $ax(Feng.ctxPath + "/ptAccount/resetPwd?accountId=" + id, function (data) {
        if (data.code === 200) {
            Feng.success(data.msg);
        }
        PtStudent.table.refresh();
    }, function (data) {
        Feng.error(data.msg);
    });
    ajax.start();
};

/**
 * 批量冻结
 */
PtStudent.updateEnable = function (status) {
    const ids = getSelections();
    if (ids.length === 0) {
        layer.msg("请至少选择一条数据", {icon: 5});
        return;
    }
    var ajaxUrl;
    if (status == 0) {
        //批量冻结
        ajaxUrl = "/ptAccount/batchDisableAccount";
    } else {
        ajaxUrl = "/ptAccount/batchUpdateEnable";
    }
    var ajax = new $ax(Feng.ctxPath + ajaxUrl, function (data) {
        if (data.code === 200) {
            Feng.success(data.msg);
        }
        PtStudent.table.refresh();
    }, function (data) {
        Feng.error(data.msg);
    });
    ajax.set("accountIds", ids);
    ajax.start();
};
/**
 * 查询列表
 */
PtStudent.search = function () {
    var queryData = {};
    queryData['name'] = $("#name").val().trim();
    queryData['account'] = $("#account").val().trim();
    queryData['id'] = $("#id").val().trim();
    queryData['studentCardId'] = $("#studentCardId").val().trim();
    queryData['enable'] = $("#status").val().trim();
    queryData['graduateState'] = $("#graduateState").val().trim();
    queryData['orgId'] = PtStudent.orgId;
    queryData['areaId'] = PtStudent.areaId;
    PtStudent.table.refresh({query: queryData});
};
PtStudent.resetSearch = function () {
    $("#userName").val("");
    $("#account").val("");
    $("#id").val("");
    $("#status").val("");
    $("#studentCardId").val("");
    $("#graduateState").val("");
    PtStudent.search();
};
PtStudent.onClickOrg = function (e, treeId, treeNode) {
    PtStudent.orgId = '0';
    PtStudent.areaId = 0;
    if (treeNode.flag == "area") {
        pid = treeNode.id;
        var data = {"areaId": pid};

        $.ajax({
            url: "/ptOrg/list",
            type: "post",
            dataType: "json",
            data: data,
            cache: false,

            success: function (data) {
                $.each(data, function (index, obj) {
                    var treeObj = $.fn.zTree.getZTreeObj("areaTree");
                    var nodes = treeObj.getSelectedNodes();
                    if (nodes && nodes.length > 0) {
                        treeObj.removeChildNodes(nodes[0]);
                    }

                    treeObj.addNodes(treeNode, {
                        id: obj.id,
                        pId: treeNode.id,
                        name: obj.shortName,
                        title: obj.name,
                        flag: "school",
                        open: true
                    });
                });
            }
        });
        PtStudent.areaId = treeNode.id;
        PtStudent.search();
    } else {
        PtStudent.orgId = treeNode.id;
        PtStudent.search();
    }
};
PtStudent.formParams = function () {
    var queryData = {};
    queryData['userName'] ="";
    queryData['account'] ="";
    queryData['orgId'] = PtStudent.orgId;
    queryData['areaId'] = PtStudent.areaId;
    return queryData;
};
$(function () {
    var defaultColunms = PtStudent.initColumn();
    var table = new BSTable("PtStudentTable", "/ptStudent/list", defaultColunms);
    table.setPaginationType("client");
    var orgId=$("#orgId").val();
    PtStudent.areaId=$("#areaId").val();
    PtStudent.orgId=orgId;
    table.setQueryParams(PtStudent.formParams());
    if (orgId==="0"){
        PtStudent.orgId=orgId;
        table.setQueryParams("");
        var ztree = new $ZTree("areaTree", "/back/sys/area/list");
        ztree.bindOnClick(PtStudent.onClickOrg);
        ztree.init();
    }
    PtStudent.table = table.init();
});