/**
 * 初始化详情对话框
 */
var PtStudentInfoDlg = {
    ptStudentInfoData: {},
    validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '姓名不能为空'
                },
                stringLength: {/*长度提示*/
                    max: 10,
                    message: '长度必须小于10'
                }
            }
        },
        studentCode: {
            validators: {
                notEmpty: {
                    message: '学籍号不能为空'
                }
            }
        }
    }
};
/**
 * 验证数据是否为空
 */
PtStudentInfoDlg.validate = function () {
    $('#studentInfoForm').data("bootstrapValidator").resetForm();
    $('#studentInfoForm').bootstrapValidator('validate');
    return $("#studentInfoForm").data('bootstrapValidator').isValid();
};
/**
 * 清除数据
 */
PtStudentInfoDlg.clearData = function () {
    this.ptStudentInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtStudentInfoDlg.set = function (key, val) {
    this.ptStudentInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtStudentInfoDlg.get = function (key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
PtStudentInfoDlg.close = function () {
    parent.layer.close(window.parent.PtStudent.layerIndex);
}

/**
 * 收集数据
 */
PtStudentInfoDlg.collectData = function () {
    this
        .set('id')
        .set('orgId')
        .set('orgName')
        .set('name')
        .set('photo', $("#avatar").val())
        .set('originalPhoto')
        .set('sex')
        .set('birthday')
        .set('nativePlace')
        .set('studentCode')
        .set('nationCode')
        .set('bloodType')
        .set('familyAddress')
        .set('telephone')
        .set('cellphone')
        .set('parentsName')
        .set('parentsPhone')
        .set('postCode')
        .set('email')
        .set("poorType")
        .set('studentCardId');
};

/**
 * 提交添加
 */
PtStudentInfoDlg.addSubmit = function () {

    this.clearData();
    this.collectData();

    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptStudent/add", function (data) {
        Feng.success("添加成功!");
        window.parent.PtStudent.table.refresh();
        PtStudentInfoDlg.close();
    }, function (data) {
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.ptStudentInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
PtStudentInfoDlg.editSubmit = function () {

    this.clearData();
    this.collectData();

    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptStudent/update", function (data) {
        Feng.success("修改成功!");
        window.parent.PtStudent.table.refresh();
        PtStudentInfoDlg.close();
    }, function (data) {
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.ptStudentInfoData);
    ajax.start();
}

$(function () {
    Feng.initValidator("studentInfoForm", PtStudentInfoDlg.validateFields);
    var sex = $("#sexValue").val();
    $("#sex").val(sex);
    // document.getElementById('sex1').innerHTML = sex;
    // $("#sex1").text(sex);
    // 初始化头像上传
    var avatarUp = new $WebUpload("avatar");
    avatarUp.setUploadBarId("progressBar");
    avatarUp.setUploadUrl($("#uploadUrl").val());
    avatarUp.init();
});
