/**
 * 管理初始化
 */
var PtOrg = {
    id: "PtOrgTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    orgId:null,
    areaId:null
};

/**
 * 初始化表格的列
 */
PtOrg.initColumn = function () {
    return [
        {field: 'selectItem', radio: true ,visible: false},
        {title: '编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '学校名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '所属区域', field: 'areaName', visible: true, align: 'center', valign: 'middle'},
        {title: '管理员账号', field: 'account', visible: true, align: 'center', valign: 'middle'},
        {title: '管理员名称', field: 'leaderName', visible: true, align: 'center', valign: 'middle'},
        {title: '创建时间', field: 'crtDttm', visible: true, halign: "center", align: "left", width: '138px', valign: 'middle'}
        , {field: 'operate', title: '操作', halign: "center", align: "left", formatter: btnGroup,    // 自定义方法，添加按钮组
            events: {               // 注册按钮组事件
                'click #edit': function (event, value, row, index) {
                    PtStudent.openEditPtStudent(row.id)
                },
                'click #showUser': function (event, value, row, index) {
                    PtStudent.openShowInfo(row.id)
                },
                'click #delUser': function (event, value, row, index) {
                    PtStudent.delete(row.id);
                },
                'click #resetPwd': function (event, value, row, index) {
                    PtStudent.resetPwd(row.id)
                }
            }
        }
    ];
};
function btnGroup() {   // 自定义方法，添加操作按钮
    // data-target="xxx" 为点击按钮弹出指定名字的模态框
    var html =
        '<a href="####" class="btn btn-primary" id="edit"  data-target="#edit" title="修改">' +
        '<span class="glyphicon glyphicon-edit"></span></a>' +
        '<a href="####" class="btn btn-info" id="showUser"  data-target="#showUser" ' +
        'style="margin-left:2px" title="查看">' +
        '<span class="glyphicon glyphicon-info-sign"></span></a>' +
        '<a href="####" class="btn btn-danger" id="delUser"  data-target="#deleteuser" ' +
        'style="margin-left:2px" title="删除">' +
        '<span class="glyphicon glyphicon-trash"></span></a>' +
        '<a href="####" class="btn btn-warning" id="resetPwd"  data-target="#resetPwd" ' +
        'style="margin-left:2px" title="重置密码">' +
        '<span class="glyphicon glyphicon-off"></span></a>';
    return html
};

/**
 * 检查是否选中
 */
PtOrg.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        PtOrg.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
PtOrg.openAddPtOrg = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptOrg/ptOrg_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看详情
 */
PtOrg.openPtOrgDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptOrg/ptOrg_update/' + PtOrg.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除
 */
PtOrg.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/ptOrg/delete", function (data) {
            Feng.success("删除成功!");
            PtOrg.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("ptOrgId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询列表
 */
PtOrg.search = function () {
    var queryData = {};
    queryData['areaId'] = PtOrg.areaId;
    PtOrg.table.refresh({query: queryData});
};
PtOrg.onClickOrg = function (e, treeId, treeNode) {
        PtOrg.areaId = treeNode.id;
        PtOrg.search();
};
$(function () {
    var defaultColunms = PtOrg.initColumn();
    var table = new BSTable(PtOrg.id, "/ptOrg/findList", defaultColunms);
    table.setPaginationType("client");
    PtOrg.table = table.init();
    var orgId=$("#orgId").val();
    PtOrg.orgId=orgId;
    if (orgId==="0"){
        PtOrg.orgId=orgId;
        var ztree = new $ZTree("areaTree", "/back/sys/area/list");
        ztree.bindOnClick(PtOrg.onClickOrg);
        ztree.init();
    }
});
