/**
 * 初始化详情对话框
 */
var PtOrgInfoDlg = {
    ptOrgInfoData: {},
    validateFields: {
        phone: {
            validators: {
                notEmpty: {
                    message: '联系方式不能为空'
                },
                regexp: {
                    regexp: /^1\d{10}$/,
                    message: '手机号格式错误'
                },
                stringLength: {/*长度提示*/
                    max: 11,
                    message: '长度必须小于11'
                },/*最后一个没有逗号*/
            }
        }
    }
    };

/**
 * 清除数据
 */
PtOrgInfoDlg.clearData = function () {
    this.ptOrgInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtOrgInfoDlg.set = function (key, val) {
    this.ptOrgInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtOrgInfoDlg.get = function (key) {
    return $("#" + key).val();
}
/**
 * 验证数据是否为空
 */
PtOrgInfoDlg.validate = function () {
    $('#orgInfoForm').data("bootstrapValidator").resetForm();
    $('#orgInfoForm').bootstrapValidator('validate');
    return $("#orgInfoForm").data('bootstrapValidator').isValid();
};
/**
 * 关闭此对话框
 */
PtOrgInfoDlg.close = function () {
    parent.layer.close(window.parent.PtOrg.layerIndex);
}

/**
 * 收集数据
 */
PtOrgInfoDlg.collectData = function () {
    this
        .set('id')
        .set('name')
        .set('shortName')
        .set('address')
        .set('leaderName')
        .set('phone')
        .set('schoolings')
        .set('image', $("#avatar").val())
        .set('schHost')
        .set('schModel', $("#xxmb_id input:radio[name='schModel']:checked").val())
        .set('sort');
}

/**
 * 提交添加
 */
PtOrgInfoDlg.addSubmit = function () {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptOrg/add", function (data) {
        Feng.success("添加成功!");
        window.parent.PtOrg.table.refresh();
        PtOrgInfoDlg.close();
    }, function (data) {
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.ptOrgInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
PtOrgInfoDlg.editSubmit = function () {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptOrg/update", function (data) {
        Feng.success("修改成功!");
        window.parent.PtOrg.table.refresh();
        PtOrgInfoDlg.close();
    }, function (data) {
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.ptOrgInfoData);
    ajax.start();
};
PtOrgInfoDlg.update = function () {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptOrg/update", function (data) {
        if (data.code === 200) {
            Feng.success(data.msg);
            window.location.reload();
        } else {
            Feng.error(data.msg);
        }
    }, function (data) {
        Feng.error(data.msg);
    });
    ajax.set(this.ptOrgInfoData);
    ajax.start();
};
$(function () {
    Feng.initValidator("orgInfoForm", PtOrgInfoDlg.validateFields);
    var avatarUp = new $WebUpload("avatar");
    avatarUp.setUploadBarId("progressBar");
    avatarUp.setUploadUrl($("#uploadUrl").val());
    avatarUp.init();
});
