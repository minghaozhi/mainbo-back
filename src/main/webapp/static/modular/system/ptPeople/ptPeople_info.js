/**
 * 初始化详情对话框
 */
var PtPeopleInfoDlg = {
    ptPeopleInfoData : {},
    validateFields: {
        studentId: {
            validators: {
                notEmpty: {
                    message: '子女姓名不能为空'
                }
            }
        },
        name: {
            validators: {
                notEmpty: {
                    message: '家长姓名不能为空'
                },
                stringLength: {/*长度提示*/
                    max: 10,
                    message: '长度必须小于10'
                }
            }
        },
        cellphone: {
            validators: {
                notEmpty: {
                    message: '手机号不能为空'
                },
                regexp: {
                    regexp: /^1\d{10}$/,
                    message: '手机号格式错误'
                }
            }
        }
    }
};

/**
 * 清除数据
 */
PtPeopleInfoDlg.clearData = function() {
    this.ptPeopleInfoData = {};
}
/**
 * 验证数据是否为空
 */
PtPeopleInfoDlg.validate = function () {
    $('#peopleInfoForm').data("bootstrapValidator").resetForm();
    $('#peopleInfoForm').bootstrapValidator('validate');
    return $("#peopleInfoForm").data('bootstrapValidator').isValid();
};
/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtPeopleInfoDlg.set = function(key, val) {
    this.ptPeopleInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtPeopleInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
PtPeopleInfoDlg.close = function() {
    parent.layer.close(window.parent.PtPeople.layerIndex);
}

/**
 * 收集数据
 */
PtPeopleInfoDlg.collectData = function() {
    this
    .set('id')
    .set('orgId')
    .set('orgName')
    .set('userType')
    .set('name')
    .set('photo',$("#avatar").val())
    .set('countryId')
    .set('englishName')
    .set('namePell')
    .set('sex')
    .set('birthday')
    .set('nativePlace')
    .set('nationCode')
    .set('papersType')
    .set('papersNumber')
    .set('bloodType')
    .set('familyAddress')
    .set('presentAddress')
    .set('domicilePlace')
    .set('telephone')
    .set('cellphone')
    .set('postCode')
    .set('email').set("studentId")
        .set("classId");
}

/**
 * 提交添加
 */
PtPeopleInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptPeople/saveParents", function(data){
        Feng.success("添加成功!");
        window.parent.PtPeople.table.refresh();
        PtPeopleInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.ptPeopleInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
PtPeopleInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptPeople/saveParents", function(data){
        Feng.success("修改成功!");
        window.parent.PtPeople.table.refresh();
        PtPeopleInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.ptPeopleInfoData);
    ajax.start();
}
function phaseRelationParentA(){
    var phaseId = $("#phaseId").val();
    if(phaseId){
        $.ajax({
            type: "get",
            async: false,
            url: Feng.ctxPath+"/ptPeople/getGradeByPhaseId",
            data: {'orgId': $("#orgId").val(),'phaseId':phaseId},
            dataType: "json",
            cache: false,
            success: function (data) {
                var gradeOptions = "<option value=''>请选择...</option>";
                if(data){
                    $.each(data,function(n,grade){
                        gradeOptions += "<option value='"+grade.id+"' ";
                        gradeOptions += ">"+grade.name+"</option>";
                    });
                }
                $("#gradeId").html(gradeOptions);
            },
        });
    }else{
        $("#gradeId").html("<option value=''>请选择...</option>");
        $("#classId").html("<option value=''>请选择...</option>");
        $("#stu_name_add").val("");
    }
}

function gradeRelationParentA(obj){
    var phaseId = $("#phaseId").val();
    var gradeId = $("#gradeId").val();
    if(phaseId && gradeId){
        $.ajax({
            type: "get",
            async: false,
            url: Feng.ctxPath+"/ptPeople/getClassByGradeId",
            data: {'orgId': $("#orgId").val(),'phaseId':phaseId,'gradeId':gradeId},
            dataType: "json",
            cache: false,
            success: function (data) {
                var classOptions = "<option value=''>请选择...</option>";
                if(data){
                    $.each(data,function(n,classInfo){
                        classOptions += "<option value='"+classInfo.id+"' ";
                        classOptions += ">"+classInfo.name+"</option>";
                    });
                }
                $("#classId").html(classOptions);
            },
        });
    }else{
        $("#classId").html("<option value=''>请选择...</option>");
        $("#stu_name_add").val("");
    }
}
function classRelationParent(){
    $("#classId").val($("#classId").val());
    $("#stu_name_add").val("");
}
$(function() {
    Feng.initValidator("peopleInfoForm", PtPeopleInfoDlg.validateFields);
    // 初始化头像上传
    var avatarUp = new $WebUpload("avatar");
    avatarUp.setUploadBarId("progressBar");
    avatarUp.setUploadUrl($("#uploadUrl").val());
    avatarUp.init();
});
