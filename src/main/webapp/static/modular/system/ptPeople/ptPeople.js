/**
 * 管理初始化
 */
var PtPeople = {
    id: "PtPeopleTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
PtPeople.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
            {title: '编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '账号', field: 'account', visible: true, align: 'center', valign: 'middle'},
            {title: '姓名', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '学校', field: 'orgName', visible: true, align: 'center', valign: 'middle'},
        {title: '绑定手机', field: 'cellphone', visible: true, align: 'center', valign: 'middle'},
        {title: '状态', field: 'enable', visible: true, width: '50px', align: 'center', valign: 'middle',
            formatter: function (value, row, index) {
                var value = "";
                if (!row.enable) {
                    value = "冻结";
                } else {
                    value = "启用";
                }

                return value;
            }},
            {title: '创建时间', field: 'crtDttm', visible: true, align: 'center', valign: 'middle'}
        , {
            field: 'operate',
            title: '操作',
            halign: "center",
            align: "left",
            formatter: btnGroupPeople,    // 自定义方法，添加按钮组
            events: {               // 注册按钮组事件
                'click #edit': function (event, value, row, index) {
                    PtPeople.openEditPtPeople(row.id)
                },
                'click #showUser': function (event, value, row, index) {
                    PtPeople.openShowInfo(row.id)
                },
                'click #delUser': function (event, value, row, index) {
                    PtPeople.delete(row.id);
                },
                'click #resetPwd': function (event, value, row, index) {
                    PtPeople.resetPwd(row.id)
                }
            }
        }
    ];
};
function paramsMatter(value, row, index, field) {
    var span = document.createElement('span');
    span.setAttribute('title', value);
    span.innerHTML = value;
    return span.outerHTML;
}

//td宽度以及内容超过宽度隐藏
function formatTableUnit(value, row, index) {
    return {
        css: {
            "white-space": 'nowrap',
            "text-overflow": 'ellipsis',
            "overflow": 'hidden',
            "max-width": '150px'
        }
    }
}

function btnGroupPeople() {   // 自定义方法，添加操作按钮
    // data-target="xxx" 为点击按钮弹出指定名字的模态框
    var html =
        '<a href="####" class="btn btn-primary" id="edit"  data-target="#edit" title="修改">' +
        '<span class="glyphicon glyphicon-edit"></span></a>' +
        '<a href="####" class="btn btn-info" id="showUser"  data-target="#showUser" ' +
        'style="margin-left:2px" title="查看">' +
        '<span class="glyphicon glyphicon-info-sign"></span></a>' +
        '<a href="####" class="btn btn-danger" id="delUser"  data-target="#deleteuser" ' +
        'style="margin-left:2px" title="删除">' +
        '<span class="glyphicon glyphicon-trash"></span></a>' +
        '<a href="####" class="btn btn-warning" id="resetPwd"  data-target="#resetPwd" ' +
        'style="margin-left:2px" title="重置密码">' +
        '<span class="glyphicon glyphicon-off"></span></a>';
    return html
};

/**
 * 点击添加
 */
PtPeople.openAddPtPeople = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['830px', '450px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptPeople/ptPeople_add?orgId='+PtPeople.orgId
    });
    this.layerIndex = index;
};

/**
 * 打开查看详情
 */
PtPeople.openEditPtPeople = function (id) {

        var index = layer.open({
            type: 2,
            title: '修改',
            area: ['830px', '450px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptPeople/ptPeople_update/' +id
        });
        this.layerIndex = index;
};
PtPeople.openShowInfo = function (id) {
    if (PtPeople.orgId === "0") {
        layer.alert("请选择学校!");
        return;
    }
    if (id === "") {
        layer.alert("请选中一条!");
        return;
    }
    var index = layer.open({
        type: 2,
        title: '查看',
        area: ['830px', '450px'], //宽高
        fix: false, //不固定
        maxmin: true,
        shade: 0,
        content: Feng.ctxPath + '/ptPeople/ptPeople_showInfo/' + id
    });
    this.layerIndex = index;
};
var ids = new Array(0);
PtPeople.getSelections=function () {
    var selected = $('#' + PtPeople.id).bootstrapTable('getSelections');

    for (var i = 0; i < selected.length; i++) {
        ids[i] = selected[i].id;
    }
    return ids;
};
 PtPeople.removeSelections=function () {
    $('#' + PtPeople.id).bootstrapTable('uncheckAll');
    ids=new Array(0);
};
/**
 * 子女管理
 */
PtPeople.childs=function(){
    const ids = this.getSelections();
    if (PtPeople.orgId === "0") {
        layer.alert("请选择学校!");
        return;
    }
    if (ids.length===0) {
        layer.alert("请选择一条数据!");

        return;
    }
    if (ids.length>1) {
        layer.alert("只能选中一条哟!");
        this.removeSelections();
        return;
    }
    var index = layer.open({
        type: 2,
        title: '子女管理',
        area: ['800px', '600px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptPeopleStudent?orgId=' + PtPeople.orgId+"&peopleId="+ids
    });
    this.layerIndex = index;
};
/**
 * 删除
 */
PtPeople.delete = function (id) {

        var ajax = new $ax(Feng.ctxPath + "/ptPeople/delete/"+id, function (data) {
            Feng.success("删除成功!");
            PtPeople.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.start();
};
/**
 * 删除
 */
PtPeople.batchDelete = function () {
    var ids = this.getSelections();
    if (ids.length === 0) {
        layer.msg("请至少选择一条数据", {icon: 5});
        return;
    }
    var ajax = new $ax(Feng.ctxPath + "/ptPeople/batchdelete", function (data) {
        if (data.code === 200) {
            Feng.success(data.msg);
        }
        PtPeople.table.refresh();
    }, function (data) {
        Feng.error(data.msg);
    });
    ajax.set("accountIds", ids.join(","));
    ajax.start();
};
/**
* 重置密码
* @param id
*/
PtPeople.resetPwd = function (id) {
    var ajax = new $ax(Feng.ctxPath + "/ptAccount/resetPwd?accountId=" + id, function (data) {
        if (data.code === 200) {
            Feng.success(data.msg);
        }
        PtPeople.table.refresh();
    }, function (data) {
        Feng.error(data.msg);
    });
    ajax.start();
};
/**
 * 批量冻结或启用
 */
PtPeople.updateEnable = function (status) {
    const ids = this.getSelections();
    if (ids.length === 0) {
        layer.msg("请至少选择一条数据", {icon: 5});
        return;
    }
    var ajaxUrl;
    if (status == 0) {
        //批量冻结
        ajaxUrl = "/ptAccount/batchDisableAccount";
    } else {
        ajaxUrl = "/ptAccount/batchUpdateEnable";
    }

    var ajax = new $ax(Feng.ctxPath + ajaxUrl, function (data) {
        if (data.code === 200) {
            Feng.success(data.msg);
        }
        PtPeople.table.refresh();
    }, function (data) {
        Feng.error(data.msg);
    });
    ajax.set("accountIds", ids.join(","));
    ajax.start();
};

/**
 * 批量导入
 */
PtPeople.import = function () {
    if (PtPeople.orgId === "0") {
        layer.alert("请选择学校!");
        return;
    }
    var index = layer.open({
        type: 2,
        title: '批量导入家长',
        area: ['800px', '460px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptAccount/batch/add?orgId=' + PtPeople.orgId + '&userType=5'
    });
    this.layerIndex = index;
};
/**
 * 查询列表
 */
PtPeople.search = function () {
    var queryData = {};
    queryData['orgId'] = PtPeople.orgId;
    queryData['areaId'] = PtPeople.areaId;
    queryData['accountName'] = $("#accountName").val();
    queryData['name'] = $("#name").val();
    queryData['id'] = $("#id").val();
    queryData['studentName'] = $("#studentName").val();
    queryData['phaseId'] = $("#phaseId").val();
    PtPeople.table.refresh({query: queryData});
};
PtPeople.onClickOrg = function (e, treeId, treeNode) {
    PtPeople.orgId = '0';
    PtPeople.areaId = 0;
    if (treeNode.flag == "area") {
        pid = treeNode.id;
        var data = {"areaId": pid};

        $.ajax({
            url: "/ptOrg/list",
            type: "post",
            dataType: "json",
            data: data,
            cache: false,

            success: function (data) {
                $.each(data, function (index, obj) {
                    var treeObj = $.fn.zTree.getZTreeObj("areaTree");
                    var nodes = treeObj.getSelectedNodes();
                    if (nodes && nodes.length > 0) {
                        treeObj.removeChildNodes(nodes[0]);
                    }

                    treeObj.addNodes(treeNode, {
                        id: obj.id,
                        pId: treeNode.id,
                        name: obj.shortName,
                        title: obj.name,
                        flag: "school",
                        open: true
                    });
                });
            }
        });
        PtPeople.areaId = treeNode.id;
        PtPeople.search();
    } else {
        PtPeople.orgId = treeNode.id;
        PtPeople.search();
    }
};

PtPeople.export = function () {

    window.location.href = Feng.ctxPath + "/ptPeople/exportPeople?userType=5&orgId=" + PtPeople.orgId + "&areaId=" + PtPeople.areaId;
};
PtPeople.formParams = function () {
    var queryData = {};
    queryData['orgId'] = PtPeople.orgId;
    queryData['areaId'] = PtPeople.areaId;
    queryData['accountName'] = $("#accountName").val();
    queryData['name'] = $("#name").val();
    queryData['id'] = $("#id").val();
    queryData['studentName'] = $("#studentName").val();
    queryData['phaseId'] = $("#phaseId").val();
    return queryData;
};

$(function () {
    var orgId=$("#orgId").val();
    PtPeople.areaId=$("#areaId").val();
    PtPeople.orgId=orgId;
    var defaultColunms = PtPeople.initColumn();
    var table = new BSTable(PtPeople.id, "/ptPeople/list", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams(PtPeople.formParams());

    if (orgId==="0"){
        PtPeople.orgId=orgId;
        table.setQueryParams("");
        var ztree = new $ZTree("areaTree", "/back/sys/area/list");
        ztree.bindOnClick(PtPeople.onClickOrg);
        ztree.init();
    }
    PtPeople.table = table.init();
});
