/**
 * 管理初始化
 */
var PtClass = {
    id: "PtClassTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    orgId:null,
    schoolYear:null
};

/**
 * 初始化表格的列
 */
PtClass.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '编号', field: 'id', visible: false, align: 'center', halign: "center", valign: 'middle'},
            {title: '班级名称', field: 'name', visible: true, align: 'left',halign: "center", valign: 'middle'},
            {title: '级届', field: 'periodName', visible: true, align: 'left',halign: "center", valign: 'middle'},
            {title: '学段', field: 'phase', visible: true, align: 'left', halign: "center", valign: 'middle',width:'70px'},
            {title: '年级', field: 'grade', visible: true, align: 'left', halign: "center",valign: 'middle',width:'100px'},
            {title: '所属学校', field: 'orgName', visible: true, align: 'left',halign: "center", valign: 'middle',width:'120px'},
            {title: '所属教室', field: 'classroomName', visible: true, align: 'left',halign: "center", valign: 'middle',width:'100px'},
            {title: '学生数', field: 'studentCount', visible: true, align: 'center',halign: "center", valign: 'middle',width:'70px'},
            {title: '班主任', field: 'teacherName', visible: true, align: 'left',halign: "center", valign: 'middle',width:'100px'},
            {title: '类型', field: 'classTypeName', visible: true, align: 'left',halign: "center", valign: 'middle',width:'100px'},
            {title: '升级状态', field: 'statusName', visible: true, align: 'left',halign: "center", valign: 'middle',width:'120px'},
            {title: '毕业状态', field: 'graduationStatusName', visible: true, align: 'left',halign: "center", valign: 'middle',width:'70px'},
        {
            field: 'operate',
            title: '操作',
            halign: "center",
            align: "left",
            formatter: btnGroup,    // 自定义方法，添加按钮组
            events: {               // 注册按钮组事件
                'click #edit': function (event, value, row, index) {
                    PtClass.openEditPtClass(row.id)
                },
                'click #showUser': function (event, value, row, index) {
                    PtClass.openShowInfo(row.id)
                },
                'click #delUser': function (event, value, row, index) {
                    PtClass.delete(row.id);
                },
                'click #setClassRoom': function (event, value, row, index) {
                    PtClass.setClassRoom(row.id);
                },
                'click #unSetClassRoom': function (event, value, row, index) {
                    PtClass.unSetClassRoom(row.id);
                }
            }
        }
    ];
};
function btnGroup() {   // 自定义方法，添加操作按钮
    // data-target="xxx" 为点击按钮弹出指定名字的模态框
    var html =
        '<a href="####" class="btn btn-primary" id="edit"  data-target="#edit" title="修改">' +
        '<span class="glyphicon glyphicon-edit"></span></a>' +
        '<a href="####" class="btn btn-info" id="showUser"  data-target="#showUser" ' +
        'style="margin-left:2px" title="查看">' +
        '<span class="glyphicon glyphicon-info-sign"></span></a>' +
    '<a href="####" class="btn btn-danger" id="delUser"  data-target="#deleteuser" ' +
    'style="margin-left:2px" title="删除">' +
    '<span class="glyphicon glyphicon-trash"></span></a>'+
    '<a href="####" class="btn btn-warning" id="setClassRoom"  data-target="#setClassRoom" title="设置教室">' +
    '<span class="glyphicon  glyphicon-circle-arrow-down"></span></a>'+
    '<a href="####" class="btn btn-default" id="unSetClassRoom"  data-target="#unSetClassRoom" title="解除教室">' +
    '<span class="glyphicon glyphicon-circle-arrow-up"></span></a>';
    return html
};
/**
 * 检查是否选中
 */
PtClass.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        PtClass.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
PtClass.openAddPtClass = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['900px', '600px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptClass/ptClass_add?orgId='+PtClass.orgId
    });
    this.layerIndex = index;
};

/**
 * 打开查看详情
 */
PtClass.openEditPtClass = function (id) {

        var index = layer.open({
            type: 2,
            title: '修改',
            area: ['800px', '600px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptClass/ptClass_update/' +id
        });
        this.layerIndex = index;
};
PtClass.openShowInfo = function (id) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['700px', '500px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptClass/ptClass_showInfo/' + id
        });
        this.layerIndex = index;
};

/**
 * 删除
 */
PtClass.delete = function (id) {
        var ajax = new $ax(Feng.ctxPath + "/ptClass/deleteClass/"+id, function (data) {
           if (data.code===200){
               Feng.success(data.msg);
               PtClass.table.refresh();
           } else {
               Feng.error(data.msg);
           }

        }, function (data) {
            Feng.error(data.msg);
        });
        ajax.start();
};
PtClass.editTeacher=function(){
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '任课教师设置',
            area: ['800px', '600px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptClass/editClassTeacher/' + PtClass.seItem.id
        });
        this.layerIndex = index;
    }
};
/**
 * 管理学生
 */
PtClass.editStudent=function(){
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '管理学生',
            area: ['770px', '400px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptClass/editClassStudent/' + PtClass.seItem.id
        });
        this.layerIndex = index;
    }
};
/**
 * 导入班级
 */
PtClass.batchImportClass=function(){

        var index = layer.open({
            type: 2,
            title: '批量导入班级',
            area: ['770px', '400px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptClass/toBatchClass?orgId=' + PtClass.orgId+"&schoolYear="+ PtClass.schoolYear
        });
        this.layerIndex = index;
};
/**
 * 导入教师
 */
PtClass.batchImportTeacher=function(){
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '批量导入教师',
            area: ['770px', '400px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptClass/toBatchTeacher?orgId=' + PtClass.orgId + "&schoolYear=" + PtClass.schoolYear
        });
        this.layerIndex = index;
    }
};
/**
 *学生
 */
PtClass.batchImportStudent=function(){
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '批量导入学生',
            area: ['770px', '400px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptClass/toBatchStudent?orgId=' + PtClass.orgId + "&schoolYear=" + PtClass.schoolYear
        });
        this.layerIndex = index;
    }
};
PtClass.classUpdate=function(){
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '班级升班',
            area: ['850px', '600px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptClass/classupdate/selectclassindex?orgId=' + PtClass.orgId + "&schoolYear=" + PtClass.schoolYear
        });
        this.layerIndex = index;
    }
};
PtClass.classEnd=function(){
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '班级毕业',
            area: ['850px', '600px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptClass/classupdate/toSelectGraduationStatus?classId=' + this.seItem.id + "&schoolYear=" + PtClass.schoolYear
        });
        this.layerIndex = index;
    }
};
PtClass.setClassRoom=function(id){
    var index = layer.open({
        type: 2,
        title: '设置教室',
        area: ['770px', '550px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptSchoolClassroom/listClassRoomUnuse?orgId=' + PtClass.orgId + "&classId=" +id
    });
    this.layerIndex = index;
};
PtClass.unSetClassRoom=function(id){
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptSchoolClassroom/ClassRemoveClassRoom", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            PtClass.table.refresh();
        }else {
            Feng.error(data.msg);
        }

    },function(data){
        Feng.error(data.msg);
    });
    ajax.set("id",id);
    ajax.start();
};
/**
 * 查询列表
 */
PtClass.search = function () {
    var queryData = {};
    queryData['schoolYear'] = $("#schoolYear").val();
    queryData['orgId'] = $("#orgId").val();
    queryData['gradeId'] = $("#gradeId").val();
    queryData['phaseId'] = $("#phaseId").val();
    queryData['name'] = $("#name").val();
    queryData['stuName'] = $("#stuName").val();
    PtClass.schoolYear=$("#schoolYear").val();
    PtClass.table.refresh({query: queryData});

};
PtClass.formParams = function() {
    var queryData = {};
    queryData['orgId'] = $("#orgId").val();
    queryData['schoolYear'] = $("#schoolYear").val();
    queryData['gradeId'] = $("#gradeId").val();
    queryData['phaseId'] = $("#phaseId").val();
    queryData['name'] = $("#name").val();
    queryData['stuName'] = $("#stuName").val();
    return queryData;
};
$(function () {
    PtClass.orgId=$("#orgId").val();
    PtClass.schoolYear=$("#schoolYear").val();
    var defaultColunms = PtClass.initColumn();
    var table = new BSTable(PtClass.id, "/ptClass/list", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams(PtClass.formParams());
    PtClass.table = table.init();
});
