/**
 * 初始化详情对话框
 */
var PtClassInfoDlg = {
    ptClassInfoData : {},
    validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '班级名称不能为空'
                }
            }
        },
        period: {
            validators: {
                notEmpty: {
                    message: '级届不能为空'
                }
            }
        },
        phaseId: {
            validators: {
                notEmpty: {
                    message: '学段不能为空'
                }
            }
        },
        gradeId: {
            validators: {
                notEmpty: {
                    message: '年级不能为空'
                }
            }
        }, sort: {
            validators: {
                notEmpty: {
                    message: '排序不能为空'
                }
            }
        }
    }
};
/**
 * 验证数据是否为空
 */
PtClassInfoDlg.validate = function () {
    $('#classInfoForm').data("bootstrapValidator").resetForm();
    $('#classInfoForm').bootstrapValidator('validate');
    return $("#classInfoForm").data('bootstrapValidator').isValid();
};
/**
 * 清除数据
 */
PtClassInfoDlg.clearData = function() {
    this.ptClassInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtClassInfoDlg.set = function(key, val) {
    this.ptClassInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtClassInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
PtClassInfoDlg.close = function() {
    parent.layer.close(window.parent.PtClass.layerIndex);
}

/**
 * 收集数据
 */
PtClassInfoDlg.collectData = function() {
    this
    .set('id')
    .set('classType')
    .set('name')
    .set('orgId')
    .set('orgName')
    .set('period')
    .set('phaseId')
    .set('gradeId')
    .set('teacherId')
    .set('teacherName')
    .set('sort')
    .set('schoolYear')
    .set('status')
    .set('graduationStatus');
};

/**
 * 提交添加
 */
PtClassInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptClass/add", function(data){
        if (data.code===200){
        Feng.success(data.msg);
        window.parent.PtClass.table.refresh();
        PtClassInfoDlg.close();
        }else {
            Feng.error(data.msg)
        }
    },function(data){
        Feng.error(data.msg)
    });
    ajax.set(this.ptClassInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
PtClassInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptClass/update", function(data){
        Feng.success("修改成功!");
        window.parent.PtClass.table.refresh();
        PtClassInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.ptClassInfoData);
    ajax.start();
};
/**
 * 提交修改
 */
PtClassInfoDlg.editClassTeacher = function() {
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptClass/saveClassTeacher", function(data){
         if (data.code===200){
             Feng.success(data.msg);
        window.parent.PtClass.table.refresh();
        PtClassInfoDlg.close();
         }else {
             Feng.error(data.msg);
         }

    },function(data){
        Feng.error(data.msg);
    });
    ajax.setData($(".form-horizontal").serialize());
    ajax.start();
};
PtClassInfoDlg.import=function(){
    if($("#file_id").val()===""){
        Feng.info("请选择文件！");
    }else{
        var orgId=$("#orgId").val();
        var schoolYear=$("#schoolYear").val();
        var filename =$("#file_id").val();
        var file =$("#file_id")[0].files[0];
        parent.layer.confirm("确定要导入文件吗", {
            btn: ['确定', '取消'],
            shade: false //不显示遮罩
        }, function (index) {
            parent.layer.close(index);
            var formData = new FormData();
            formData.append("orgId",orgId);
            formData.append("schoolYear",schoolYear);
            formData.append("file",file);
            $.ajax({
                type: "POST",
                url:Feng.ctxPath+"/ptClass/batchImportClass",
                data:formData,
                contentType: false,
                processData: false,
                success: function(data) {
                    if(200 == data.code){
                        parent.layer.msg(data.msg,{icon: 6,time:3000},)
                    }else {
                        parent.layer.msg(data.msg,{icon: 5,time:3000})
                    }
                    var index = parent.layer.getFrameIndex(window.name);//获取当前弹窗的Id
                    parent.layer.close(index);//关闭
                        window.parent.PtClass.table.refresh();

                }
            });
        });
        return false;
    }
};
/**
 *导入教师
 * @returns {boolean}
 */
PtClassInfoDlg.batchImportTeacher=function(){
    if($("#file_id").val()===""){
        Feng.info("请选择文件！");
    }else{
        var orgId=$("#orgId").val();
        var schoolYear=$("#schoolYear").val();
        var file =$("#file_id")[0].files[0];
        parent.layer.confirm("确定要导入文件吗", {
            btn: ['确定', '取消'],
            shade: false //不显示遮罩
        }, function (index) {
            parent.layer.close(index);
            var formData = new FormData();
            formData.append("orgId",orgId);
            formData.append("schoolYear",schoolYear);
            formData.append("file",file);
            $.ajax({
                type: "POST",
                url:Feng.ctxPath+"/ptClass/batchImportTeacher",
                data:formData,
                contentType: false,
                processData: false,
                success: function(data) {
                    if(200 == data.code){
                        parent.layer.msg(data.msg,{icon: 6,time:3000},)
                    }else {
                        parent.layer.msg(data.msg,{icon: 5,time:3000})
                    }
                    var index = parent.layer.getFrameIndex(window.name);//获取当前弹窗的Id
                    parent.layer.close(index);//关闭
                    window.parent.PtClass.table.refresh();

                }
            });
        });
        return false;
    }
};
PtClassInfoDlg.batchImportStudent=function(){
    if($("#file_id").val()===""){
        Feng.info("请选择文件！");
    }else{
        var orgId=$("#orgId").val();
        var schoolYear=$("#schoolYear").val();
        var file =$("#file_id")[0].files[0];
        parent.layer.confirm("确定要导入文件吗", {
            btn: ['确定', '取消'],
            shade: false //不显示遮罩
        }, function (index) {
            parent.layer.close(index);
            var formData = new FormData();
            formData.append("orgId",orgId);
            formData.append("schoolYear",schoolYear);
            formData.append("file",file);
            $.ajax({
                type: "POST",
                url:Feng.ctxPath+"/ptClass/batchImportStudent",
                data:formData,
                contentType: false,
                processData: false,
                success: function(data) {
                    if(200 == data.code){
                        parent.layer.msg(data.msg,{icon: 6,time:3000},)
                    }else {
                        parent.layer.msg(data.msg,{icon: 5,time:3000})
                    }
                    var index = parent.layer.getFrameIndex(window.name);//获取当前弹窗的Id
                    parent.layer.close(index);//关闭
                    window.parent.PtClass.table.refresh();

                }
            });
        });
        return false;
    }
};


PtClassInfoDlg.addToClass=function(){
    let  classId=$("#classId").val();
    let classroomId=$("#classroomId").val();
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptSchoolClassroom/addClassRoomToClass", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            window.parent.PtClass.table.refresh();
            PtClassInfoDlg.close();
        }else {
            Feng.error(data.msg);
        }

    },function(data){
        Feng.error(data.msg);
    });
    ajax.set("classId",classId);
    ajax.set("classroomId",classroomId);
    ajax.start();
};
$(function() {
    Feng.initValidator("classInfoForm", PtClassInfoDlg.validateFields);
});
