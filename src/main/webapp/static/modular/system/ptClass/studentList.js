/**
 * 管理初始化
 */
var StudentList = {
    id: "StudentListTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    orgId:null,
    schoolYear:null
};
/**
 * 初始化表格的列
 */
StudentList.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
        {title: '编号', field: 'id', visible: false, align: 'center', halign: "center", valign: 'middle'},
        {title: '账号', field: 'account', visible: true, align: 'left',halign: "center", valign: 'middle'},
        {title: '年级', field: 'grade', visible: true, align: 'left', halign: "center",width:'100px',valign: 'middle'},
        {title: '班级', field: 'className', visible: true, align: 'left',halign: "center",width:'100px', valign: 'middle'},
        {title: '姓名', field: 'userName', visible: true, align: 'left',halign: "center",width:'100px', valign: 'middle'},
        {title: '毕业状态', field: 'graduationStatus', visible: true, align: 'left',halign: "center",width:'100px', valign: 'middle',formatter: function (value, row, index) {
                var value = "";
                if (row.graduationStatus===5) {
                    value = "未毕业";
                } else {
                    value = "已毕业";
                }

                return value;
            }},
        {title: '创建时间', field: 'crtDttm', visible: true, align: 'left',halign: "center", valign: 'middle'}
    ];
};
var ids = new Array(0);
StudentList.getSelections=function () {
    var selected = $('#' + StudentList.id).bootstrapTable('getSelections');

    for (var i = 0; i < selected.length; i++) {
        ids[i] = selected[i].id;
    }
    return ids;
};
StudentList.removeSelections=function () {
    $('#' + StudentList.id).bootstrapTable('uncheckAll');
    ids=new Array(0);
};
StudentList.finish=function(){
    const ids = this.getSelections();
    if (ids.length===0) {
        layer.alert("请选择一条数据!");
        return;
    }
    var ajax = new $ax(Feng.ctxPath + "/ptClass/classupdate/chanageClassUserGraduationStatus", function (data) {
        if (data.code===200) {
            Feng.success("毕业成功!");
            StudentList.table.refresh();
            PtClass.table.refresh();
        }
    }, function (data) {
        Feng.error(data.msg);
    });
    ajax.set("userIds",ids.join(","));
    ajax.set("classIds",$("#classIds").val());
    ajax.set("graduationStatus",$("#graduateState").val());
    ajax.start();
};
/**
 * 查询列表
 */
StudentList.search = function () {
    var queryData = {};
    queryData['classId'] = $("#classId").val();
    queryData['userName'] = $("#userName").val();
    queryData['orgId'] = $("#orgId").val();
    queryData['schoolYear'] =$("#schoolYear").val();
    queryData['gradeId'] =$("#gradeId").val();
    queryData['graduationStatus'] =$("#graduationStatus").val();
    queryData['classIds'] =$("#classIds").val();
    StudentList.table.refresh({query: queryData});

};
StudentList.formParams = function() {
    var queryData = {};
    queryData['orgId'] = $("#orgId").val();
    queryData['schoolYear'] =$("#schoolYear").val();
    queryData['gradeId'] =$("#gradeId").val();
    queryData['graduationStatus'] =$("#graduationStatus").val();
    queryData['classIds'] =$("#classIds").val();
    return queryData;
};


$(function () {
    var defaultColunms = StudentList.initColumn();
    var table = new BSTable(StudentList.id, "/ptClass/classupdate/findStudents", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams(StudentList.formParams());
    StudentList.table = table.init();
});