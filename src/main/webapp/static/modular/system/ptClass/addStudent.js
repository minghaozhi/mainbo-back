/**
 * 管理初始化
 */
var PtEditClassStudent = {
    id: "PtEditClassStudentTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    classId:null,
    orgId:null,
    schoolYear: null,
    ptClassStudentData : {}
};
/**
 * 清除数据
 */
PtEditClassStudent.clearData = function() {
    this.ptClassStudentData = {};
};
PtEditClassStudent.set = function(key, val) {
    this.ptClassStudentData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
};
PtEditClassStudent.close = function() {
    parent.layer.close(window.parent.PtEditClassStudent.layerIndex);
};
PtEditClassStudent.collectData = function() {
    this
        .set('orgId')
        .set('schoolYear')
        .set('classId')
        .set('userId');
};
/**
 * 初始化表格的列
 */
PtEditClassStudent.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {
            field: 'Number',
            title: '序号',
            align: 'center',
            halign: "center",
            width:'70px',
            formatter: function (value, row, index) {
                return index+1;
            }
        },
        {
            title: 'id',
            field: 'id',
            visible: false,
            align: 'center',
            halign: "center",
            valign: 'middle',
            width:'100px',
        },
        {
            title: '学生姓名',
            field: 'userName',
            visible: true,
            align: 'center',
            halign: "center",
            valign: 'middle',
            width:'100px',
        },
        {
            field: 'operate',
            title: '操作',
            halign: "center",
            align: "center",
            width:'100px',
            formatter: btnGroup,    // 自定义方法，添加按钮组
            events: {               // 注册按钮组事件

                'click #delUser': function (event, value, row, index) {
                    PtEditClassStudent.deleteStudent(row);
                }
            }
        }
    ];
};
function btnGroup() {   // 自定义方法，添加操作按钮
                        // data-target="xxx" 为点击按钮弹出指定名字的模态框
    var html = '<a href="####" class="btn btn-danger" id="delUser"  data-target="#deleteuser" ' +
        'style="margin-left:2px" title="删除">' +
        '<span class="glyphicon glyphicon glyphicon-remove"></span></a>';
    return html
};
PtEditClassStudent.addRow=function(){
    var index =layer.open({
        type: 2,
        title: '添加学生',
        area: ['500px', '300px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptClass/addClassStudent?orgId=' + PtEditClassStudent.orgId+"&schoolYear="+PtEditClassStudent.schoolYear+"&classId="+PtEditClassStudent.classId
    });
    this.layerIndex = index;
};

PtEditClassStudent.addSubmit=function(){
    if ($("#userId").val()===''){
        layer.alert("请选择数据");
        return;
    }
    this.clearData();
    this.collectData();
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptClass/saveClassStudent", function(data){
        if (data.code===200){
        Feng.success("添加成功!");
            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            parent.layer.close(index); //再执行关闭                        //刷新父页面
            parent.location.reload();
        }else {
            Feng.error(data.msg);
        }
    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.ptClassStudentData);
    ajax.start();
};
$(function () {
    PtEditClassStudent.classId=$("#classId").val();
    PtEditClassStudent.orgId=$("#orgId").val();
    PtEditClassStudent.schoolYear=$("#schoolYear").val();
    var defaultColunms = PtEditClassStudent.initColumn();
    var table = new BSTable(PtEditClassStudent.id, "/ptClass/findStudentList/"+ PtEditClassStudent.classId, defaultColunms);
    table.setPaginationType("client");
    PtEditClassStudent.table = table.init();
});