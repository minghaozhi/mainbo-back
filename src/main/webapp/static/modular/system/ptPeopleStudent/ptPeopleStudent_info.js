/**
 * 初始化详情对话框
 */
var PtPeopleStudentInfoDlg = {
    ptPeopleStudentInfoData : {}
};

/**
 * 清除数据
 */
PtPeopleStudentInfoDlg.clearData = function() {
    this.ptPeopleStudentInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtPeopleStudentInfoDlg.set = function(key, val) {
    this.ptPeopleStudentInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtPeopleStudentInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
PtPeopleStudentInfoDlg.close = function() {
    parent.layer.close(window.parent.PtPeopleStudent.layerIndex);
}

/**
 * 收集数据
 */
PtPeopleStudentInfoDlg.collectData = function() {
    this
    .set('id')
    .set('peopleId')
    .set('studentId')
    .set('orgId')
    .set('studentName');
};

/**
 * 提交添加
 */
PtPeopleStudentInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptPeopleStudent/saveOrUpdate", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            window.parent.PtPeopleStudent.table.refresh();
            PtPeopleStudentInfoDlg.close();
        } else {
            Feng.error(data.msg);
        }


    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.ptPeopleStudentInfoData);
    ajax.start();
};

/**
 * 提交修改
 */
PtPeopleStudentInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptPeopleStudent/saveOrUpdate", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            window.parent.PtPeopleStudent.table.refresh();
            PtPeopleStudentInfoDlg.close();
        } else {
            Feng.error(data.msg);
        }

    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.ptPeopleStudentInfoData);
    ajax.start();
};

$(function() {

});
