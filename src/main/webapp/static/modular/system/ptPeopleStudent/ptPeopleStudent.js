/**
 * 管理初始化
 */
var PtPeopleStudent = {
    id: "PtPeopleStudentTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    orgId:null,
    peopleId:null
};

/**
 * 初始化表格的列
 */
PtPeopleStudent.initColumn = function () {
    return [
        {field: 'selectItem', radio: false},
            {title: '编号', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '班级id', field: 'classId', visible: false, align: 'center', valign: 'middle'},
            {title: '级届', field: 'period', visible: true, align: 'center', valign: 'middle'},
            {title: '年级', field: 'grade', visible: true, align: 'center', valign: 'middle'},
            {title: '班级', field: 'className', visible: true, align: 'center', valign: 'middle'},
            {title: '姓名', field: 'studentName', visible: true, align: 'center', valign: 'middle'},
        {
            field: 'operate',
            title: '操作',
            halign: "center",
            align: "left",
            formatter: btnGroupPtPeopleStudent,    // 自定义方法，添加按钮组
            events: {               // 注册按钮组事件
                'click #edit': function (event, value, row, index) {
                    PtPeopleStudent.openEditPtPeopleStudent(row)
                },
                'click #delUser': function (event, value, row, index) {
                    PtPeopleStudent.delete(row.id);
                }
            }
        }
    ];
};
function btnGroupPtPeopleStudent() {   // 自定义方法，添加操作按钮
                              // data-target="xxx" 为点击按钮弹出指定名字的模态框
    var html =
        '<a href="####" class="btn btn-primary" id="edit"  data-target="#edit" title="修改">' +
        '<span class="glyphicon glyphicon-edit"></span></a>' +
        '<a href="####" class="btn btn-danger" id="delUser"  data-target="#deleteuser" ' +
        'style="margin-left:2px" title="删除">' +
        '<span class="glyphicon glyphicon-trash"></span></a>';
    return html
};

/**
 * 检查是否选中
 */
PtPeopleStudent.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        PtPeopleStudent.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
PtPeopleStudent.openAddPtPeopleStudent = function () {
    var index = layer.open({
        type: 2,
        title: '添加子女',
        area: ['630px', '350px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptPeopleStudent/ptPeopleStudent_add?orgId=' + PtPeopleStudent.orgId+"&peopleId="+PtPeopleStudent.peopleId
    });
    this.layerIndex = index;
};

/**
 * 打开修改页面
 */
PtPeopleStudent.openEditPtPeopleStudent = function (row) {

        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['630px', '350px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptPeopleStudent/ptPeopleStudent_update?id=' + row.id+"&classId="+row.classId
        });
        this.layerIndex = index;
};

/**
 * 删除
 */
PtPeopleStudent.delete = function (id) {
        var ajax = new $ax(Feng.ctxPath + "/ptPeopleStudent/delete/"+id, function (data) {
            if (data.code===200){
                Feng.success(data.msg);
                PtPeopleStudent.table.refresh();
            } else {
                Feng.error(data.msg);
            }

        }, function (data) {
            Feng.error(data.msg);
        });
        ajax.start();
};

/**
 * 查询列表
 */
PtPeopleStudent.search = function () {
    var queryData = {};
    queryData['orgId'] = PtPeopleStudent.orgId;
    queryData['peopleId'] = PtPeopleStudent.peopleId;
    PtPeopleStudent.table.refresh({query: queryData});
};
PtPeopleStudent.formParams = function () {
    var queryData = {};
    queryData['orgId'] = PtPeopleStudent.orgId;
    queryData['peopleId'] = PtPeopleStudent.peopleId;
    return queryData;
};
$(function () {
    PtPeopleStudent.orgId=$("#orgId").val();
    PtPeopleStudent.peopleId=$("#peopleId").val();
    var defaultColunms = PtPeopleStudent.initColumn();
    var table = new BSTable(PtPeopleStudent.id, "/ptPeopleStudent/list", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams(PtPeopleStudent.formParams());
    PtPeopleStudent.table = table.init();
});
