/**
 * 节次管理管理初始化
 */
var PtSectionCategory = {
    id: "PtSectionCategoryTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    orgId:null
};

/**
 * 初始化表格的列
 */
PtSectionCategory.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '', field: 'id', visible: false, align: 'center', valign: 'middle'},
            {title: '节次名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '所属学期', field: 'termName', visible: true, align: 'center', valign: 'middle'},
            {title: '节次数量', field: 'sections', visible: true, align: 'center', valign: 'middle'},
            {title: '应用时间', field: 'useTime', visible: true, align: 'center', valign: 'middle'},
           {title: '状态', field: 'enable', visible: true, align: 'center', valign: 'middle',formatter: function (value, row, index) {
                   var value = "";
                   if (!row.enable) {
                       value = "冻结";
                   } else {
                       value = "启用";
                   }
                   return value;
               }}
    ];
};

/**
 * 检查是否选中
 */
PtSectionCategory.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        PtSectionCategory.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加节次管理
 */
PtSectionCategory.openAddPtSectionCategory = function () {
    var index = layer.open({
        type: 2,
        title: '添加节次管理',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptSectionCategory/ptSectionCategory_add?orgId='+PtSectionCategory.orgId
    });
    this.layerIndex = index;
};

/**
 * 打开查看节次管理详情
 */
PtSectionCategory.openPtSectionCategoryDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '节次管理详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptSectionCategory/ptSectionCategory_update/' + PtSectionCategory.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除节次管理
 */
PtSectionCategory.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/ptSectionCategory/delete/"+this.seItem.id, function (data) {
            if (data.code===200){
            Feng.success(data.msg);
            PtSectionCategory.table.refresh();
            }else {
                Feng.error(data.msg);
            }
        }, function (data) {
            Feng.error(data.msg);
        });
        ajax.start();
    }
};
PtSectionCategory.updateEnable=function(){
    if (this.check()) {
        if (this.seItem.enable===1){
            status="禁用";
            enable=0;
        }else {
            status="启用";
            enable=1;
        }
        var id=this.seItem.id;
        var message = "您确定要"+status+"吗？";
        parent.layer.confirm(message, {
            btn: ['确定', '取消'],
            shade: false //不显示遮罩
        }, function (index) {
            parent.layer.close(index);
            var ajax = new $ax(Feng.ctxPath + "/ptSectionCategory/disableSection/"+id+"/"+enable, function (data) {
                if (data.code===200){
                    Feng.success(data.msg);
                    PtSectionCategory.table.refresh();
                }else {
                    Feng.error(data.msg);
                }

            }, function (data) {
                Feng.error(data.msg);
            });
            ajax.start();
        });
    }
};

/**
 * 查询节次管理列表
 */
PtSectionCategory.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    PtSectionCategory.table.refresh({query: queryData});
};
PtSectionCategory.formParams = function () {
    var queryData = {};
    queryData['orgId'] = PtSectionCategory.orgId;
   return queryData;
};
$(function () {
    PtSectionCategory.orgId=$("#orgId").val();
    var defaultColunms = PtSectionCategory.initColumn();
    var table = new BSTable(PtSectionCategory.id, "/ptSectionCategory/list", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams(PtSectionCategory.formParams());
    PtSectionCategory.table = table.init();
});
