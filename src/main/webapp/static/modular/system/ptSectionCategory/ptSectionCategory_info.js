/**
 * 初始化节次管理详情对话框
 */
var PtSectionCategoryInfoDlg = {
    ptSectionCategoryInfoData : {}
};

/**
 * 清除数据
 */
PtSectionCategoryInfoDlg.clearData = function() {
    this.ptSectionCategoryInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtSectionCategoryInfoDlg.set = function(key, val) {
    this.ptSectionCategoryInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtSectionCategoryInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
PtSectionCategoryInfoDlg.close = function() {
    parent.layer.close(window.parent.PtSectionCategory.layerIndex);
}

/**
 * 收集数据
 */
PtSectionCategoryInfoDlg.collectData = function() {
    this
    .set('id')
    .set('name')
    .set('orgId')
    .set('orgName')
    .set('termId')
    .set('termName')
    .set('gradeId')
    .set('gradeName')
    .set('startTime')
    .set('endTime')
        .set("stStartTime")
        .set("stEndTime")
    .set('week')
    .set('sections');
}

/**
 * 提交添加
 */
PtSectionCategoryInfoDlg.addSubmit = function() {
    //
    // this.clearData();
    // this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptSectionCategory/add", function(data){
        if (data.code===200){
            Feng.success("添加成功!");
            window.parent.PtSectionCategory.table.refresh();
            PtSectionCategoryInfoDlg.close();
        } else {
            Feng.error(data.msg);
        }
    },function(data){
        Feng.error(data.msg);
    });
    ajax.setData($(".form-horizontal").serialize());
    ajax.start();
};

/**
 * 提交修改
 */
PtSectionCategoryInfoDlg.editSubmit = function() {

    // this.clearData();
    // this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptSectionCategory/update", function(data){
        if (data.code===200) {
        Feng.success(data.msg);
        window.parent.PtSectionCategory.table.refresh();
        PtSectionCategoryInfoDlg.close();
        }else {
            Feng.error(data.msg);
        }
    },function(data){
        Feng.error(data.msg);
    });
    ajax.setData($(".form-horizontal").serialize());
    ajax.start();
}

$(function() {

});
