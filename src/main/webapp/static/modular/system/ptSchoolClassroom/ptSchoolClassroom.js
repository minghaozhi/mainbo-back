/**
 * 管理初始化
 */
var PtSchoolClassroom = {
    id: "PtSchoolClassroomTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    buildingId:null
};

/**
 * 初始化表格的列
 */
PtSchoolClassroom.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '教室名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '教室类型', field: 'classroomTypeId', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row, index) {
                var value = "";
                if (row.classroomTypeId===50) {
                    value = "普通班";
                } else if (row.classroomTypeId===51) {
                    value = "重点班";
                }else {
                    value = "实验班";
                }
                return value;
            }},
        {title: '教室位置', field: 'classroomPosition', visible: true, align: 'center', valign: 'middle'},
        {title: '容纳学生数数', field: 'galleryful', visible: true, align: 'center', valign: 'middle'},
        {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
PtSchoolClassroom.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        PtSchoolClassroom.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
PtSchoolClassroom.openAddPtSchoolClassroom = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptSchoolClassroom/ptSchoolClassroom_add?buildingId='+ PtSchoolClassroom.buildingId
    });
    this.layerIndex = index;
};

/**
 * 打开查看详情
 */
PtSchoolClassroom.openPtSchoolClassroomDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptSchoolClassroom/ptSchoolClassroom_update/' + PtSchoolClassroom.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除
 */
PtSchoolClassroom.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/ptSchoolClassroom/delete/"+this.seItem.id, function (data) {
            if (data.code===200){
            Feng.success("删除成功!");
            PtSchoolClassroom.table.refresh();
            }
        }, function (data) {
            Feng.error(data.msg);
        });
        ajax.start();
    }
};
/**
 * 详情
 */
PtSchoolClassroom.detail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptSchoolClassroom/detail/' + PtSchoolClassroom.seItem.id
        });
        this.layerIndex = index;
    }
};
/**
 * 查询列表
 */
PtSchoolClassroom.search = function () {
    var queryData = {};
    queryData['name'] = $("#name").val();
    queryData['classroomTypeId'] = $("#classType").val();
    queryData['buildingId'] = PtSchoolClassroom.buildingId;
    PtSchoolClassroom.table.refresh({query: queryData});
};
PtSchoolClassroom.formParams = function() {
    var queryData = {};
    queryData['buildingId'] = PtSchoolClassroom.buildingId;
    queryData['classroomTypeId'] =$("#classType").val();
    return queryData;
};
$(function () {
    PtSchoolClassroom.buildingId=$("#buildingId").val();
    var defaultColunms = PtSchoolClassroom.initColumn();
    var table = new BSTable(PtSchoolClassroom.id, "/ptSchoolClassroom/findListClassRoom", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams(PtSchoolClassroom.formParams())
    PtSchoolClassroom.table = table.init();
});
