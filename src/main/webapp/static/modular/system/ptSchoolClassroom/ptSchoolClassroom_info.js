/**
 * 初始化详情对话框
 */
var PtSchoolClassroomInfoDlg = {
    ptSchoolClassroomInfoData : {},
    validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '名称不能为空'
                },
                stringLength: {
                    max: 6,
                    message: '长度必须小于6'
                }
            }
        },
        galleryful: {
            validators: {
                notEmpty: {
                    message: '人数不能为空'
                },
                regexp: {/* 只需加此键值对，包含正则表达式，和提示 */
                    regexp: /^[0-9]+$/,
                    message: '只能是数字'
                }, stringLength: {/*长度提示*/
                    max: 3,
                    message: '长度必须小于3'
                }
            }
        },
        floorCount: {
            validators: {
                notEmpty: {
                    message: '位置不能为空'
                },
                regexp: {/* 只需加此键值对，包含正则表达式，和提示 */
                    regexp: /^[0-9]+$/,
                    message: '只能是数字'
                }, stringLength: {/*长度提示*/
                    max: 3,
                    message: '长度必须小于3'
                }
            }
        },
    }
};

/**
 * 清除数据
 */
PtSchoolClassroomInfoDlg.clearData = function() {
    this.ptSchoolClassroomInfoData = {}
};
PtSchoolClassroomInfoDlg.validate = function () {
    $('#classRoomForm').data("bootstrapValidator").resetForm();
    $('#classRoomForm').bootstrapValidator('validate');
    return $("#classRoomForm").data('bootstrapValidator').isValid();
};
/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtSchoolClassroomInfoDlg.set = function(key, val) {
    this.ptSchoolClassroomInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtSchoolClassroomInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
PtSchoolClassroomInfoDlg.close = function() {
    parent.layer.close(window.parent.PtSchoolClassroom.layerIndex);
}

/**
 * 收集数据
 */
PtSchoolClassroomInfoDlg.collectData = function() {
    this
    .set('id')
    .set('name')
    .set('galleryful')
    .set('orgId')
    .set('buildingId')
    .set('buildingName')
    .set('floor')
    .set('serialNo')
    .set('classroomPosition')
    .set('classroomTypeId')
    .set('note')
    .set('createTime')
    .set('modifyTime')
    .set('isUsed')
    .set('enable')
    .set('deleted')
    .set('lastupId')
    .set('lastupDttm');
}

/**
 * 提交添加
 */
PtSchoolClassroomInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptSchoolClassroom/add", function(data){
       if (data.code===200){
           Feng.success(data.msg);
           window.parent.PtSchoolClassroom.table.refresh();
           PtSchoolClassroomInfoDlg.close();
       }

    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.ptSchoolClassroomInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
PtSchoolClassroomInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptSchoolClassroom/update", function(data){
        Feng.success("修改成功!");
        window.parent.PtSchoolClassroom.table.refresh();
        PtSchoolClassroomInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.ptSchoolClassroomInfoData);
    ajax.start();
}

$(function() {
    Feng.initValidator("classRoomForm", PtSchoolClassroomInfoDlg.validateFields);

});
