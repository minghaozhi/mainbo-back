/**
 * 管理初始化
 */
var Postmanage = {
    id: "PtRolesTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    accountId: "0",
    userType: null,
    orgId:null,
    areaId: 0
};

/**
 * 初始化表格的列
 */
Postmanage.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: false ,visible: false,},
        {title: '学段', field: 'phaseName', visible: true, width: '98px', align: 'center', valign: 'middle'},
        {title: '年级', field: 'gradeName', visible: true, width: '98px', align: 'center', valign: 'middle'},
        {title: '学科', field: 'subjectName', visible: true, halign: "center", align: "left", width: '100px', valign: 'middle'},
        {title: '职务', field: 'roleName', visible: true, width: '98px', align: 'center', valign: 'middle'},
        {title: '状态', field: 'enable', visible: true, width: '50px', align: 'center', valign: 'middle',
            formatter: function (value, row, index) {
                var value = "";
                if (!row.enable) {
                    value = "禁用";
                } else {
                    value = "启用";
                }

                return value;
            }},
        {title: '最近更新时间', field: 'lastupDttm', visible: true, width: '160px', align: 'center', valign: 'middle'},
        {
            field: 'operate',
            title: '操作',
            halign: "center",
            align: "left",
            formatter: btnGroup,    // 自定义方法，添加按钮组
            events: {               // 注册按钮组事件
                'click #edit': function (event, value, row, index) {
                    Postmanage.openEditPostManage(row.id)
                },
                'click #delUser': function (event, value, row, index) {
                    Postmanage.deleteUserPost(row.id);
                },
                'click #resetPwd': function (event, value, row, index) {
                    Postmanage.updateEnable(row)
                }
            }
        }
    ];
};

function btnGroup() {   // 自定义方法，添加操作按钮
    // data-target="xxx" 为点击按钮弹出指定名字的模态框
    var html =
        '<a href="####" class="btn btn-primary" id="edit"  data-target="#edit" title="修改">' +
        '<span class="glyphicon glyphicon-edit"></span></a>' +
        '<a href="####" class="btn btn-danger" id="delUser"  data-target="#deleteuser" ' +
        'style="margin-left:2px" title="删除">' +
        '<span class="glyphicon glyphicon-trash"></span></a>' +
        '<a href="####" class="btn btn-warning" id="resetPwd"  data-target="#resetPwd" ' +
        'style="margin-left:2px" title="禁用或启用">' +
        '<span class="glyphicon glyphicon-off"></span></a>';
    return html
};
Postmanage.addOrEditPostManage=function(){
    var index =layer.open({
        type: 2,
        title: '添加',
        area: ['500px', '300px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptAccount/addOrEditPostManage?accountId=' + Postmanage.accountId
    });
    this.layerIndex = index;
};
Postmanage.openEditPostManage=function(id){
    var index =layer.open({
        type: 2,
        title: '修改',
        area: ['500px', '300px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptAccount/addOrEditPostManage?accountId=' + Postmanage.accountId+"&spaceId="+id
    });
    this.layerIndex = index;
};
Postmanage.deleteUserPost=function(id){
    parent.layer.confirm('确定删除该职务吗？', {
        btn: ['确定', '取消'],
        shade: false //不显示遮罩
    }, function () {
        var ajax = new $ax(Feng.ctxPath + "/ptAccount/deleteUserPost", function (data) {
            if (data.code===200){
                Feng.success(data.msg);
                var index =parent.layer.getFrameIndex(window.name);//获取当前弹窗的Id
                layer.close(index);//关闭
                Postmanage.table.refresh();
                window.parent.PtTeacher.table.refresh();
            }else {
                Feng.error(data.msg);
            }
        }, function (data) {
            Feng.error(data.msg);
        });
        ajax.set("id", id);
        ajax.start();
    });
};
Postmanage.updateEnable=function(row){
    let status='';
    let enable;
    if (row.enable===true){
        status="禁用";
        enable=false;
    }else {
        status="启用";
        enable=true;
    }
    var message = "您确定要"+status+"该职务吗？";
    parent.layer.confirm(message, {
        btn: ['确定', '取消'],
        shade: false //不显示遮罩
    }, function () {
        var ajax = new $ax(Feng.ctxPath + "/ptAccount/disabledUserPost", function (data) {
            if (data.code===200){
                Feng.success(data.msg);
                var index =parent.layer.getFrameIndex(window.name);//获取当前弹窗的Id
                layer.close(index);//关闭
                Postmanage.table.refresh();
                window.parent.PtTeacher.table.refresh();
            }else {
                Feng.error(data.msg);
            }

        }, function (data) {
            Feng.error(data.msg);
        });
        ajax.set("id", row.id);
        ajax.set("enable", enable);
        ajax.start();
    });
};

$(function () {
    const accountId=$("#accountId").val();
    Postmanage.accountId=accountId;
    var defaultColunms = Postmanage.initColumn();
    var table = new BSTable(Postmanage.id, "/ptAccount/findPostManage?accountId="+Postmanage.accountId, defaultColunms);
    table.setPaginationType("client");
    Postmanage.table = table.init();
});