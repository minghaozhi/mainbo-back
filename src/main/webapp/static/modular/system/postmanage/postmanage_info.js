var PostmanageInfoDlg = {
    postmanageInfo : {},
    accountId:null,
    orgId:null,
    areaId:null
};
PostmanageInfoDlg.clearData = function() {
    this.postmanageInfo = {};
};
PostmanageInfoDlg.set = function(key, val) {
    this.postmanageInfo[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
};

PostmanageInfoDlg.collectData = function() {
    this.
        set("id")
        .set("accountId",PostmanageInfoDlg.accountId)
        .set("orgId",PostmanageInfoDlg.orgId)
        .set("roleId",$("#sys_ro_id").val())
        .set("phaseId",$("#js_xg").val())
        .set("gradeId",$("#add_box_nj").val())
        .set("subjectId",$("#add_box_xd").val())
};

PostmanageInfoDlg.addSubmit=function(){
    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptAccount/saveAreaPostManage", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            window.parent.Postmanage.table.refresh();
            window.parent.parent.PtTeacher.table.refresh();
            PostmanageInfoDlg.close();
        }else {
            Feng.error(data.msg);
        }

    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.postmanageInfo);
    ajax.start();
};
PostmanageInfoDlg.editSubmit=function(){
    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptAccount/saveAreaPostManage", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            window.parent.Postmanage.table.refresh();
            window.parent.parent.PtTeacher.table.refresh();
            PostmanageInfoDlg.close();
        }else {
            Feng.error(data.msg);
        }

    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.postmanageInfo);
    ajax.start();
};
PostmanageInfoDlg.close = function() {
    parent.layer.close(window.parent.Postmanage.layerIndex);
};


PostmanageInfoDlg.roleIdChange=function(roleId){
    $("#yhgl_xd_").hide();
    $("#yhgl_nj_").hide();
    $("#yhgl_xk_").hide();
    $.ajax({
        async : false,
        type: 'POST',
        url:Feng.ctxPath+"/ptAccount/showInfoBySysRoleId",
        data:{'roleId':roleId},
        dataType:"json",
        cache: false,
        success: function(data){
            if(data.showPhase){
                $("#yhgl_xd_").show();
                $("#js_xg").addClass("required");
                if(data.showGrade){
                    $("#yhgl_nj_").show();
                    $("#add_box_nj").addClass("required");
                    $("#add_box_nj").prop({disabled: false});
                }else{
                    $("#add_box_xd").prop({disabled: true});
                }
                if(data.showSubject){
                    $("#yhgl_xk_").show();
                    $("#add_box_xd").addClass("required");
                    $("#add_box_xd").prop({disabled: false});
                }else{
                    $("#add_box_xd").prop({disabled: true});
                }
            }
        },
    })
};
PostmanageInfoDlg.phaseIdChange= function(){
    var phaseId=$("#js_xg").val();
    var orgId=PostmanageInfoDlg.orgId;
    var areaId=PostmanageInfoDlg.areaId;
    $.ajax({
        type: 'POST',
        url:Feng.ctxPath+"/ptTeacher/sujectAndGradeInfoByPhase",
        data:{'phaseId':phaseId,'id':orgId,'areaIds':areaId},
        dataType:"json",
        success: function(data){
            var grades=data.grades;
            if(grades){
                var gradesStr = '<option value="">请选择</option>';
                for(var i=0;i<grades.length;i++){
                    gradesStr += '<option value="'+grades[i].id+'">'+grades[i].name+'</option>';
                }
                $("#add_box_nj").html(gradesStr);
            }else{
                $("#add_box_nj").html('<option value="">请选择</option>');
            }
            var subjects=data.subjects;
            if(subjects){
                var subjectStr = '<option value="">请选择</option>';
                for(var i=0;i<subjects.length;i++){
                    subjectStr += '<option value="'+subjects[i].id+'">'+subjects[i].name+'</option>';
                }
                $("#add_box_xd").html(subjectStr);
            }else{
                $("#add_box_xd").html('<option value="">请选择</option>');
            }
        },
    })
};

$(function () {
    PostmanageInfoDlg.accountId=$("#accountId").val();
    PostmanageInfoDlg.orgId=$("#orgId").val();
    PostmanageInfoDlg.areaId=$("#areaId").val();
});