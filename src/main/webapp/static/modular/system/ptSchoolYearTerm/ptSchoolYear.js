/**
 * 初始化详情对话框
 */
var PtSchoolYearInfoDlg = {
    ptSchoolYearInfoData : {}
};
/**
 * 清除数据
 */
PtSchoolYearInfoDlg.clearData = function() {
    this.ptSchoolYearInfoData = {};
};
/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtSchoolYearInfoDlg.set = function(key, val) {
    this.ptSchoolYearInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
};

/**
 * 关闭此对话框
 */
PtSchoolYearInfoDlg.close = function() {
    parent.layer.close(window.parent.PtSchoolYearTerm.layerIndex);
};
/**
* 收集数据
*/
PtSchoolYearInfoDlg.collectData = function() {
    this
        .set('id')
        .set('orgId')
        .set('areaId')
        .set('startYear')
        .set('endYear')
        .set('startTime')
        .set("name", $("#startYear").val()+"-"+$("#endYear").val())
        .set('endTime');
};

/**
 * 提交添加
 */
PtSchoolYearInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptSchoolYearTermManage/schoolyear/saveOrUpdate", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            window.parent.PtSchoolYearTerm.refresh();
            PtSchoolYearInfoDlg.close();
        }else {
            Feng.error(data.msg);
        }

    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.ptSchoolYearInfoData);
    ajax.start();
};
/**
* 提交修改
*/
PtSchoolYearInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptSchoolYearTermManage/schoolyear/saveOrUpdate", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            window.parent.PtSchoolYearTerm.refresh();
            PtSchoolYearInfoDlg.close();
        }

    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.ptSchoolYearInfoData);
    ajax.start();
};


