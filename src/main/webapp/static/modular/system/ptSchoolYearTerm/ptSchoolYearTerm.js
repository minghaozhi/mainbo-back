/**
 * 管理初始化
 */
var PtSchoolYearTerm = {
    id: "PtSchoolYearTermTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    orgId:null,
    areaId:null
};

/**
 * 初始化表格的列
 */
PtSchoolYearTerm.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},

        {title: '学年', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '学期范围', field: 'termScope', visible: true, align: 'center', valign: 'middle'},
        {title: '教学日期范围', field: 'teacherScope', visible: true, align: 'center', valign: 'middle'}
    ];

};

/**
 * 检查是否选中
 */
PtSchoolYearTerm.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        PtSchoolYearTerm.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
PtSchoolYearTerm.openAddSchoolYear = function () {
    var index = layer.open({
        type: 2,
        title: '添加学年',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptSchoolYearTermManage/schoolyear/toAddOrEdit/-1'+"?orgId="+ PtSchoolYearTerm.orgId+"&areaId="+PtSchoolYearTerm.areaId
    });
    this.layerIndex = index;
};

/**
 * 打开修改页面
 */
PtSchoolYearTerm.openEditSchoolYear = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '修改学年',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptSchoolYearTermManage/schoolyear/toAddOrEdit/'+ PtSchoolYearTerm.seItem.id+"?orgId="+ PtSchoolYearTerm.orgId+"&areaId="+PtSchoolYearTerm.areaId
        });
        this.layerIndex = index;
    }
};
PtSchoolYearTerm.toManageTerm=function(){
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '管理学期',
            area: ['800px', '500px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptSchoolYearTermManage/schoolyear/editTerm/'+ PtSchoolYearTerm.seItem.id,
            cancel: function(){
                window.location.reload();
            }
        });
        this.layerIndex = index;
    }
    };
/**
 * 删除
 */
PtSchoolYearTerm.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/ptConfig/delete/"+this.seItem.id, function (data) {
            if (data.code===200){
                Feng.success("删除成功!");
               window.location.reload();
            }
        }, function (data) {
            Feng.error(data.msg);
        });
        ajax.start();
    }
};
/**
 * 详情
 */
PtSchoolYearTerm.detail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptSchoolClassroom/detail/' + PtSchoolClassroom.seItem.id
        });
        this.layerIndex = index;
    }
};
/**
 * 查询列表
 */
PtSchoolYearTerm.search = function () {
    var queryData = {};
    queryData['orgId'] = PtSchoolYearTerm.orgId;
    queryData['areaId'] = PtSchoolYearTerm.areaId;
    PtSchoolYearTerm.refresh({query: queryData});
};
PtSchoolYearTerm.formParams = function() {
    var queryData = {};
    queryData['orgId'] = PtSchoolYearTerm.orgId;
    queryData['areaId'] = PtSchoolYearTerm.areaId;
    return queryData;
};
PtSchoolYearTerm.refresh=function (parms) {
    if (typeof parms != "undefined") {
        $('#'+PtSchoolYearTerm.id).bootstrapTable('refresh', parms);
    } else {
        $('#'+PtSchoolYearTerm.id).bootstrapTable('refresh');
    }
}
$(function () {
    PtSchoolYearTerm.orgId=$("#orgId").val();
    PtSchoolYearTerm.areaId=$("#areaId").val();
    $('#'+PtSchoolYearTerm.id).bootstrapTable({
        url:  Feng.ctxPath+"/ptSchoolYearTermManage/findList",
        sidePagination: "true",
        pageNumber: 1,
        striped: true,
        cache: false,
        contentType: "application/x-www-form-urlencoded",
        pageSize: "10",
        pageList: [10, 50, 100],
        pagination: true, // 是否分页
        queryParamsType: 'limit',
        columns: PtSchoolYearTerm.initColumn(),
        queryParams:PtSchoolYearTerm.formParams(),
        paginationType:'client',
        icons: {
            refresh: 'glyphicon-repeat',
            toggle: 'glyphicon-list-alt',
            columns: 'glyphicon-list'
        },
        iconSize: 'outline',
        onLoadSuccess: function () {//当所有数据被加载时触发处理函数
        var data =$('#'+PtSchoolYearTerm.id).bootstrapTable('getData', true);//获取当前页数据
        mergeCells(data,'name',1,$('#'+PtSchoolYearTerm.id));
    },
    onPageChange: function (){//当页面更改页码或页面大小时触发
        var data =  $('#'+PtSchoolYearTerm.id).bootstrapTable('getData', true);
        mergeCells(data,'name',1,$('#'+PtSchoolYearTerm.id));
    },
});
    function mergeCells(data,fieldName,colspan,target) {
        //声明一个map计算相同属性值在data对象出现的次数和
        var sortMap = {};
        for (var i = 0; i < data.length; i++) {
            for (var prop in data[i]) {
                if (prop == fieldName) {
                    var key = data[i][prop]     //fieldName的value
                    if (sortMap.hasOwnProperty(key)) {
                        sortMap[key] = sortMap[key] * 1 + 1;
                    } else {
                        sortMap[key] = 1;
                    }
                    break;
                }
            }
        }
        /*for(var prop in sortMap){
            console.log(prop,sortMap[prop])
        }*/
        //合并单元格
        var index = 0;
        for (var prop in sortMap) {
            var count = sortMap[prop] * 1;
            //注意：合并前Java后台Sql要对field字段排序(order by)
            //index：合并起始位置，field：合并列名，colspan：合并成一列，rowspan：合并行数
            $(target).bootstrapTable('mergeCells', {index: index, field: fieldName, colspan: colspan, rowspan: count});
             $(target).bootstrapTable('mergeCells', {index: index, field: Number, colspan: colspan, rowspan: count});
            index += count;
        }
    }

});
