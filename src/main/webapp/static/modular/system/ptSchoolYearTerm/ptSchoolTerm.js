/**
 * 管理初始化
 */
var PtSchoolTerm = {
    id: "PtSchoolTermTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    orgId:null,
    areaId:null,
    schoolYearId:null,
    ptSchoolTermInfoData : {},
    validateFields: {
        termStartTime: {
            validators: {
                notEmpty: {
                    message: '时间不能为空'
                },
            }
        },
        termEndTime: {
            validators: {
                notEmpty: {
                    message: '时间不能为空'
                },
            }
        },
        teachStartTime: {
            validators: {
                notEmpty: {
                    message: '时间不能为空'
                },
            }
        },
        teachEndTime: {
            validators: {
                notEmpty: {
                    message: '时间不能为空'
                },
            }
        },
    }
};
/**
 * 清除数据
 */
PtSchoolTerm.clearData = function() {
    this.ptSchoolTermInfoData = {};
};
PtSchoolTerm.validate = function () {
    $('#schoolTerm').data("bootstrapValidator").resetForm();
    $('#schoolTerm').bootstrapValidator('validate');
    return $("#schoolTerm").data('bootstrapValidator').isValid();
};
/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtSchoolTerm.set = function(key, val) {
    this.ptSchoolTermInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
};
/**
 * 关闭此对话框
 */
PtSchoolTerm.close = function() {
    parent.layer.close(window.parent.PtSchoolTerm.layerIndex);
};

/**
 * 收集数据
 */
PtSchoolTerm.collectData = function() {
    this
        .set('id')
        .set('orgId')
        .set("termId")
        .set('sysTermId')
        .set('termStartTime')
        .set('termEndTime')
        .set('teachStartTime')
        .set('teachEndTime');
};

/**
 * 初始化表格的列
 */
PtSchoolTerm.initColumn = function () {
    return [
        {field: 'selectItem',  radio: true},
        {title: 'id', field: 'termId', visible: false, align: 'center', valign: 'middle'},
        {
            field: 'number',
            title: '序号',
            align: "center",
            width: '60px',
            formatter: function (value, row, index) {
                //获取每页显示的数量
                var pageSize = $('#' + PtSchoolTerm.id).bootstrapTable('getOptions').pageSize;
                //获取当前是第几页
                var pageNumber = $('#' + PtSchoolTerm.id).bootstrapTable('getOptions').pageNumber;
                //返回序号，注意index是从0开始的，所以要加上1
                return pageSize * (pageNumber - 1) + index + 1;
            }
        },

        {title: '学期', field: 'termName', visible: true, align: 'center', valign: 'middle'},
        {title: '学期范围', field: 'termScope', visible: true, align: 'center', valign: 'middle'},
        {title: '教学日期范围', field: 'teacherScope', visible: true, align: 'center', valign: 'middle'}
    ];

};

PtSchoolTerm.openAddSchoolTerm=function(){
    var index = layer.open({
        type: 2,
        title: '添加学期',
        area: ['700px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptSchoolYearTermManage/schoolterm/edit/-1'+'?schoolYearId='+PtSchoolTerm.schoolYearId
    });
    this.layerIndex = index;
};
/**
 * 检查是否选中
 */
PtSchoolTerm.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        PtSchoolTerm.seItem = selected[0];
        return true;
    }
};
PtSchoolTerm.openEditSchoolTerm=function(){
    if (this.check()){
    var index = layer.open({
        type: 2,
        title: '修改学期',
        area: ['700px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptSchoolYearTermManage/schoolterm/edit/'+this.seItem.termId+'?schoolYearId='+PtSchoolTerm.schoolYearId
    });
    this.layerIndex = index;
    }
};
/**
 * 提交添加
 */
PtSchoolTerm.addSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptSchoolYearTermManage/schoolterm/save", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            window.parent.PtSchoolTerm.table.refresh();
            PtSchoolTerm.close();
        }else {
            Feng.error(data.msg);
        }

    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.ptSchoolTermInfoData);
    ajax.start();
};

/**
 * 删除
 */
PtSchoolTerm.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/ptConfig/delete/"+this.seItem.termId, function (data) {
            if (data.code===200){
                Feng.success("删除成功!");
                PtSchoolTerm.table.refresh();
            }else {
                Feng.error(data.msg);
            }
        }, function (data) {
            Feng.error(data.msg);
        });
        ajax.start();
    }
};

$(function() {
    Feng.initValidator("schoolTerm", PtSchoolTerm.validateFields);

});




$(function () {
    PtSchoolTerm.schoolYearId=$("#schoolYearId").val();
    var defaultColunms = PtSchoolTerm.initColumn();
    var table = new BSTable(PtSchoolTerm.id, "/ptSchoolYearTermManage/schoolyear/findTermList?schoolYearId="+PtSchoolTerm.schoolYearId, defaultColunms);
    table.setPaginationType("client");
    PtSchoolTerm.table = table.init();
});