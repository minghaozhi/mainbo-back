/**
 * 管理初始化
 */
var PtRoleType = {
    id: "PtRoleTypeTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
PtRoleType.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'roleTypeName', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'roleTypeDesc', visible: true, align: 'center', valign: 'middle'},
            {title: '排序', field: 'sort', visible: true, align: 'center', valign: 'middle'},
            {title: '菜单应用范围。1区域、2学校、3系统', field: 'scope', visible: true, align: 'center', valign: 'middle'},
            {title: 'dic_id', field: 'code', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'homeUrl', visible: true, align: 'center', valign: 'middle'}
    ];
};
/**
 * 权限配置
 */
PtRoleType.assign = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '权限配置',
            area: ['300px', '450px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptRoleType/role_assign/' + this.seItem.id
        });
        this.layerIndex = index;
    }
};
/**
 * 检查是否选中
 */
PtRoleType.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        PtRoleType.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
PtRoleType.openAddPtRoleType = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptRoleType/ptRoleType_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看详情
 */
PtRoleType.openPtRoleTypeDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptRoleType/ptRoleType_update/' + PtRoleType.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除
 */
PtRoleType.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/ptRoleType/delete", function (data) {
            Feng.success("删除成功!");
            PtRoleType.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("ptRoleTypeId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询列表
 */
PtRoleType.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    PtRoleType.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = PtRoleType.initColumn();
    var table = new BSTable(PtRoleType.id, "/ptRoleType/list", defaultColunms);
    table.setPaginationType("client");
    PtRoleType.table = table.init();
});
