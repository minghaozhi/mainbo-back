/**
 * 初始化详情对话框
 */
var PtRoleTypeInfoDlg = {
    ptRoleTypeInfoData : {}
};

/**
 * 清除数据
 */
PtRoleTypeInfoDlg.clearData = function() {
    this.ptRoleTypeInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtRoleTypeInfoDlg.set = function(key, val) {
    this.ptRoleTypeInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtRoleTypeInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
PtRoleTypeInfoDlg.close = function() {
    parent.layer.close(window.parent.PtRoleType.layerIndex);
}

/**
 * 收集数据
 */
PtRoleTypeInfoDlg.collectData = function() {
    this
    .set('id')
    .set('roleTypeName')
    .set('roleTypeDesc')
    .set('sort')
    .set('scope')
    .set('code')
    .set('homeUrl');
}

/**
 * 提交添加
 */
PtRoleTypeInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptRoleType/add", function(data){
        Feng.success("添加成功!");
        window.parent.PtRoleType.table.refresh();
        PtRoleTypeInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.ptRoleTypeInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
PtRoleTypeInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptRoleType/update", function(data){
        Feng.success("修改成功!");
        window.parent.PtRoleType.table.refresh();
        PtRoleTypeInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.ptRoleTypeInfoData);
    ajax.start();
}

$(function() {

});
