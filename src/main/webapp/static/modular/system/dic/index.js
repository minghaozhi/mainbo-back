/**
 * 管理初始化
 */
var DicList = {
    id: "DicTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    orgId:null,
    code:null
};

/**
 * 初始化表格的列
 */
DicList.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '编号', field: 'id', visible: true, align: 'center',halign:"center",  valign: 'middle'},
        {title: '名称', field: 'name', visible: true, align: 'center',halign:"center", valign: 'middle'},
        {title: '顺序', field: 'sort', visible: true, align: 'center', valign: 'middle'},
        {title: '描述', field: 'descs', visible: true, align: 'center', valign: 'middle'}
    ];
};


DicList.openAddDic=function(){
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '450px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptConfig/toAdd?orgId='+DicList.orgId+"&code="+DicList.code
    });
    this.layerIndex = index;
};

/**
 * 检查是否选中
 */
DicList.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        DicList.seItem = selected[0];
        return true;
    }
};
DicList.openEditDic=function(){
    if (this.check()){
    var index = layer.open({
        type: 2,
        title: '修改',
        area: ['800px', '450px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptConfig/toUpdate?id='+this.seItem.id
    });
    this.layerIndex = index;
    }
};
/**
 * 删除字典
 */
DicList.delete = function () {
    if (this.check()) {

        var operation = function(){
            var ajax = new $ax(Feng.ctxPath + "/ptConfig/delete/"+ DicList.seItem.id, function (data) {
                Feng.success("删除成功!");
                DicList.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.start();
        };

        Feng.confirm("是否刪除字典 " + DicList.seItem.name + "?", operation);
    }
};
DicList.formParams = function () {
    var queryData = {};
    queryData['orgId'] = DicList.orgId;
    queryData['code'] = DicList.code;
    return queryData;
};
$(function () {
    DicList.orgId=$("#orgId").val();
    DicList.code=$("#code").val();
    var defaultColunms = DicList.initColumn();
    var table = new BSTable(DicList.id, "/ptConfig/findList", defaultColunms);
    table.setPaginationType("client");
    table.setQueryParams(DicList.formParams());
    DicList.table = table.init();
});
