/**
 * 初始化字典详情对话框
 */
var DicInfoDlg = {
    dicInfoData : {},
};

/**
 * 清除数据
 */
DicInfoDlg.clearData = function() {
    this.dicInfoData = {};
};

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
DicInfoDlg.set = function(key, val) {
    this.dicInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
};
/**
 * 关闭此对话框
 */
DicInfoDlg.close = function() {
    parent.layer.close(window.parent.DicList.layerIndex);
};
/**
 * 收集数据
 */
DicInfoDlg.collectData= function() {
    this
        .set('id')
        .set('name')
        .set('sort')
        .set("code")
        .set("orgId")
        .set('descs');
};
DicInfoDlg.addSubmit=function () {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptConfig/addOrUpdate", function(data){
        if (data.code===200){
            Feng.success(data.msg);
            window.parent.DicList.table.refresh();
            DicInfoDlg.close();
        }else {
            Feng.error(data.msg);
        }
    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.dicInfoData);
    ajax.start();
};

DicInfoDlg.editSubmit=function () {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptConfig/addOrUpdate", function(data){
        if (data.code===200){
            Feng.success("修改成功");
            window.parent.DicList.table.refresh();
            DicInfoDlg.close();
        }else {
            Feng.error(data.msg);
        }
    },function(data){
        Feng.error(data.msg);
    });
    ajax.set(this.dicInfoData);
    ajax.start();
};