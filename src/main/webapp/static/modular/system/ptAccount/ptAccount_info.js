/**
 * 初始化详情对话框
 */
var PtAccountInfoDlg = {
    ptAccountInfoData : {},
    validateFields: {
        cellphone: {
            validators: {
                regexp: {
                    regexp: /^1\d{10}$/,
                    message: '手机号格式错误'
                }
            }
        }
    }
};


/**
 * 清除数据
 */
PtAccountInfoDlg.clearData = function() {
    this.ptAccountInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtAccountInfoDlg.set = function(key, val) {
    this.ptAccountInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PtAccountInfoDlg.get = function(key) {
    return $("#" + key).val();
};

/**
 * 关闭此对话框
 */
PtAccountInfoDlg.close = function() {
    var index = parent.layer.getFrameIndex(window.name);//获取当前弹窗的Id
    parent.layer.close(index);//关闭
};

/**
 * 收集数据
 */
PtAccountInfoDlg.collectData = function() {
    this
    .set('id')
    .set('userType')
    .set('email')
    .set('cellphone')
    .set('orgId')
    .set('account')
    .set('password')
    .set('logincode')
    .set('salt')
    .set('lastLogin')
    .set('deleted')
    .set('enable')
    .set('disableReason')
    .set('remark');
};
PtAccountInfoDlg.collectData1 = function () {
    this.set('id').set('account').set('sex').set('password').set('photo',$("#avatar").val())
        .set('email').set('name').set('birthday').set('rePassword').set('phone').set('userType').set('cellphone');
};
/**
 * 验证数据
 */
PtAccountInfoDlg.validate = function () {
    $('#userInfoForm').data("bootstrapValidator").resetForm();
    $('#userInfoForm').bootstrapValidator('validate');
    return $("#userInfoForm").data('bootstrapValidator').isValid();
};
/**
 * 提交修改
 */
PtAccountInfoDlg.editInfo = function () {

    this.clearData();
    this.collectData1();
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptAccount/update", function (data) {
        if (data.code===200){
            parent.layer.alert(data.msg,{icon: 6});
            top.location.href=Feng.ctxPath+"/";
        }else {
            parent.layer.alert(data.msg,{icon: 5});
        }
    }, function (data) {
        parent.layer.alert(data.msg,{icon: 5});
    });
    ajax.set(this.ptAccountInfoData);
    ajax.start();
};
/**
 * 修改密码
 */
PtAccountInfoDlg.chPwd = function () {
    var ajax = new $ax(Feng.ctxPath + "/ptAccount/changePwd", function (data) {
        if (data.code===200){
            parent.layer.alert(data.msg,{icon: 6});
            top.location.href=Feng.ctxPath+"/";
        }else {
            parent.layer.alert(data.msg,{icon: 5});
        }
    }, function (data) {
        parent.layer.alert(data.msg,{icon: 5});
    });
    ajax.set("flago");
    ajax.set("password");
    ajax.set("flags");
    ajax.start();

};
/**
 * 提交添加
 */
PtAccountInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptAccount/add", function(data){
        Feng.success("添加成功!");
        window.parent.PtAccount.table.refresh();
        PtAccountInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.ptAccountInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
PtAccountInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ptAccount/update", function(data){
        Feng.success("修改成功!");
        window.parent.PtAccount.table.refresh();
        PtAccountInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.ptAccountInfoData);
    ajax.start();
}
PtAccountInfoDlg.import=function(){
    if($("#file_id").val()===""){
        Feng.info("请选择文件！");
    }else{
        debugger
        var type=$("#userType").val();
        var orgId=$("#orgId").val();
        var orgName=$("#orgName").val();
        var phase = $("#phase_sel :selected").text();
        var filename =$("#file_id").val();
        var file =$("#file_id")[0].files[0];
        console.log(file);
        var role;
        console.log(type);
        if(type==='3'){//教师
            role = "教师";
        }else if(type==='5'){//家长
            role = "家长";
        }else if(type==='4'){//学生
            role = "学生";
        }
        var strFileName=filename.replace(/^.+?\\([^\\]+?)(\.[^\.\\]*?)?$/gi,"$1");  //正则表达式获取文件名，不带后缀
        var message = "您当前导入的用户角色为："+role+"，学校："+orgName+"，学段："+phase+"，导入文件："+strFileName+"，请确认是否进行导入？";
        parent.layer.confirm(message, {
            btn: ['确定', '取消'],
            shade: false //不显示遮罩
        }, function (index) {
            parent.layer.close(index);
            var formData = new FormData();
            formData.append("orgId",orgId);
            formData.append("userType",type);
            formData.append("phaseId", $("#phase-select").val());
            formData.append("file",file);
            $.ajax({
                type: "POST",
                url:Feng.ctxPath+"/ptAccount/batchRegister",
                data:formData,
                contentType: false,
                processData: false,
                success: function(data) {
                    if(200 == data.code){
                        parent.layer.msg(data.msg,{icon: 6,time:3000},)
                    }else {
                        parent.layer.msg(data.msg,{icon: 5,time:3000})
                    }
                    var index = parent.layer.getFrameIndex(window.name);//获取当前弹窗的Id
                    parent.layer.close(index);//关闭

                    if(type==3){//教师
                        window.parent.PtTeacher.table.refresh();
                    }else if(type==5){//家长
                        window.parent.PtPeople.table.refresh();
                    }else if(type==4){//学生
                        window.parent.PtStudent.table.refresh();
                    }

                }
            });
        });
        return false;
    }
};
$(function() {
    Feng.initValidator("userInfoForm", PtAccountInfoDlg.validateFields);
    var sex = $("#sexValue").val();
    $("#sex").val(sex);
    // 初始化头像上传
    var avatarUp = new $WebUpload("avatar");
    avatarUp.setUploadBarId("progressBar");
    avatarUp.setUploadUrl($("#uploadUrl").val());
    avatarUp.init();
});
