/**
 * 管理初始化
 */
var PtAccount = {
    id: "PtAccountTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
PtAccount.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '用户id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'userType', visible: true, align: 'center', valign: 'middle'},
            {title: '验证邮箱', field: 'email', visible: true, align: 'center', valign: 'middle'},
            {title: '验证电话', field: 'cellphone', visible: true, align: 'center', valign: 'middle'},
            {title: '学校信息表id', field: 'orgId', visible: true, align: 'center', valign: 'middle'},
            {title: '登录帐号', field: 'account', visible: true, align: 'center', valign: 'middle'},
            {title: '帐号密码', field: 'password', visible: true, align: 'center', valign: 'middle'},
            {title: '第三方登录代码', field: 'logincode', visible: true, align: 'center', valign: 'middle'},
            {title: '安全盐值', field: 'salt', visible: true, align: 'center', valign: 'middle'},
            {title: '最近登录时间', field: 'lastLogin', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'deleted', visible: true, align: 'center', valign: 'middle'},
            {title: '帐号状态(0:正常，1：禁用 默认0)', field: 'enable', visible: true, align: 'center', valign: 'middle'},
            {title: '禁用或冻结原因', field: 'disableReason', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'remark', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
PtAccount.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        PtAccount.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
PtAccount.openAddPtAccount = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ptAccount/ptAccount_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看详情
 */
PtAccount.openPtAccountDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ptAccount/ptAccount_update/' + PtAccount.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除
 */
PtAccount.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/ptAccount/delete", function (data) {
            Feng.success("删除成功!");
            PtAccount.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("ptAccountId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询列表
 */
PtAccount.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    PtAccount.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = PtAccount.initColumn();
    var table = new BSTable(PtAccount.id, "/ptAccount/list", defaultColunms);
    table.setPaginationType("client");
    PtAccount.table = table.init();
});
