@/*
    头像参数的说明:
    name : 名称
    id : 头像的id
@*/
<div class="form-group">
    <label    @if(isNotEmpty(class)){
           class="${class}"
           @}else{
              class="col-sm-3 control-label head-scu-label "
              @}
    >${name}</label>
    <div class="col-sm-4">
        <div id="${id}PreId">
            <div><img width="100px" height="100px"
                @if(isEmpty(avatarImg)||isEmpty(storageUrl)){
                      src="${ctxPath}/static/img/user1.png"></div>
                @}else{
            src="${storageUrl}/resource/${avatarImg}/preview"></div>
                @}
        </div>
    </div>
</div>
@if(isNotEmpty(underline) && underline == 'true'){
    <div class="hr-line-dashed"></div>
@}


