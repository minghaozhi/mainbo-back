@/*
select标签中各个参数的说明:
name : select的名称
id : select的id
underline : 是否带分割线
style : 附加的css属性
@*/
<div class="form-group">
    <label class="col-sm-3 control-label">${name}
        @if(isNotEmpty(span)){
        <span style="color: red; ">*</span>
        @}
    </label>
    <div class="col-sm-9"  @if(isNotEmpty(style)){
         style="${style}"
         @}
    >
        <select class="form-control" id="${id}" name="${id}"
                @if(isNotEmpty(style)){
                style="${style}"
                @}
                @if(isNotEmpty(onchange)){
                onchange="${onchange}"
                @}
        >
            ${tagBody!}
        </select>
        @if(isNotEmpty(hidden)){
        <input class="form-control" type="hidden" id="${hidden}" value="${hiddenValue!}">
        @}
    </div>
</div>
@if(isNotEmpty(underline) && underline == 'true'){
<div class="hr-line-dashed"></div>
@}


