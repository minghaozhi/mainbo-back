
/*Table structure for table `code_dbinfo` */

DROP TABLE IF EXISTS `code_dbinfo`;

CREATE TABLE `code_dbinfo` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(20) DEFAULT NULL COMMENT '别名',
  `db_driver` VARCHAR(100) NOT NULL COMMENT '数据库驱动',
  `db_url` VARCHAR(200) NOT NULL COMMENT '数据库地址',
  `db_user_name` VARCHAR(100) NOT NULL COMMENT '数据库账户',
  `db_password` VARCHAR(100) NOT NULL COMMENT '连接密码',
  `db_type` VARCHAR(10) DEFAULT NULL COMMENT '数据库类型',
  `create_time` DATETIME DEFAULT NULL COMMENT '创建时间',
  `update_time` DATETIME DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=INNODB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='数据库链接信息';

/*Data for the table `code_dbinfo` */

/*Table structure for table `sys_dept` */

DROP TABLE IF EXISTS `sys_dept`;

CREATE TABLE `sys_dept` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `num` INT(11) DEFAULT NULL COMMENT '排序',
  `pid` INT(11) DEFAULT NULL COMMENT '父部门id',
  `pids` VARCHAR(255) DEFAULT NULL COMMENT '父级ids',
  `simplename` VARCHAR(45) DEFAULT NULL COMMENT '简称',
  `fullname` VARCHAR(255) DEFAULT NULL COMMENT '全称',
  `tips` VARCHAR(255) DEFAULT NULL COMMENT '提示',
  `version` INT(11) DEFAULT NULL COMMENT '版本（乐观锁保留字段）',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COMMENT='部门表';

/*Data for the table `sys_dept` */

INSERT  INTO `sys_dept`(`id`,`num`,`pid`,`pids`,`simplename`,`fullname`,`tips`,`version`) VALUES (29,1,0,'[0],','北京市','北京市','',NULL),(30,2,0,'[0],','河南省','河南省','',NULL),(31,1,29,'[0],[29],','昌平区','昌平区','',NULL),(32,3,0,'[0],','河北省','河北省','',NULL),(33,5,1,NULL,'测试','测试fullname','测试tips',1);

/*Table structure for table `sys_dict` */

DROP TABLE IF EXISTS `sys_dict`;

CREATE TABLE `sys_dict` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `num` INT(11) DEFAULT NULL COMMENT '排序',
  `pid` INT(11) DEFAULT NULL COMMENT '父级字典',
  `name` VARCHAR(255) DEFAULT NULL COMMENT '名称',
  `tips` VARCHAR(255) DEFAULT NULL COMMENT '提示',
  `code` VARCHAR(255) DEFAULT NULL COMMENT '值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=INNODB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='字典表';

/*Data for the table `sys_dict` */

INSERT  INTO `sys_dict`(`id`,`num`,`pid`,`name`,`tips`,`code`) VALUES (50,0,0,'性别',NULL,'sys_sex'),(51,1,50,'男',NULL,'1'),(52,2,50,'女',NULL,'2'),(53,0,0,'状态',NULL,'sys_state'),(54,1,53,'启用',NULL,'1'),(55,2,53,'禁用',NULL,'2'),(56,0,0,'账号状态',NULL,'account_state'),(57,1,56,'启用',NULL,'1'),(58,2,56,'冻结',NULL,'2'),(59,3,56,'已删除',NULL,'3'),(69,0,0,'字典测试','这是一个字典测试','test'),(70,1,69,'测试1',NULL,'1'),(71,2,69,'测试2',NULL,'2'),(72,0,0,'测试','备注','tes'),(73,1,72,'测试1',NULL,'1'),(74,2,72,'测试2',NULL,'2');

/*Table structure for table `sys_expense` */

DROP TABLE IF EXISTS `sys_expense`;

CREATE TABLE `sys_expense` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `money` DECIMAL(20,2) DEFAULT NULL COMMENT '报销金额',
  `desc` VARCHAR(255) DEFAULT '' COMMENT '描述',
  `createtime` DATETIME DEFAULT NULL COMMENT '创建时间',
  `state` INT(11) DEFAULT NULL COMMENT '状态: 1.待提交  2:待审核   3.审核通过 4:驳回',
  `userid` INT(11) DEFAULT NULL COMMENT '用户id',
  `processId` VARCHAR(255) DEFAULT NULL COMMENT '流程定义id',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='报销表';

/*Data for the table `sys_expense` */

/*Table structure for table `sys_login_log` */

DROP TABLE IF EXISTS `sys_login_log`;

CREATE TABLE `sys_login_log` (
  `id` INT(65) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `logname` VARCHAR(255) DEFAULT NULL COMMENT '日志名称',
  `userid` VARCHAR(64) DEFAULT NULL COMMENT '管理员id',
  `createtime` DATETIME DEFAULT NULL COMMENT '创建时间',
  `succeed` VARCHAR(255) DEFAULT NULL COMMENT '是否执行成功',
  `message` TEXT COMMENT '具体消息',
  `ip` VARCHAR(255) DEFAULT NULL COMMENT '登录ip',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=881 DEFAULT CHARSET=utf8 COMMENT='登录记录';

/*Data for the table `sys_login_log` */

INSERT  INTO `sys_login_log`(`id`,`logname`,`userid`,`createtime`,`succeed`,`message`,`ip`) VALUES (869,'退出日志','3a9ed4d365944b3c8574a4f9bb42ef7c','2020-03-16 11:26:54','成功',NULL,'0:0:0:0:0:0:0:1'),(870,'退出日志','3a9ed4d365944b3c8574a4f9bb42ef7c','2020-03-16 11:49:29','成功',NULL,'0:0:0:0:0:0:0:1'),(871,'退出日志','3a9ed4d365944b3c8574a4f9bb42ef7c','2020-03-16 11:50:01','成功',NULL,'0:0:0:0:0:0:0:1'),(872,'退出日志','3a9ed4d365944b3c8574a4f9bb42ef7c','2020-03-16 11:50:39','成功',NULL,'0:0:0:0:0:0:0:1'),(873,'退出日志','3a9ed4d365944b3c8574a4f9bb42ef7c','2020-03-16 12:34:18','成功',NULL,'0:0:0:0:0:0:0:1'),(874,'退出日志','3a9ed4d365944b3c8574a4f9bb42ef7c','2020-03-16 12:34:49','成功',NULL,'0:0:0:0:0:0:0:1'),(875,'退出日志','3a9ed4d365944b3c8574a4f9bb42ef7c','2020-03-16 12:37:02','成功',NULL,'0:0:0:0:0:0:0:1'),(876,'退出日志','3a9ed4d365944b3c8574a4f9bb42ef7c','2020-03-16 12:38:01','成功',NULL,'0:0:0:0:0:0:0:1'),(877,'退出日志','3a9ed4d365944b3c8574a4f9bb42ef7c','2020-03-16 12:39:11','成功',NULL,'0:0:0:0:0:0:0:1'),(878,'退出日志','3a9ed4d365944b3c8574a4f9bb42ef7c','2020-03-16 12:53:52','成功',NULL,'0:0:0:0:0:0:0:1'),(879,'退出日志','3a9ed4d365944b3c8574a4f9bb42ef7c','2020-03-16 13:10:13','成功',NULL,'0:0:0:0:0:0:0:1'),(880,'退出日志','3a9ed4d365944b3c8574a4f9bb42ef7c','2020-03-16 13:17:10','成功',NULL,'0:0:0:0:0:0:0:1');

/*Table structure for table `sys_menu` */

DROP TABLE IF EXISTS `sys_menu`;

CREATE TABLE `sys_menu` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `code` VARCHAR(255) DEFAULT NULL COMMENT '菜单编号',
  `pcode` VARCHAR(255) DEFAULT NULL COMMENT '菜单父编号',
  `pcodes` VARCHAR(255) DEFAULT NULL COMMENT '当前菜单的所有父菜单编号',
  `name` VARCHAR(255) DEFAULT NULL COMMENT '菜单名称',
  `icon` VARCHAR(255) DEFAULT NULL COMMENT '菜单图标',
  `url` VARCHAR(255) DEFAULT NULL COMMENT 'url地址',
  `num` INT(65) DEFAULT NULL COMMENT '菜单排序号',
  `levels` INT(65) DEFAULT NULL COMMENT '菜单层级',
  `ismenu` INT(11) DEFAULT NULL COMMENT '是否是菜单（1：是  0：不是）',
  `tips` VARCHAR(255) DEFAULT NULL COMMENT '备注',
  `status` INT(65) DEFAULT NULL COMMENT '菜单状态 :  1:启用   0:不启用',
  `isopen` INT(11) DEFAULT NULL COMMENT '是否打开:    1:打开   0:不打开',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=1236825400295235644 DEFAULT CHARSET=utf8 COMMENT='菜单表';

/*Data for the table `sys_menu` */


/*Table structure for table `sys_notice` */

DROP TABLE IF EXISTS `sys_notice`;

CREATE TABLE `sys_notice` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` VARCHAR(255) DEFAULT NULL COMMENT '标题',
  `type` INT(11) DEFAULT NULL COMMENT '类型',
  `content` TEXT COMMENT '内容',
  `createtime` DATETIME DEFAULT NULL COMMENT '创建时间',
  `creater` CHAR(32) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='通知表';

/*Data for the table `sys_notice` */

INSERT  INTO `sys_notice`(`id`,`title`,`type`,`content`,`createtime`,`creater`) VALUES (6,'世界',10,'欢迎使用Guns管理系统','2017-01-11 08:53:20','1'),(8,'你好',NULL,'你好','2017-05-10 19:28:57','1'),(9,'123213',NULL,'<p>1233223323</p>','2020-03-12 11:04:23','zxcdfsksl123sfwafd46sesa642s4dfs');

/*Table structure for table `sys_operation_log` */

DROP TABLE IF EXISTS `sys_operation_log`;

CREATE TABLE `sys_operation_log` (
  `id` INT(65) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `logtype` VARCHAR(255) DEFAULT NULL COMMENT '日志类型',
  `logname` VARCHAR(255) DEFAULT NULL COMMENT '日志名称',
  `userid` VARCHAR(64) DEFAULT NULL COMMENT '用户id',
  `classname` VARCHAR(255) DEFAULT NULL COMMENT '类名称',
  `method` TEXT COMMENT '方法名称',
  `createtime` DATETIME DEFAULT NULL COMMENT '创建时间',
  `succeed` VARCHAR(255) DEFAULT NULL COMMENT '是否成功',
  `message` TEXT COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=880 DEFAULT CHARSET=utf8 COMMENT='操作日志';

/*Data for the table `sys_operation_log` */

/*Table structure for table `sys_relation` */

DROP TABLE IF EXISTS `sys_relation`;

CREATE TABLE `sys_relation` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menuid` BIGINT(11) DEFAULT NULL COMMENT '菜单id',
  `roleid` INT(11) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=6306 DEFAULT CHARSET=utf8 COMMENT='角色和菜单关联表';

/*Data for the table `sys_relation` */

INSERT  INTO `sys_relation`(`id`,`menuid`,`roleid`) VALUES (3377,105,5),(3378,106,5),(3386,114,5),(3387,115,5),(3388,116,5),(3389,117,5),(3390,118,5),(3391,119,5),(3392,120,5),(3393,121,5),(3394,122,5),(3395,150,5),(3396,151,5),(3920,1231510240424415238,243),(4792,105,1),(4793,106,1),(4794,1231127578715217921,1),(4795,1231127578715217922,1),(4796,1231127578715217923,1),(4797,1231127578715217924,1),(4798,1231127578715217925,1),(4799,1231127578715217926,1),(4800,1231127616245850113,1),(4801,1231127616245850114,1),(4802,1231127616245850115,1),(4803,1231127616245850116,1),(4804,1231127616245850117,1),(4805,1231127616245850118,1),(4806,1231782918679334920,1),(4807,1231782918679334921,1),(4808,1231782918679334922,1),(4809,1231782918679334923,1),(4810,1231782918679334924,1),(4811,1231127654187524097,1),(4812,1231127654187524098,1),(4813,1231127654187524099,1),(4814,1231127654187524100,1),(4815,1231127654187524101,1),(4816,1231127654187524102,1),(4817,1231782918679334925,1),(4818,1231782918679334926,1),(4819,1231782918679334927,1),(4820,1231782918679334928,1),(4821,1231782918679334929,1),(4822,1231782918679334930,1),(4823,1231782918679334932,1),(4824,1231782918679334933,1),(4825,1231782918679334934,1),(4826,1231782918679334935,1),(4827,114,1),(4828,115,1),(4829,116,1),(4830,117,1),(4831,118,1),(4832,162,1),(4833,163,1),(4834,164,1),(4835,119,1),(4836,120,1),(4837,121,1),(4838,122,1),(4839,150,1),(4840,151,1),(4841,128,1),(4842,134,1),(4843,158,1),(4844,159,1),(4845,130,1),(4846,132,1),(4847,138,1),(4848,139,1),(4849,140,1),(4850,155,1),(4851,156,1),(4852,157,1),(4853,133,1),(4854,160,1),(4855,161,1),(4856,141,1),(4857,142,1),(4858,143,1),(4859,144,1),(4860,1231510240424415233,1),(4861,1231510240424415234,1),(4862,1231510240424415235,1),(4863,1231510240424415236,1),(4864,1231510240424415237,1),(4865,1231510240424415238,1),(4866,1231782918679334914,1),(4867,1231782918679334915,1),(4868,1231782918679334916,1),(4869,1231782918679334917,1),(4870,1231782918679334918,1),(4871,1231782918679334919,1),(4872,1231782918679334936,1),(4873,1230851042921504770,1),(4874,1230851042921504771,1),(4875,1230851042921504772,1),(4876,1230851042921504773,1),(4877,1230851042921504774,1),(4878,1230851042921504775,1),(4879,145,1),(4880,148,1),(4881,149,1),(6190,105,300),(6191,106,300),(6192,1231127578715217921,300),(6193,1231127578715217922,300),(6194,1231127578715217923,300),(6195,1231127578715217924,300),(6196,1231127578715217925,300),(6197,1231127578715217926,300),(6198,1231127616245850113,300),(6199,1231127616245850114,300),(6200,1231127616245850115,300),(6201,1231127616245850116,300),(6202,1231127616245850117,300),(6203,1231127616245850118,300),(6204,1231782918679334920,300),(6205,1231782918679334921,300),(6206,1231782918679334922,300),(6207,1231782918679334923,300),(6208,1231782918679334924,300),(6209,1231127654187524097,300),(6210,1231127654187524098,300),(6211,1231127654187524099,300),(6212,1231127654187524100,300),(6213,1231127654187524101,300),(6214,1231127654187524102,300),(6215,1231782918679334925,300),(6216,1231782918679334926,300),(6217,1231782918679334927,300),(6218,1231782918679334928,300),(6219,1231782918679334929,300),(6220,1231782918679334930,300),(6221,1231782918679334932,300),(6222,1231782918679334933,300),(6223,1231782918679334934,300),(6224,1231782918679334935,300),(6225,1231782918679334936,300),(6226,1230851042921504770,300),(6227,1230851042921504771,300),(6228,1230851042921504772,300),(6229,1230851042921504773,300),(6230,1230851042921504774,300),(6231,1230851042921504775,300),(6232,1231782918679334914,300),(6233,1231782918679334915,300),(6234,1231782918679334916,300),(6235,1231782918679334917,300),(6236,1231782918679334918,300),(6237,1231782918679334919,300),(6238,1231782918679334937,300),(6239,1231782918679334938,300),(6240,1231782918679334939,300),(6241,1231782918679334940,300),(6242,1231782918679334941,300),(6243,1231782918679334942,300),(6244,1231782918679334943,300),(6245,1231782918679334944,300),(6246,1231782918679334945,300),(6247,1236825400286846978,300),(6248,1236825400295235586,300),(6249,1236825400295235587,300),(6250,1236825400295235588,300),(6251,1236825400295235589,300),(6252,1236825400295235590,300),(6253,1236825400295235591,300),(6254,1236825400295235592,300),(6255,1236825400295235593,300),(6256,1236825400295235594,300),(6257,1236825400295235595,300),(6258,1236825400295235596,300),(6259,1236825400295235597,300),(6260,1236825400295235598,300),(6261,1236825400295235599,300),(6262,1236825400295235600,300),(6263,1236825400295235601,300),(6264,1236825400295235602,300),(6265,1236825400295235603,300),(6266,1236825400295235604,300),(6267,1236825400295235605,300),(6268,1236825400295235606,300),(6269,1236825400295235607,300),(6270,1236825400295235608,300),(6271,1236825400295235609,300),(6272,1236825400295235610,300),(6273,1236825400295235611,300),(6274,1236825400295235612,300),(6275,1236825400295235613,300),(6276,1236825400295235614,300),(6277,1236825400295235615,300),(6278,1236825400295235616,300),(6279,1236825400295235617,300),(6280,1236825400295235618,300),(6281,1236825400295235619,300),(6282,1236825400295235620,300),(6283,1236825400295235621,300),(6284,1236825400295235622,300),(6285,1236825400295235623,300),(6286,1236825400295235624,300),(6287,1236825400295235625,300),(6288,1236825400295235626,300),(6289,1236825400295235627,300),(6290,1236825400295235628,300),(6291,1236825400295235629,300),(6292,1236825400295235630,300),(6293,1236825400295235631,300),(6294,1236825400295235632,300),(6295,1236825400295235633,300),(6296,1236825400295235634,300),(6297,1236825400295235635,300),(6298,1236825400295235636,300),(6299,1236825400295235637,300),(6300,1236825400295235638,300),(6301,1236825400295235639,300),(6302,1236825400295235640,300),(6303,1236825400295235641,300),(6304,1236825400295235642,300),(6305,1236825400295235643,300);

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `num` INT(11) DEFAULT NULL COMMENT '序号',
  `pid` INT(11) DEFAULT NULL COMMENT '父角色id',
  `name` VARCHAR(255) DEFAULT NULL COMMENT '角色名称',
  `deptid` INT(11) DEFAULT NULL COMMENT '部门名称',
  `tips` VARCHAR(255) DEFAULT NULL COMMENT '提示',
  `version` INT(11) DEFAULT NULL COMMENT '保留字段(暂时没用）',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='角色表';

/*Data for the table `sys_role` */

INSERT  INTO `sys_role`(`id`,`num`,`pid`,`name`,`deptid`,`tips`,`version`) VALUES (1,1,0,'超级管理员',24,'administrator',1),(5,2,1,'临时',26,'temp',NULL);

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `avatar` VARCHAR(255) DEFAULT NULL COMMENT '头像',
  `account` VARCHAR(45) DEFAULT NULL COMMENT '账号',
  `password` VARCHAR(45) DEFAULT NULL COMMENT '密码',
  `salt` VARCHAR(45) DEFAULT NULL COMMENT 'md5密码盐',
  `name` VARCHAR(45) DEFAULT NULL COMMENT '名字',
  `birthday` DATETIME DEFAULT NULL COMMENT '生日',
  `sex` INT(11) DEFAULT NULL COMMENT '性别（1：男 2：女）',
  `email` VARCHAR(45) DEFAULT NULL COMMENT '电子邮件',
  `phone` VARCHAR(45) DEFAULT NULL COMMENT '电话',
  `roleid` VARCHAR(255) DEFAULT NULL COMMENT '角色id',
  `deptid` INT(11) DEFAULT NULL COMMENT '部门id',
  `status` INT(11) DEFAULT NULL COMMENT '状态(1：启用  2：冻结  3：删除）',
  `createtime` DATETIME DEFAULT NULL COMMENT '创建时间',
  `version` INT(11) DEFAULT NULL COMMENT '保留字段',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COMMENT='管理员表';
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (105, 'system', '0', '[0],', '系统管理', 'fa-user', '#', 4, 1, 1, NULL, 1, 1);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (106, 'mgr', 'system', '[0],[system],', '用户管理', '', '/mgr', 2, 2, 1, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (114, 'role', 'system', '[0],[system],', '角色管理', NULL, '/role', 2, 2, 1, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (115, 'role_add', 'role', '[0],[system],[role],', '添加角色', NULL, '/role/add', 1, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (116, 'role_edit', 'role', '[0],[system],[role],', '修改角色', NULL, '/role/edit', 2, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (117, 'role_remove', 'role', '[0],[system],[role],', '删除角色', NULL, '/role/remove', 3, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (118, 'role_setAuthority', 'role', '[0],[system],[role],', '配置权限', NULL, '/role/setAuthority', 4, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (119, 'menu', 'system', '[0],[system],', '菜单管理', '', '/menu', 5, 2, 1, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (120, 'menu_add', 'menu', '[0],[system],[menu],', '添加菜单', NULL, '/menu/add', 1, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (121, 'menu_edit', 'menu', '[0],[system],[menu],', '修改菜单', NULL, '/menu/edit', 2, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (122, 'menu_remove', 'menu', '[0],[system],[menu],', '删除菜单', NULL, '/menu/remove', 3, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (128, 'log', 'system', '[0],[system],', '业务日志', '', '/log', 7, 2, 1, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (130, 'druid', 'system', '[0],[system],', '监控管理', NULL, '/druid', 7, 2, 1, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (132, 'dict', 'system', '[0],[system],', '字典管理', '', '/dict', 6, 2, 1, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (133, 'loginLog', 'system', '[0],[system],', '登录日志', NULL, '/loginLog', 6, 2, 1, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (134, 'log_clean', 'log', '[0],[system],[log],', '清空日志', NULL, '/log/delLog', 3, 3, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (138, 'dict_add', 'dict', '[0],[system],[dict],', '添加字典', NULL, '/dict/add', 1, 3, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (139, 'dict_update', 'dict', '[0],[system],[dict],', '修改字典', NULL, '/dict/update', 1, 3, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (140, 'dict_delete', 'dict', '[0],[system],[dict],', '删除字典', NULL, '/dict/delete', 1, 3, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (141, 'notice', 'system', '[0],[system],', '通知管理', NULL, '/notice', 9, 2, 1, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (142, 'notice_add', 'notice', '[0],[system],[notice],', '添加通知', NULL, '/notice/add', 1, 3, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (143, 'notice_update', 'notice', '[0],[system],[notice],', '修改通知', NULL, '/notice/update', 2, 3, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (144, 'notice_delete', 'notice', '[0],[system],[notice],', '删除通知', NULL, '/notice/delete', 3, 3, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (145, 'hello', '0', '[0],', '通知', 'fa-rocket', '/notice/hello', 1, 1, 1, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (148, 'code', '0', '[0],', '代码生成', 'fa-code', '/code', 3, 1, 1, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (149, 'api_mgr', '0', '[0],', '接口文档', 'fa-leaf', '/swagger-ui.html', 2, 1, 1, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (150, 'to_menu_edit', 'menu', '[0],[system],[menu],', '菜单编辑跳转', '', '/menu/menu_edit', 4, 3, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (151, 'menu_list', 'menu', '[0],[system],[menu],', '菜单列表', '', '/menu/list', 5, 3, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (155, 'to_dict_edit', 'dict', '[0],[system],[dict],', '修改菜单跳转', '', '/dict/dict_edit', 4, 3, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (156, 'dict_list', 'dict', '[0],[system],[dict],', '字典列表', '', '/dict/list', 5, 3, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (157, 'dict_detail', 'dict', '[0],[system],[dict],', '字典详情', '', '/dict/detail', 6, 3, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (158, 'log_list', 'log', '[0],[system],[log],', '日志列表', '', '/log/list', 2, 3, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (159, 'log_detail', 'log', '[0],[system],[log],', '日志详情', '', '/log/detail', 3, 3, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (160, 'del_login_log', 'loginLog', '[0],[system],[loginLog],', '清空登录日志', '', '/loginLog/delLoginLog', 1, 3, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (161, 'login_log_list', 'loginLog', '[0],[system],[loginLog],', '登录日志列表', '', '/loginLog/list', 2, 3, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (162, 'to_role_edit', 'role', '[0],[system],[role],', '修改角色跳转', '', '/role/role_edit', 5, 3, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (163, 'to_role_assign', 'role', '[0],[system],[role],', '角色分配跳转', '', '/role/role_assign', 6, 3, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (164, 'role_list', 'role', '[0],[system],[role],', '角色列表', '', '/role/list', 7, 3, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1230851042921504770, 'ptOrg', 'organization', '[0],[system],[organization],', '学校管理', '', '/ptOrg', 1, 3, 1, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1230851042921504771, 'ptOrg_list', 'ptOrg', '[0],[system],[ptOrg],', '列表', '', '/ptOrg/list', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1230851042921504772, 'ptOrg_add', 'ptOrg', '[0],[system],[ptOrg],', '添加', '', '/ptOrg/add', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1230851042921504773, 'ptOrg_update', 'ptOrg', '[0],[system],[ptOrg],', '更新', '', '/ptOrg/update', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1230851042921504774, 'ptOrg_delete', 'ptOrg', '[0],[system],[ptOrg],', '删除', '', '/ptOrg/delete', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1230851042921504775, 'ptOrg_detail', 'ptOrg', '[0],[system],[ptOrg],', '详情', '', '/ptOrg/detail', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231127578715217921, 'ptPeople', 'mgr', '[0],[system],[mgr],', '家长用户管理', '', '/ptPeople', 3, 3, 1, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231127578715217922, 'ptPeople_list', 'ptPeople', '[0],[system],[mgr],[ptPeople],', '列表', '', '/ptPeople/list', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231127578715217923, 'ptPeople_add', 'ptPeople', '[0],[system],[mgr],[ptPeople],', '添加', '', '/ptPeople/add', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231127578715217924, 'ptPeople_update', 'ptPeople', '[0],[system],[mgr],[ptPeople],', '更新', '', '/ptPeople/update', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231127578715217925, 'ptPeople_delete', 'ptPeople', '[0],[system],[mgr],[ptPeople],', '删除', '', '/ptPeople/delete', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231127578715217926, 'ptPeople_detail', 'ptPeople', '[0],[system],[mgr],[ptPeople],', '详情', '', '/ptPeople/detail', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231127616245850113, 'ptStudent', 'mgr', '[0],[system],[mgr],', '学生用户管理', '', '/ptStudent', 1, 3, 1, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231127616245850114, 'ptStudent_list', 'ptStudent', '[0],[system],[mgr],[ptStudent],', '列表', '', '/ptStudent/list', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231127616245850115, 'ptStudent_add', 'ptStudent', '[0],[system],[mgr],[ptStudent],', '添加', '', '/ptStudent/add', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231127616245850116, 'ptStudent_update', 'ptStudent', '[0],[system],[mgr],[ptStudent],', '更新', '', '/ptStudent/update', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231127616245850117, 'ptStudent_delete', 'ptStudent', '[0],[system],[mgr],[ptStudent],', '删除', '', '/ptStudent/delete', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231127616245850118, 'ptStudent_detail', 'ptStudent', '[0],[system],[mgr],[ptStudent],', '详情', '', '/ptStudent/detail', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231127654187524097, 'ptTeacher', 'mgr', '[0],[system],[mgr],', '教师用户管理', '', '/ptTeacher', 2, 3, 1, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231127654187524098, 'ptTeacher_list', 'ptTeacher', '[0],[system],[mgr],[ptTeacher],', '列表', '', '/ptTeacher/list', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231127654187524099, 'ptTeacher_add', 'ptTeacher', '[0],[system],[mgr],[ptTeacher],', '添加', '', '/ptTeacher/add', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231127654187524100, 'ptTeacher_update', 'ptTeacher', '[0],[system],[mgr],[ptTeacher],', '更新', '', '/ptTeacher/update', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231127654187524101, 'ptTeacher_delete', 'ptTeacher', '[0],[system],[mgr],[ptTeacher],', '删除', '', '/ptTeacher/delete', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231127654187524102, 'ptTeacher_detail', 'ptTeacher', '[0],[system],[mgr],[ptTeacher],', '详情', '', '/ptTeacher/detail', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231510240424415233, 'ptRoleType', 'system', '[0],[system],', '权限管理', '', '/ptRoleType', 4, 2, 1, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231510240424415234, 'ptRoleType_list', 'ptRoleType', '[0],[system],[ptRoleType],', '列表', '', '/ptRoleType/list', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231510240424415235, 'ptRoleType_add', 'ptRoleType', '[0],[system],[ptRoleType],', '添加', '', '/ptRoleType/add', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231510240424415236, 'ptRoleType_update', 'ptRoleType', '[0],[system],[ptRoleType],', '更新', '', '/ptRoleType/update', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231510240424415237, 'ptRoleType_delete', 'ptRoleType', '[0],[system],[ptRoleType],', '删除', '', '/ptRoleType/delete', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231510240424415238, 'ptRoleType_detail', 'ptRoleType', '[0],[system],[ptRoleType],', '详情', '', '/ptRoleType/detail', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334914, 'ptClass', 'organization', '[0],[system],[organization],', '班级管理', '', '/ptClass', 99, 3, 1, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334915, 'ptClass_list', 'ptClass', '[0],[system],[ptClass],', '列表', '', '/ptClass/list', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334916, 'ptClass_add', 'ptClass', '[0],[system],[ptClass],', '添加', '', '/ptClass/add', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334917, 'ptClass_update', 'ptClass', '[0],[system],[ptClass],', '更新', '', '/ptClass/update', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334918, 'ptClass_delete', 'ptClass', '[0],[system],[ptClass],', '删除', '', '/ptClass/delete', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334919, 'ptClass_detail', 'ptClass', '[0],[system],[ptClass],', '详情', '', '/ptClass/detail', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334920, 'ptStudent_import', 'ptStudent', '[0],[system],[mgr],[ptStudent],', '导入', 'fa fa-cloud-download', '/ptStudent/import', 4, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334921, 'ptStudent_export', 'ptStudent', '[0],[system],[mgr],[ptStudent],', '导出', '', '/ptStudent/export', 5, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334922, 'ptStudent_batchDelete', 'ptStudent', '[0],[system],[mgr],[ptStudent],', '批量删除', '', '/ptStudent/batchDelete', 6, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334923, 'ptStudent_updateEnable', 'ptStudent', '[0],[system],[mgr],[ptStudent],', '批量冻结', '', '/ptStudent/updateEnable', 7, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334924, 'ptStudent_updateEnable1', 'ptStudent', '[0],[system],[mgr],[ptStudent],', '批量启用', '', '/ptStudent/updateEnable', 8, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334925, 'ptTeacher_import', 'ptTeacher', '[0],[system],[mgr],[ptTeacher],', '导入', 'fa fa-cloud-download', '/ptTeacher/import', 4, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334926, 'ptTeacher_export', 'ptTeacher', '[0],[system],[mgr],[ptTeacher],', '导出', '', '/ptTeacher/export', 5, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334927, 'ptTeacher_batchDelete', 'ptTeacher', '[0],[system],[mgr],[ptTeacher],', '批量删除', '', '/ptTeacher/batchDelete', 6, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334928, 'ptTeacher_updateEnable', 'ptTeacher', '[0],[system],[mgr],[ptTeacher],', '批量冻结', '', '/ptTeacher/updateEnable', 7, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334929, 'ptTeacher_updateEnable1', 'ptTeacher', '[0],[system],[mgr],[ptTeacher],', '批量启用', '', '/ptTeacher/updateEnable', 8, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334930, 'postmanage_toPostManage', 'ptTeacher', '[0],[system],[mgr],[ptTeacher],', '职务管理', '', '/postmanage/toPostManage', 9, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334932, 'postmanage_addOrEditPostManage', 'postmanage_toPostManage', '[0],[system],[mgr],[ptTeacher],[postmanage_toPostManage],', '添加职务', '', '/postmanage/addOrEditPostManage', 1, 5, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334933, '/postmanage/addOrEditPostManage1', 'postmanage_toPostManage', '[0],[system],[mgr],[ptTeacher],[postmanage_toPostManage],', '修改职务', '', '/postmanage/addOrEditPostManage', 2, 5, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334934, 'postmanage_delPostManage', 'postmanage_toPostManage', '[0],[system],[mgr],[ptTeacher],[postmanage_toPostManage],', '删除职务', '', '/postmanage/delPostManage', 3, 5, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334935, 'postmanage_disablePostManage', 'postmanage_toPostManage', '[0],[system],[mgr],[ptTeacher],[postmanage_toPostManage],', '禁用职务', '', '/postmanage/disablePostManage', 4, 5, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334936, 'organization', 'system', '[0],[system],', '组织机构管理', '', 'organization', 1, 2, 1, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334937, 'ptClass_editTeacher', 'ptClass', '[0],[system],[organization],[ptClass],', '任课教师设置', '', '/ptClass/editTeacher', 3, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334938, 'ptClass_editStudent', 'ptClass', '[0],[system],[organization],[ptClass],', '管理学生', '', '/ptClass/editStudent', 4, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334939, 'ptClass_batchImportClass', 'ptClass', '[0],[system],[organization],[ptClass],', '批量导入班级', '', '/ptClass/batchImportClass', 5, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334940, 'ptClass_batchImportTeacher', 'ptClass', '[0],[system],[organization],[ptClass],', '批量设置任课教师', '', '/ptClass/batchImportTeacher', 6, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334941, 'ptClass_batchImportStudent', 'ptClass', '[0],[system],[organization],[ptClass],', '批量管理学生', '', '/ptClass/batchImportStudent', 7, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334942, 'ptClass_classUpdate', 'ptClass', '[0],[system],[organization],[ptClass],', '班级升班', '', '/ptClass/classUpdate', 8, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334943, 'ptClass/classEnd', 'ptClass', '[0],[system],[organization],[ptClass],', '班级毕业', '', '/ptClass/classEnd', 9, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334944, 'ptClass_setClassRoom', 'ptClass', '[0],[system],[organization],[ptClass],', '设置教室', '', '/ptClass/setClassRoom', 9, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1231782918679334945, 'ptClass_removeClassRoom', 'ptClass', '[0],[system],[organization],[ptClass],', '解除教室', '', '/ptClass/removeClassRoom', 11, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400286846978, 'ptSchoolBuilding', 'system', '[0],[system],', '场所管理', '', '/ptSchoolBuilding', 9, 2, 1, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235586, 'ptSchoolBuilding_list', 'ptSchoolBuilding', '[0],[system],[ptSchoolBuilding],', '场所管理列表', '', '/ptSchoolBuilding/list', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235587, 'ptSchoolBuilding_add', 'ptSchoolBuilding', '[0],[system],[ptSchoolBuilding],', '场所管理添加', '', '/ptSchoolBuilding/add', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235588, 'ptSchoolBuilding_update', 'ptSchoolBuilding', '[0],[system],[ptSchoolBuilding],', '场所管理更新', '', '/ptSchoolBuilding/update', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235589, 'ptSchoolBuilding_delete', 'ptSchoolBuilding', '[0],[system],[ptSchoolBuilding],', '场所管理删除', '', '/ptSchoolBuilding/delete', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235590, 'ptSchoolBuilding_detail', 'ptSchoolBuilding', '[0],[system],[ptSchoolBuilding],', '场所管理详情', '', '/ptSchoolBuilding/detail', 99, 3, 0, NULL, 1, 0);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235591, 'ptClassRoom_manager', 'ptSchoolBuilding', '[0],[system],[ptSchoolBuilding],', '管理教室', '', '/ptClassRoom/manager', 1, 3, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235592, 'ptClassRoom_add', 'ptClassRoom_manager', '[0],[system],[ptSchoolBuilding],[ptClassRoom_manager],', '新建教室', '', '/ptClassRoom/add', 1, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235593, 'ptClassRoom_edit', 'ptClassRoom_manager', '[0],[system],[ptSchoolBuilding],[ptClassRoom_manager],', '修改教室', '', '/ptClassRoom/edit', 2, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235594, 'ptClassRoom_delete', 'ptClassRoom_manager', '[0],[system],[ptSchoolBuilding],[ptClassRoom_manager],', '删除教室', '', '/ptClassRoom/delete', 3, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235595, 'ptClassRoom_detail', 'ptClassRoom_manager', '[0],[system],[ptSchoolBuilding],[ptClassRoom_manager],', '查看', '', '/ptClassRoom/detail', 4, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235596, 'teacherInfo', 'system', '[0],[system],', '教学信息管理', '', '/teacherInfo', 3, 2, 1, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235597, 'schoolyearAndTerm', 'teacherInfo', '[0],[system],[teacherInfo],', '学年学期管理', '', '/ptSchoolYearTermManage', 1, 3, 1, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235598, 'schoolYear_add', 'schoolyearAndTerm', '[0],[system],[teacherInfo],[schoolyearAndTerm],', '添加学年', '', '/schoolYear/add', 1, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235599, 'schoolYear_edit', 'schoolyearAndTerm', '[0],[system],[teacherInfo],[schoolyearAndTerm],', '修改学年', '', '/schoolYear/edit', 3, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235600, 'schoolYear_delete', 'schoolyearAndTerm', '[0],[system],[teacherInfo],[schoolyearAndTerm],', '删除学年', '', '/schoolYear/delete', 4, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235601, 'schoolTerm', 'schoolyearAndTerm', '[0],[system],[teacherInfo],[schoolyearAndTerm],', '管理学期', '', '/schoolTerm', 2, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235602, 'schoolTerm_add', 'schoolTerm', '[0],[system],[teacherInfo],[schoolyearAndTerm],[schoolTerm],', '添加学期', '', '/schoolTerm/add', 1, 5, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235603, 'schoolTerm_edit', 'schoolTerm', '[0],[system],[teacherInfo],[schoolyearAndTerm],[schoolTerm],', '修改学期', '', '/schoolTerm/edit', 2, 5, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235604, 'schoolTerm_delete', 'schoolTerm', '[0],[system],[teacherInfo],[schoolyearAndTerm],[schoolTerm],', '删除学期', '', '/schoolTerm/delete', 3, 5, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235605, 'sectionManage', 'teacherInfo', '[0],[system],[teacherInfo],', '节次管理', '', '/ptSectionCategory', 1, 3, 1, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235606, 'ptSectionCategory_add', 'sectionManage', '[0],[system],[teacherInfo],[sectionManage],', '新建节次', '', '/ptSectionCategory/add', 1, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235607, 'ptSectionCategory_edit', 'sectionManage', '[0],[system],[teacherInfo],[sectionManage],', '修改节次', '', '/ptSectionCategory/edit', 2, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235608, 'ptSectionCategory_updateEnable', 'sectionManage', '[0],[system],[teacherInfo],[sectionManage],', '禁用', '', '/ptSectionCategory/updateEnable', 3, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235609, 'ptSectionCategory_delete', 'sectionManage', '[0],[system],[teacherInfo],[sectionManage],', '删除节次', '', '/ptSectionCategory/delete', 4, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235610, 'schoolManage', 'system', '[0],[system],', '校园门户管理', '', '/schoolManage', 10, 2, 1, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235611, 'noticeManager', 'schoolManage', '[0],[system],[schoolManage],', '公告管理', '', '/ptAnnouncement', 1, 3, 1, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235612, 'ptAnnouncement_add', 'noticeManager', '[0],[system],[schoolManage],[noticeManager],', '添加公告', '', '/ptAnnouncement/add', 1, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235613, 'ptAnnouncement_update', 'noticeManager', '[0],[system],[schoolManage],[noticeManager],', '修改公告', '', '/ptAnnouncement/update', 2, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235614, 'ptAnnouncement_delete', 'noticeManager', '[0],[system],[schoolManage],[noticeManager],', '删除公告', '', '/ptAnnouncement/delete', 3, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235653, 'dicCatory', 'schoolManage', '[0],[system],[schoolManage],', '公告类型', '', '/ptConfig/SYS_TZGG_TYPE_CODE/index', 20, 3, 1, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235615, 'dic_add', 'dicCatory', '[0],[system],[schoolManage],[noticeManager],', '新增公告类型', '', '/dic/add', 4, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235616, 'dic_edit', 'dicCatory', '[0],[system],[schoolManage],[noticeManager],', '修改公告类型', '', '/dic/edit', 5, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235617, 'dic_delete', 'dicCatory', '[0],[system],[schoolManage],[noticeManager],', '删除公告类型', '', '/dic/delete', 6, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235618, 'achieveShow', 'schoolManage', '[0],[system],[schoolManage],', '成果展示', '', '/achieveShow', 2, 3, 1, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235619, 'achieveShow_add', 'achieveShow', '[0],[system],[schoolManage],[achieveShow],', '上传成果', '', '/achieveShow/add', 1, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235620, 'achieveShow_edit', 'achieveShow', '[0],[system],[schoolManage],[achieveShow],', '修改成果', '', '/achieveShow/edit', 2, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235621, 'achieveShow_delete', 'achieveShow', '[0],[system],[schoolManage],[achieveShow],', '删除成果', '', '/achieveShow/delete', 3, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235622, 'achieveShow/batchDel', 'achieveShow', '[0],[system],[schoolManage],[achieveShow],', '批量删除', '', '/achieveShow/batchDel', 4, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235623, 'ptNewsCategory', 'achieveShow', '[0],[system],[schoolManage],[achieveShow],', '增加成果分类', '', '/ptNewsCategory', 5, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235624, 'achieveCategory_add', 'ptNewsCategory', '[0],[system],[schoolManage],[achieveShow],[ptNewsCategory],', '添加成果分类', '', '/achieveCategory/add', 1, 5, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235625, 'achieveCategory_edit', 'ptNewsCategory', '[0],[system],[schoolManage],[achieveShow],[ptNewsCategory],', '修改成果分类', '', '/achieveCategory/edit', 2, 5, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235626, 'achieveCategory_delete', 'ptNewsCategory', '[0],[system],[schoolManage],[achieveShow],[ptNewsCategory],', '删除成果分类', '', '/achieveCategory/delete', 3, 5, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235627, 'ptNews', 'schoolManage', '[0],[system],[schoolManage],', '新闻管理', '', '/ptNews', 3, 3, 1, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235628, 'ptNews_add', 'ptNews', '[0],[system],[schoolManage],[ptNews],', '撰写新闻', '', '/ptNews/add', 1, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235629, 'ptNews_edit', 'ptNews', '[0],[system],[schoolManage],[ptNews],', '修改新闻', '', '/ptNews/edit', 2, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235630, 'ptNews_batchDel', 'ptNews', '[0],[system],[schoolManage],[ptNews],', '批量删除新闻', '', '/ptNews/batchDel', 3, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235631, 'campusMien', 'schoolManage', '[0],[system],[schoolManage],', '校园风采', '', '/campusMien', 4, 3, 1, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235632, 'campusMien_add', 'campusMien', '[0],[system],[schoolManage],[campusMien],', '添加风采', '', '/campusMien/add', 1, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235633, 'campusMien_edit', 'campusMien', '[0],[system],[schoolManage],[campusMien],', '修改风采', '', '/campusMien/edit', 2, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235634, 'campusMien_delete', 'campusMien', '[0],[system],[schoolManage],[campusMien],', '删除风采', '', '/campusMien/delete', 3, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235635, 'campusColumn', 'schoolManage', '[0],[system],[schoolManage],', '校园专栏', '', '/campusColumn', 5, 3, 1, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235636, 'campusColumn_add', 'campusColumn', '[0],[system],[schoolManage],[campusColumn],', '发布内容', '', '/campusColumn/add', 1, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235637, 'campusColumn_edit', 'campusColumn', '[0],[system],[schoolManage],[campusColumn],', '修改', '', '/campusColumn/edit', 2, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235638, 'campusColumn_batchDel', 'campusColumn', '[0],[system],[schoolManage],[campusColumn],', '批量删除', '', '/campusColumn/batchDel', 3, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235639, 'menHu', 'schoolManage', '[0],[system],[schoolManage],', '门户信息定制', '', '/menHu', 6, 3, 1, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235640, 'banner', 'schoolManage', '[0],[system],[schoolManage],', 'banner配置', '', '/banner', 7, 3, 1, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235641, 'banner_add', 'banner', '[0],[system],[schoolManage],[banner],', '添加', '', '/banner/add', 1, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235642, 'banner_edit', 'banner', '[0],[system],[schoolManage],[banner],', '修改', '', '/banner/edit', 2, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235643, 'banner_delete', 'banner', '[0],[system],[schoolManage],[banner],', '删除', '', '/banner/delete', 3, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235644, 'ptPeople_childs', 'ptPeople', '[0],[system],[mgr],[ptPeople],', '子女管理', '', '/ptPeople/childs', 2, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235645, 'ptPeople_Import', 'ptPeople', '[0],[system],[mgr],[ptPeople],', '批量新建', '', '/ptPeople/Import', 3, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235646, 'ptPeople_batchDel', 'ptPeople', '[0],[system],[mgr],[ptPeople],', '批量删除', '', '/ptPeople/batchDel', 5, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235647, 'ptPeople_batchDisable', 'ptPeople', '[0],[system],[mgr],[ptPeople],', '批量冻结', '', '/ptPeople/batchDisable', 6, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235648, 'ptPeople_batchEnable', 'ptPeople', '[0],[system],[mgr],[ptPeople],', '批量启用', '', '/ptPeople/batchEnable', 7, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235649, 'ptPeople_export', 'ptPeople', '[0],[system],[mgr],[ptPeople],', '导出用户', '', '/ptPeople/export', 8, 4, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235650, 'ptPeopleStudent_add', 'ptPeople_childs', '[0],[system],[mgr],[ptPeople],[ptPeople_childs],', '添加', '', '/ptPeopleStudent/add', 1, 5, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235651, 'ptPeopleStudent_update', 'ptPeople_childs', '[0],[system],[mgr],[ptPeople],[ptPeople_childs],', '修改', '', '/ptPeopleStudent/update', 2, 5, 0, NULL, 1, NULL);
INSERT INTO `sys_menu`(`id`, `code`, `pcode`, `pcodes`, `name`, `icon`, `url`, `num`, `levels`, `ismenu`, `tips`, `status`, `isopen`) VALUES (1236825400295235652, 'ptPeopleStudent_delete', 'ptPeople_childs', '[0],[system],[mgr],[ptPeople],[ptPeople_childs],', '删除', '', '/ptPeopleStudent/delete', 3, 5, 0, NULL, 1, NULL);
